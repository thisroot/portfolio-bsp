/* eslint-disable no-restricted-globals */
import { skipWaiting, clientsClaim } from "workbox-core"
import { precacheAndRoute, createHandlerBoundToURL } from 'workbox-precaching'
import { NavigationRoute, registerRoute } from "workbox-routing"
import { CacheFirst, NetworkFirst } from "workbox-strategies"
import { ExpirationPlugin } from "workbox-expiration"
import { BackgroundSyncPlugin } from 'workbox-background-sync'
import { PWAMessages } from './constants/common'
// import * as navigationPreload from 'workbox-navigation-preload'


skipWaiting()
clientsClaim()

// @ts-ignore
// precacheAndRoute(self.__WB_MANIFEST)
// @ts-ignore
precacheAndRoute(self.__precacheManifest)

const handler = createHandlerBoundToURL('/index.html')
const navigationSPARoute = new NavigationRoute(handler)
registerRoute(navigationSPARoute)


//TODO: https://love2dev.com/pwa/service-worker-preload/ - требует осмысления и доработки
// TODO: https://developers.google.com/web/updates/2017/02/navigation-preload
// navigationPreload.enable()
// const navigation = new NetworkFirst({
//     cacheName: 'cached-navigations',
//     plugins: [
//         new ExpirationPlugin({
//             maxAgeSeconds: 14 * 24 * 60 * 60
//         })
//     ],
// })
//
// const navigationRoute = new NavigationRoute(navigation)
//registerRoute(navigationRoute)

registerRoute(/\.(?:png|gif|jpg|jpeg|ico|svg)$/,
  new CacheFirst({
        cacheName: 'images',
        plugins: [
            new ExpirationPlugin({
                maxEntries: 60,
                maxAgeSeconds: 30 * 24 * 60 * 60
            }) ]
    },
  )
)

const bgSyncPlugin = new BackgroundSyncPlugin('api-requests-sync', {
    maxRetentionTime: 24 * 60 // Retry for max of 24 Hours (specified in minutes)
})

//TODO: кеширование Post запросов запрещено, можно обойти, написав кастомный обработчик ответа. С целью использования стратегии отличной от Network only

registerRoute(
  /api\.*/,
  new NetworkFirst({
      cacheName: 'api-requests',
      plugins: [ bgSyncPlugin ]
  }),
  'POST'
)

self.addEventListener('message', (event => {
    if (event.data.message &&
      event.data.message.length > 0) {
        switch (event.data.type) {
            case PWAMessages.SKIP_WAITING:
                skipWaiting()
                break
            default:
                break
        }
    }
}))
