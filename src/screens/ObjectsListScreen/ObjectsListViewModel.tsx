import { inject } from "react-ioc"
import { action, computed, observable, observe } from "mobx"
import { DataContext } from "services/DataContext"
import { ApiService } from "services/ApiService"
import { StorageService } from "services/StorageService"
import { AppService } from "services/AppService"
import { toaster } from "../../components/UI/Toast"
import { RouterService } from "../../services/RouterService"
import { get, throttle } from 'lodash'
import { LocalisationService } from "../../services/LocalisationService"
import { ObjectsFilterViewModel } from "components/Filters/ObjectsFilter"

export enum LIST_VIEWS {
    LIST = 'list',
    MAP = 'map'
}


export interface ObjectsFilter {
    status?: string,
    volume_percent_from?: number,
    volume_percent_to?: number,
    deviation_day_from?: number,
    deviation_day_to?: number,
    project_type_id?: Array<number>,
    region_id?: number,
}


class ObjectsListViewModel {
    @inject dataContext: DataContext
    @inject storageService: StorageService
    @inject api: ApiService
    @inject app: AppService
    @inject router: RouterService
    @inject pfvm: ObjectsFilterViewModel
    @inject lang: LocalisationService
    @observable
    public objectsCount = 0

    @observable
    public wasInitialized = false
    @observable
    public pending = true
    @observable
    public searchPending = false

    @action
    public initialize = () => {
        this.pending = false
    }

    @observable
    public filter: ObjectsFilter = {}

    @observable
    public searchString: string

    public step = 50

    @observable
    public shift = 0


    @computed
    public get currentTab() {
        return this.router.location.hash.replace('#', '') || LIST_VIEWS.LIST
    }

    @computed
    public get isNeedToFillBackground(): boolean {
        return this.app.scrollOnTop > 0
    }

    @action
    public init = async () => {
        try {
            this.pending = true
            this.pfvm.registerFilter('263a0540-ecaf-4018-ac12-fbbf40ecab5c')
            await this.pfvm.initFilterForm()
            await this.load()
        } catch (e) {
            console.log(e)
        } finally {
            this.initialize()
        }
    }

    private sortOnChange = observe(this.pfvm, 'selectedSort', async () => {
        await this.load(true)
    })

    public dispose = () => {
        this.sortOnChange()
    }

    @action
    public load = async (reset: boolean = false) => {
        try {

            let sort = {}
            if (!this.pfvm.selectedSort) {
                sort = { [`name_${ this.lang.lang }`]: 1 }
            } else if (this.pfvm.selectedSort.key === null) {
                sort = { [`name_${ this.lang.lang }`]: this.pfvm.selectedSort.value }
            } else {
                sort = {
                    [`name_${ this.lang.lang }`]: 1,
                    [`passport_${ this.lang.lang }`]: {
                        object_type_id: +this.pfvm.selectedSort.key.split('_')[0],
                        key: this.pfvm.selectedSort.key.split('_')[1],
                        direction: this.pfvm.selectedSort.value
                    },
                }
            }

            const result = await this.dataContext.cacheFirst(this.api.object.list, {
                filter: {
                    ...this.pfvm.formValues,
                    [`name_${ this.lang.lang }`]: this.searchString
                }, sort
            }, { noCaching: true })
            if (result.status !== 'error' && result.status !== 'cached') {
                this.objectsCount = get(result, 'body.object_list', []).length
                if (reset) {
                    this.dataContext.object_list.clear()
                    this.dataContext.object.clear()
                }
                this.dataContext.applySnapshot(result.body, false)
            }
        } catch (e) {
            console.log(e)
            toaster.error()
            // throw new Error(e)
        }
    }

    @action onReset = async (e) => {
        this.pfvm.resetForm(e)
        await this.load(true)
    }

    @action onSubmit = async (e) => {
        await this.pfvm.submitForm(e)
        await this.load(true)
    }

    @action
    public onSearch = throttle(async (value?: string) => {

        if (value !== this.searchString && value !== undefined) {
            try {
                this.searchPending = true
                this.searchString = value
                await this.load(true)
            } catch (e) {
                toaster.error()
            } finally {
                this.searchPending = false
            }
        }
    }, 150, { trailing: false })

    @computed
    public get next() {
        return false
    }

    @computed
    public get objects() {
        const urlParams = new URLSearchParams(this.router.location.search)
        const filter = urlParams.get('filter')
        switch (filter) {
            case 'problematic':
                return this.dataContext.object_list.filter(item => item.status === 1)
            default:
                return this.dataContext.object_list
        }
    }
}

export {
    ObjectsListViewModel
}
