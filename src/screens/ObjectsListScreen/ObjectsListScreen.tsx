import React from "react"
import { observer } from "mobx-react"
import { inject, provider } from "react-ioc"
import { LIST_VIEWS, ObjectsListViewModel } from "./ObjectsListViewModel"
import { StickyScreen } from "components/StickyScreen"
import { PendingPaths } from "components/BaseListComponent"
import { t } from "@lingui/macro"
import styles from "./styles.scss"
import { loc } from "utils/i18n"
import { ButtonsGroup, DefaultButton } from "components/UI/Button"
import cx from 'classnames'
import { ObjectsMap } from "../../components/ObjectsMap"
import { RouterService } from "services/RouterService"
import { TYPE_MAPS } from "constants/geocoding"
import { Search, SearchType } from "../../components/UI/Search/Search"
import { Popover } from "../../components"
import { ObjectsFilter, ObjectsFilterViewModel } from "../../components/Filters/ObjectsFilter"
import { ObjectsList } from "./ObjectsList"


@observer
class Objects extends React.Component {

    @inject
    public view: ObjectsListViewModel
    @inject
    public filterView: ObjectsFilterViewModel

    @inject
    public routerService: RouterService


    public async componentDidMount(): Promise<void> {
        await this.view.init()
        this.filterView.registerOuterSubmit(this.view.onSubmit)
    }

    public renderTopComponent = () => {
        return <div className={ styles.topContainer }>
            <div className={ styles.left }>
                <div>
                    <h1>{ loc._(t`components.router.objects`) }
                        &nbsp;
                        { this.view.objectsCount
                          ? <span className={ styles.counter }>{ `(${ this.view.objectsCount })` }</span>
                          : null
                        }
                    </h1>
                </div>
                <div className={ styles.viewContainer }>
                    <ButtonsGroup>
                        <DefaultButton
                          className={ cx(styles.viewBtn, { [styles.activeBtn]: this.view.currentTab === LIST_VIEWS.LIST }) }
                          icon={ 'lists-bullet-list-text' }
                          text={ loc._(t`basic.view.onList`) }
                          onClick={ async () => {
                              this.routerService.push({ hash: LIST_VIEWS.LIST })
                              await this.view.init()
                          } }
                        />
                        <DefaultButton
                          className={ cx(styles.viewBtn, { [styles.activeBtn]: this.view.currentTab === LIST_VIEWS.MAP }) }
                          icon={ 'map-pin' }
                          text={ loc._(t`components.projects.objectsOnMap`) }
                          onClick={ async () => {
                              this.routerService.push({ hash: LIST_VIEWS.MAP })
                              await this.view.init()
                          } }
                        />
                    </ButtonsGroup>
                </div>
            </div>
            { this.view.currentTab !== LIST_VIEWS.MAP &&
            // TODO: UNUSED_COMPONENT
            <div className={ styles.right }>
                <Search
                  className={ styles.search }
                  searchFields={ [] }
                  keyName={ '' }
                  items={ [] }
                  onSearch={ this.view.onSearch }
                  onReset={ () => this.view.onSearch() }
                  itemName={ 'firstname' }
                  theme={ SearchType.line }
                  placeholder={ loc._(t`screens.objectList.search.placeholder`) }
                  autoHide={ true }
                  isMinimized={ true }
                  saveSelection
                />
                {
                    this.filterView.renderFavorites
                }
                <Popover onClickOutside={ this.filterView.togglePopover }
                         isOpen={ this.filterView.isPopoverOpen }
                         align={ "start" } position={ 'left' }
                         content={ <ObjectsFilter onSubmit={ this.view.onSubmit }
                                                  onReset={ this.view.onReset }/> }>
                    <div style={ { position: 'relative', display: 'flex' } } onClick={ this.filterView.togglePopover }>
                        <DefaultButton
                          className={ cx(styles.filter, {
                              [styles.open]: this.filterView.isPopoverOpen,
                          }) }
                          icon={ 'internet-filter-sort-b' }/>
                        { this.filterView.countFilledFields > 0 &&
                        <span className={ styles.badge }>{ this.filterView.countFilledFields }</span> }
                    </div>
                </Popover>
            </div>
            }
        </div>
    }

    public renderContent = () => {
        switch (this.view.currentTab) {
            case LIST_VIEWS.LIST:
                return <ObjectsList/>
            case LIST_VIEWS.MAP:
                return <ObjectsMap
                  mapPositionKey={ 'objects-page' }
                  type={ TYPE_MAPS.GOOGLE }
                  entities={ this.view.objects }
                />
            default:
                break
        }
        return null
    }

    public render() {
        const pending: PendingPaths = {
            middleLeft: this.view.pending,
            content: this.view.pending
        }

        return <StickyScreen
          topComponent={ this.renderTopComponent }
          contentComponent={ this.renderContent }
          pending={ pending }/>
    }
}

const ObjectsListScreen = provider()(Objects)
ObjectsListScreen.register(ObjectsListViewModel, ObjectsFilterViewModel)
export default ObjectsListScreen
