import React from "react"
import Loadable, { LoadableComponent } from '@loadable/component'
import { Preloader } from "components/UI/Preloader"

const AsyncObjectsListScreen: LoadableComponent<any> = Loadable(() => import(/* client-side */'./ObjectsListScreen'), {
    fallback: <Preloader /> } )

export { AsyncObjectsListScreen }
