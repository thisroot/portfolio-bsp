import React, { Component } from 'react'
import { inject } from "react-ioc"
import { ObjectsListViewModel } from "../ObjectsListViewModel"
import { observer } from 'mobx-react'
import { Link, RouterService } from 'services/RouterService'
import { get, unescape } from 'lodash'
import cx from 'classnames'
import ProgressBar from "components/UI/ProgressBar"
import styles from './styles.scss'
import { AbbreviateField } from "components/Widgets/AbbreviateField"
import { FormatDateToQuarter } from "components/Widgets/FormatDateToQuarter/FormatDateToQuarter"
import { Spinner } from "components/UI/Spinner"
import InfiniteScroll from "react-infinite-scroll-component"
import { ENTITY_FIELDS, getMetaInfoFields, isMarkdown } from "utils/helpers"
import { renderMarkdown } from "utils/render"
import { Icon } from "../../../components/UI/Icon"
import { NoResultContainer } from "../../../components/NoResultContainer"
import { ObjectsFilterViewModel } from "../../../components/Filters/ObjectsFilter"
import { LocalisationService } from "../../../services/LocalisationService"
import { loc } from "../../../utils/i18n"
import { StatusDataLayers } from "../../../models/mst/StatusDataLayers"

@observer
class ObjectsList extends Component {
    @inject
    public view: ObjectsListViewModel
    @inject
    public filter: ObjectsFilterViewModel
    @inject
    public router: RouterService
    @inject
    public lang: LocalisationService


    public renderPreloader = () => <div className={ styles.preloader }><Spinner/></div>

    render() {
        return (
          <div style={ { width: '100%' } }>
              <InfiniteScroll
                className={ styles.listContainer }
                dataLength={ this.view.objects.length }
                next={ this.view.load }
                hasMore={ this.view.next }
                loader={ this.renderPreloader() }
              >
                  { this.view.objects && (this.view.objects.length > 0) ? this.view.objects.map((objectModel) => {

                      const statusData: StatusDataLayers = get(objectModel, 'status_data')
                      const finish = get(objectModel, 'date_finish')
                      const status = get(objectModel, 'status')
                      const objectImg = get(objectModel, 'main_image.converted.small.url')
                      const { slots } = getMetaInfoFields(objectModel, 'object', this.lang.lang, ENTITY_FIELDS.LIST_WIDGET)

                      const renderField = (slot) => {
                          switch (true) {
                              case isMarkdown(get(slot, 'fieldType')):
                                  return <div className={ styles.mdContainer }
                                              dangerouslySetInnerHTML={ renderMarkdown(get(slot, 'fieldValue')) }/>
                              default:
                                  return <AbbreviateField type={ get(slot, 'fieldType') } swap
                                                          value={ get(slot, 'fieldValue', '') }
                                                          label={ get(slot, 'fieldLabel', '') }/>
                          }
                      }

                      const image = 'account-home-door'
                      return <Link to={ `/projects/${ objectModel.project_id.project_id }/objects/${ objectModel.object_id }` }
                                   className={ styles.row }
                                   key={ objectModel.object_id }>
                          <div className={ styles.main }>
                              <div className={ styles.left }>
                                  <div className={ styles.image }>
                                      { objectImg ? <div
                                          className={ styles.nativeImg }
                                          style={ { backgroundImage: `url(${ objectImg })` } }/>
                                        : <Icon className={ styles.icon } name={ image }/> }
                                  </div>
                              </div>
                              <div className={ styles.middle }>
                                  <div className={ cx(styles.column, 'p-45') }>
                                      <div className={ styles.name }>
                                          { objectModel[`name_${ this.lang.lang }`] }
                                      </div>
                                      <div className={ styles.address }>
                                          { unescape(get(slots, '[0].fieldValue')) }
                                      </div>
                                  </div>
                                  <div className={ cx(styles.column, 'p-20') }>
                                      <object>
                                      { renderField(slots[1]) }
                                      </object>
                                  </div>
                                  <div className={ cx(styles.column, 'p-20') }>
                                      <object>
                                      { renderField(slots[2]) }
                                      </object>
                                  </div>
                                  <div className={ cx(styles.column, 'p-15') }>
                                      <object>
                                      { renderField(slots[3]) }
                                      </object>
                                  </div>
                              </div>
                              <div className={ styles.right }>
                                  <div className={ cx(styles.column) }>
                                      <FormatDateToQuarter className={ styles.quarter } date={ finish }
                                        status={ status } lastFactDate={ statusData.foreman.last_fact_datetime }/>
                                  </div>
                                  <div onClick={ (e) => {
                                      e.preventDefault()
                                      e.stopPropagation()
                                      this.router.push({
                                          pathname: `/projects/${ objectModel.project_id.project_id }/objects/${ objectModel.object_id }`,
                                          hash: `reports/guntt`,
                                          search: `view=showFinance&expandAll=true`
                                      })
                                  } } className={ cx(styles.column, 'p-30', styles.marginRight) }>
                                      <ProgressBar
                                        title={ loc._('basic.report_fact_type.foreman') }
                                        status={ statusData.foreman.status }
                                        value={ statusData.foreman.volume_percent }/>
                                  </div>
                                  <div
                                    onClick={ (e) => {
                                        e.preventDefault()
                                        e.stopPropagation()
                                        this.router.push({
                                            pathname: `/projects/${ objectModel.project_id.project_id }/objects/${ objectModel.object_id }`,
                                            hash: `reports/guntt`,
                                            search: `view=showFinance&expandAll=true`
                                        })
                                    } }
                                    className={ cx(styles.column, 'p-30') }>
                                      <ProgressBar
                                        title={ loc._('basic.report_fact_type.revisor') }
                                        status={ statusData.revisor.status }
                                        value={ statusData.revisor.volume_percent }/>
                                  </div>
                              </div>
                          </div>
                      </Link>
                  }) : <NoResultContainer isFilterDefault={ this.filter.isFilterDefault }/> }
              </InfiniteScroll></div>)
    }
}

export {
    ObjectsList
}
