import React from "react"
import Loadable, { LoadableComponent } from '@loadable/component'
import { Preloader } from "components/UI/Preloader"

const AsyncFilesArchiveScreen: LoadableComponent<any> = Loadable(() => import(/* client-side */'./FilesArchiveScreen'), {
    fallback: <Preloader /> } )

export { AsyncFilesArchiveScreen }
