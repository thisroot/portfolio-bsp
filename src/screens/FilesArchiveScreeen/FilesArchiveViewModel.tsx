import { IFileBrowserEntity } from "../../components/FileManager"
import { inject } from "react-ioc"
import {
    AnalyticsService,
    ApiService,
    AppService,
    DataContext,
    DictionaryService,
    FileLoaderService,
    RouterService
} from "../../services"
import { action, observable, observe, runInAction } from "mobx"
import { File } from "../../models/mst/File"
import { FileListModel } from "../../models/entities/FileListModel"
import { get, throttle } from "lodash"
import { saveAs } from 'file-saver'
import { toaster } from "../../components/UI/Toast"
import { FilesArchiveFilterViewModel } from "../../components/Filters/FilesArchiveFilter"

interface DragItem extends IFileBrowserEntity {
    index: number
    type: string
}

class FilesArchiveViewModel {
    @inject dataContext: DataContext
    @inject api: ApiService
    @inject router: RouterService
    @inject fileLoader: FileLoaderService
    @inject dictionary: DictionaryService
    @inject analytics: AnalyticsService
    @inject app: AppService
    @inject filter: FilesArchiveFilterViewModel

    acceptedFiles = []

    @observable pending = false
    @observable wasInitialized = false
    @observable browserList: Array<File> = [] // this.files['root'].children
    @observable currentFolderId = null
    @observable path: Array<File> = []
    @observable selectedFile: DragItem = null

    @action selectFile = (item: DragItem) => {
        this.selectedFile = item
    }

    @observable filesMap = new FileListModel(this.fileLoader, true, false)

    @action public init = async () => {
        if (!this.wasInitialized) {
            try {
                this.pending = true

                await this.dictionary.init()
                await this.filter.initFilterForm()
                this.acceptedFiles = this.dictionary.get('extension').map(item => `.${ item }`)
                await this.load()

            } catch (e) {
                this.analytics.sendError(e)
            } finally {
                runInAction(() => {
                    this.wasInitialized = true
                })
            }
        }
    }

    @action downloadItem = async (file: DragItem) => {
        try {
            // const result = await this.api.files.downloadFromUrl(file.file.url)
            // if (result.status !== 'error') {
                saveAs(file.file.url, file.name)
            // }
        } catch (e) {
            this.analytics.sendError(e)
        }
    }

    @action public load = async () => {
        try {
            this.pending = true
            const result = await this.api.files.userList({
                extended: true,
                filter: {
                    ...this.filter.formValues,
                    search: this.searchString
                },
                sort: {
                    ...this.filter.sortValues
                }
            })
            if (result.status !== 'error') {
                const files = get(result, 'body.file')

                this.filesMap.files.clear()

                await this.filesMap.push(files).catch(e => {
                    this.analytics.sendError(e)
                })

                this.browserList = get(result, 'body.file_list').map(item => this.filesMap.files.get(item))
                this.pending = false
            }
        } catch (e) {
            this.analytics.sendError(e)
        }
    }

    @observable
    public searchPending = false
    @observable
    public searchString: string
    @action
    public onSearch = throttle(async (value?: string) => {
        if (value !== this.searchString && value !== undefined) {
            try {
                this.searchPending = true
                this.searchString = value
                await this.load()
            } catch (e) {
                toaster.error()
            } finally {
                this.searchPending = false
            }
        }
    }, 300, { trailing: false })

    private sortOnChange = observe(this.filter, 'selectedSort', async () => {
        await this.load()
    })

    public dispose = () => {
        this.sortOnChange()
    }

    @action onReset = async (e) => {
        await this.filter.resetForm(e)
        await this.load()
    }

    @action onSubmit = async (e) => {
        await this.filter.submitForm(e)
        await this.load()
    }
}


export { FilesArchiveViewModel }
