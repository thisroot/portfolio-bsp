import { observer } from "mobx-react"
import { inject, provider, useInstance } from "react-ioc"
import React from "react"
import { FilesArchiveViewModel } from "./FilesArchiveViewModel"
import { PendingPaths } from "../../components/BaseListComponent"
import { StickyScreen } from "../../components/StickyScreen"
import styles from './styles.scss'
import { ROUTES } from "../../constants"
import { loc } from "../../utils/i18n"
import { FileManager } from "../../components/FileManager"
import { FilesArchiveFilter, FilesArchiveFilterViewModel } from "../../components/Filters/FilesArchiveFilter"
import { Popover } from "../../components/UI/Popover"
import { DefaultButton } from "../../components/UI/Button"
import cx from 'classnames'

const pageName = loc._(ROUTES.FILES_ARCHIVE.NAME)

const
  Filter: React.FC<any> = observer(() => {

      const view = useInstance(FilesArchiveViewModel)
      const filter = useInstance(FilesArchiveFilterViewModel)

      return <Popover
        onClickOutside={ () => {
            filter.togglePopover()
        } }
        isOpen={ filter.isPopoverOpen }
        align={ "start" } position={ 'left' }
        content={ <FilesArchiveFilter onSubmit={ view.onSubmit } onReset={ view.onReset }/> }
      >
          <div className={ styles.filterContainer } onClick={ filter.togglePopover }>
              <DefaultButton
                className={ cx(styles.filter, { [styles.open]: filter.isPopoverOpen }) }
                icon={ 'internet-filter-sort-b' }
              />
              { filter.countFilledFields > 0 &&
              <span className={ styles.badge }>{ filter.countFilledFields }</span> }
          </div>
      </Popover>
  })

@observer
class FilesArchive extends React.Component {

    @inject
    public view: FilesArchiveViewModel


    public async componentDidMount(): Promise<void> {
        await this.view.init()
    }


    public renderTopComponent() {
        return <div className={ styles.topContainer }>
            <div className={ styles.left }>
                <h1>{ pageName }</h1>
            </div>
        </div>
    }

    public renderContent = () => {
        return <div className={ styles.contentWrapper }><FileManager
          renderFilter={ Filter }
          storageId={ '93ff7efb-e7d7-492c-9b90-547c8886b137' }
          selectedFile={ this.view.selectedFile }
          selectFile={ this.view.selectFile }
          pending={ !this.view.wasInitialized }
          initialized={ this.view.wasInitialized }
          entities={ this.view.browserList }
          acceptedFiles={ this.view.acceptedFiles }
          downloadFile={ this.view.downloadItem }
          onSearch={ this.view.onSearch }
          searchString={ this.view.searchString }
          listPending={ this.view.pending }
        /></div>
    }

    public render() {
        const pending: PendingPaths = {
            middleLeft: !this.view.wasInitialized,
            content: !this.view.wasInitialized
        }

        return <StickyScreen
          topComponent={ this.renderTopComponent }
          contentComponent={ this.renderContent }
          pending={ pending }/>
    }
}

const FilesArchiveScreen = provider()(FilesArchive)
FilesArchiveScreen.register(FilesArchiveViewModel, FilesArchiveFilterViewModel)
export default FilesArchiveScreen
