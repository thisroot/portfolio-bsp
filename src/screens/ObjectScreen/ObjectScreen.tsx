import React from "react"
import { observer } from "mobx-react"
import { inject, provider } from "react-ioc"
import cx from 'classnames'
import { ObjectViewModel } from "./ObjectViewModel"
import { ReportCardViewModel } from "./ObjectTabs/ReportCard/ReportCardViewModel"
import { ScheduleListBuilderCardViewModel } from "./ObjectTabs/ScheduleListBuilderCard/ScheduleListBuilderCardViewModel"
import { Link } from 'services/RouterService'
import { PendingPaths } from "components/BaseListComponent"
import { AsyncReportCard, AsyncScheduleListBuilderCard, MainCard, ObjectGallery, ObjectVideo, } from "./ObjectTabs"
import ReportCardHeader from "./ObjectTabs/ReportCard/ReportCardHeader"
import ScheduleListBuilderCardHeader from "./ObjectTabs/ScheduleListBuilderCard/ScheduleListBuilderCardHeader"
import styles from './styles.scss'
import { GunttViewModel } from "./ObjectTabs/ReportCard/Reports/Guntt/GunntViewModel"
import { GunttFilterViewModel } from "components/Filters/GunttFilter"
import { FactValuesByDayViewModel } from "./ObjectTabs/ReportCard/Reports/FactValuesByDay/FactValuesByDayViewModel"
import { ObjectGalleryViewModel } from "./ObjectTabs/ObjectGallery/ObjectGalleryViewModel"
import { PhotoFactsViewModel } from "./ObjectTabs/ReportCard/Reports/PhotoFacts/PhotoFactsViewModel"
import { StickyScreen, StickyScreenDoubleHeader } from "../../components/StickyScreen"
import { PassportCard } from "./ObjectTabs/PassportCard"
import { Icon } from 'components/UI/Icon'
import { t } from '@lingui/macro'
import { loc } from "utils/i18n"
import { ObjectHistoryCard, ObjectHistoryCardHeader, ObjectHistoryViewModel } from "./ObjectTabs/ObjectHistoryCard"
import { ObjectHistoryFilterViewModel } from 'components/Filters/ObjectHistoryFilter'
import { LocalisationService } from "../../services/LocalisationService"
import { ArrowReportViewModel } from "./ObjectTabs/ReportCard/Reports/ArrowReport/ArrowReportViewModel"
import { get, isEmpty } from 'lodash'
import { ScheduleActualValuesFormModel } from "./ObjectTabs/ScheduleListBuilderCard/ScheduleActualValuesForm/ScheduleActualValuesFormModel"
import FileManagerCard from "./ObjectTabs/FileManagerCard/FileManagerCard"
import { FileManagerCardViewModel } from "./ObjectTabs/FileManagerCard/FileManagerCardViewModel"
import { ObjectFilesFilterViewModel } from "../../components/Filters/ObjectFilesFilter/ObjectFilesFilterViewModel"
import {
    RevisorCorrectionCard,
    RevisorCorrectionCardHeader,
    RevisorCorrectionViewModel
} from "screens/ObjectScreen/ObjectTabs/RevisorCorrectionCard"
import { RevisorCorrectionFilterViewModel } from "components/Filters/RevisorCorrectionFilter"
import { RevisorCorrectionDocumentDataModel } from 'screens/ObjectScreen/ObjectTabs/RevisorCorrectionCard/RevisorCorrectionModal'
import { BudgetCard, BudgetCardViewModel } from "./ObjectTabs/BudgetCard"
import { BudgetCardHeader } from "./ObjectTabs/BudgetCard/BudgetCardHeader"

@observer
class ObjectS extends React.Component {
    @inject public view: ObjectViewModel
    @inject constr: ScheduleListBuilderCardViewModel
    @inject public lang: LocalisationService
    @inject public reports: ReportCardViewModel

    public async componentDidMount(): Promise<void> {
        await this.view.init()
    }

    public renderTopComponent = () =>
      <StickyScreenDoubleHeader
        name={ get(this.view.currentObject, `name_${ this.lang.lang }`) }
        statusData={ this.view.statusData }
        dateFinish={ this.view.dateFinish }
      />

    public renderTab = () => {
        switch (this.view.tabScreen.name) {
            case 'object':
                return <MainCard/>
            case 'constructor':
                return <AsyncScheduleListBuilderCard/>
            case 'objectGallery':
                return <ObjectGallery/> //TODO: добавить загрузку через лейзилоуд
            case 'objectVideo':
                return <ObjectVideo/> //TODO: добавить загрузку через лейзилоуд
            case 'passport':
                return <PassportCard/>
            case 'documents':
                return <FileManagerCard/>
            case 'budget':
                return <BudgetCard />
            case 'history':
                return <ObjectHistoryCard/>
              case 'revisorCorrection':
                return <RevisorCorrectionCard/>
            default:
                return <AsyncReportCard/>
        }
    }

    public renderTabHeader = () => {
        switch (this.view.tabScreen.name) {
            case 'reports':
                return <ReportCardHeader/>
            case 'constructor':
                return <ScheduleListBuilderCardHeader/>
            case 'history':
                return <ObjectHistoryCardHeader/>
            case 'budget':
                return <BudgetCardHeader />
            case 'revisorCorrection':
                return <RevisorCorrectionCardHeader/>
            case 'description':
            case 'model':
            case 'timeline':
            case 'documents':
            default:
                return null
        }
    }
    public renderContentComponent = () => <div className={ styles.contentWrapper }>{ this.renderTab() }</div>

    public renderControlsContainer = () => (
      <div className={ styles.controlsContainer }>
          { this.view.currentObject && <>
          <div className={ styles.left }>
              {
                  this.view.tabScreens.filter((control) => !control.hidden)
                    .map((control) => {
                        const key = `component.objectScreen.tabMenu.${ control.name }`
                        return <div key={ control.name }
                                    className={ cx(styles.controlItem, { [styles.active]: control.name === this.view.tabScreen.name }) }>
                            <Link to={ `#${ control.name }` }
                                  title={ control.title }>
                                <span>{ this.view.app.i18n._(key) }</span>
                            </Link>
                        </div>
                    })
              }
          </div>
          { !isEmpty(this.reports.reportNavigation)
            ? (
              <div className={ styles.middle }>
                  <Link
                    to={ `${ this.reports.reportNavigation.prev.path }` }
                    title={ this.reports.reportNavigation.prev.name }
                    className={ cx(styles.reportName, styles.leftReport) }
                  >
                      <Icon name='arrows-arrow-r'/>
                      <span>{ this.reports.reportNavigation.prev.name }</span>
                  </Link>
                  <Link
                    to={ `${ this.reports.reportNavigation.next.path }` }
                    title={ this.reports.reportNavigation.next.name }
                    className={ cx(styles.reportName, styles.rightReport) }
                  >
                      { this.reports.reportNavigation.next.name }
                      <Icon name='arrows-arrow-r'/>
                  </Link>
              </div>
            )
            : null }
          <div className={ styles.right }>
              { get(this.view.currentObject, 'favorite') ? (
                <Icon
                  className={ cx(styles.addBookMarkIcon, styles.inFavorite, { [styles.cantWrite]: !this.view.currentObject.canRead() }) }
                  textStyle={ { color: 'var(--basic-350)' } }
                  text={ this.view.currentObject.canRead() ? loc._(t`basic.removeFromFavorites`) : loc._(t`basic.inTheFavorites`) }
                  name={ 'account-bookmarks-remove' }
                  onClick={ this.view.currentObject.canRead() ? this.view.removeFromFavoriteHandler : null }
                />
              ) : (
                <Icon
                  className={ cx(styles.addBookMarkIcon, { [styles.cantWrite]: !this.view.currentObject.canRead() }) }
                  text={ this.view.currentObject.canRead() ? loc._(t`basic.addToFavorite`) : loc._(t`basic.notInTheFavorites`) }
                  name={ 'internet-favorite-b' }
                  onClick={ this.view.currentObject.canRead() ? this.view.addToFavoriteHandler : null }
                />
              ) }
          </div>
          </> }
      </div>
    )

    public renderControls = () => {
        return (
          !this.view.pending && <div className={ styles.headerContainer }>
              { this.renderControlsContainer() }
              { this.renderTabHeader() }
              <div className={ styles.line }/>
          </div> || null
        )
    }

    public render() {
        const pendingPath: PendingPaths = {
            all: this.view.pending
        }

        return <StickyScreen topComponent={ this.renderTopComponent }
                             middleComponent={ this.renderControls }
                             contentComponent={ this.renderContentComponent }
                             pending={ pendingPath }/>
    }
}

const ObjectScreen = provider()(ObjectS)
ObjectScreen.register(
  ObjectViewModel,
  GunttViewModel,
  GunttFilterViewModel,
  ReportCardViewModel,
  ScheduleListBuilderCardViewModel,
  ScheduleActualValuesFormModel,
  FactValuesByDayViewModel,
  ObjectGalleryViewModel,
  PhotoFactsViewModel,
  ObjectHistoryViewModel,
  ObjectHistoryFilterViewModel,
  ArrowReportViewModel,
  FileManagerCardViewModel,
  ObjectFilesFilterViewModel,
  ArrowReportViewModel,
  RevisorCorrectionViewModel,
  RevisorCorrectionFilterViewModel,
  RevisorCorrectionDocumentDataModel,
  BudgetCardViewModel,
)
export default ObjectScreen
