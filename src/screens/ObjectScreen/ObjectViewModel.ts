import { inject } from "react-ioc"
import { action, computed, observable, runInAction } from "mobx"
import { get, isEmpty } from 'lodash'
import { t } from '@lingui/macro'
import { loc } from 'utils/i18n'
import { 
  getMetaInfoFields,
  initMetaform,
  onSaveMeta,
  parsePath,
  ENTITY_FIELDS,
  IWidgetProps,
} from "utils/helpers"
import { ObjectModel, Organisation, Project, User } from "models/mst"
import { RouterService } from "services/RouterService"
import { APP_STATE, AppService } from "services/AppService"
import { ApiService } from "services/ApiService"
import { DataContext } from "services/DataContext"
import { canUpdate, ROLE, ROUTES, canRealRead } from "../../constants"
import { getSnapshot } from "mobx-state-tree"
import MobxReactForm from 'mobx-react-form'
import { AuthService } from "../../services"
import { toaster } from "../../components/UI/Toast"
import { FileModel } from "../../models/entities"
import { LocalisationService } from "../../services"

interface IObjectScreen {
    name: string;
    title: string;
    hidden?: boolean;
}

class ObjectViewModel {
    @inject dataContext: DataContext
    @inject router: RouterService
    @inject app: AppService
    @inject api: ApiService
    @inject auth: AuthService
    @inject lang: LocalisationService

    @computed
    public get tabScreens(): Array<IObjectScreen> {
        const filteredTabs: Array<IObjectScreen> = []
        filteredTabs.push(
          { name: "object", title: loc._(t`component.objectScreen.tabMenu.object`) },
          { name: 'passport', title: loc._(t`component.objectScreen.tabMenu.passport`) },
        )
        if (this.canRead('schedule')) {
            filteredTabs.push({ name: "constructor", title: loc._(t`component.objectScreen.tabMenu.constructor`) })
        }
        // if (this.canRead('report')) {
            filteredTabs.push({ name: "reports", title: loc._(t`component.objectScreen.tabMenu.reports`) })
        // }
        if (this.canRead('image')) {
            filteredTabs.push({ name: "objectGallery", title: loc._(t`component.objectScreen.tabMenu.objectGallery`), hidden: true })
        }
        if(this.canRead('schedule.fact.file') || this.canRead('file') || this.canRead('schedule.fact')) {
            filteredTabs.push({ name: "documents", title: loc._(t`component.objectScreen.tabMenu.documents`) })
        }
        if(this.canRead('suek.budget.plan')) {
            filteredTabs.push({ name: "budget", title: loc._(t`component.objectScreen.tabMenu.budget`) })
        }
        if (this.auth.req.currentPermissions.has(canRealRead(ROLE.USER, 'history'))) {
            filteredTabs.push({ name: "history", title: loc._(t`component.objectScreen.tabMenu.history`) })
        }
        if (this.canRead('revisor.correction') && this.auth.req.currentPermissions.has(canRealRead(ROLE.USER, 'revisor.correction'))) {
            filteredTabs.push({ name: "revisorCorrection", title: loc._(t`component.objectScreen.tabMenu.revisorCorrection`) })
        }
        filteredTabs.push(
          { name: "objectVideo", title: loc._(t`component.objectScreen.tabMenu.objectVideo`) }
        )
        return filteredTabs
    }

    @observable
    public metaform: any

    @observable
    public virtualModelUrl: string

    @observable
    public pending = true

    @observable dateMask = loc._(t`basic.dateFormat.DD.MM.YY`)

    @observable
    public isEditMode = false

    @computed
    public get userCanEditMeta() {
        return this.auth.isAllow(canUpdate(ROLE.MANAGER, 'object')) && this.canWrite()
    }

    @action
    public changeObjectPosition = async (e: any) => {
        if(this.currentObject.canWrite()) {
            this.currentObject.geo_coord = {
                lat: +e.latLng.lat().toFixed(6),
                long: +e.latLng.lng().toFixed(6)
            }
            await this.api.object.update(getSnapshot(this.currentObject))
        }
    }

    @action
    public toggleEdit = () => {
        this.isEditMode = !this.isEditMode
    }

    @action
    public resetEdit = () => {
        this.metaform.reset()
        this.isEditMode = false
    }

    @action
    public saveMeta = () => {
        this.metaform.submit()
    }

    @action
    public onSaveMeta = async (form: any) => {
        onSaveMeta(form, this.currentObject, this.toggleEdit, this.api.object.update, this.metaform, 'object', this.lang.lang)
    }

    @action
    public onErrorMeta = (form: any) => {
        console.log(form)
    }

    @computed
    public get isMetaFieldsExist() {
        return !isEmpty(this.currentObject.meta)
    }

    @computed get mainMataInfoProps(): IWidgetProps {
        return getMetaInfoFields(this.currentObject, 'object')
    }

    @computed get guntMetaInfoProps(): IWidgetProps {
      return getMetaInfoFields(this.currentObject, 'object', this.lang.lang, ENTITY_FIELDS.GUNT_WIDGET);
  }


    @action
    public initMetaform() {
        const formFields = initMetaform(this.currentObject, loc._(t`components.metaEditor.ungroupedFieldsGroupName`), 'object', this.lang.lang)

        if (!formFields) return


        this.metaform = new MobxReactForm({ fields: formFields }, {
            hooks: {
                onSuccess: this.onSaveMeta,
                onError: this.onErrorMeta,
            }
        })
    }

    @action
    public init = async () => {
        this.pending = true
        try {
            this.virtualModelUrl = get(this.currentObject, 'meta._virtualModelUrl') || 'https://165104.selcdn.ru/eps/ifc/1.ifc'

            const referenceObjectTypes = await this.dataContext.cacheFirst(this.api.reference.object_type, {}, { timeInvalidate: 10 * 60 * 1000 })

            if (referenceObjectTypes.status !== 'error' && referenceObjectTypes.status !== 'cached') {
                this.dataContext.applySnapshot(referenceObjectTypes.body)
            }

            const resp = await this.dataContext.cacheFirst(this.api.project.list, {
                project_id: [ +this.projectIdByRoute ]
            })

            if (resp.status !== 'error' && resp.status !== 'cached') {
                isEmpty(resp.body.project) ? runInAction(() => this.app.state = APP_STATE.noMatch) : this.dataContext.applySnapshot(resp.body)
            }

            const object_model = await this.dataContext.cacheFirst(this.api.object.list, {
                project_id: [ +this.projectIdByRoute ],
                object_id: [ +this.objectIdByRoute ]
            })

            if (object_model.status !== 'error' && object_model.status !== 'cached') {
                isEmpty(object_model.body.object) ? runInAction(() => this.app.state = APP_STATE.noMatch) :
                  this.dataContext.applySnapshot(object_model.body)
                runInAction(() => {
                    this.dataContext.mergeArray(this.currentProject.objects, [ ...Object.keys(object_model.body.object) ])
                })
            }

            this.initMetaform()

        } catch (e) {
            console.log('error', e)
        } finally {
            this.pending = false
        }
    }

    @computed get metaFormFields() {
        return this.metaform ? [ ...this.metaform.fields.values() ] : null
    }

    @computed get metaTypeFields() {
        return [ ...this.dataContext.reference.object_type.values() ]
    }

    @observable isChangeMetaTypeMode = false

    @action toggleChangeMetaTypeMode = () => this.isChangeMetaTypeMode = !this.isChangeMetaTypeMode

    @action changeMetaType = async (value: { value: any, key: string }) => {
        try {
            const key = get(value, 'object_type_id', null)
            this.currentObject.object_type_id = key
            this.initMetaform()
            this.toggleChangeMetaTypeMode()
            await this.api.object.update(getSnapshot(this.currentObject))
        } catch (e) {
            toaster.error()
        }
    }

    // @action selectAsMain = async (file: FileModel) => {
    //     this.currentObject.meta = { ...this.currentObject.meta, mainImage: file.getSnapshot }
    //     try {
    //         await this.api.object.update(getSnapshot(this.currentObject))
    //     } catch (e) {
    //         toaster.error()
    //     }
    // }

    @action selectAsMain = async (file: FileModel) => {
        try {
            if(this.mainImageId === file.id) return
            const result = await this.api.files.setMainImage(get(file.getSnapshot, 'image_id'))
            this.currentObject.main_image = file.getSnapshot
            result.status === 'error' && toaster.error()
        } catch (e) {
            toaster.error()
        }
    }

    @action
    public addToFavoriteHandler = async () => {
        try {
            const { status } = await this.api.object.favoriteAdd({ object_id: [this.currentObject.object_id] });
            if (status !== 'error') {
                this.currentObject.favorite = true;
            }
        } catch (e) {
            toaster.error()
        }
    }

    @action
    public removeFromFavoriteHandler = async () => {
        try {
            const { status } = await this.api.object.favoriteDelete({ object_id: [this.currentObject.object_id] });
            if (status !== 'error') {
                this.currentObject.favorite = false;
            }
        } catch (e) {
            toaster.error()
        }
    }

    @computed get mainImage() {
        return get(this.currentObject, 'main_image.converted.medium.url')
    }

    @computed get mainImageId() {
        return get(this.currentObject, 'main_image.image_id')
    }

    @computed get currentMetaType() {
        return get(this.currentObject, 'object_type_id')
    }

    @computed
    public get tabScreen(): IObjectScreen {
        const found = this.tabScreens.filter((item) => item.name === this.router.location.hash.split('/')[0].replace('#', ''))
        return found && found.length > 0 ? found[0] : this.tabScreens[3]
    }

    @computed
    public get subPath(): any {
        return this.router.location.hash.split('/').slice(1)
    }

    @computed
    public get projectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.projectId')
    }

    @computed
    public get objectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId')
    }

    @computed
    public get currentProject(): Project {
        return this.dataContext.project.get(this.projectIdByRoute) || undefined
    }

    @computed
    public get currentObject(): ObjectModel {
        return this.dataContext.object.get(this.objectIdByRoute)
    }

    @computed
    public get creator(): User {
        return `${ get(this.currentProject, 'created_by.firstname', '') } ${ get(this.currentProject, 'created_by.lastname', '') }`
    }

    @computed
    public get organisation(): Organisation {
        return get(this.currentProject, 'organisation_id.name', '')
    }

    @computed get dateFinish() {
        return get(this.currentObject, 'date_finish')
    }

    @computed get statusData() {
        return get(this.currentObject, 'status_data')
    }

    @action canWrite(alias?: string) {
        return this.currentObject && this.currentObject.canWrite(alias)
    }

    @action canRead(alias?: string) {
        return this.currentObject && this.currentObject.canRead(alias)
    }

    @action canDelete(alias?: string) {
        return this.currentObject && this.currentObject.canDelete(alias)
    }

    @action canUploadModel(alias?: string) {
        return this.currentObject && this.currentObject.canUpload(alias)
    }
}


export {
    ObjectViewModel
}
