import React from "react"
import { observer } from "mobx-react-lite"
import styles from "./styles.scss"
import { Icon } from "components/UI/Icon"
import { Badge } from "components/UI/Badge"
import { Spinner } from "components/UI/Spinner"
import { EditableField } from "components/EditableField"
import dayjs from "dayjs"
import filesize from "filesize.js"
import { Button, BUTTON_TYPE } from "components/UI/Button"
import cx from 'classnames'
import { getFileTypeIcon } from "../../../../../components/FileManager/utils"
import { IFileBrowserRowProps } from "../../../../../components/FileManager"
import { useInstance } from "react-ioc"
import { FileManagerCardViewModel } from "../FileManagerCardViewModel"
import { FILE_RELATION } from "../../../../../constants"
import { DataContext } from "../../../../../services"
import { get } from 'lodash'
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)

const RowItem: React.FC<IFileBrowserRowProps> = observer((
  {
      file,
      isOver,
      canDropHere,
      isCurrentPlace,
      isSelected,
      updateFile,
      removeFile,
      userCanUpdateFile,
      userCanRemoveFile
  }) => {

    const iconName = getFileTypeIcon({ ...file.getSnapshot, isOver, canDropHere, isCurrentPlace })

    const view = useInstance(FileManagerCardViewModel)
    const dataContext = useInstance(DataContext)
    const entityName = view.currentRelation === FILE_RELATION.OBJECT_FACT ? get(dataContext.schedule_fact.get(`${ file.relationId }`), 'schedule_id.name', '') : view.currentRelation === FILE_RELATION.OBJECT_SCHEDULE ?
      get(dataContext.schedule.get(`${ file.relationId }`), 'name', '') : null

    const fileCanUpdate = file.folder && isSelected
    const onSaveFile = async (e) => await updateFile(file, e)

    const leftRightStyles = view.currentRelation === FILE_RELATION.OBJECT ? [ 'p-60', 'p-40' ] : [ 'p-40', 'p-60' ]

    return <div className={ cx(styles.listBody, { [styles.filePending]: file.isProgressVisible }) }>
        <div className={ cx(styles.left, leftRightStyles[0]) }>
            <div className={ styles.fileType }>
                <Icon className={ styles.icon } name={ iconName }/>
            </div>
            <div className={ styles.fileIndex }>
                { !file.isProgressVisible && <Badge text={ file.index.toString() }/> }
                { file.isProgressVisible && <Spinner/> }
            </div>
            <div className={ cx('p-90') }>
                <EditableField
                  centered={ false }
                  value={ file.name }
                  label={ file.name }
                  onSave={ onSaveFile }
                  isEditable={ userCanUpdateFile && fileCanUpdate }
                />
            </div>
        </div>
        <div className={ cx(styles.right, leftRightStyles[1]) }>
            <div className={ cx(styles.rowData, 'p-90') }>
                <div className={ entityName ? 'p-15' : 'p-30' }>
                    { dayjs.utc(file.created).format('DD MM YYYY') }
                </div>
                <div className={ entityName ? 'p-10' : 'p-20' }>
                    { dayjs.utc(file.created).format('HH:mm') }
                </div>
                <div className={ entityName ? 'p-15' : 'p-20' }>
                    { !file.folder ? filesize(file.size) : "" }
                </div>
                <div className={ entityName ? 'p-10' : 'p-20' }>
                    { file.ext }
                </div>
                {
                    entityName &&
                    <div className={ 'p-40' }>
                        { entityName }
                    </div>
                }
            </div>
            <div className={ styles.fileDelete }>
                {
                    userCanRemoveFile && !file.pending &&
                    <Button className={ styles.deleteBtn } onClick={ async (e) => {
                        e.stopPropagation()
                        await removeFile(file)
                    } }
                            buttonType={ BUTTON_TYPE.DANGER }
                            icon={ 'account-trash-delete-bin' }/>
                }
            </div>
        </div>
    </div>
})

export {
    RowItem
}
