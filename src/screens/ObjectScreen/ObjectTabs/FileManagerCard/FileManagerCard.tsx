import React from 'react'
import { FileManager } from "components/FileManager"
import { useInstance } from "react-ioc"
import { FileManagerCardViewModel } from "./FileManagerCardViewModel"
import { observer } from "mobx-react-lite"
import { useEffectOnce } from "react-use"
import styles from "./styles.scss"
import { DefaultButton } from "../../../../components"
import cx from 'classnames'
import { Popover } from "../../../../components"
import { ObjectFilesFilter } from "../../../../components/Filters/ObjectFilesFilter/ObjectFilesFilters"
import { ObjectFilesFilterViewModel } from "../../../../components/Filters/ObjectFilesFilter/ObjectFilesFilterViewModel"
import { Dropdown } from "../../../../components"
import { RowItem } from "./RowItem"

const Filter: React.FC<any> = observer(() => {

    const filter = useInstance(ObjectFilesFilterViewModel)
    const view = useInstance(FileManagerCardViewModel)

    return <Popover
      onClickOutside={ () => {
          filter.togglePopover()
      } }
      isOpen={ filter.isPopoverOpen }
      align={ "start" } position={ 'left' }
      content={ <ObjectFilesFilter onSubmit={ view.onSubmit } onReset={ view.onReset }/> }
    >
        <div className={ styles.filterContainer } onClick={ filter.togglePopover }>
            <DefaultButton
              className={ cx(styles.filter, { [styles.open]: filter.isPopoverOpen }) }
              icon={ 'internet-filter-sort-b' }
            />
            { filter.countFilledFields > 0 &&
            <span className={ styles.badge }>{ filter.countFilledFields }</span> }
        </div>
    </Popover>
})


const LeftMenu: React.FC<any> = observer(() => {
    const view = useInstance(FileManagerCardViewModel)
    return <div className={ styles.leftMenuContainer }>
        { view.wasInitialized &&
        <Dropdown initialItem={ 'object' } keyName={ 'key' } itemName={ 'value' }
                  items={ view.fileRelations }
                  onChange={ view.changeFileRelationView }/> }
    </div>
})


const FileManagerCard: React.FC = observer(() => {
    const view = useInstance(FileManagerCardViewModel)
    const filter = useInstance(ObjectFilesFilterViewModel)

    useEffectOnce(() => {
        view.init()
    })

    return (
      <FileManager
        renderRow={ RowItem }
        renderFilter={ Filter }
        renderLeftMenu={ LeftMenu }
        move={ view.move }
        confirmRemoveFile={ view.confirmRemoveFile }
        userCanWriteFile={ view.userCanWriteFile }
        userCanRemoveFile={ view.userCanRemoveFile }
        updateFile={ view.updateFile }
        userCanAddFolder={ view.userCanAddFolder }
        storageId={ '7b409a18-842e-46d0-a07b-dc5f4a1ad953' }
        selectedFile={ view.selectedFile }
        selectFile={ view.selectFile }
        userCanUpdateFile={ view.userCanUpdateFile }
        addFolder={ view.addFolder }
        onSelectToUpload={ view.onSelectToUpload }
        pending={ view.pending }
        initialized={ view.wasInitialized }
        path={ view.path }
        openFolder={ view.openFolder }
        goTo={ view.goTo }
        entities={ view.browserList }
        acceptedFiles={ view.acceptedFiles }
        removeFile={ view.confirmRemoveFile }
        downloadFile={ view.downloadItem }
        onSearch={ view.onSearch }
        searchString={ view.searchString }
        isFilterFill={ filter.countFilledFields > 0 }
      />
    )
})

export default FileManagerCard
