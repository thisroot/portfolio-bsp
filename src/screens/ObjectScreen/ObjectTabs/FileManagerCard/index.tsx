import React from "react"
import Loadable, { LoadableComponent } from '@loadable/component'
import { Preloader } from "components/UI/Preloader"

const AsyncFileManagerCard: LoadableComponent<any> = Loadable(() => import(/* client-side */'./FileManagerCard'), {
    fallback: <Preloader /> } )

export { AsyncFileManagerCard }
