import { get, throttle } from 'lodash'
import { action, computed, observable, observe, runInAction } from "mobx"

import { inject } from "react-ioc"
import {
    AnalyticsService,
    ApiService,
    AppService,
    DataContext,
    DictionaryService,
    FileLoaderService,
    RouterService
} from "services"
import { File } from 'models/mst/File'
import { getMaxDefaultNameIndex, parsePath } from "utils/helpers"
import { FILE_RELATION, ROUTES } from "constants/index"
import { toaster } from "components/UI/Toast"
import { loc } from "utils/i18n"
import { t } from "@lingui/macro"
import { Project } from "models/mst/Project"
import { FileListModel } from "models/entities/FileListModel"
import { Modal } from "components/UI/Modal"
import { Domain } from "components/UI/Domain"
import colors from "static/styles/colors.scss"
import { ACTION_TYPE, Badge } from "components/UI/Badge"
import { BasicButton, DangerButton, SuccessButton } from "components/UI/Button"
import React from "react"
import styles from './styles.scss'
import { saveAs } from 'file-saver'
import { ObjectFilesFilterViewModel } from "components/Filters/ObjectFilesFilter/ObjectFilesFilterViewModel"
import { ObjectModel } from "models/mst/ObjectModel"
import { DropResult, IFileBrowserEntity } from "components/FileManager"
import { FileModel } from "../../../../models/entities/FileModel"
import { Input } from 'components/UI'


interface DragItem extends IFileBrowserEntity {
    index: number
    type: string
}


export class FileManagerCardViewModel {

    @inject dataContext: DataContext
    @inject api: ApiService
    @inject router: RouterService
    @inject fileLoader: FileLoaderService
    @inject dictionary: DictionaryService
    @inject analytics: AnalyticsService
    @inject app: AppService
    @inject filter: ObjectFilesFilterViewModel

    acceptedFiles = []

    @observable pending = false
    @observable wasInitialized = false
    @observable browserList: Array<File> = []
    @observable currentFolderId = null
    @observable path: Array<File> = []

    @observable selectedFile: DragItem = null

    @action selectFile = (item: DragItem) => {
        this.selectedFile = item
    }

    @computed get userCanUpdateFile() {
        return this.userCanAddFolder
    }

    @action updateFile = async (file: DragItem, value: string) => {
        const oldVal = file.name
        if (oldVal !== value) {
            file.file.name = value
            const result = await this.api.files.folderMove({
                file_id: file.id,
                name: value,
                parent_id: this.currentFolderId
            })
            if (result.status === 'error') {
                toaster.error(result.message)
                file.file.name = oldVal
            }
        }
    }

    public fileRelations = []
    @observable currentRelation: FILE_RELATION = FILE_RELATION.OBJECT

    @action public changeFileRelationView = async (selection: any) => {
        if (selection && this.currentRelation !== get(selection, 'key', FILE_RELATION.OBJECT)) {
            this.currentFolderId = null
            this.currentRelation = get(selection, 'key', FILE_RELATION.OBJECT)
            await this.load()
        }
    }

    @observable filesMap = new FileListModel(this.fileLoader, true, false)

    @action public init = async () => {
        if (!this.wasInitialized) {
            try {
                this.pending = true

                if (this.currentObject.canRead('file')) {
                    this.fileRelations.push({ value: loc._(t`basic.fileRelation.object`), key: FILE_RELATION.OBJECT })
                }

                if (this.currentObject.canRead('schedule.file')) {
                    this.fileRelations.push({
                        value: loc._(t`basic.fileRelation.object_schedule`),
                        key: FILE_RELATION.OBJECT_SCHEDULE
                    })
                }

                if (this.currentObject.canRead('schedule.fact.file')) {
                    this.fileRelations.push({
                        value: loc._(t`basic.fileRelation.object_fact`),
                        key: FILE_RELATION.OBJECT_FACT
                    })
                }

                await this.dictionary.init()
                await this.filter.initFilterForm()
                this.acceptedFiles = this.dictionary.get('extension').map(item => `.${ item }`)
                await this.load()

            } catch (e) {
                this.analytics.sendError(e)
            } finally {
                runInAction(() => {
                    this.wasInitialized = true
                })
            }
        }
    }

    private sortOnChange = observe(this.filter, 'selectedSort', async () => {
        await this.load()
    })

    public dispose = () => {
        this.sortOnChange()
    }

    @action onReset = async (e) => {
        await this.filter.resetForm(e)
        await this.load()
    }

    @action onSubmit = async (e) => {
        await this.filter.submitForm(e)
        await this.load()
    }


    @action public load = async () => {
        try {

            let sort = {}
            if (this.filter.selectedSort) {
                sort = {
                    [this.filter.selectedSort.key]: this.filter.selectedSort.value
                }
            }

            this.pending = true
            const result = await this.api.files.fileList({
                file_relation: this.currentRelation,
                file_relation_id: [ +this.objectIdByRoute ],
                parent_id: this.currentFolderId,
                extended: true,
                sort,
                filter: {
                    ...this.filter.formValues,
                    search: this.searchString
                }
            })
            if (result.status !== 'error') {
                const files = get(result, 'body.file')
                this.dataContext.applySnapshot(result.body)
                this.filesMap.files.clear()

                await this.filesMap.push(files, this.currentRelation, +this.objectIdByRoute, this.currentFolderId).catch(e => {
                    this.analytics.sendError(e)
                })

                this.browserList = get(result, 'body.file_list').map(item => this.filesMap.files.get(item))
                const list = get(result, 'body.path_list')
                this.path = list && list.length > 0 && list.map(item => new FileModel(get(result, `body.path[${ item }]`))) || []
                this.pending = false
            }
        } catch (e) {
            this.analytics.sendError(e)
        }
    }

    @computed
    public get projectIdByRoute(): number {
        return Number(get(parsePath(this.router.location.pathname, ROUTES.PROJECT_PAGE.ROUTE), 'params.projectId'))
    }

    @computed
    public get objectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId')
    }

    @action goTo = async (file?: IFileBrowserEntity) => {
        this.currentFolderId = get(file, 'id', null)
        await this.load()
    }

    @action
    public move = async (item: DragItem, target: DropResult, oldName?: string) => {
        if (!get(target, 'id') || (item.parentId !== target.id)) {
            try {
                const file = this.filesMap.files.get(item.id)
                const oldParent = item.parentId
                file.file.parent_id = get(target, 'id', null)

                const result = file.folder ?
                  await this.api.files.folderMove({
                      file_id: file.id,
                      parent_id: get(target, 'id', null),
                      name: file.name
                  }) :
                  await this.api.files.fileMove({ file_id: file.id, parent_id: get(target, 'id', null) })
                if (result.status !== 'error') {
                    this.browserList = this.browserList.filter(el => el.id !== item.id)
                } else {
                    item.file.parent_id = oldParent
                    item.file.name = oldName && oldName || item.file.name
                    await this.confirmRenameFile(item, target, oldName || item.file.name)
                }
            } catch (e) {
                this.browserList.push(item)
                this.analytics.sendError(e)
            }
        }
    }

    @action confirmRenameFile = async (item: DragItem, target: DropResult, oldName: string) => {
        this.app.modalControls.renderModal = () => {
            return <Modal
              children={
                  <div className={ styles.modalContentContainer }>
                      <div className={ styles.col }>
                          <h4>
                              { loc._(t`components.fileManager.modal.renameFile`) }
                          </h4>
                          <div style={ { marginTop: '10px' } }>
                              <Input disablePlaceholderOnTop value={ item.name }
                                     onChange={ (e) => item.file.name = e.target.value }/>
                          </div>
                      </div>
                  </div>
              }
              renderConfirm={ () => (
                <DangerButton
                  text={ loc._(t`basic.abort`) }
                  onClick={ () => {
                      this.app.modalControls.close()
                      item.file.name = oldName
                  } }
                  disabled={ false }
                />
              )
              }
              renderAbort={ () => (
                <SuccessButton
                  style={ { border: 'none' } }
                  text={ loc._(t`basic.rename`) }
                  onClick={ async () => {
                      this.app.modalControls.close()
                      await this.move(item, target, oldName)
                  } }
                />
              )
              }
            />
        }
        this.app.modalControls.open()
    }

    // @action
    // public changeOrder = async (dragIndex: number, hoverIndex: number) => {
    //     const dragCard = this.browserList[dragIndex]
    //     this.browserList = update(this.browserList, {
    //         $splice: [
    //             [ dragIndex, 1 ],
    //             [ hoverIndex, 0, dragCard ],
    //         ],
    //     })
    //     if (this.currentFolderId) {
    //         const parent = this.dataContext.file.get(this.currentFolderId)
    //         parent.children = this.browserList
    //     }
    // }

    @action
    public openFolder = async (item) => {
        this.currentFolderId = item.id
        await this.load()
    }

    @action
    public onSelectToUpload = async (Files: Array<File>) => {
        try {
            if (Files.length > 10) {
                toaster.info(loc._(t`components.gallery.maxFilesCountSelect`))
            } else {
                this.filesMap.push(Files, this.currentRelation, +this.objectIdByRoute, this.currentFolderId).then(() => {
                    this.browserList = [ ...this.filesMap.files.values() ]
                })
                this.browserList = [ ...this.filesMap.files.values() ]
            }
        } catch (e) {
            toaster.error(e.message)
        }
    }

    @action addFolder = async () => {
        const defaultName = getMaxDefaultNameIndex(this.browserList, loc._(t`components.fileManager.newFolderName`), 'name')
        const result = await this.api.files.folderCreate({
            file_relation_id: +this.objectIdByRoute,
            file_relation: this.currentRelation,
            parent_id: this.currentFolderId,
            name: defaultName
        })
        if (result.status !== 'error') {
            await this.filesMap.push({ [result.body.file.file_id]: result.body.file })
            this.browserList = [ ...this.filesMap.files.values() ]
        } else {
            toaster.error()
        }
    }

    @observable
    public searchPending = false
    @observable
    public searchString: string

    @action
    public onSearch = throttle(async (value?: string) => {

        if (value !== this.searchString && value !== undefined) {
            try {
                this.searchPending = true
                this.searchString = value
                await this.load()
            } catch (e) {
                toaster.error()
            } finally {
                this.searchPending = false
            }
        }
    }, 150, { trailing: false })

    @computed
    public get currentProject(): Project {
        return this.dataContext.project.get(this.projectIdByRoute.toString())
    }

    @computed
    public get currentObject(): ObjectModel {
        return this.dataContext.object.get(this.objectIdByRoute.toString())
    }

    @computed
    public get userCanWriteFile() {
        return ((this.currentRelation === FILE_RELATION.OBJECT && this.currentObject.canWrite('file')) // ||
          // (this.currentRelation === FILE_RELATION.OBJECT_FACT && this.currentObject.canWrite('file')) ||
          // (this.currentRelation === FILE_RELATION.OBJECT_SCHEDULE && this.currentObject.canRead('schedule.fact.file'))
        )
    }

    @computed
    public get userCanRemoveFile() {
        return ((this.currentRelation === FILE_RELATION.OBJECT && this.currentObject.canWrite('file')) ||
          (this.currentRelation === FILE_RELATION.OBJECT_FACT && this.currentObject.canDelete('schedule.fact.file')) ||
          (this.currentRelation === FILE_RELATION.OBJECT_SCHEDULE && this.currentObject.canWrite('schedule.file'))
        )
    }

    @computed get userCanAddFolder() {
        return (this.currentRelation === FILE_RELATION.OBJECT && this.currentObject.canWrite('folder'))
    }

    @action downloadItem = async (file: DragItem) => {
        try {
            // const result = await this.api.files.download(file.id)
            // if (result.status !== 'error') {
            //     saveAs(new Blob([ result.body ]), file.name)
            // }
            saveAs(file.file.url, file.name)
        } catch (e) {
            this.analytics.sendError(e)
        }
    }

    @action removeFile = async (file: DragItem) => {
        try {

            file.folder ? await this.api.files.folderDelete([ file.id ]) : await this.api.files.deleteFile([ file.id ])
            this.filesMap.removeFile(file.id)
            this.browserList = this.browserList.filter(item => item.id !== file.id)
        } catch (e) {
            this.analytics.sendError(e)
        }
    }

    @action
    public confirmRemoveFile = async (file: DragItem) => {
        this.app.modalControls.renderModal = () => (
          <Modal
            children={
                <div className={ styles.modalContentContainer }>
                    <div className={ styles.col }>
                        <Domain labelColor={ colors.danger500 } icon={ 'account-trash-delete-bin' } text={ '' }/>
                    </div>
                    <div className={ styles.col }>
                        <h4>
                            { loc._(t`components.fileManager.modal.removeFile`) }
                        </h4>
                        <div style={ { marginTop: '10px' } }>
                            <Badge style={ { fontSize: '1rem', marginRight: '5px' } } invert type={ ACTION_TYPE.DANGER }
                                   text={ `${ loc._(t`basic.attention`) }!` }/>
                            { loc._(t`components.fileManager.modal.confirmRemoveFile`) }
                        </div>
                    </div>
                </div>
            }
            renderConfirm={ () => (
              <DangerButton
                text={ loc._(t`basic.delete`) }
                onClick={ () => {
                    this.app.modalControls.close()
                    this.removeFile(file)
                } }
                disabled={ false }
              />
            )
            }
            renderAbort={ () => (
              <BasicButton
                style={ { border: 'none' } }
                outline
                text={ loc._(t`basic.abort`) }
                onClick={ () => {
                    this.app.modalControls.close()
                } }
              />
            )
            }
          />
        )
        this.app.modalControls.open()
    }
}
