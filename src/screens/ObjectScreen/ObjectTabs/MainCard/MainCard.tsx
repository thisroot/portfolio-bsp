import React, { useEffect, useRef, useState } from 'react'
import { useInstance } from 'react-ioc'
import { Icon } from 'components/UI/Icon'
import { ObjectViewModel } from 'screens/ObjectScreen/ObjectViewModel'
import { ReportCardViewModel } from "../ReportCard/ReportCardViewModel"
import { RouterService } from "services/RouterService"
import styles from 'screens/ObjectScreen/ObjectTabs/MainCard/styles.scss'
import { observer } from "mobx-react-lite"
import { MainInfoTopWidget } from "components/Widgets"
import { ObjectsMap } from "../../../../components/ObjectsMap"
import cx from 'classnames'
import { ReportListMini } from 'components/ReportList'

const MainCard: React.FC = observer(() => {

    const view = useInstance(ObjectViewModel)
    const router = useInstance(RouterService)
    const reports = useInstance(ReportCardViewModel)
    const ref: React.Ref<HTMLDivElement> = useRef(null)
    const [ height, setHeight ] = useState(0);
    useEffect(() => ref.current && setHeight(ref.current.getBoundingClientRect().height), [ref])
    const image = view.mainImage

    const imageOnClickHandler = (e) => {
        if (view.canRead('image')) {
            e.preventDefault()
            e.stopPropagation()
            router.push({
                pathname: `/projects/${ view.currentObject.project_id.project_id }/objects/${ view.currentObject.object_id }`,
                hash: '#objectGallery'
            })
        }
    }

    return <div ref={ ref } className={ styles.mainCardWrapper }>
        <div className={ styles.mainCard }>
            <div
                className={ cx( styles.imageWrapper, { [styles.isActive]: view.canRead('image') } ) }
                onClick={ imageOnClickHandler }
            >
                { image && <div
                  className={ styles.image }
                  style={ { backgroundImage: `url(${ image })` } }/>
                }
                {
                    !image && <div className={ styles.iconWrapper }><Icon name={ 'account-home-door' }/></div>
                }
            </div>
            <div className={ styles.infoWrapepr }>
                <div className={ styles.info }>
                    <MainInfoTopWidget { ...view.mainMataInfoProps } />
                </div>
                <div className={ styles.reports }>
                    <ReportListMini items={ reports.reportList.filter(el => el.alias !== 'arrow') }/>
                </div>
            </div>
        </div>
        <ObjectsMap showEntity={ false } className={ styles.mapWrapper }
                    style={ { height: `calc(${ height }px - 220px)` } }
                    mapPositionKey={ `object-${ view.currentObject.object_id }` }
                    entities={ [ view.currentObject ] }
                    onMapClick={ view.changeObjectPosition }
        />
    </div>

})

export { MainCard }
