import React from "react"
import Loadable, { LoadableComponent } from '@loadable/component'
import { Preloader } from "components/UI/Preloader"

const AsyncReportCard: LoadableComponent<any> = Loadable(() => import(/* client-side */'./ReportCard'), {
    fallback: <Preloader /> } )

export { AsyncReportCard }
