import React from "react"
import { useInstance } from "react-ioc"
import { ObjectViewModel } from "screens/ObjectScreen/ObjectViewModel"
import styles from "./styles.scss"
import { observer } from "mobx-react-lite"
import { REPORTS } from "./ReportCard"
import GunttHeader from "./Reports/Guntt/GunntHeader"
import FactValuesByDayHeader from "./Reports/FactValuesByDay/FactValuesByDayHeader"
import PhotoFactsHeader from "./Reports/PhotoFacts/PhotoFactsHeader"

const ReportCardHeader: React.FC = observer(() => {
    const object = useInstance(ObjectViewModel)

    const renderContent = () => {
        switch (object.subPath[0]) {
            case REPORTS.GUNTT:
                return <div className={ `${ styles.tableHeader } ${ styles.reports_header }` }><GunttHeader/></div>
            case REPORTS.VALUES_BY_DAY:
                return <div className={ `${ styles.tableHeader } ${ styles.reports_header }` }><FactValuesByDayHeader/></div>
            case REPORTS.PHOTO_FACTS:
                return <div className={ `${ styles.tableHeader } ${ styles.reports_header }` }><PhotoFactsHeader/></div>
            default:
                return null;
        }
    }
    return renderContent()
})

export default ReportCardHeader