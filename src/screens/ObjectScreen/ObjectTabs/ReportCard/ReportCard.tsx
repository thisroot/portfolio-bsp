import { observer } from "mobx-react-lite"
import React from "react"
import { useInstance } from "react-ioc"
import styles from "./styles.scss"
import { Spinner } from "components/UI/Spinner"
import { useAsync } from "react-use"
import { ObjectViewModel } from "../../ObjectViewModel"
import { ReportCardViewModel } from "./ReportCardViewModel"
import Guntt from "./Reports/Guntt/Gunnt"
import FactValuesByDay from "./Reports/FactValuesByDay/FactValuesByDay"
import { PhotoFacts } from "./Reports/PhotoFacts"
import { ReportList } from 'components/ReportList'
import { ArrowReport } from "./Reports/ArrowReport"

export enum REPORTS {
    GUNTT = 'guntt',
    VALUES_BY_DAY = 'values_by_day',
    PHOTO_FACTS = 'photo_facts',
    ARROW = 'arrow'
}

const ReportCard: React.FC = observer(() => {
    const object = useInstance(ObjectViewModel)
    const view = useInstance(ReportCardViewModel)



    useAsync(async () => {
    })

    const renderContent = () => {
        switch (object.subPath[0]) {
            case REPORTS.GUNTT:
                return <Guntt/>
            case REPORTS.VALUES_BY_DAY:
                return <FactValuesByDay/>
            case REPORTS.PHOTO_FACTS:
                return <PhotoFacts/>
            case REPORTS.ARROW:
                return <ArrowReport />
            default:
                return <ReportList items={ view.reportList }/>
        }
    }

    return (
      <div className={ styles.reportCard }>
          {
              !view.pending && renderContent()
          }
          {
              view.pending && <Spinner/>
          }
      </div>)
})

export default ReportCard
