import { t } from '@lingui/macro';
import { action, computed, observable, runInAction } from 'mobx';
import React from 'react';
import { inject } from 'react-ioc';
import { ObjectViewModel } from 'screens/ObjectScreen/ObjectViewModel';
import { findKey, get, isEmpty } from 'lodash';
import { ObjectModel, SchedulePlanFact } from 'models/mst';
import { ApiService, DataContext, RouterService } from 'services';
import { loc } from 'utils/i18n';
import { IDataModes } from './interfaces';
import { getRuler } from 'components/GGChart/helpers/calculateTimeGrid';
import { getProjectDates } from 'components/GGChart/helpers/getProjectDates';
import { calculateCurrentDateLine } from 'components/GGChart/helpers/calculateCurrentDateLine';
import { AppService } from 'services/AppService';
import { ReferenceIdentifier } from 'mobx-state-tree';
import { SimpleConfirmAbortModal } from 'components/UI/Modal';
import { REPORT_FACT_TYPE } from 'constants/locale';
import { ReportCardViewModel } from 'screens/ObjectScreen/ObjectTabs/ReportCard/ReportCardViewModel';
import { toggleExpandRecursive, IField } from '../../../../../../utils/helpers';
import { SaveAsPdfModal } from './SaveAsPdfModal';
import { GunttFilterViewModel } from 'components/Filters/GunttFilter';
import styles from './styles.scss';

class GunttViewModel {
  readonly nodeKey = 'schedule_id';

  @inject dataContext: DataContext;
  @inject api: ApiService;
  @inject object: ObjectViewModel;
  @inject gunttFilter: GunttFilterViewModel;
  @inject app: AppService;
  @inject reportCard: ReportCardViewModel;
  @inject router: RouterService;

  @observable pending = false;
  @observable wasInitialized = false;
  @observable dataMode: IDataModes = IDataModes.PlanFact;
  @observable isFinanceShown: boolean = false;
  @observable dateMask = loc._(t`basic.dateFormat.DD.MM.YY`);
  @observable isVirtualModelShown: boolean = false;
  @observable countNodes = 0;
  @observable countExpanded = 0;
  @observable isLegendShown: boolean = false;
  @observable requisitesVisibility: boolean = true;
  @observable currentScheduleVersion: number | null = null;

  @observable roundValues = {
    round_percent: false,
    round_day: false,
    round_expenditure: false,
    round_value: false,
    round_percent_expenditure: false
  };

  serverDateMask = 'YYYY-MM-DD';
  backUpSnapshot = {};

  @observable selectedNode: SchedulePlanFact = null;

  @computed get projectDates() {
    return this.schedule && getProjectDates(this.schedule);
  }

  @computed get currentDateLineStyle() {
    return this.schedule && calculateCurrentDateLine(this.projectDates);
  }

  @computed get ruler() {
    return getRuler(this.schedule, this.gunttFilter.filterFields 
      ? this.gunttFilter.filterFields.get(this.gunttFilter.fields.grid_step.name).value 
      : this.gunttFilter.fields.grid_step.value);
  }

  @computed
  public get userCanReadExpenditure(): boolean {
    return this.object.currentObject.canRead('schedule.expenditure');
  }

  @action
  public init = async (currentScheduleVersion?: number) => {
    const object_id = Number(this.object.objectIdByRoute);
    if (!currentScheduleVersion) {
      this.pending = true;
    }
    try {
      const reqBody = { 
        object_id,
        fact_type: this.gunttFilter.filterFields 
          ? this.gunttFilter.filterFields.get(this.gunttFilter.fields.fact_type.name).value 
          : this.gunttFilter.fields.fact_type.value,
        date_to: this.gunttFilter.filterFields 
          ? this.gunttFilter.filterFields.get(this.gunttFilter.fields.date_to.name).value 
          : this.gunttFilter.fields.date_to.value,
      };

      if (currentScheduleVersion) {
        Object.assign(reqBody, { schedule_version_id: currentScheduleVersion });
      }

      const fact_type = await this.dataContext.cacheFirst(this.api.reference.fact_type);
      if (fact_type.status !== 'error' && fact_type.status !== 'cached') {
        this.dataContext.applySnapshot(fact_type.body);
      }

      const result = await this.dataContext.cacheFirst(this.api.report.planFactReport, reqBody, {
        noCaching: true,
      });

      if (result.status !== 'error' && result.status !== 'cached' && !isEmpty(result.body)) {
        this.dataContext.schedule_plan_fact.delete('0');
        this.dataContext.applySnapshot({
          schedule_plan_fact: result.body.schedule_report_guntt,
        });

        runInAction(() => {
          this.object.currentObject.schedule_plan_fact = String(
            findKey(result.body.schedule_report_guntt, item => item.parent_id === null),
          );
          this.gunttFilter.fields.grid_step.value = get(this.gunttFilter.placeholderData, 'step');
          this.wasInitialized = true;

          const search = new URLSearchParams(this.router.location.search);
          if (search.get('view') === 'showFinance') {
            this.isFinanceShown = true;
          }
          const isNeedExpand = search.get('expandAll') === 'true';
          this.countNodes = toggleExpandRecursive(
            this.schedule,
            'expand',
            'children',
            isNeedExpand,
          );
          this.countExpanded = isNeedExpand ? this.countNodes : 0;
          this.roundValues = this.currentObject.project_id.settings;
        });
      }
    } catch (e) {
      console.log(e);
      this.dataContext.schedule_plan_fact.delete(`${object_id}`);
    } finally {
      this.pending = false;
      await this.gunttFilter.initFilterForm()
    }
  };

  @action onSubmit = async (e) => {
    await this.gunttFilter.togglePopover();
    await this.gunttFilter.submitForm(e);
    await this.init(this.currentScheduleVersion);
  };

  @action onReset = async (e) => {
    await this.gunttFilter.togglePopover();
    await this.gunttFilter.resetForm(e)
    await this.init(this.currentScheduleVersion);
  }

  @computed get isAllExpanded() {
    return this.countExpanded === this.countNodes;
  }

  @action toggleExpandAll = () => {
    toggleExpandRecursive(this.schedule, 'expand', 'children', !this.isAllExpanded);
    this.countExpanded = this.isAllExpanded ? 0 : this.countNodes;
  };

  @action public toggleIsFinanceShown = (state: boolean) => {
    this.isFinanceShown = state;
    this.isVirtualModelShown = false;
  };

  @action public setVirtualModelShown = () => {
    // if(e.ctrlKey) {
    //     window.location.href = 'https://autode.sk/38yYLyX'
    // } else {
    //     window.location.href = 'https://autode.sk/2TK5QHy'
    // }
    this.isVirtualModelShown = true;
    this.isFinanceShown = false;
  };

  @action public selectNode = node => {
    this.selectedNode = get(this.selectedNode, this.nodeKey) === node[this.nodeKey] ? null : node;
  };

  @action
  public setPlan = (scheduleId: ReferenceIdentifier, plan: number) => {
    this.dataContext.schedule_plan_fact.get(`${scheduleId}`).volume_planned = plan;
  };
  @action
  public setFact = (scheduleId: ReferenceIdentifier, fact: number) => {
    this.dataContext.schedule_plan_fact.get(`${scheduleId}`).volume_fact = fact;
  };
  @action
  public setTitle = (scheduleId: ReferenceIdentifier, title: string) => {
    this.dataContext.schedule_plan_fact.get(`${scheduleId}`).title = title;
  };
  @action
  public setDateStart = (scheduleId: ReferenceIdentifier, date: string) => {
    this.dataContext.schedule_plan_fact.get(`${scheduleId}`).date_start = date;
  };
  @action
  public setDateEnd = (scheduleId: ReferenceIdentifier, date: string) => {
    this.dataContext.schedule_plan_fact.get(`${scheduleId}`).date_finish = date;
  };

  @action addNode = () => {
    const currentTime = new Date().getTime();
    let newNodeId = `${this.objectModelId}-${currentTime}`;
    let selectedNode =
      this.selectedNode ||
      (this.dataContext.schedule_plan_fact.get(`${this.objectModelId}-${0}`) as SchedulePlanFact);
    let selectedNodeId = selectedNode.schedule_id;
    if (selectedNode.children.length !== 0) {
      selectedNode.expand = true;
      this.dataContext.schedule_plan_fact.put({
        schedule_id: newNodeId,
        object_id: this.objectModelId,
        title: loc._(t`component.reportCard.newElement`),
        parent_id: selectedNodeId,
      });

      const par = this.dataContext.schedule_plan_fact.get(selectedNodeId);
      par.children.push(newNodeId);
    } else {
      let parent = this.selectedNode.parent_id;
      parent.children = parent.children.filter(item => item[this.nodeKey] !== selectedNodeId);
      this.dataContext.schedule_plan_fact.put({
        schedule_id: newNodeId,
        object_id: this.objectModelId,
        title: loc._(t`component.reportCard.newElement`),
        parent_id: parent[this.nodeKey],
        children: [selectedNodeId],
        expand: true,
      });
      selectedNode.parent_id = newNodeId;
      parent.children.push(newNodeId);
      this.selectedNode = this.dataContext.schedule_plan_fact.get(newNodeId);
    }
  };

  @action checkAddNode = e => {
    e.stopPropagation();
    let parent =
      this.selectedNode ||
      (this.dataContext.schedule_plan_fact.get(`${this.objectModelId}-${0}`) as SchedulePlanFact);
    if (parent.children.length === 0) {
      this.app.modalControls.renderModal = () => (
        <SimpleConfirmAbortModal
          title={loc._(t`components.reportCard.modal.nonGroupedcomponent`)}
          body={loc._(t`components.reportCard.modal.makeComponentAsGrouped`)}
          onConfirm={() => {
            this.addNode();
            this.app.modalControls.close();
          }}
          onAbort={this.app.modalControls.close}
        />
      );
      this.app.modalControls.open();
    } else {
      this.addNode();
    }
  };

  @action
  public toggleDataMode = () => {
    this.dataMode =
      this.dataMode === IDataModes.PlanFact ? IDataModes.Deviation : IDataModes.PlanFact;
  };

  @action
  public toggleRequisitesVisibility = () => {
    this.requisitesVisibility = !this.requisitesVisibility;
  };

  @computed
  public get widgetData(): IField[] {
    return get(this.object.guntMetaInfoProps, 'slots');
  }

  @computed
  public get widgetWithValues(): boolean {
    return this.widgetData.some(({ fieldValue }) => fieldValue !== undefined);
  }

  @computed
  public get requisites(): JSX.Element[] {
    return this.widgetData.map(({ fieldLabel, fieldValue }, index) => (
      <li key={`${fieldValue}_${index}`} className={styles.widget__item}>
        <span className={styles.widget__label}>{fieldLabel + ": "}</span>
        <span className={styles.widget__value}>{fieldValue}</span>
      </li>
    ));
  }

  @computed
  public get objectModelId(): number {
    return get(this.object.currentObject, 'object_id');
  }

  @computed
  public get schedule() {
    return (
      this.wasInitialized &&
      this.dataContext.schedule_plan_fact.get(
        get(this.object.currentObject, 'schedule_plan_fact.schedule_id'),
      )
    );
  }

  @action toggleExpand = (node: SchedulePlanFact) => {
    this.countExpanded = node.expand ? this.countExpanded - 1 : this.countExpanded + 1;
    node.expand = !node.expand;
  };

  @computed get currentObject(): ObjectModel {
    return this.dataContext.object.get(this.object.objectIdByRoute);
  }

  @observable isVersionFormOpen = false;
  @action toggleVersionForm = () => (this.isVersionFormOpen = !this.isVersionFormOpen);
  @action setCurrentVersion = async (currentVersion: number) => {
    this.currentScheduleVersion = currentVersion;
    if (this.currentObject.currentScheduleReportVersion !== currentVersion) {
      const result = await this.api.schedule.setCurrentVersion({
        schedule_version_id: currentVersion,
      });
      if (result.status !== 'error') {
        this.init(currentVersion);
      }
    }
  };

  @action toggleLegendPopover = () => (this.isLegendShown = !this.isLegendShown);

  @observable isDownloadPopoverShown = false;
  @action toggleDownloadPopover = () =>
    (this.isDownloadPopoverShown = !this.isDownloadPopoverShown);

  @action openDownloadModal = () => {
    const renderOptions = [
      {
        name: loc._(t`components.gunnt.options.isDecimalView`),
        value: this.gunttFilter.decimalOptions.filter(o => {
          const decimal = this.gunttFilter.filterFields 
            ? this.gunttFilter.filterFields.get(this.gunttFilter.fields.decimal_view.name).value 
            : this.gunttFilter.fields.decimal_view.default;
          return o.value === decimal
        })[0].label,
      },
      {
        name: loc._(t`components.gunnt.options.fact_type`),
        value: loc._(REPORT_FACT_TYPE[this.gunttFilter.filterFields ? this.gunttFilter.filterFields.get(this.gunttFilter.fields.fact_type.name).value : this.gunttFilter.fields.fact_type.default]),
      },
      {
        name: loc._(t`components.gunnt.options.view`),
        value: this.isFinanceShown
          ? loc._(t`components.reports.guntt.tabs.byFinance`)
          : loc._(t`components.reports.guntt.tabs.byValue`),
      },
    ];

    this.app.modalControls.renderModal = () => <SaveAsPdfModal renderOptions={renderOptions} />;
    this.app.modalControls.open();
  };
}

export { GunttViewModel };
