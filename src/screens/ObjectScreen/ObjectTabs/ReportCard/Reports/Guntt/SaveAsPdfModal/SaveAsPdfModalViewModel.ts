import { inject } from "react-ioc"
import { action, computed, observable } from "mobx"
import jsPDF from "jspdf"
import html2canvas from "html2canvas"
import { get } from "lodash"
import { abbreviateNumber, ENTITY_FIELDS, getMetaInfoFields, parsePath } from "../../../../../../../utils/helpers"
import { LocalisationService } from "services/LocalisationService"
import { ROUTES } from "../../../../../../../constants"
import { ObjectModel } from "../../../../../../../models/mst/ObjectModel"
import { RouterService } from "../../../../../../../services/RouterService"
import { AppService, DataContext } from "../../../../../../../services"
import { FIELDS } from "../../../../../../../constants/forms"
import MobxReactForm from 'mobx-react-form'
import vjf from "mobx-react-form/lib/validators/VJF"
import dayjs from "dayjs"
import { t } from "@lingui/macro"
import { loc } from "../../../../../../../utils/i18n"

const font = require('static/fonts/TTInterfaces/regular/tt-interphases-regular.ttf')

const plugins = {
    vjf: vjf()
}

class SaveAsPdfModalViewModel {

    @inject app: AppService
    @inject lang: LocalisationService
    @inject router: RouterService
    @inject dataContext: DataContext
    @observable pending = true

    @computed
    public get objectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId')
    }

    @computed get currentObject(): ObjectModel {
        return this.dataContext.object.get(this.objectIdByRoute)
    }

    @computed get passport() {
        const { slots } = getMetaInfoFields(this.currentObject, 'object', this.lang.lang, ENTITY_FIELDS.ALL)
        if (slots && slots.length > 0) {
            return slots.map(item => ({
                key: item.fieldKey,
                value: item.fieldLabel,
                label: item.fieldValue,
                type: item.fieldType
            }))
        }
        return slots
    }

    @computed get fields() {
        return {
            passport_fields: {
                ...FIELDS.passport_fields,
                extra: this.passport
            }
        }
    }

    @observable disableReportPrepare = false

    @observable formInitialized = false
    @observable passportForm: any
    @observable renderOptions: Array<{ name: string, value: string }>
    @action initReportForm = async (options: Array<{ name: string, value: string }>) => {
        if (!this.formInitialized) {
            this.renderOptions = options
            this.renderOptions.push({
                name: t`components.gunnt.options.dateReport`,
                value: dayjs().format('YYYY-MM-DD')
            })
            this.passportForm = new MobxReactForm({ fields: this.fields }, {
                plugins
            })
            this.formInitialized = true
            this.pending = false
        }
    }


    @computed get passportFields() {
        return this.passportForm ? this.passportForm.fields : null
    }

    @computed get formValues() {
        return this.passportForm ? this.passportForm.get() : {}
    }

    @action onSubmit = async (e) => {
        this.passportForm.onSubmit(e)
        await this.printPDF()
    }

    @observable reportPending = false
    @action printPDF = async () => {

        this.reportPending = true
        this.disableReportPrepare = false

        const report = document.getElementById('root')
        const reportTitle = document.getElementById('sticky-screen-header')
        const tableHeader = document.getElementById('gunnt-header')
        const tableRows: NodeListOf<HTMLElement> = report.querySelectorAll('.gunnt-node')

        const top_left_margin = 10
        const sizeIMG = {
            title: { canvas_image_height: reportTitle.clientHeight },
            header: { canvas_image_height: tableHeader.clientHeight },
            top_left_margin: 0,
            widthDOM: reportTitle.clientWidth,
            PDF_Width: reportTitle.clientWidth + top_left_margin * 2,
            PDF_Height:
              (reportTitle.clientWidth + top_left_margin * 2) * 0.6 + top_left_margin * 2,
            canvas_image_width: reportTitle.clientWidth
        }
        const pdf = new jsPDF("l", "pt", [ sizeIMG.PDF_Width, sizeIMG.PDF_Height ])
        const cropOptions = { scale: 2 }

        let titleImage = ""
        await html2canvas(reportTitle, cropOptions).then((canvas) => {
            titleImage = canvas.toDataURL("image/jpeg", 1.0)
            pdf.addImage(
              titleImage,
              "JPG",
              top_left_margin,
              top_left_margin,
              sizeIMG.canvas_image_width,
              sizeIMG.title.canvas_image_height
            )
        })


        pdf.addFont(font, "TTInterfaces", "normal")
        pdf.setFont('TTInterfaces')

        if (get(this.currentObject, 'volume_percent')) {
            pdf.setFontSize(24)
            pdf.text(`${ abbreviateNumber(Math.round(+this.currentObject.volume_percent)) }%`, sizeIMG.canvas_image_width - 200, top_left_margin + sizeIMG.title.canvas_image_height / 2 + 5)
        }
        pdf.setFontSize(14)
        pdf.setTextColor('747B8B')

        const marginText = 25

        let heightElements =
          sizeIMG.title.canvas_image_height + top_left_margin

        if (this.formValues['passport_fields'].value && this.formValues['passport_fields'].value.length > 0) {
            heightElements += marginText
            this.formValues['passport_fields'].extra.forEach(val => {
                if (this.formValues['passport_fields'].value.includes(val.key)) {
                    pdf.text(`${ val.value }: ${ val.label || "---" }`, 50, heightElements)
                    heightElements += 18
                }
            })
        }


        let headerImage = ""
        await html2canvas(tableHeader, cropOptions).then((canvas) => {

            headerImage = canvas.toDataURL("image/jpeg", 1.0)

            pdf.addImage(
              headerImage,
              "JPG",
              top_left_margin,
              heightElements,
              sizeIMG.canvas_image_width,
              sizeIMG.header.canvas_image_height
            )
            heightElements += sizeIMG.header.canvas_image_height
        })

        const filterHeight = 40
        pdf.text(this.renderOptions.map(o => `${ loc._(o.name) }: ${ o.value }`).join('; '), top_left_margin, sizeIMG.PDF_Height - top_left_margin)

        for (let index = 0; index < [ ...tableRows ].length; index++) {
            const element = [ ...tableRows ][index]
            await html2canvas(element, cropOptions).then((canvas) => {
                canvas.getContext("2d")
                const img = canvas.toDataURL("image/jpeg", 1.0)
                pdf.addImage(
                  img,
                  "JPG",
                  top_left_margin,
                  heightElements,
                  sizeIMG.canvas_image_width,
                  element.clientHeight
                )
                heightElements += element.clientHeight
                if (
                  heightElements + element.clientHeight + filterHeight >= sizeIMG.PDF_Height &&
                  index !== [ ...tableRows ].length - 1
                ) {
                    heightElements =
                      sizeIMG.title.canvas_image_height +
                      sizeIMG.header.canvas_image_height
                    pdf.addPage([ sizeIMG.PDF_Width, sizeIMG.PDF_Height ], "l")

                    pdf.addImage(
                      titleImage,
                      "JPG",
                      top_left_margin,
                      top_left_margin,
                      sizeIMG.canvas_image_width,
                      sizeIMG.title.canvas_image_height
                    )

                    if (get(this.currentObject, 'volume_percent')) {
                        pdf.setDrawColor(0)
                        pdf.setFillColor(245, 246, 249)
                        pdf.rect(
                          sizeIMG.canvas_image_width - 100,
                          top_left_margin,
                          sizeIMG.canvas_image_width,
                          sizeIMG.title.canvas_image_height,
                          "F")
                        pdf.setFontSize(40)
                        pdf.text(`${ Math.round(+this.currentObject.volume_percent) }%`, sizeIMG.canvas_image_width - 80, top_left_margin + sizeIMG.title.canvas_image_height / 2 + 10)
                    }

                    pdf.addImage(
                      headerImage,
                      "JPG",
                      top_left_margin,
                      sizeIMG.title.canvas_image_height + top_left_margin,
                      sizeIMG.canvas_image_width,
                      sizeIMG.header.canvas_image_height
                    )
                    pdf.setFontSize(14)
                    pdf.text(this.renderOptions.map(o => `${ loc._(o.name) }: ${ o.value }`).join('; '), top_left_margin, sizeIMG.PDF_Height - top_left_margin)
                }
            })
        }
        if (!this.disableReportPrepare) {
            await pdf.save(`report.pdf`)
        }
        this.app.modalControls.close()
        this.disableReportPrepare = false
        this.reportPending = false
    }
}

export { SaveAsPdfModalViewModel }
