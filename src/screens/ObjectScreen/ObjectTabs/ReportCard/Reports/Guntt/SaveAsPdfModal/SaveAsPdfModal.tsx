import { SimpleConfirmAbortModal } from "components/UI/Modal/SimpleConfirmAbortModal"
import { loc } from "utils/i18n"
import { t, Trans } from "@lingui/macro"
import React from "react"
import { provider, useInstance } from "react-ioc"
import { AppService } from "services/AppService"
import { SaveAsPdfModalViewModel } from "./SaveAsPdfModalViewModel"
import { observer } from "mobx-react-lite"
import { MultiSelectCheckbox } from "../../../../../../../components/UI/MultiSelectCheckbox"
import { useEffectOnce } from "react-use"
import styles from './styles.scss'
import { Preloader } from "../../../../../../../components/UI/Preloader"

export interface IModalP {
    renderOptions: Array<{ name: string, value: string }>
}

const Modal: React.FC<IModalP> = observer((props) => {

    const app = useInstance(AppService)
    const view = useInstance(SaveAsPdfModalViewModel)

    let passportFields

    useEffectOnce(() => {
        view.initReportForm(props.renderOptions)
    })

    if (!view.pending) {
        passportFields = view.passportFields.get(view.fields.passport_fields.name)
    }


    return <SimpleConfirmAbortModal
      confirmIsDisabled={ view.reportPending }
      title={ loc._(t`components.reportCard.modal.downloadAsPdf`) }
      onConfirm={ (e) => {
          view.onSubmit(e)
      } }
      onAbort={ (e) => {
          view.disableReportPrepare = true
          view.passportForm.onReset(e)
          app.modalControls.close()
      } }
    >
        <div className={ styles.formContainer }>
            {
                (view.pending || view.reportPending) &&
                <Preloader text={ view.reportPending && loc._(t`components.gunnt.reportPending`) }/>
            }
            {
                !view.pending && !view.reportPending && passportFields.extra.length > 0 && (
                  <MultiSelectCheckbox
                    className={styles.multiSelect}
                    items={ passportFields.extra }
                    value={ passportFields.value }
                    onChange={ (val) => {
                        passportFields.set(val)
                    } }
                  />
                )
            }
            {
                !view.pending && !view.reportPending && !passportFields.extra.length && (
                  <div><Trans>basic.noOptions</Trans></div>
                )
            }
        </div>
    </SimpleConfirmAbortModal>


})

const SaveAsPdfModal = provider()(Modal)
SaveAsPdfModal.register(SaveAsPdfModalViewModel)

export {
    SaveAsPdfModal
}
