import { observer } from 'mobx-react'
import React from 'react'
import { inject } from 'react-ioc'
import { GunttViewModel } from './GunntViewModel'
import { ObjectViewModel } from 'screens/ObjectScreen/ObjectViewModel'
import { GunttFilterViewModel, GunttInlineFilter } from 'components/Filters/GunttFilter'
import { get } from 'lodash'
import styles from './styles.scss'
import { Trans } from '@lingui/macro'
import { Icon } from 'components/UI/Icon'
import cx from 'classnames'
import { Spinner } from 'components/UI/Spinner'
import { Link } from 'services/RouterService'
import { DefaultButton } from '../../../../../../components/UI/Button'
import { Popover } from 'components/UI/Popover'
import { GunntLegend } from 'components/Legends/GunntLegend'

@observer
class GunttHeader extends React.Component {
    @inject view: GunttViewModel
    @inject object: ObjectViewModel
    @inject filterView: GunttFilterViewModel

    public render() {
        const currentSchedule = get(this.object.currentObject, 'schedule_plan_fact')
        const renderPreloader = !this.view.wasInitialized
        const isRenderSchedule = currentSchedule && currentSchedule.children.length && !renderPreloader

        return isRenderSchedule ? (
          <div className={ styles.headersContainer }>
              <div className={ styles.table_header }>
                  <div className={ styles.leftPart }>
                      <Link to={ this.view.reportCard.currentReportPath } className={ styles.backIcon }>
                          <Icon name="arrows-arrow-r"/>
                      </Link>
                      <div
                        className={ cx(styles.col, styles.maxHeight, styles.tabs, {
                            [styles.active]: !this.view.isVirtualModelShown && !this.view.isFinanceShown,
                        }) }
                        onClick={
                            this.view.isFinanceShown || this.view.isVirtualModelShown
                              ? () => this.view.toggleIsFinanceShown(false)
                              : null
                        }
                      >
                          <Trans>components.reports.guntt.tabs.byValue</Trans>
                      </div>
                      { this.view.userCanReadExpenditure && (
                        <div
                          className={ cx(styles.col, styles.maxHeight, styles.tabs, {
                              [styles.active]: !this.view.isVirtualModelShown && this.view.isFinanceShown,
                          }) }
                          onClick={
                              !this.view.isFinanceShown ? () => this.view.toggleIsFinanceShown(true) : null
                          }
                        >
                            <Trans>components.reports.guntt.tabs.byFinance</Trans>
                        </div>
                      ) }
                  </div>
                  { !this.view.isVirtualModelShown && (
                    <div className={ styles.rightPart }>
                        <div className={ styles.col }>
                            <DefaultButton
                              outline
                              withoutBorders
                              disabled={ false }
                              onClick={ this.view.toggleRequisitesVisibility }
                              className={ styles.addNode }
                              icon={ this.view.requisitesVisibility ? 'lists-remove' : 'lists-add-b' }
                              text={ this.view.requisitesVisibility ? (
                                <Trans>components.reports.guntt.tabs.hideRequisites</Trans>
                              ) : (
                                <Trans>components.reports.guntt.tabs.showRequisites</Trans>
                              ) }
                            />
                        </div>
                        <div className={ styles.col }>
                            <Popover
                              onClickOutside={ this.view.toggleLegendPopover }
                              isOpen={ this.view.isLegendShown }
                              align={ 'end' }
                              position={ 'bottom' }
                              content={ <GunntLegend/> }
                              isArrow={ false }
                            >
                                <DefaultButton
                                  outline
                                  withoutBorders
                                  disabled={ false }
                                  onClick={ this.view.toggleLegendPopover }
                                  className={ styles.addNode }
                                  icon={ 'business-chart-research-t' }
                                  text={ <Trans>basic.legend</Trans> }
                                />
                            </Popover>
                        </div>
                        <div className={ styles.col }>
                            <DefaultButton
                              outline
                              withoutBorders
                              disabled={ false }
                              onClick={ this.view.toggleVersionForm }
                              className={ styles.addNode }
                              icon={ 'shapes-basic-shapes-circle-b' }
                              text={ <Trans>basic.versions</Trans> }
                            />
                        </div>
                        <div className={ styles.col }>
                            <DefaultButton
                              outline
                              withoutBorders
                              disabled={ false }
                              onClick={ this.view.toggleExpandAll }
                              className={ cx(styles.addNode) }
                              icon={ 'arrows-circle-code' }
                              iconStyle={ styles.collapseIcon }
                              text={
                                  this.view.isAllExpanded ? (
                                    <Trans>basic.collapseAll</Trans>
                                  ) : (
                                    <Trans>basic.expandAll</Trans>
                                  )
                              }
                            />
                        </div>
                        <div className={ styles.col }>
                            <Popover
                              onClickOutside={ this.view.toggleDownloadPopover }
                              isOpen={ this.view.isDownloadPopoverShown }
                              align={ 'end' }
                              position={ 'bottom' }
                              content={
                                  <div className={ styles.downloadContainer }>
                                      <div className={ styles.downloadItem } onClick={ this.view.openDownloadModal }>
                                          <Trans>basic.download</Trans> <Trans>basic.in</Trans> PDF
                                      </div>
                                  </div>
                              }
                              isArrow={ false }
                            >
                                <DefaultButton
                                  outline
                                  withoutBorders
                                  disabled={ false }
                                  onClick={ this.view.toggleDownloadPopover }
                                  className={ styles.addNode }
                                  icon={ 'files-folder-down' }
                                  text={ <Trans>basic.download</Trans> }
                                />
                            </Popover>
                        </div>
                    </div>
                  ) }
              </div>
              <div className={ styles.table_header }>
                  { this.filterView.formInitialized &&
                    <GunttInlineFilter onSubmit={ this.view.onSubmit } onReset={ this.view.onReset }/> }
              </div>
          </div>
        ) : (
          <div className={ styles.table_header }>
              <div className={ styles.leftPart }>
                  <Link to={ this.view.reportCard.currentReportPath } className={ styles.backIcon }>
                      <Icon name="arrows-arrow-r"/>
                  </Link>
                  <div className={ cx(styles.col, styles.maxHeight, styles.noData) }>
                      { renderPreloader ? <Spinner/> : <Trans>basic.noDataAvailable</Trans> }
                  </div>
              </div>
          </div>
        )
    }
}

export default GunttHeader
