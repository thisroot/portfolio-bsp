import { CSSProperties } from "react"
import { PlanFactReportResp } from 'services/ApiService/Interfaces/schedulePlanFact'
import { ProjectSettings } from "../../../../../../models/mst/Project"

export interface IProjectDates {
    projectStartedAt: number;
    projectFinishedAt: number;
    projectDuration: number;
}

export enum IDataModes {
    PlanFact,
    Deviation
}

interface IStyles {
    beforeStart: CSSProperties;
    duration: CSSProperties;
    completed: CSSProperties;
    color: CSSProperties;
    colorLine: CSSProperties;
    label: string;
    labelFinance: string;
    completedFinance: CSSProperties;
    rulerWidth: CSSProperties;
    finishRule: string;
}

export type IGetStyles =
    (object: PlanFactReportResp, objectDates: IProjectDates, ruler: string[], roundValues: ProjectSettings) => IStyles;
