import { observer } from "mobx-react-lite"
import React, { useRef } from "react"
import { useInstance } from "react-ioc"
import { GunttViewModel } from "./GunntViewModel"
import { GunttFilterViewModel } from "components/Filters/GunttFilter"
import styles from "./styles.scss"
import { Trans } from "@lingui/macro"
import { Spinner } from "components/UI/Spinner"
import { GGChart } from "components/GGChart"
import { useAsync, useClickAway } from "react-use"
import cx from 'classnames'
import { isClickOnButton } from "utils/helpers"
import { ScheduleVersionEditForm } from "components/ScheduleVersionEditForm"
import { Accordion } from "components/UI/Accordion"
import { getStyles } from "../../../../../../components/GGChart/helpers/getStyles"
import { LocalisationService } from "../../../../../../services"
// import { AsyncDAEModel } from "./AsyncDAEModel"

const Guntt: React.FC = observer(() => {
    const view = useInstance(GunttViewModel)
    const locale = useInstance(LocalisationService)
    const viewGunttFilter = useInstance(GunttFilterViewModel)
    const tableRef = useRef()

    useAsync(async () => {
        await view.init()
    })

    useClickAway(tableRef, (e) => {
        if (!isClickOnButton(e)) {
            view.selectedNode = null
        }
    })

    const cs = !view.pending && view.schedule && view.schedule.children.length !== 0 && getStyles(view.schedule, view.projectDates, view.ruler, view.roundValues)

    return (
      <div className={ styles.reportCard }>
          { !view.pending && view.schedule && view.schedule.children.length !== 0 && (
            <div className={ styles.reports_wrapper }>
                <div className={ cx(styles.versionContainer, { [styles.open]: view.isVersionFormOpen }) }>
                    <ScheduleVersionEditForm
                      currentObject={ view.object.objectIdByRoute }
                      versionOnChange={ view.init }
                      disabled={ true }
                    />
                </div>
                { view.widgetWithValues && <Accordion
                  isOpen={ view.requisitesVisibility }
                >
                    <ul className={ styles.widgets }>
                        { view.requisites }
                    </ul>
                </Accordion> }
                <div className={ styles.table_diagram_gannt }>
                    <div className={ styles.table_content }>
                        <div id={ 'gunnt-header' } className={ styles.table_header }>
                            <div className={ styles.table_header_name }>
                                <div
                                  className={ cx(styles.table_header_name_title, { [styles.finMode]: view.isFinanceShown }) }>
                                    <div className={ styles.levels }>&nbsp;</div>
                                    <Trans>component.SchedulePlanFact.tableHeader.constructive</Trans>
                                    <div className={ styles.filter_block }>
                                    </div>
                                </div>
                                <div
                                  className={ cx(styles.table_header_name_beginning_ending, { [styles.finMode]: view.isFinanceShown }) }>
                                    <div><Trans>basic.dateStart</Trans></div>
                                    <div><Trans>basic.dateEnd</Trans></div>
                                </div>
                                <div
                                  className={ cx(styles.table_header_name_plan_fact, { [styles.finMode]: view.isFinanceShown }) }>
                                    <div><Trans>basic.plan</Trans></div>
                                    <div><Trans>basic.fact</Trans></div>
                                </div>
                            </div>
                            <div className={ styles.headerDates }>
                                <div className={ styles.element_ruler } style={ cs.rulerWidth }>
                                    { view.ruler && view.ruler.map(item => (
                                      <span className={ styles.element_step } key={ item }>
                                <span className={ styles.element_name }>{ item }</span>
                                </span>
                                    ))
                                    }
                                </div>
                                { cs.finishRule && (
                                  <div className={ styles.finishRule }>
                                      <span className={ styles.element_step } key={ cs.finishRule }>
                                        <span className={ styles.element_name }>{ cs.finishRule }</span>
                                      </span>
                                  </div>
                                ) }
                            </div>
                        </div>
                        <div id={ 'gunnt-body' } ref={ tableRef }>
                            <GGChart
                              currentLocale={ locale.lang }
                              isFinanceShown={ view.isFinanceShown }
                              nodeKey={ view.nodeKey }
                              projectDates={ view.projectDates }
                              level={ 0 }
                              construct={ view.schedule }
                              dataMode={ view.dataMode }
                              ruler={ view.ruler }
                              currentDateLineStyle={ view.currentDateLineStyle }
                              expand={ view.schedule.expand }
                              toggleExpand={ view.toggleExpand }
                              viewMode={ viewGunttFilter.filterFields ? viewGunttFilter.filterFields.get(viewGunttFilter.fields.decimal_view.name).value : viewGunttFilter.fields.decimal_view.default }
                              roundValues={ view.roundValues }
                            />
                        </div>
                    </div>
                </div>
            </div>)
          }
          {
              view.pending && <Spinner className={ styles.preloader }/>
          }
      </div>)
})

export default Guntt
