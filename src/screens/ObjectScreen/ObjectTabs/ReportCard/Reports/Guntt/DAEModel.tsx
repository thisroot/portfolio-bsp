// import React, { useEffect, useRef, useState } from 'react'
// import { DAEModel as DM, DirectionLight } from 'react-3d-viewer'
// import { useInstance } from "react-ioc"
// import { GunttViewModel } from "./GunntViewModel"
// import { Spinner } from "../../../../../../components/UI/Spinner"
//
//
// const DAEModel: React.FC<any> = () => {
//
//     const view = useInstance(GunttViewModel)
//
//     const virtualModelContainerRef: React.MutableRefObject<HTMLDivElement> = useRef()
//     const [ virtualModelContainerWidth, setVirtualModelContainerWidth ] = useState(0)
//     const [ virtualModelContainerHeight, setVirtualModelContainerHeight ] = useState(0)
//
//     useEffect(() => {
//         if (virtualModelContainerRef.current) {
//             const rect = virtualModelContainerRef.current.getBoundingClientRect()
//             setVirtualModelContainerWidth(rect.width)
//             setVirtualModelContainerHeight(rect.height)
//         }
//     }, [ virtualModelContainerRef ])
//
//
//     const [ isModelLoading, setIsModelLoading ] = useState<boolean>(true)
//
//     useEffect(() => {
//         setIsModelLoading(true)
//     }, [ view.isVirtualModelShown ])
//
//
//     return <div ref={ virtualModelContainerRef } style={ { display: "flex", flex: '1 1', height: '100%' } }>
//         { isModelLoading && <Spinner/> }
//         <DM src={ `https://esbunoto.lad24.ru/ifc?file=${ view.object.virtualModelUrl }` }
//             width={ virtualModelContainerWidth }
//             height={ virtualModelContainerHeight }
//             onLoad={ () => {
//                 setIsModelLoading(false)
//             } }
//         >
//             <DirectionLight color={ 0xffffff }/>
//         </DM>
//     </div>
// }
//
// export default DAEModel