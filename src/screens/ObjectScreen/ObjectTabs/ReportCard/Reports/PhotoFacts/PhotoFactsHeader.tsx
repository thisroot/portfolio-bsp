import React from "react"
import styles from "./styles.scss"
import { Icon } from "components/UI/Icon"
import { useInstance } from "react-ioc"
import { PhotoFactsViewModel } from "./PhotoFactsViewModel"
import { observer } from "mobx-react-lite"
import { Link } from "services/RouterService"
import { Select } from "../../../../../../components/UI/Select"

const PhotoFactsHeader: React.FC = observer(() => {

    const view = useInstance(PhotoFactsViewModel);

    return <div className={ styles.reportContainer }>
        <div className={ styles.controls }>
            <div className={ styles.leftPart } >
                    <Link to={ view.reportCard.currentReportPath } className={ styles.backIcon }>
                        <Icon name='arrows-arrow-r' />
                    </Link>
            </div>
            <div className={ styles.rightPart}>
                <Select
                  options={ view.sortOrder }
                  handle={ view.setSortOrder }
                  disabled={ view.pending }
                />
            </div>
        </div>
    </div>
})
export default PhotoFactsHeader
