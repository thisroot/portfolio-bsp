import { action, computed, observable, runInAction } from "mobx"
import { inject } from "react-ioc"
import { AnalyticsService, ApiService, AppService, AuthService, FileLoaderService, RouterService } from "services"
import { FILE_RELATION, ROUTES } from "constants/index"
import { get } from "lodash"
import { parsePath } from "utils/helpers"
import { FileListModel } from "models/entities/FileListModel"
import { toaster } from "components/UI/Toast"
import { ReportCardViewModel } from 'screens/ObjectScreen/ObjectTabs/ReportCard/ReportCardViewModel'
import { DataContext } from "services/DataContext"
import { DictionaryService } from "services/DictionaryService"
import { ObjectViewModel } from "../../../../ObjectViewModel"
import { Modal } from "../../../../../../components/UI/Modal"
import styles from "../../../ScheduleListBuilderCard/styles.scss"
import { Domain } from "../../../../../../components/UI/Domain"
import colors from "../../../../../../static/styles/colors.scss"
import { loc } from "../../../../../../utils/i18n"
import { t } from "@lingui/macro"
import { ACTION_TYPE, Badge } from "../../../../../../components/UI/Badge"
import { BasicButton, DangerButton } from "../../../../../../components/UI/Button"
import React from "react"
import dayjs from "dayjs"
import { FileModel } from "../../../../../../models/entities"

export class PhotoFactsViewModel {

    @inject fileLoader: FileLoaderService
    @inject api: ApiService
    @inject app: AppService
    @inject router: RouterService
    @inject reportCard: ReportCardViewModel
    @inject dataContext: DataContext
    @inject dictionaryService: DictionaryService
    @inject object: ObjectViewModel
    @inject analytics: AnalyticsService
    @inject authService: AuthService

    @observable mediaFiles = new FileListModel(this.fileLoader)
    @observable photoFact = []
    @observable imageList = []

    @observable
    public wasInitialized = false
    @observable
    public pending = false

    @observable
    selectedMedia = null

    @observable
    public currentMedia = 0
    @observable
    public lastCurrentInLightBox = -1

    @observable
    public isLightBoxOpen = false

    @observable order = -1

    public sortOrder = [ {
        value: -1,
        label: loc._(t`basic.sort.new`)
    }, {
        value: 1,
        label: loc._(t`basic.sort.old`)
    } ]

    @action
    public setSortOrder = async (order: number) => {
        if (this.order !== order) {
            this.order = order
            await this.load()
        }
    }

    @computed
    public get userCanDeletePhotoFact(): boolean {
        return this.object.canDelete('schedule.fact.image')
    }

    @action
    public canDelete = (media: FileModel) => {
        const isYangThanDay = dayjs() < dayjs(media.created).add(1, 'day')
        const isUserOwner = media.createdBy === this.authService.currentUser.user_id
        return this.userCanDeletePhotoFact || (isYangThanDay && isUserOwner)
    }

    @action
    public onDelete = async (fileId: number, factId: number) => {
        try {
            await this.api.files.deleteImage([ fileId ])
            this.mediaFiles.files.delete(fileId)
            this.imageList = this.imageList.filter(image => image.id !== fileId)
            const fact = this.photoFact.find(f => f.schedule_fact_id === factId)
            fact.images = fact.images.filter(image => image.id !== fileId)
        } catch (e) {
            this.analytics.sendError(e)
        }
    }

    @action
    public confirmDelete = async (fileId: number, factId: number) => {
        this.app.modalControls.renderModal = () => (
          <Modal
            children={
                <div className={ styles.modalContentContainer }>
                    <div className={ styles.col }>
                        <Domain labelColor={ colors.danger500 } icon={ 'account-trash-delete-bin' } text={ '' }/>
                    </div>
                    <div className={ styles.col }>
                        <h4>
                            { loc._(t`components.photoFacts.modal.removePhotoModalTitle`) }
                        </h4>
                        <div style={ { marginTop: '10px' } }>
                            <Badge style={ { fontSize: '1rem', marginRight: '5px' } } invert type={ ACTION_TYPE.DANGER }
                                   text={ `${ loc._(t`basic.attention`) }!` }/>
                            { loc._(t`components.photoFacts.modal.removePhotoBody`) }
                        </div>
                    </div>
                </div>
            }
            renderConfirm={ () => (
              <DangerButton
                text={ loc._(t`basic.delete`) }
                onClick={ () => {
                    this.app.modalControls.close()
                    this.onDelete(fileId, factId)
                } }
                disabled={ false }
              />
            )
            }
            renderAbort={ () => (
              <BasicButton
                style={ { border: 'none' } }
                outline
                text={ loc._(t`basic.abort`) }
                onClick={ () => {
                    this.app.modalControls.close()
                } }
              />
            )
            }
          />
        )
        this.app.modalControls.open()
    }

    @action init = async () => {
        try {
            if (!this.wasInitialized) {
                this.pending = true
                await this.dictionaryService.init()
                await this.load()

            }
        } catch (e) {
            this.analytics.sendError(e)
        } finally {
            runInAction(() => {
                this.wasInitialized = true
                this.pending = false
            })
        }
    }

    @action load = async () => {
        try {
            const result = await this.dataContext.cacheFirst(this.api.scheduleFact.list, {
                extended: true,
                filter: { object_id: this.objectIdByRoute, foto: true },
                sort: {
                    created: this.order
                }
            }, { noCaching: true })

            if (result.status !== 'error' && result.status !== 'cached') {
                this.dataContext.applySnapshot(result.body)
                this.photoFact = get(result, 'body.schedule_fact_list').map(item => this.dataContext.schedule_fact.get(item))
                await this.mediaFiles.push(get(result, 'body.image'), FILE_RELATION.OBJECT, this.objectIdByRoute)
                const images = get(result, 'body.image_list').map(image => this.mediaFiles.files.get(image))

                this.imageList = get(result, 'body.schedule_fact_list').reduce((acc, item) => {
                    acc.push(...images.filter(image => image.relationId === item))
                    return acc
                }, [])
            }
        } catch (e) {
            this.analytics.sendError(e)
        }
    }

    @computed
    public get objectIdByRoute(): number {
        return Number(get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId'))
    }

    @action
    public toggleLightBox = (index?: number) => {
        if (this.isLightBoxOpen) {
            this.lastCurrentInLightBox = index
        } else {
            this.currentMedia = index
        }
        this.isLightBoxOpen = !this.isLightBoxOpen
    }

    @action
    public onSelectDay(day) {
        console.log('day as callback', day)
    }

    @action
    public onSelectToUpload = async (Files: Array<File>) => {
        try {
            await this.mediaFiles.push(Files, FILE_RELATION.OBJECT, this.objectIdByRoute)
        } catch (e) {
            toaster.error(e.message)
        }
        // const promises = []
        // for (const item of Files) {
        //     const file = await this.fileLoader.initFile(item)
        //     runInAction(() => {
        //         this.mediaFiles.push(file)
        //     })
        //     promises.push(this.fileLoader.uploadFile(file, IMAGE_RELATION.OBJECT, this.objectIdByRoute))
        // }
        // Promise.all(promises).then(_ => toaster.success(loc._(t`basic.uploadImage.success`)))
    }

    @action
    public mapPhotoFactToProxy = (photoFact: any) => (
      Object.values(photoFact)
        .map(fact => this.dataContext.schedule_fact.get(get(fact, 'schedule_fact_id'))
        )
    )
}
