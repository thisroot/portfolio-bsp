import React from 'react'
import { Gallery } from "components/Gallery"
import { useInstance } from "react-ioc"
import { PhotoFactsViewModel } from "./PhotoFactsViewModel"
import { observer } from "mobx-react-lite"
import { useAsync } from "react-use"
import { ObjectViewModel } from "../../../../ObjectViewModel"

export const PhotoFacts: React.FC = observer(() => {
    const view = useInstance(PhotoFactsViewModel)
    const viewObject = useInstance(ObjectViewModel)

    useAsync(view.init, [])
    return <Gallery
      mediaFiles={ view.imageList }
      photoFact={ view.photoFact }
      selectedMedia={ view.selectedMedia }
      onSelectDay={ view.onSelectDay }
      isLightBoxOpen={ view.isLightBoxOpen }
      toggleLightBox={ view.toggleLightBox }
      currentMedia={ view.currentMedia }
      lastCurrentMedia={ view.lastCurrentInLightBox }
      pending={ view.pending }
      initialized={ view.wasInitialized }
      selectAsMain={ viewObject.selectAsMain }
      onDelete={ view.confirmDelete }
      canDelete={ view.canDelete }
    />
})
