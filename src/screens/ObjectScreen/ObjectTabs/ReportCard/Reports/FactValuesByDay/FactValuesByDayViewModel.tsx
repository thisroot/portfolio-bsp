import React from "react"
import { action, computed, observable, runInAction } from "mobx"
import { inject } from "react-ioc"
import { ApiService, DataContext } from "services"
import { ObjectViewModel } from "screens/ObjectScreen/ObjectViewModel"
import { abbreviateNumber } from "utils/helpers"
import styles from "./styles.scss"
import dayjs from "dayjs"
import { SERVER_DATE_MASK } from 'constants/common'
import { get, isEmpty } from 'lodash'
import { ScheduleFactFrequencyResp } from 'services/ApiService/Interfaces/scheduleFactFrequency'
import { REPORT_FACT_TYPE } from 'constants/locale'
import { loc } from 'utils/i18n'
import { t } from '@lingui/macro'
import cx from 'classnames'
import { ReportCardViewModel } from 'screens/ObjectScreen/ObjectTabs/ReportCard/ReportCardViewModel'

interface ObjectScheduleFactFrequency {
    [key: number]: ScheduleFactFrequencyResp;
}

class FactValuesByDayViewModel {

    @inject dataContext: DataContext
    @inject api: ApiService
    @inject object: ObjectViewModel
    @inject reportCard: ReportCardViewModel

    public FactValuesByDayHeaderRef = null

    @observable pending: boolean = false
    @observable initPending: boolean = false
    @observable wasInitialized: boolean = false

    private columnsInHeatMap: number = 21

    @observable scheduleFactFrequency: ObjectScheduleFactFrequency = {}
    private lastYLabels: { name: string }[] = []

    @observable
    public dateStart = dayjs()

    @computed
    public get dateEnd() {
        return this.dateStart.subtract(this.columnsInHeatMap, 'day')
    }

    // TODO: тоже объединить с похожей функцией запроса в init
    @action
    public changeDate = async (direction: number) => {
        const currentDateStart = this.dateStart
        let newDateStart = currentDateStart.add(this.columnsInHeatMap * direction, 'day')
        if (newDateStart > dayjs()) {
            newDateStart = dayjs()
        }
        if (newDateStart.date() !== currentDateStart.date()) {
            this.pending = true
            try {
                const reqBody = {
                    object_id: Number(this.object.objectIdByRoute),
                    date_from: newDateStart.subtract(this.columnsInHeatMap, 'day').format(SERVER_DATE_MASK),
                    date_to: newDateStart.format(SERVER_DATE_MASK),
                    fact_type: this.selectedFactType,
                    fact_source: this.sendingFactSource
                }
                const result = await this.dataContext.cacheFirst(this.api.report.scheduleFactFrequency, reqBody, { noCaching: true })
                const factFrequency = get(result, 'body.report_schedule_fact_frequency')
                if (result.status !== 'error' && result.status !== 'cached') {
                    runInAction(() => {
                        this.dateStart = newDateStart
                        this.scheduleFactFrequency = factFrequency
                    })
                } else if (result.status === 'error') {
                    throw new Error()
                }
            } catch (e) {
                console.log(e)
            } finally {
                this.pending = false
            }
        }
    }

    @computed
    public get factTypeOptions(): { value: string, label: string }[] {
        return this.dataContext.reference.fact_type.map(item => ({ value: item, label: loc._(REPORT_FACT_TYPE[item]) }))
    }

    @observable
    public selectedFactType: string = 'foreman'

    // TODO: очень похоже на функцию init (за мелкими исключениями) - надо будет вынести в отдельную функцию
    // а еще будет такая же при изменении дат
    @action
    public setFactType = async (item) => {
        if (item !== this.selectedFactType) {
            this.pending = true
            try {
                const reqBody = {
                    object_id: Number(this.object.objectIdByRoute),
                    date_from: this.dateEnd.format(SERVER_DATE_MASK),
                    date_to: this.dateStart.format(SERVER_DATE_MASK),
                    fact_type: item,
                    fact_source: this.sendingFactSource
                }
                this.selectedFactType = item // TODO: если сделать здесь, а не после получения ответа - все ok, но тогда надо возвращать обратно в catch
                const result = await this.dataContext.cacheFirst(this.api.report.scheduleFactFrequency, reqBody, { noCaching: true })
                const factFrequency = get(result, 'body.report_schedule_fact_frequency')
                if (result.status !== 'error' && result.status !== 'cached' && factFrequency) {
                    runInAction(() => {
                        this.scheduleFactFrequency = factFrequency
                    })
                } else if (result.status === 'error' || !factFrequency) {
                    throw new Error()
                }
            } catch (e) {
                console.log(e)
                this.scheduleFactFrequency = {}
            } finally {
                this.pending = false
            }
        }
    }

    public factSource = [
        {
            value: 'all', label: loc._(t`basic.report_fact_type.all`)
        },
        {
            value: 'mobile', label: loc._(t`basic.report_fact_type.mobile`)
        },
        {
            value: 'web', label: loc._(t`basic.report_fact_type.web`)
        }
    ]

    public factType = [
        {
            value: 'volume', label: loc._(t`basic.report_fact_type.physical_volumes`)
        },
        {
            value: 'expenditure', label: loc._(t`basic.report_fact_type.mastering`)
        },
    ]

    @observable currentFactType: string = this.factType[0].value

    @action setCurrentFactType = (value) => {this.currentFactType = value}

    @observable public selectedFactSource: string = 'all'
    @ computed
    private get sendingFactSource() {
        return this.selectedFactSource === 'all' ? null : this.selectedFactSource
    }

    @action setFactSource = async (item) => {
        if (item !== this.selectedFactSource) {
            this.pending = true
            try {
                this.selectedFactSource = item
                const reqBody = {
                    object_id: Number(this.object.objectIdByRoute),
                    date_from: this.dateEnd.format(SERVER_DATE_MASK),
                    date_to: this.dateStart.format(SERVER_DATE_MASK),
                    fact_type: this.selectedFactType,
                    fact_source: this.sendingFactSource
                }
                const result = await this.dataContext.cacheFirst(this.api.report.scheduleFactFrequency, reqBody, { noCaching: true })
                const factFrequency = get(result, 'body.report_schedule_fact_frequency')
                if (result.status !== 'error' && result.status !== 'cached' && factFrequency) {
                    runInAction(() => {
                        this.scheduleFactFrequency = factFrequency
                    })
                } else if (result.status === 'error' || !factFrequency) {
                    throw new Error()
                }
            } catch (e) {
                console.log(e)
                this.scheduleFactFrequency = {}
            } finally {
                this.pending = false
            }
        }
    }

    @action
    public init = async (dateStart = this.dateStart) => {
        this.dateStart = dateStart
        const object_id = Number(this.object.objectIdByRoute)
        this.pending = true
        this.initPending = true
        try {
            const fact_type = await this.dataContext.cacheFirst(this.api.reference.fact_type)
            if (fact_type.status !== 'error') {
                this.wasInitialized = true
            }
            if (fact_type.status !== 'error' && fact_type.status !== 'cached') {
                this.dataContext.applySnapshot(fact_type.body)
                this.selectedFactType = this.dataContext.reference.fact_type[0]
            } else if (fact_type.status === 'error') {
                throw new Error()
            }
            const reqBody = {
                object_id,
                date_from: this.dateEnd.format(SERVER_DATE_MASK),
                date_to: dateStart.format(SERVER_DATE_MASK),
                fact_type: this.selectedFactType,
                fact_source: this.sendingFactSource
            }
            const result = await this.dataContext.cacheFirst(this.api.report.scheduleFactFrequency, reqBody, { noCaching: true })
            const factFrequency = get(result, 'body.report_schedule_fact_frequency')
            if (result.status !== 'error' && result.status !== 'cached' && factFrequency) {
                runInAction(() => {
                    this.scheduleFactFrequency = factFrequency
                })
            } else if (result.status === 'error' || !factFrequency) {
                throw new Error()
            }
        } catch (e) {
            console.log(e)
            this.scheduleFactFrequency = {}
        } finally {
            this.pending = false
            this.initPending = false
        }
    }

    public xLabelsVisibility = new Array(this.columnsInHeatMap).fill(0).map((_, i) => (i % 2 === 0 ? true : false))

    @computed
    public get xLabels() {
        return new Array(this.columnsInHeatMap).fill(0).map((_, i) => <div
          className={ styles.xLabel }> { this.dateEnd.add(i + 1, 'day').format(loc._(t`basic.dateFormat.DD.MM.YYYY`)) }</div>)
    }

    @computed
    public get yLabels() {
        const allFacts = Object.values(this.scheduleFactFrequency)
        const currentYLables = isEmpty(allFacts) ? this.lastYLabels : allFacts
        if (!isEmpty(allFacts)) {
            this.lastYLabels = allFacts.map(el => ({ name: el.name }))
        }
        return currentYLables.map((el: ScheduleFactFrequencyResp) => <div key={ el.schedule_id }
                                                                          className={ styles.yLabelsTitleContainer }
                                                                          style={ { height: '30px' } }>
            <div title={ el.name } className={ styles.yLabelsTitleCol }><span
              className={ styles.oneLine }>{ el.name }</span></div>
            <div className={ styles.yLabelsTitleCol }>{ el[`${this.currentFactType}_fact`] && abbreviateNumber(el[`${this.currentFactType}_fact`]) }</div>
            <div className={ styles.yLabelsTitleCol }>{ el[`${this.currentFactType}_planned`] && abbreviateNumber(el[`${this.currentFactType}_planned`]) }</div>
        </div>)
    }

    @computed
    public get data() {
        const facts = Object.values(this.scheduleFactFrequency).map(el => {
            const mappedFactInDay = el.fact_count.reduce((acc, el) => {
                acc[el.date] = acc.hasOwnProperty(el.date) ? acc[el.date] + el[this.currentFactType] : el[this.currentFactType]
                return acc
            }, {})
            return new Array(this.columnsInHeatMap).fill(0).map((_, i) => {
                const currentDate = this.dateEnd.add(i + 1, 'day').format(SERVER_DATE_MASK)
                return !Object.keys(mappedFactInDay).includes(currentDate) || mappedFactInDay[currentDate] === null ? -1 : mappedFactInDay[currentDate]
            })
        })
        return isEmpty(facts) ? new Array(this.lastYLabels.length).fill(0).map(_ => new Array(this.columnsInHeatMap).fill(-1)) : facts
    }

    // TODO: это перенести в screen header
    public renderYLabels = () => <div className={ styles.yLabelsTitleContainer }>
        <div className={ cx(styles.yLabelsTitleCol, styles.yLabelsFirstCol) }>Этап работы</div>
        <div className={ cx(styles.yLabelsTitleCol, styles.yLabelsFirstCol) }>Фактическое значение</div>
        <div className={ cx(styles.yLabelsTitleCol, styles.yLabelsFirstCol) }>Плановое значение</div>
    </div>
}

export { FactValuesByDayViewModel }