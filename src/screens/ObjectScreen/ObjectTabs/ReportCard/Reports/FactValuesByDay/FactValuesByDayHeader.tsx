import React, { useCallback, useEffect, useRef } from "react"
import styles from "./styles.scss"
import { Icon } from "components/UI/Icon"
import { useInstance } from "react-ioc"
import { FactValuesByDayViewModel } from "./FactValuesByDayViewModel"
import { Spinner } from "components/UI/Spinner"
import { Select } from "components/UI/Select"
import { Trans } from "@lingui/macro"
import { observer } from "mobx-react-lite"
import { isEmpty } from 'lodash';
import cx from 'classnames'
import { loc } from 'utils/i18n'
import { t } from '@lingui/macro'
import { Link } from "services/RouterService"

const FactValuesByDayHeader: React.FC = observer(() => {

    const view = useInstance(FactValuesByDayViewModel);
    
    const headerRef = useRef(null);

    useEffect(() => {
        view.FactValuesByDayHeaderRef = headerRef; 
    }, [view.FactValuesByDayHeaderRef, headerRef]);

    const blockIsPending = useCallback(fn => !view.pending && fn(), [view.pending]);
    const redoOnClickHandler = useCallback(() => view.changeDate(1), [view]);
    const undoOnClickHandler = useCallback(() => view.changeDate(-1), [view]);

    return <div className={ styles.reportContainer }>
        <div className={ styles.controls }>
            <div className={ styles.leftPart } >
            { view.initPending ? <Spinner/> : (
                <>
                    <Link to={ view.reportCard.currentReportPath } className={ styles.backIcon }>
                        <Icon name='arrows-arrow-r' />
                    </Link>
                    { view.wasInitialized && isEmpty(view.data) &&
                    <div className={ styles.noData }>
                        <div><Trans>components.reports.values_by_day.noDataAvailableStart</Trans>
                        {` ${view.dateEnd.format(loc._(t`basic.dateFormat.DD.MM.YYYY`))} - 
                        ${view.dateStart.format(loc._(t`basic.dateFormat.DD.MM.YYYY`))} `}
                        <Trans>components.reports.values_by_day.noDataAvailableEnd</Trans></div>
                    </div> }
                </>
            )}
            </div>
            { !view.initPending && (
                <div className={ styles.rightPart }>
                    { !isEmpty(view.factType) && (
                      <div className={ styles.selectControl }>
                          <div className={ styles.table_ruler_step }>
                              <Select
                                options={ view.factType }
                                handle={view.setCurrentFactType}
                                disabled={ view.pending || !view.object.canRead('schedule.expenditure')}
                              />
                          </div>
                      </div>
                    )}
                    { !isEmpty(view.factTypeOptions) && (
                        <div className={ styles.selectControl }>
                            <div className={ styles.table_ruler_step }>
                                <Select
                                    options={ view.factTypeOptions }
                                    handle={ view.setFactType }
                                    disabled={ view.pending }
                                />
                            </div>
                        </div>
                    )}
                    { !isEmpty(view.factSource) && (
                      <div className={ styles.selectControl }>
                          <div className={ styles.table_ruler_step }>
                              <Select
                                options={ view.factSource }
                                handle={ view.setFactSource }
                                disabled={ view.pending }
                              />
                          </div>
                      </div>
                    )}
                    <div className={ cx(styles.groupBtn, { [styles.disabled]: view.pending }) }>
                        <Icon
                            onClick={() => blockIsPending(undoOnClickHandler)}
                            className={ styles.undo }
                            name={ 'arrows-forward' }
                        />
                        <Icon
                            onClick={() => blockIsPending(redoOnClickHandler)}
                            className={ styles.redo }
                            name={ 'arrows-forward' }
                        />
                    </div>
                </div>
            )}
        </div>
        <div ref={headerRef}></div>
    </div>
})
export default FactValuesByDayHeader