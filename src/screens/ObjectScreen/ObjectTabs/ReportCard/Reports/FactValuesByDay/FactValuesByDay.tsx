import React from "react"
import styles from "./styles.scss"
import { abbreviateNumber, hexToRgb } from "utils/helpers"
import { HeatMap } from "components/HeatMap"
import { useInstance } from "react-ioc"
import { FactValuesByDayViewModel } from "./FactValuesByDayViewModel"
import { useAsync } from "react-use"
import { Spinner } from "components/UI/Spinner"
import { isEmpty } from 'lodash';
import { observer } from "mobx-react-lite"
import colors from "static/styles/colors.scss"

const FactValuesByDay: React.FC = observer(() => {
    
    const view = useInstance(FactValuesByDayViewModel)

    useAsync(async () => {
        await view.init()
    });

    const rgb = hexToRgb(colors['cyan600'])
    
    return <div className={ styles.reportContainer }>
        { !isEmpty(view.data) && (
            <HeatMap
                headerRef={view.FactValuesByDayHeaderRef}
                pending={ view.pending }
                background={ rgb }
                yLabelTitle={ view.renderYLabels() }
                yLabelWidth={ 480 }
                xLabelWidth={ 20 }
                xLabels={ view.xLabels }
                xLabelsStyle={ styles.xLabelBox}
                yLabels={ view.yLabels }
                data={ view.data }
                cellRender={ (value) => value === -1 ? '' : abbreviateNumber(value, 0, false, 3, true)}
                cellStyle={ (b, value, min, max) => ({
                    background: value === -1 ? colors['basic050'] : `rgb(${ b.r }, ${ b.g }, ${ b.b }, ${ (1 - 0.8 * (max - value) / (max - min)) })`,
                    fontSize: "10px",
                    color: (max - value) / (max - min) < 0.2 ? 'white' : 'black',
                }) }
                xLabelsVisibility={ view.xLabelsVisibility }
                title={ (value, _, __) => value === -1 ? 0 : `${ value }` }
            />
        )}
        { (view.initPending || (view.pending && isEmpty(view.data))) && <div className={ styles.spinnerContainer}><Spinner/></div> }
    </div>
})
export default FactValuesByDay