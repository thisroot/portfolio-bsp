import React from 'react'

import { ArrowChartFluid, IArrowChartData } from 'components/ArrowChart'

import classes from './styles.scss'
import { observer } from "mobx-react-lite"
import { useInstance } from "react-ioc"
import { ArrowReportViewModel } from "./ArrowReportViewModel"
import { useEffectOnce } from "react-use"
import { Spinner } from "../../../../../../components/UI/Spinner"
import styles from "../Guntt/styles.scss"
import { NoResultContainer } from "../../../../../../components/NoResultContainer"


export const ArrowReport = observer(() => {
    const view = useInstance(ArrowReportViewModel)


    useEffectOnce(() => {
        view.init()
    })

    return (
      <div className={ classes.root }>
          <div className={ classes.chart }>
              { !view.pending && view.arrowData && <ArrowChartFluid
                key={ 'arrow-report' }
                currentDate={ view.currentDate }
                data={ view.arrowData as IArrowChartData }
              />
              }
              {
                  view.pending && <Spinner className={ styles.preloader }/>
              }
              {
                  !view.pending && !view.arrowData && <NoResultContainer/>
              }
          </div>
      </div>
    )
})
