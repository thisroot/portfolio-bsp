import { action, computed, observable } from "mobx"
import { inject } from "react-ioc"
import { ApiService } from "../../../../../../services/ApiService"
import { ObjectViewModel } from "../../../../ObjectViewModel"
import { AppService } from "../../../../../../services/AppService"

export class ArrowReportViewModel {

    @inject api: ApiService
    @inject object: ObjectViewModel
    @inject app: AppService

    @observable
    public pending = false

    @computed
    public get currentDate() {
        const now = new Date()
        return `${ now.getFullYear() }-${ (now.getMonth() + 1).toString().padStart(2, '0') }-${ (now.getDate()).toString().padStart(2, '0') }`
    }

    @observable
    public arrowData = null

    @action
    public init = async () => {
        this.pending = true
        try {
            if (!this.object.canRead('report.arrow')) {
                this.app.pageNotFound()
            } else {
                const data = await this.api.report.arrow({ object_id: +this.object.objectIdByRoute })
                if (data.status !== 'error') {
                    this.arrowData = data.body.report_arrow
                }
            }
        } catch (e) {
            console.log(e)
        } finally {
            this.pending = false
        }
    }
}
