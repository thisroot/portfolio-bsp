import { computed, observable } from "mobx"
import { inject } from "react-ioc"
import { ObjectViewModel } from "screens/ObjectScreen/ObjectViewModel"
import { ApiService, DataContext, RouterService } from "services"
import { AppService } from "services/AppService"
import { formatString } from "../../../../utils/helpers"
import { ROUTES } from "../../../../constants"
import { loc } from "../../../../utils/i18n"
import { t } from "@lingui/macro"
import colors from "../../../../static/styles/colors.scss"
import { get } from 'lodash'

class ReportCardViewModel {
    @inject dataContext: DataContext
    @inject api: ApiService
    @inject object: ObjectViewModel
    @inject app: AppService
    @inject router: RouterService
    @observable pending = false
    @observable wasInitialized = false

    @computed
    public get currentReportPath() {
        return `${ formatString(ROUTES.OBJECT_PAGE.PATH, this.object.projectIdByRoute, this.object.objectIdByRoute) }#reports`
    }

    @computed
    public get currentReport() {
        return get(this.router.location.hash.split('/'), '[1]')
    }

    @computed
    public get reportNavigation() {
        const currentIndex = this.reportList.findIndex(el => el.alias === this.currentReport)
        if (currentIndex === -1) {
            return {}
        }
        const prevIndex = currentIndex === 0 ? this.reportList.length - 1 : currentIndex - 1
        const nextIndex = currentIndex === this.reportList.length - 1 ? 0 : currentIndex + 1
        return {
            prev: this.reportList[prevIndex],
            next: this.reportList[nextIndex]
        }
    }

    @computed
    public get reportList() {
        const reportList = [
            {
                name: loc._(t`components.reports.values_by_day.name`),
                icon: 'business-chart-research-d',
                path: `${ this.currentReportPath }/values_by_day`,
                itemBackground: colors['orange400'],
                iconBackground: colors['orange460'],
                iconColor: colors['orange600'],
                alias: 'values_by_day',
            },
            {
                name: loc._(t`components.reports.guntt.name`),
                icon: 'business-chart-metrics',
                path: `${ this.currentReportPath }/guntt`,
                itemBackground: colors['cyan400'],
                iconBackground: colors['cyan500'],
                iconColor: colors['cyan700'],
                alias: 'guntt',
            },
            // {
            //     name: 'Финансовый прогноз',
            //     icon: 'business-calculator',
            //     path: `${view.currentReportPath}`,
            //     itemBackground: colors['success530'],
            //     iconBackground: colors['success700'],
            //     iconColor: colors['success750'],
            // },
            // {
            //     name: 'Люди и механизмы',
            //     icon: 'business-chart-circle',
            //     path: `${view.currentReportPath}`,
            //     itemBackground: colors['warning500'],
            //     iconBackground: colors['warning600'],
            //     iconColor: colors['warning700'],
            // },
            {
                name: loc._(t`components.reports.photoFacts.name`),
                icon: 'business-setting',
                path: `${ this.currentReportPath }/photo_facts`,
                itemBackground: colors['purple420'],
                iconBackground: colors['purple500'],
                iconColor: colors['purple700'],
                alias: 'photo_facts',
            },
        ]

        if (this.object.canRead('report.arrow')) {
            reportList.push({
                name: loc._(t`components.reports.arrowReport.name`),
                icon: 'business-chart-metrics',
                path: `${ this.currentReportPath }/arrow`,
                itemBackground: colors['success400'],
                iconBackground: colors['success530'],
                iconColor: colors['success750'],
                alias: 'arrow',
            })
        }

        return reportList
    }
}

export { ReportCardViewModel }
