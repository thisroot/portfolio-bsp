import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { useInstance } from "react-ioc"
import { ObjectHistoryViewModel } from "./ObjectHistoryViewModel"
import { Preloader, Spinner } from "components/UI"
import { Table } from 'components/Table'
import cx from 'classnames'
import styles from "./styles.scss"

export const ObjectHistoryCard: React.FC = observer(() => {
    const view = useInstance(ObjectHistoryViewModel)

    useEffect(() => {
        view.init()
    }, [view])

    return (
        <div className={ styles.historyContainer }>
            { view.initialPending
                ? <Preloader />
                : (
                    <div className={ cx(styles.contentContainer) } >
                        { view.pagPending
                            ? <div className={ styles.pagPendingContainer }><Spinner className={ styles.pagPending } /></div>
                            : null
                        }
                        <Table
                            upperHeader={ view.tabs.upperHeader }
                            rows={ view.tabs.rows }
                            columns={ view.tabs.columns }
                            hidden={ view.tabs.hidden }
                            markFirstColumn={ false }
                            isGridNeeded={ true }
                            colClassName={ styles.colClassName }
                        />
                    </div>
                )
            }
        </div>
    )
})