import React from "react"
import { observer } from "mobx-react-lite"
import { useInstance } from "react-ioc"
import { ObjectHistoryViewModel } from "./ObjectHistoryViewModel"
import { ObjectHistoryFilterViewModel, ObjectHistoryFilter } from 'components/Filters/ObjectHistoryFilter'
import { Pagination } from "components/UI/Pagination"
import { DefaultButton } from "components/UI"
import { Popover } from "components/UI/Popover"
import cx from 'classnames'
import styles from "./styles.scss"

export const ObjectHistoryCardHeader: React.FC = observer(() => {
    const view = useInstance(ObjectHistoryViewModel)
    const filterView = useInstance(ObjectHistoryFilterViewModel)

    return (
        !view.initialPending &&(
            <div className={ styles.historyHeader }>
                <div className={ styles.left }>
                    <Pagination
                        current={ view.pagCurrent }
                        total={ view.pagTotal }
                        pageSize={ view.pagPageSize }
                        disabled={ filterView.isPopoverOpen || view.initialPending || view.pagPending || view.pagTotal <= view.pagPageSize }
                        onChange={ view.pagOnChange }
                    />
                </div>
                <div className={ styles.right }>
                    <Popover
                        onClickOutside={ () => filterView.togglePopover() }
                        isOpen={ filterView.isPopoverOpen }
                        align={ "start" } position={ 'left' }
                        content={ <ObjectHistoryFilter onSubmit={ view.onSubmit } onReset={ view.onReset }/> }
                    >
                        <div className={ styles.filterContainer } onClick={ filterView.togglePopover }>
                            <DefaultButton
                            className={ cx(styles.filter, { [styles.open]: filterView.isPopoverOpen }) }
                            icon={ 'internet-filter-sort-b' }
                            />
                            { filterView.countFilledFields > 0 &&
                                <span className={ styles.badge }>{ filterView.countFilledFields }</span> }
                        </div>
                    </Popover>
                </div>
            </div>
        )
    )
})