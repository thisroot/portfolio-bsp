import { inject } from "react-ioc"
import { Project } from "models/mst"
import { ApiService, DataContext, RouterService, DictionaryService, LocalisationService } from "services"
import { ObjectHistoryFilterViewModel } from 'components/Filters/ObjectHistoryFilter'
import { action, computed, observable } from "mobx"
import { loc } from "utils/i18n"
import { t } from "@lingui/macro"
import { toaster } from "components/UI/Toast"
import { get, isNull } from 'lodash'
import dayjs from "dayjs"
import { parsePath, getUserOrAdminFullName, parseMeasureUnit } from "utils/helpers"
import { ROUTES } from "constants/routes"
import { HISTORY_RELATION, HISTORY_ACTION } from "constants/locale"
import { PICKER_SHORT_YEAR_DATE_MASK, PICKER_TIME_MASK } from "constants/common"

class ObjectHistoryViewModel {
    @inject dataContext: DataContext
    @inject api: ApiService
    @inject router: RouterService
    @inject filterView: ObjectHistoryFilterViewModel
    @inject dictionary: DictionaryService
    @inject local: LocalisationService

    @observable
    public initialPending = false
    @observable
    public wasInitialized = false
    @action
    public initialize = () => {
        this.initialPending = false
        this.pagPending = false
    }

    @observable
    public pagCurrent = 1
    @observable
    public pagTotal = 0
    @observable
    public pagPageSize = 50
    @observable
    public pagPending = false
    @action
    public pagOnChange = async (current) => {
        this.pagCurrent = current
        this.pagPending = true
        await this.load()
    }

    @observable
    private history = {}
    @observable
    private historyList = []

    @computed
    public get tabs() {
        return {
            upperHeader: {
                firstHeader: [ '', 0.31 ],
                secondHeader: [ 'Значения', this.isCommentRequire ? 0.37 : 0.4 ],
                thirdHeader: [ 'Классификаторы', this.isCommentRequire ? 0.2 : 0.26 ],
                fourthHeader: [ '', 0.03 ],
            },
            columns: {
                id: [ '' ],
                index: [ '№', 0.03 ],
                date: [ loc._(t`basic.date`), 0.06 ],
                time: [ loc._(t`basic.time`), 0.04 ],
                action: [ loc._(t`basic.actions`), 0.08 ],
                userName: [ loc._(t`basic.userName`), 0.1 ],
                subjectName: [ loc._(t`component.objectHistory.subjectName`), this.isCommentRequire ? 0.1 : 0.13 ],
                dateStart: [ loc._(t`component.objectHistory.dateStart`), 0.06 ],
                dateFinish: [ loc._(t`component.objectHistory.dateFinish`), 0.06 ],
                volume: [ loc._(t`component.objectHistory.volumePlan`), 0.05 ],
                expenditure: [ loc._(t`component.objectHistory.costPlan`), 0.05 ],
                measure: [ loc._(t`component.objectHistory.measure`), 0.05 ],
                job: [ loc._(t`component.objectHistory.jobType`), this.isCommentRequire ? 0.1 : 0.13 ],
                element: [ loc._(t`component.objectHistory.element`), this.isCommentRequire ? 0.1 : 0.13 ],
                comment: [ loc._(t`component.objectHistory.comment`), 0.09 ],
                version: [ loc._(t`component.objectHistory.version`), 0.03 ],
            },
            rows: Object.values(this.history)
              .map((item: any, idx: number) => {
                  const jobId = get(item, 'new_record.job_id')
                  const job = isNull(jobId) ? '' : this.dataContext.reference.job.get(jobId).name_ru
                  const elementId = get(item, 'new_record.element_id')
                  const element = isNull(elementId) ? '' : this.dataContext.reference.element.get(elementId).name_ru
                  const jobUnit = get(item, 'new_record.job_unit')
                  const measure = isNull(jobUnit)
                    ? parseMeasureUnit(get(this.dataContext.reference.job.get(jobId), 'job_unit'))
                    : parseMeasureUnit(jobUnit)
                  const schedule = this.dataContext.schedule.get(item['history_relation_id'])
                  const start = dayjs(get(schedule, 'date_start', null))
                  const finish = dayjs(get(schedule, 'date_finish', null))

                  return {
                      id: item.history_id,
                      index: idx,
                      date: dayjs(item.created).format(PICKER_SHORT_YEAR_DATE_MASK),
                      time: dayjs(item.created).format(PICKER_TIME_MASK),
                      action: loc._(HISTORY_ACTION.find(action => action.key === item.action).value),
                      userName: getUserOrAdminFullName(get(item, 'user'), this.local.lang),
                      subjectName: get(item, HISTORY_RELATION[item.history_relation].field, ''),
                      dateStart: dayjs(start).isValid() ? start.format(PICKER_SHORT_YEAR_DATE_MASK) : '',
                      dateFinish: dayjs(finish).isValid() ? finish.format(PICKER_SHORT_YEAR_DATE_MASK) : '',
                      volume: get(schedule, 'volume_planned', ''),
                      expenditure: get(schedule, 'expenditure_planned', ''),
                      measure,
                      job,
                      element,
                      comment: get(item, 'new_record.comment', '') === 'update from browser' ? '' : get(item, 'new_record.comment', ''),
                      version: get(item, 'new_record.schedule_version_id', '')
                  }
              })
              .sort((a, b) => this.historyList.indexOf(a.id) - this.historyList.indexOf(b.id))
              .map((item, idx) => ({ ...item, index: (this.pagCurrent - 1) * this.pagPageSize + idx + 1 }))
            ,
            hidden: this.isCommentRequire ? [ 'id' ] : [ 'id', 'comment' ],
        }
    }

    @action
    public init = async () => {
        try {
            this.initialPending = true
            await this.dictionary.init()

            const object_id = Number(this.objectIdByRoute)
            const schedule = await this.dataContext.cacheFirst(
              this.api.schedule.getScheduleList,
              { object_id },
              { noCaching: true }
            )
            if (schedule.status !== 'error' && schedule.status !== 'cached') {
                this.dataContext.applySnapshot(schedule.body)
                this.dataContext.object.get(`${ object_id }`).schedule_builder = get(schedule.body, 'schedule_list', [])
            }

            await this.load()
            this.wasInitialized = true
        } catch (e) {
            toaster.error()
        } finally {
            this.initialize()
        }
    }

    @action
    private load = async () => {
        try {
            const result = await this.api.logging.historyList({
                sort: { created: -1 },
                limit: this.pagPageSize,
                offset: (this.pagCurrent - 1) * this.pagPageSize,
                filter: {
                    ...this.filterView.formValues,
                    'history_relation': [ HISTORY_RELATION.schedule.name ],
                    'history_relation_id': this.currentObject.schedule_builder.map(i => i.schedule_id)
                }
            })
            if (result.status === 200) {
                this.history = get(result, 'body.history', {})
                this.historyList = get(result, 'body.history_list', [])
                this.pagTotal = get(result, 'body.total', 0)
            }
        } catch (e) {
            toaster.error()
        } finally {
            this.initialize()
        }
    }

    @computed
    public get projectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.projectId')
    }

    @computed
    public get currentProject(): Project {
        return this.dataContext.project.get(this.projectIdByRoute) || undefined
    }

    @computed
    public get isCommentRequire(): boolean {
        return get(this.currentProject, 'settings.schedule_update_requires_comment', false)
    }

    @computed
    public get currentObject() {
        return this.dataContext.object.get(this.objectIdByRoute)
    }

    @computed
    public get objectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId')
    }

    @action
    public onSubmit = async (e) => {
        this.filterView.filterForm.onSubmit(e)
        this.pagCurrent = 1
        this.pagPending = true
        await this.load()
    }

    @action
    public onReset = async (e) => {
        this.filterView.filterForm.onReset(e)
        this.pagCurrent = 1
        this.pagPending = true
        await this.load()
    }
}

export { ObjectHistoryViewModel }
