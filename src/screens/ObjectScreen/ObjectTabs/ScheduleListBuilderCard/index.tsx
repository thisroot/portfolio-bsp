import React from "react"
import Loadable, { LoadableComponent } from '@loadable/component'
import { Preloader } from "components/UI/Preloader"

const AsyncScheduleListBuilderCard: LoadableComponent<any> = Loadable(() => import(/* client-side */'./ScheduleListBuilderCard'), {
    fallback: <Preloader /> } )

export { AsyncScheduleListBuilderCard }