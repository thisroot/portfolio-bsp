import styles from "./styles.scss"
import React from "react"
import { observer } from "mobx-react-lite"
import { useInstance } from "react-ioc"
import { ScheduleListBuilderCardViewModel } from "./ScheduleListBuilderCardViewModel"
import { ScheduleListBuilder } from "components/SheduleTree/ScheduleListBuilder"
import { useAsync, useKey } from "react-use"
import { Spinner } from "components/UI/Spinner"
import cx from 'classnames'
import { ScheduleVersionEditForm } from "components/ScheduleVersionEditForm"
import { AppService } from "../../../../services/AppService"

const ScheduleListBuilderCard: React.FC = observer(() => {
    const view = useInstance(ScheduleListBuilderCardViewModel)
    const app = useInstance(AppService)

    useAsync(async () => {
        await view.init()
    })
    useKey('Escape', view.resetSelectedNodes)

    return (
      <div className={ styles.constructorCard }>
          {
              !view.pending &&
              <>
                  { view.userCanWriteVersions &&
                  <div className={ cx(styles.versionContainer, { [styles.open]: view.isVersionFormOpen }) }>
                      <ScheduleVersionEditForm
                        currentObject={ view.object.objectIdByRoute }
                        versionOnChange={ view.init }
                        onInit={ view.setActiveVersion }
                      />
                  </div>
                  }
                  <ScheduleListBuilder
                    scrollOnTop={ app.scrollOnTop }
                    addFileToUpload={ view.addFileToUpload }
                    scheduleFiles={ view.scheduleFiles }
                    disabledUpload={ view.filesUploadDisabled }
                    acceptedFiles={ view.acceptedFiles }
                    onDirty={ view.onDirty }
                    onSelectNode={ view.onSelectNode }
                    selectedNodes={ [ ...view.selectedNodes ] }
                    resetSelectedNodes={ view.resetSelectedNodes }
                    currentProject={ view.object.currentProject }
                    tree={ view.treeFromFlatData }
                    onChange={ view.onChange }
                    onMoveNode={ view.confirmMoveNode }
                    onVisibilityToggle={ view.onVisibilityToggle }
                    treeHasChanged={ view.treeHasChanged }
                    editingNodesSnaps={ view.editingNodesSnaps }
                    isTreePathSelectMode={ view.isTreePathSelectMode }
                    canDrop={ view.canDrop }
                  /></>
          }
          {
              view.pending && <Spinner className={ styles.preloader }/>
          }
      </div>
    )
})

export default ScheduleListBuilderCard
