import React, { Fragment } from "react"
import { observer } from "mobx-react"
import { inject } from "react-ioc"
import { ScheduleListBuilderCardViewModel } from "screens/ObjectScreen/ObjectTabs/ScheduleListBuilderCard/ScheduleListBuilderCardViewModel"
import { ObjectViewModel } from "screens/ObjectScreen/ObjectViewModel"
import cx from 'classnames'
import { loc } from "utils/i18n"
import { t, Trans } from "@lingui/macro"
import styles from "./styles.scss"
import { get } from "lodash"
import { Button, DangerButton, Spinner } from "components"
import { DefaultButton, InfoButton } from "../../../../components"
import { ScheduleActualValuesFormModel } from "./ScheduleActualValuesForm/ScheduleActualValuesFormModel"

@observer
class ScheduleListBuilderCardHeader extends React.Component {
    @inject view: ObjectViewModel
    @inject constr: ScheduleListBuilderCardViewModel
    @inject modal: ScheduleActualValuesFormModel

    public render() {
        // const isNoChild = !get(this.constr.dataContext.schedule.get(`${ this.constr.lastNode }`), 'children.length')
        const isNoJob = !get(this.constr.dataContext.schedule.get(`${ this.constr.lastNode }`), 'job_id')
        const countSelected = this.constr.selectedNodes.size
        const countAll = this.constr.countNodes


        const weightingFactor = this.constr.currentObject.weighting_factor ? this.constr.currentObject.weighting_factor.toFixed(3) : 0

        return (
          <Fragment>
              {
                  !this.constr.pending && (
                    <>
                        <div className={ `${ styles.tableHeader } ${ styles.reports_header }` }>
                            <div className={ cx(styles.tableCol, styles.maxWidth) }>
                                <div className={ styles.col }>
                                    {
                                        this.constr.isTreePathSelectMode && isNoJob && this.constr.userCanWrite &&
                                        <InfoButton
                                          onClick={ this.constr.isActiveVersion ? this.constr.toggleSelectedMode : undefined }
                                          className={ cx(styles.addNode, styles.roundBtn) }
                                          icon={ "signs-delete-close" }
                                        />
                                    }
                                    {
                                        // this.constr.isTreePathSelectMode && isNoJob &&
                                        <DefaultButton
                                          disabled={ !this.constr.isActiveVersion }
                                          withoutBorders
                                          icon={ 'signs-checkbox-done' }
                                          className={ cx(styles.addNode, styles.nodeSelector) }
                                          onClick={ () => {
                                              if (this.constr.isActiveVersion) {
                                                  countSelected < countAll ? this.constr.selectAllNodes() : this.constr.selectedNodes.clear()
                                              }
                                          } }
                                          text={ [
                                              <span key={ 'count' }>{ countSelected }</span>,
                                              <span key={ 'label' }>
                                                  { countSelected < countAll ?
                                                    loc._(t`basic.selectAll`) :
                                                    loc._(t`basic.unselectAll`)
                                                  }
                                              </span> ]
                                          }/>
                                    }
                                    {
                                        !this.constr.isTreePathSelectMode && this.constr.userCanWrite &&
                                        <>
                                            <InfoButton
                                              disabled={ !this.constr.isActiveVersion }
                                              outline
                                              onClick={ this.constr.isActiveVersion ? this.constr.toggleSelectedMode : undefined }
                                              className={ styles.addNode }
                                              icon={ 'edit-pen' }
                                            />
                                            {
                                                isNoJob && <InfoButton
                                                  disabled={ !this.constr.isActiveVersion }
                                                  onClick={ this.constr.isActiveVersion ? this.constr.confirmAppendNode : undefined }
                                                  className={ cx(styles.addNode, styles.rotate) }
                                                  icon={ 'signs-delete-close' }
                                                />
                                            }
                                        </>
                                    }
                                    {
                                        !this.constr.isTreePathSelectMode &&
                                        this.constr.selectedNodes.size > 1 &&
                                        this.constr.userCanWrite && (
                                          <DangerButton
                                            pending={ this.constr.upsertNodePending }
                                            disabled={ !this.constr.isActiveVersion }
                                            withoutBorders
                                            outline
                                            className={ cx(styles.addNode, styles.remove) }
                                            onClick={ this.constr.confirmRemoveNode }
                                            icon={ "account-trash-delete-bin" }
                                            title={ loc._(t`basic.delete`) }
                                            text={ loc._(t`basic.delete`) }
                                          />
                                        )
                                    }
                                    { !this.constr.isAnyNeedToSave && !this.constr.isTreeEmpty && this.constr.isTreePathSelectMode && this.constr.userCanWriteFact &&

                                    <DefaultButton
                                      disabled={ this.constr.selectedNodes.size === 0 || !this.constr.isActiveVersion }
                                      outline
                                      withoutBorders
                                      onClick={ this.constr.isActiveVersion ? this.modal.openModalInputFactValues : undefined }
                                      className={ cx(styles.addNode) }
                                      icon={ "business-chart-metrics" }
                                      title={ loc._(t`components.scheduleListBuilder.addSelectedNodesToForm`) }
                                    />
                                    }
                                    { this.view.canUploadModel() &&
                                    <DefaultButton
                                      disabled={ !this.constr.isActiveVersion }
                                      outline
                                      withoutBorders
                                      onClick={ this.constr.isActiveVersion ? this.constr.openModalUploadModel : undefined }
                                      icon={ "signs-plus-add" }
                                      text={ this.constr.isTreeEmpty ?
                                        loc._(t`components.scheduleListBuilder.uploadModel`) :
                                        loc._(t`components.scheduleListBuilder.updatedModel`)
                                      }
                                      className={ cx(styles.addNode) }
                                      textStyle={ { color: 'var(--basic-600)' } }
                                    />
                                    }
                                    {
                                        this.view.currentObject.calculation_method === 'weighting_factor' && this.constr.userCanReadWeightingFactor &&
                                        <Button
                                          disabled={ !this.constr.isActiveVersion }
                                          onClick={ this.constr.isActiveVersion ? this.constr.resetWeightingFactorModal : undefined }
                                          title={ loc._(t`components.scheduleListBuilder.resetWeightingFactor`) + " " + weightingFactor }
                                          icon={ 'signs-tree' }
                                          withoutBorders
                                          outline
                                          text={ weightingFactor }/>
                                    }
                                    { this.constr.userCanWriteVersions &&
                                    <Button
                                      outline
                                      withoutBorders
                                      onClick={ this.constr.toggleVersionForm }
                                      icon={ "shapes-basic-shapes-circle-b" }
                                      text={ <Trans>basic.versions</Trans> }
                                      className={ cx(styles.addNode) }
                                    />
                                    }
                                    {
                                        this.constr.acceptedFiles.length > 0 && this.constr.countUploadFiles > 0 &&
                                        <Button
                                          pending={ this.constr.pendingUpload }
                                          outline
                                          withoutBorders
                                          onClick={ () => this.constr.uploadScheduleFiles() }
                                          icon={ 'internet-cloud-upload' }
                                          text={ loc._(t`basic.upload`) + ` (${ this.constr.countUploadFiles })` }
                                        />
                                    }
                                </div>
                            </div>

                        </div>
                        <div className={ `${ styles.tableHeader } ${ styles.reports_header }` }>
                            <div className={ cx(styles.tableCol, styles.maxWidth) }></div>
                            {
                                this.view.currentObject.calculation_method === 'weighting_factor' && this.constr.userCanReadWeightingFactor &&
                                <div
                                  className={ styles.tableCol }>{ loc._(t`basic.weighting_factor`) }
                                </div>
                            }
                            <div className={ styles.tableCol }>{ loc._(t`basic.construction`) }</div>
                            <div className={ styles.tableCol }>{ loc._(t`basic.dateStart`) }</div>
                            <div className={ styles.tableCol }>{ loc._(t`basic.dateEnd`) }</div>
                            <div className={ styles.tableCol }>{ loc._(t`basic.measure`) }</div>
                            <div className={ styles.tableCol }>{ loc._(t`basic.planValue`) }</div>
                            <div className={ styles.tableCol }>{ loc._(t`basic.factValue`) }</div>
                            { this.constr.userCanReadExpenditure &&
                            <div className={ styles.tableCol }>{ loc._(t`basic.planCost`) }</div> }
                            { this.constr.userCanReadExpenditure &&
                            <div className={ styles.tableCol }>{ loc._(t`basic.actualCost`) }</div>
                            }
                            { this.constr.acceptedFiles.length > 0 &&
                            <div className={ styles.tableCol }/> }
                        </div>
                    </>
                  )
              }
              {
                  this.view.pending && <Spinner/>
              }
          </Fragment>
        )
    }
}

export default ScheduleListBuilderCardHeader
