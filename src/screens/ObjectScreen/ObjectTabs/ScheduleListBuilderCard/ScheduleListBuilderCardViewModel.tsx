import React from 'react'
import { inject } from "react-ioc"
import { action, computed, observable, runInAction, toJS } from "mobx"
import { DataContext } from "services/DataContext"
import { ApiService, createScheduleReq, updateScheduleReq } from "services/ApiService"
import { ObjectModel, ScheduleBuilder } from "models/mst"
import { AppService, ScrollDirection } from "services/AppService"
import { Modal, SimpleConfirmAbortModal } from 'components/UI/Modal'
import { getTreeFromFlatData, TreeItem } from "react-sortable-tree"
import { get, isEmpty } from 'lodash'
import {
    filterNestedNodes,
    getAllChildren,
    getAllParents,
    getUnselectParents,
    parseMeasureUnit,
    toggleExpandRecursive
} from 'utils'
import { applySnapshot, getSnapshot } from "mobx-state-tree"
import { t } from '@lingui/macro'
import { loc } from "utils/i18n"
import { ScheduleListRespMST } from "services/ApiService/Interfaces"
import { AuthService } from "services/AuthService"
import { canDelete, canUpdate, canWrite, ROLE, } from 'constants/permissions'
import { FILE_RELATION, UPLOAD_RESTRICTIONS } from 'constants/common'
import { toaster } from "components/UI/Toast"
import { BasicButton, DangerButton, Domain, InputSelect, Preloader } from 'components'
import { DropZone } from "components/DropZone"
import { ObjectViewModel } from "../../ObjectViewModel"
import dayjs from "dayjs"
import { makeHash } from "utils/helpers"
import styles from './styles.scss'
import colors from 'static/styles/colors.scss'
import { ACTION_TYPE, Badge } from "components/UI/Badge"
import { DictionaryService } from "../../../../services/DictionaryService"
import { FileLoaderService, LocalisationService } from "services"
import { Spinner } from "components/UI/Spinner"
import { SPREAD_SHEETS_MIME_TYPES } from "../../../../models/entities/FileModel"
import { FILE_VALIDATION } from "../../../../constants"
import { FileListModel } from "../../../../models/entities/FileListModel"

class ScheduleListBuilderCardViewModel {

    public defaultTitle = loc._(t`components.scheduleListBuilder.newNode.name`)

    @inject app: AppService
    @inject dataContext: DataContext
    @inject api: ApiService
    @inject object: ObjectViewModel
    @inject auth: AuthService
    @inject dictionaryService: DictionaryService
    @inject localisation: LocalisationService
    @inject fileLoader: FileLoaderService

    @observable public pending = true

    @observable public pendingModalRequest = false
    @observable public selectedNodes: Set<number> = new Set<number>()
    @observable public treeHasChanged
    @observable isTreePathSelectMode = false
    @observable public editingNodesSnaps: Map<number, any> = new Map()
    @observable private uploadModelFinanceFormat: { key: string; value: string } | null = null
    @observable private uploadModelPending: boolean = false
    @observable public upsertNodePending = false
    @observable mediaFiles = new FileListModel(this.fileLoader, false)


    private FINANCE_FORMAT_ITEMS = [
        { key: '1', value: loc._(t`components.scheduleListBuilder.modalSelect.usual`) },
        { key: '1000', value: loc._(t`components.scheduleListBuilder.modalSelect.thousand`) },
    ]

    @computed
    public get userCanWrite(): boolean {
        return this.auth.userPermissions.has(canWrite(ROLE.MANAGER, 'schedule')) && this.object.canWrite('schedule')
    }

    @computed
    public get userCanDelete(): boolean {
        return this.auth.userPermissions.has(canDelete(ROLE.MANAGER, 'schedule')) && this.object.canWrite('schedule')
    }

    @computed
    public get userCanUpdate(): boolean {
        return this.auth.userPermissions.has(canUpdate(ROLE.MANAGER, 'schedule')) && this.object.canWrite('schedule')
    }

    @computed
    public get userCanWriteVersions(): boolean {
        return this.auth.userPermissions.has(canWrite(ROLE.MANAGER, 'schedule.version')) && this.object.canWrite('schedule')
    }

    @computed
    public get userCanWriteFact(): boolean {
        return (this.object.canWrite('foreman.fact') || this.object.canWrite('revisor.fact'))
    }

    @computed
    public get userCanReadExpenditure(): boolean {
        return this.object.canRead('schedule.expenditure')
    }

    @computed
    public get userCanWriteExpenditure(): boolean {
        return this.object.canWrite('schedule.expenditure')
    }

    @computed
    public get userCanWriteWeightingFactor(): boolean {
        return this.object.canWrite('schedule.weighting.factor')
    }

    @computed get userCanReadWeightingFactor(): boolean {
        return this.object.canRead('schedule.weighting.factor')
    }

    @computed
    public get isAnyNeedToSave(): boolean {
        return this.editingNodesSnaps.size !== 0
    }

    @computed
    public get lastNode(): number {
        return [ ...this.selectedNodes ][0]
    }

    @observable activeScheduleVersion: number

    @computed get isActiveVersion() {
        return this.activeScheduleVersion ? this.currentObject.currentScheduleBuilderVersion === this.activeScheduleVersion : true
    }

    @action setActiveVersion = (e) => this.activeScheduleVersion = e

    @action
    public init = async (currentScheduleVersion?: number, isActive: boolean = false) => {
        const object_id = Number(this.object.objectIdByRoute)
        if (!currentScheduleVersion) {
            this.pending = true
        }
        try {
            if (object_id) {
                const reqBody = { object_id }
                if (currentScheduleVersion) {
                    Object.assign(reqBody, { schedule_version_id: currentScheduleVersion })
                    if (isActive) {
                        this.activeScheduleVersion = currentScheduleVersion
                    }
                }

                await this.dictionaryService.init()
                const schedule = await this.dataContext.cacheFirst(this.api.schedule.getScheduleList, reqBody, { noCaching: !!currentScheduleVersion })
                if (schedule.status !== 'error' && schedule.status !== 'cached') {
                    this.dataContext.applySnapshot(schedule.body)
                    this.dataContext.object.get(`${ object_id }`).schedule_builder = get(schedule.body, 'schedule_list', [])
                }
                this.isTreePathSelectMode = this.userCanWriteFact
            }
        } catch (e) {
            console.log(e)
        } finally {
            runInAction(() => this.pending = false)
        }
    }

    @computed get countNodes() {
        let count = 0
        this.dataContext.schedule.forEach(item => {
              if (item.object_id === +this.object.objectIdByRoute) {
                  ++count
              }
          }
        )
        return count
    }

    @computed get currentObject(): ObjectModel {
        return this.dataContext.object.get(this.object.objectIdByRoute)
    }

    @computed get isWeightingFactor() {
        return this.currentObject.calculation_method === 'weighting_factor'
    }

    @action toggleSelectedMode = () => {
        this.isTreePathSelectMode = !this.isTreePathSelectMode
        this.selectedNodes.clear()
    }

    @computed
    public get isTreeEmpty() {
        return this.treeFromFlatData.length === 0
    }

    @computed
    public get referenceJobOptions() {
        return [ { job_id: 0, name: 'Этап модели объекта' }, ...this.dataContext.reference['job'].values() ].map(el => (
          <option key={ el.job_id } value={ el.job_id }>{ el[`name_${ this.localisation.lang }`] }</option>))
    }

    @computed
    public get jobUnitDictionary(): string[] {
        return [ ...this.dataContext.reference['job_unit'] ]
    }

    @computed
    public get generateJobUnitListOption(): {}[] {
        return this.jobUnitDictionary.map((item, i) => {
              return ({
                  id: i,
                  value: item,
                  name: parseMeasureUnit(item) // loc._(isNil(MEASURE_UNITS[item]) ? item : MEASURE_UNITS[item])
              })
          }
        )
    }

    @computed
    public get jobDictionary() {
        return [ ...this.dataContext.reference['job'].values() ]
    }

    @action
    public generateElementDictionary(job: any) {
        return [ ...this.dataContext.reference['element'].values() ]
          .filter(item => item['job_element_group'] === get(this.dataContext.reference.job.get(job), 'job_element_group'))
    }

    @computed
    public get jobDictionaryTitles() {
        return [ ...this.dataContext.reference['job'].values() ].map(item => item[`name_${ this.localisation.lang }`])
    }

    public getParentsFromChildren(children: Array<any>) {
        let parents = []
        children.forEach((item) => {
            const node = this.dataContext.schedule.get(item)
            node && parents.push(node.schedule_id)
            if (!isEmpty(node.parent_id)) {
                parents.push(this.getParentsFromChildren([ node ]))
            }
        })
        return parents
    }

    @computed
    public get selectedNodesTree() {
        let parents = this.getParentsFromChildren([ ...this.selectedNodes ])
        let treeData = []
        parents.forEach((item) => {
            const node = this.dataContext.schedule.get(item)
            node && treeData.push(node)
        })
        return treeData
    }

    @computed
    public get currentScheduleBuilder() {
        return this.dataContext.object.get(`${ this.object.objectIdByRoute }`).schedule_builder
    }

    @action
    public setCurrentScheduleBuilder(arr) {
        this.dataContext.object.get(`${ this.object.objectIdByRoute }`).schedule_builder = arr
    }

    @computed
    public get treeFromFlatData(): Array<ScheduleBuilder> {
        return getTreeFromFlatData<ScheduleBuilder, number, number, null>({
            flatData: this.currentScheduleBuilder,
            getKey: node => node.schedule_id,
            getParentKey: node => node.parent_id,
            rootKey: null
        })
    };

    @action
    public onVisibilityToggle = ({ node, expanded }) => {
        this.dataContext.schedule.get(node.schedule_id).expanded = expanded
        this.onChange()
    }

    @action
    public onChange = () => {
        this.treeHasChanged = !this.treeHasChanged
    }

    @action
    public onSelectNode = (node: any, path: Array<number | string>) => {
        const currentNode = path[path.length - 1]
        const underNodePath = getAllChildren(node).map(item => item.schedule_id)
        const overNodePath = getAllParents(node).map(item => item.schedule_id)
        const isAllChildrenSelected = item => get(item, 'children', []).every(item => this.selectedNodes.has(item.schedule_id))
        if (this.selectedNodes.has(+currentNode)) {
            this.selectedNodes.delete(+currentNode)
            underNodePath.forEach(item => this.selectedNodes.delete(+item))

            if (this.isTreePathSelectMode) {
                const unselectedParents = getUnselectParents(node, [ ...this.selectedNodes ], 'schedule_id')
                unselectedParents.forEach(item => this.selectedNodes.delete(item))
            } else {
                overNodePath.forEach(item => this.selectedNodes.delete(item))
            }
        } else {
            this.selectedNodes.add(+currentNode)
            underNodePath.forEach(item => this.selectedNodes.add(+item))

            if (this.isTreePathSelectMode) {
                overNodePath.forEach(item => this.selectedNodes.add(+item))
            } else {
                getAllParents(node)
                  .forEach(item => isAllChildrenSelected(item) && this.selectedNodes.add(item.schedule_id))
            }
        }
    }

    @action
    public resetSelectedNodes = () => {
        this.selectedNodes.clear()
    }

    @action selectAllNodes = () => {
        this.dataContext.schedule.forEach(item => {
              if (item.object_id === +this.object.objectIdByRoute) {
                  this.selectedNodes.add(item.schedule_id)
              }
          }
        )
    }

    @action
    public confirmNodeChangesCancellation = (e: any, node: TreeItem): void => {
        e.stopPropagation()
        this.app.modalControls.renderModal = () => (
          <SimpleConfirmAbortModal
            title={ loc._(t`components.reportCard.modal.cancelEditTitle`) }
            children={ loc._(t`components.reportCard.modal.cancelEditBody`) }
            onConfirm={ () => {
                if (node.isVirtual) {
                    this.onSelectNode(node, [ node.schedule_id ])
                    this.dataContext.schedule.delete(`${ node.schedule_id }`)
                } else {
                    const target = this.dataContext.schedule.get(`${ node.schedule_id }`)
                    const source = this.editingNodesSnaps.get(node.schedule_id)
                    if (target && source) {
                        applySnapshot(target, toJS(source))
                    }
                }
                this.editingNodesSnaps.delete(node.schedule_id)

                this.app.modalControls.close()
                this.onChange()
            } }
            onAbort={ () => {
                this.resetSelectedNodes()
                this.app.modalControls.close()
            } }
          />
        )

        this.app.modalControls.open()
    }

    @action
    public removeNode = async (node?: number) => {
        this.upsertNodePending = true
        let allSelected = [ node ]
        let schedule_id = [ node ]
        if (!node) {
            allSelected = [ ...this.selectedNodes ]
            schedule_id = filterNestedNodes(this.currentScheduleBuilder, [ ...this.selectedNodes ])
        }

        const { status } = await this.api.schedule.delete({ schedule_id })
        if (status === 200) {
            runInAction(() => {
                allSelected.forEach(item => this.dataContext.schedule.delete(String(item)))
                this.selectedNodes.clear()
                this.upsertNodePending = false
                this.onChange()
            })
            await this.updateCurrentObject()
            toaster.success(loc._(t`components.scheduleListBuilder.deleteNodeSuccess`))
        } else {
            this.upsertNodePending = false
            toaster.error()
        }
    }

    @action
    public confirmRemoveNode = async (e, node?: number) => {
        e.stopPropagation()
        this.app.modalControls.renderModal = () => (
          <Modal
            children={
                <div className={ styles.modalContentContainer }>
                    <div className={ styles.col }>
                        <Domain labelColor={ colors.danger500 } icon={ 'account-trash-delete-bin' } text={ '' }/>
                    </div>
                    <div className={ styles.col }>
                        <h4>
                            { loc._(t`components.scheduleListBuilder.modal.removeNodeModalTitle`) }
                        </h4>
                        <div style={ { marginTop: '10px' } }>
                            <Badge style={ { fontSize: '1rem', marginRight: '5px' } } invert type={ ACTION_TYPE.DANGER }
                                   text={ `${ loc._(t`basic.attention`) }!` }/>
                            { loc._(t`components.scheduleListBuilder.modal.removeNodeModalBody`) }
                        </div>
                    </div>
                </div>
            }
            renderConfirm={ () => (
              <DangerButton
                text={ loc._(t`basic.delete`) }
                onClick={ () => {
                    this.app.modalControls.close()
                    this.removeNode(node)
                } }
                disabled={ false }
              />
            )
            }
            renderAbort={ () => (
              <BasicButton
                style={ { border: 'none' } }
                outline
                text={ loc._(t`basic.abort`) }
                onClick={ () => {
                    this.resetSelectedNodes()
                    this.app.modalControls.close()
                } }
              />
            )
            }
          />
        )
        this.app.modalControls.open()
    }

    @action
    private onMoveNode = async ({ treeData, node, nextParentNode }) => {
        try {
            const schedule_id = get(node, "schedule_id")
            const parentId = get(node, "parent_id.schedule_id", null)
            const new_parent_id = get(nextParentNode, 'schedule_id', null)
            const neighbours = new_parent_id ? get(nextParentNode, 'children') : treeData
            const after_schedule_id = neighbours && get(neighbours, '[0]') && neighbours[0] !== node
              ? neighbours[neighbours.indexOf(node) - 1].schedule_id
              : null

            if (parentId !== null) {
                this.dataContext.schedule.get(parentId).children = this.dataContext.schedule.get(parentId).children
                  .filter(el => el.schedule_id !== schedule_id)
            }
            this.dataContext.schedule.get(schedule_id).parent_id = new_parent_id
            this.dataContext.schedule.get(schedule_id).hash = makeHash(4)

            if (new_parent_id !== null) {
                const oldChildren = this.dataContext.schedule.get(new_parent_id).children
                  .map(el => el.schedule_id)
                const index = after_schedule_id ? oldChildren.indexOf(after_schedule_id) : -1
                const newChildren = oldChildren.slice(0, index + 1)
                  .concat(schedule_id, oldChildren.slice(index + 1))

                toggleExpandRecursive(this.dataContext.schedule.get(new_parent_id))
                this.dataContext.schedule.get(new_parent_id).children = newChildren
            }
            const newScheduleBuilder = treeData.map(item => item.schedule_id)

            this.setCurrentScheduleBuilder(newScheduleBuilder)
            await this.api.schedule.move({ schedule_id, new_parent_id, after_schedule_id })
            toaster.success(loc._(t`components.scheduleListBuilder.moveNodeSuccess`))
        } catch (e) {
            toaster.error()
        }
    }

    @action
    public confirmMoveNode = async ({ treeData, node, nextParentNode, prevTreeIndex, nextTreeIndex }) => {
        if (prevTreeIndex === nextTreeIndex) {
            return
        }
        await this.onMoveNode({ treeData, node, nextParentNode })
    }

    private generateNewNode(parentId: number): ScheduleListRespMST {
        const nodeId = new Date().getTime() * -1
        const currentVersion = this.currentObject.currentScheduleBuilderVersion

        return {
            schedule_id: nodeId,
            schedule_version_id: currentVersion,
            object_id: +this.object.objectIdByRoute,
            parent_id: parentId,
            name: this.defaultTitle,
            job_id: null,
            job_unit: null,
            volume_actual: 0,
            volume_planned: 0,
            expenditure_actual: 0,
            expenditure_planned: 0,
            weighting_factor: 0,
            date_start: dayjs().format('YYYY-MM-DD'),
            date_finish: dayjs().add(1, 'week').format('YYYY-MM-DD'),
            meta: {},
            children: [],
            expanded: false,
            isVirtual: true,
            parent_path: `${ nodeId }`,
            invalidFields: {},
            hash: makeHash(4),
            sort_order: 9999
        }
    };

    @action
    public appendVirtualNode = async (parent: any) => {
        const virtualNode = this.generateNewNode(parent ? parent.schedule_id : null)
        this.editingNodesSnaps.set(virtualNode.schedule_id, { ...virtualNode })
        this.dataContext.schedule.put(virtualNode)
        if (parent) {
            parent.children.push(virtualNode.schedule_id)
            parent.expanded = true
        } else {
            this.currentScheduleBuilder.push(virtualNode.schedule_id)
            this.app.scrollTo(ScrollDirection.DOWN)
        }
        this.onChange()
    }

    @action
    public saveNodeChanges = (node: TreeItem): void => {
        this.dataContext.schedule.put(node)
    }


    @observable
    public isAnyNodeWasUpdated = false

    @action
    public updateNode = async (e: any, node: TreeItem): Promise<any> => {
        console.log('update')
        e.stopPropagation()
        const dataNode = this.dataContext.schedule.get(node.schedule_id)
        dataNode.pending = true
        const {
            children,
            expanded,
            isVirtual,
            model_job_id,
            parent_id,
            project_model_id,
            meta, sort_order,
            parent_path,
            invalidFields,
            hash,
            ...sendingObject
        } = getSnapshot(dataNode)

        if (!this.isWeightingFactor || !node.job_id || !this.userCanWriteWeightingFactor) {
            delete sendingObject.weighting_factor
            delete sendingObject.reset_weighting_factor
        } else {
            sendingObject.reset_weighting_factor = false
        }

        try {
            const response = await this.api.schedule.update({ ...sendingObject, comment: "update from browser" } as updateScheduleReq)
            if (response.status === 200) {
                this.editingNodesSnaps.delete(node.schedule_id)
                node.isVirtual = false
                node.pending = false

                if (!(!this.isWeightingFactor || !node.job_id || !this.userCanWriteWeightingFactor)) {
                    this.isAnyNodeWasUpdated = true
                }

                await this.updateCurrentObject()
                this.onChange()
                toaster.success(loc._(t`basic.success.request.create`))
            } else {
                throw new Error()
            }
        } catch (e) {
            dataNode.pending = false
            toaster.error(loc._(t`error.scheduleBuilder.node.update`))
            this.app.modalControls.renderModal = () => (
              <SimpleConfirmAbortModal
                title={ loc._(t`basic.error`) }
                children={ loc._(t`components.scheduleListBuilder.modal.errorSavingNodeModalBody`) }
                onConfirm={ this.app.modalControls.close }
              />
            )
            this.app.modalControls.open()
        }
    }

    @action
    public createNode = async (e: any, node: TreeItem, recalculate: boolean = false): Promise<any> => {
        e.stopPropagation()
        const dataNode = this.dataContext.schedule.get(node.schedule_id)
        dataNode.pending = true
        const nodeSnap: ScheduleListRespMST = getSnapshot(dataNode)
        const {
            children,
            expanded,
            isVirtual,
            sort_order,
            invalidFields,
            hash,
            parent_path,
            schedule_id,
            ...sendingObject
        } = nodeSnap

        const parentId = get(node, 'parent_id.schedule_id') || null
        const parent = this.dataContext.schedule.get(parentId)
        sendingObject.date_start = dayjs(node.date_start).format('YYYY-MM-DD')
        sendingObject.date_finish = dayjs(node.date_finish).format('YYYY-MM-DD')
        sendingObject.object_id = +this.object.objectIdByRoute
        sendingObject.parent_id = parentId
        sendingObject.reset_weighting_factor = recalculate
        if (!sendingObject.job_id || !this.isWeightingFactor || !this.userCanWriteWeightingFactor) {
            delete sendingObject.weighting_factor
            delete sendingObject.reset_weighting_factor
        }

        try {
            const response = await this.api.schedule.create(sendingObject as createScheduleReq)
            if (response.status === 200) {
                const oldVirtualId = node.schedule_id
                const newNodeId = get(response, 'body.schedule.schedule_id')
                this.editingNodesSnaps.delete(oldVirtualId)
                this.dataContext.schedule.delete(String(oldVirtualId))
                const newNode = get(response, 'body.schedule')
                newNode.children = []
                newNode.expanded = false
                newNode.isVirtual = false
                newNode.invalidFields = {}
                newNode.hash = makeHash(4)

                this.dataContext.schedule.put(newNode)
                if (parent) {
                    parent.children.push(String(newNodeId))
                } else {
                    this.currentScheduleBuilder.push(String(newNodeId))
                }
                this.onChange()
                toaster.success(loc._(t`basic.success.request.create`))
            } else {
                throw new Error()
            }
        } catch (e) {
            dataNode.pending = false
            toaster.error(loc._(t`error.scheduleBuilder.node.create`))
        }
    }

    @action
    public createNodeModal = async (e: any, node: TreeItem) => {
        if (!this.isWeightingFactor || !node.job_id || !this.userCanWriteWeightingFactor) {
            this.upsertNodePending = true
            await this.createNode(e, node, false)
            this.upsertNodePending = false
        } else {
            this.upsertNodePending = true
            await this.createNode(e, node, false)
            this.upsertNodePending = false
        }
        await this.updateCurrentObject()
    }

    @action resetWeightingFactorModal = async () => {
        this.app.modalControls.renderModal = () => (
          <SimpleConfirmAbortModal
            // title={ loc._(t`components.scheduleListBuilder.modal.createModalTitle`) }
            children={
                <>
                    { this.upsertNodePending && <Preloader/> }
                    { !this.upsertNodePending && loc._(t`components.scheduleListBuilder.modal.createModalBody`) }
                </>
            }
            onConfirm={ async () => {
                this.upsertNodePending = true
                await this.api.object.resetWeightingFactor({ object_id: this.currentObject.object_id })
                await this.updateCurrentObject()
                await this.init(this.currentObject.currentScheduleBuilderVersion)
                this.upsertNodePending = false
                this.app.modalControls.close()
            } }
            onAbort={ async () => {
                this.app.modalControls.close()
            } }
            confirmIsDisabled={ this.upsertNodePending }
            abortIsDisabled={ this.upsertNodePending }
            onSuccessText={ loc._(t`components.scheduleListBuilder.modal.yesCalculate`) }
            onAbortText={ loc._(t`components.scheduleListBuilder.modal.notCalculate`) }
          />
        )
        this.app.modalControls.open()
    }

    @action updateCurrentObject = async () => {
        //update object
        const res = await this.dataContext.cacheFirst(this.api.object.list, {
            object_id: [
                this.currentObject.object_id
            ]
        }, { noCaching: true })
        if (res.status !== 'error') {
            this.dataContext.applySnapshot(res.body)
        }
    }

    @action
    public onDirty = (node: any) => {
        if (!this.editingNodesSnaps.has(node.schedule_id)) {
            const nodeSnap: ScheduleListRespMST = getSnapshot(this.dataContext.schedule.get(`${ node.schedule_id }`))
            this.editingNodesSnaps.set(node.schedule_id, nodeSnap)
            this.onChange()
        }
    }

    @action
    public confirmAppendNode = async (e, node?: TreeItem) => {
        e.stopPropagation()
        const parent = node || this.dataContext.schedule.get(`${ [ ...this.selectedNodes ][0] }`)

        if (!parent) {
            this.app.modalControls.renderModal = () => (
              <SimpleConfirmAbortModal
                title={ loc._(t`components.scheduleListBuilder.modal.addRootNodeModalTitle`) }
                children={ loc._(t`components.scheduleListBuilder.modal.addRootNodeModalBody`) }
                onConfirm={ async () => {
                    this.app.modalControls.close()
                    await this.appendVirtualNode(undefined)

                } }
                onAbort={ () => {
                    this.resetSelectedNodes()
                    this.app.modalControls.close()
                } }
              />
            )
            this.app.modalControls.open()
        } else {
            this.appendVirtualNode(parent)
        }
    }

    @observable isVersionFormOpen = false
    @action toggleVersionForm = () => this.isVersionFormOpen = !this.isVersionFormOpen

    @action
    public canDrop = ({ node, prevPath, nextPath, nextParent }): boolean => {
        if (get(nextParent, "job_id")) {
            return false
        }
        if (get(node, "job_id")) {
            return true
        }
        return prevPath.length === nextPath.length
    }

    private isFileTypeCorrect = (File: File): boolean => {
        const extension = File.name.split('.').pop().toLowerCase()
        return UPLOAD_RESTRICTIONS.OBJECT_MODEL_TYPE.includes(extension)
    }

    @action
    public openModalUploadModel = async (e) => {
        e.stopPropagation()
        this.app.modalControls.renderModal = () => (
          <Modal
            title={
                <div className={ styles.uploadModalTitle }>
                    <Domain labelColor={ colors.cyan500 } icon={ 'account-calendar-schedule-add' } text={ '' }/>
                    <div><h4>{ loc._(t`components.scheduleListBuilder.modalTitle.uploadModel`) }</h4></div>
                </div>
            }
            children={
                <div className={ styles.uploadModalChildren }>
                    { this.uploadModelPending && <div className={ styles.uploadModalSpinner }><Spinner/></div> }
                    <div className={ styles.line }>
                        <InputSelect
                          placeholder={ loc._(t`components.scheduleListBuilder.modalSelect.placeholder`) }
                          items={ this.FINANCE_FORMAT_ITEMS }
                          itemName='value'
                          keyName='key'
                          onChange={ selection => runInAction(() => this.uploadModelFinanceFormat = selection) }
                          initialItem={ this.uploadModelFinanceFormat }
                        />
                    </div>
                    <div className={ styles.line }>
                        { this.mediaFiles.files.size === 0
                          ? (
                            <DropZone accept={ SPREAD_SHEETS_MIME_TYPES }
                                      onFilesSelect={ async (File: File[]) => {
                                          try {
                                              if (File.length !== 1) {
                                                  toaster.error(!File.length ? loc._(FILE_VALIDATION.wrongFormat) : loc._(FILE_VALIDATION.wrongCount))
                                              } else if (!this.isFileTypeCorrect(File[0])) {
                                                  toaster.error(loc._(t`components.scheduleListBuilder.uploadModal.wrongType`))
                                              } else {
                                                  await this.mediaFiles.push(File, FILE_RELATION.OBJECT, +this.object.objectIdByRoute)
                                              }
                                          } catch (e) {
                                              toaster.error(e.message)
                                          }
                                      } }/>
                          )
                          : <div className={ styles.uploadModalFileNameContainer }>
                              <Badge text={ `${ loc._(t`components.scheduleListBuilder.modalSelect.selectedFile`) }` }/>
                              <span className={ styles.uploadModalFileName }>
                                    { get([ ...this.mediaFiles.files.values() ], '[0].native.native.path') }
                                </span>
                          </div>
                        }
                    </div>
                </div>
            }
            renderConfirm={ () => (
              <BasicButton
                text={ loc._(t`basic.upload`) }
                onClick={ async () => {
                    this.uploadModelPending = true
                    try {
                        const result = await this.api.object.uploadModel(
                          [ ...this.mediaFiles.files.values() ][0].native,
                          this.currentObject.object_id,
                          this.uploadModelFinanceFormat.key
                        )
                        if (result.status === 'error') {
                            return toaster.error(result.errorCode === 400 && loc._(t`components.scheduleListBuilder.errorUploadModel`))
                        }

                        // TODO: надо посоветоваться с Егором и порефакторить это место
                        const object_id = this.currentObject.object_id
                        const scheduleVersion = await this.dataContext.cacheFirst(this.api.schedule.versionList, { object_id }, { noCaching: true })
                        if (scheduleVersion.status !== 'error' && scheduleVersion.status !== 'cached') {
                            this.dataContext.applySnapshot({ ...scheduleVersion.body })
                            this.dataContext.object.get(`${ object_id }`).schedule_version = [ ...Object.keys(scheduleVersion.body.schedule_version) ]

                            const schedule = await this.dataContext.cacheFirst(this.api.schedule.getScheduleList, { object_id }, { noCaching: true })
                            if (schedule.status !== 'error' && schedule.status !== 'cached') {
                                this.dataContext.schedule_list.clear()
                                this.dataContext.applySnapshot(schedule.body)
                                this.dataContext.object.get(`${ object_id }`).schedule_builder = get(schedule.body, 'schedule_list', [])
                            }
                        }
                        this.dataContext.table_state.clear()

                        this.uploadModelFinanceFormat = null
                        this.mediaFiles.files.clear()
                        return toaster.success(loc._(t`components.scheduleListBuilder.modalOk`))
                    } catch (e) {
                        return toaster.error()
                    } finally {
                        this.app.modalControls.close()
                        this.uploadModelPending = false
                    }
                } }
                className={ styles.uploadConfirmButton }
                disabled={ !this.uploadModelFinanceFormat || this.mediaFiles.files.size === 0 }
              />
            ) }
            renderAbort={ () => (
              <BasicButton
                text={ loc._(t`basic.abort`) }
                onClick={ () => {
                    this.uploadModelFinanceFormat = null
                    this.mediaFiles.files.clear()
                    this.app.modalControls.close()
                } }
                className={ styles.uploadAbortButton }
              />
            ) }
            footerClassName={ styles.uploadModalFooter }
          />
        )
        this.app.modalControls.open({ preventEscape: true, preventClickAway: true })
    }

    @computed get acceptedFiles() {
        if (!this.currentObject) return []
        let accepted = []
        if (this.currentObject.canWrite('schedule.file')) {
            accepted.push(
              ...Object.values(UPLOAD_RESTRICTIONS.DOCUMENTS_MIME_TYPES),
              ...Object.values(UPLOAD_RESTRICTIONS.SPREAD_SHEETS_MIME_TYPES))
        }
        return accepted
    }

    @computed get filesUploadDisabled() {
        return this.countUploadFiles >= 10
    }

    @observable scheduleFiles: Map<number, FileListModel> = new Map<number, FileListModel>()

    @computed get countUploadFiles() {
        return [ ...this.scheduleFiles ].map(item => item[1].size).reduce((acc, item) => acc + item, 0)
    }

    @action
    public addFileToUpload = async (fileList: Array<File>, entityId: number) => {
        if (this.scheduleFiles.get(entityId)) {
            await this.scheduleFiles.get(entityId).push(fileList, FILE_RELATION.SCHEDULE, entityId)
        } else {
            const fileListModel = new FileListModel(this.fileLoader, false)
            await fileListModel.push(fileList, FILE_RELATION.SCHEDULE, entityId)
            this.scheduleFiles.set(entityId, fileListModel)
        }
    }

    @observable pendingUpload = false

    @action
    public uploadScheduleFiles = async () => {
        try {
            this.pendingUpload = true
            for (const list of this.scheduleFiles.values()) {
                await list.uploadFiles()
                list.files.clear()
            }
            toaster.success(loc._(t`basic.success.request.create`))
        } catch (e) {
            toaster.error(loc._(t`error.files.upload`))
        } finally {
            runInAction(() => this.pendingUpload = false)
        }
    }
}

export {
    ScheduleListBuilderCardViewModel
}
