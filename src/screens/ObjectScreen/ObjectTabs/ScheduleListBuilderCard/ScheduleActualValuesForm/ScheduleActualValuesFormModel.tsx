import { action, computed, observable } from "mobx"
import { FILE_RELATION, UPLOAD_RESTRICTIONS } from "../../../../../constants"
import { FileListModel } from "../../../../../models/entities/FileListModel"
import { get, some } from "lodash"
import { toaster } from "../../../../../components/UI/Toast"
import { loc } from "../../../../../utils/i18n"
import { t } from "@lingui/macro"
import { inject } from "react-ioc"
import { ObjectViewModel } from "../../../ObjectViewModel"
import { ObjectModel } from "../../../../../models/mst/ObjectModel"
import { ApiService, AppService, AuthService, DataContext, FileLoaderService } from "../../../../../services"
import { ScheduleActualValuesModal } from "./ScheduleActualValuesModal"
import React from "react"
import { getSnapshot } from "mobx-state-tree"
import { ScheduleListBuilderCardViewModel } from "../ScheduleListBuilderCardViewModel"

class ScheduleActualValuesFormModel {
    @inject object: ObjectViewModel
    @inject dataContext: DataContext
    @inject fileLoader: FileLoaderService
    @inject app: AppService
    @inject api: ApiService
    @inject constr: ScheduleListBuilderCardViewModel
    @inject auth: AuthService

    @observable public modalPending = false
    @observable mediaFiles = new FileListModel(this.fileLoader, false)

    @computed get currentObject(): ObjectModel {
        return this.dataContext.object.get(this.object.objectIdByRoute)
    }

    @observable actualValuesMediaFiles: Map<number, FileListModel> = new Map<number, FileListModel>()

    @computed get countUploadFiles() {
        return [ ...this.actualValuesMediaFiles ].map(item => item[1].size).reduce((acc, item) => acc + item, 0)
    }

    @computed get filesUploadDisabled() {
        return this.countUploadFiles >= 10
    }

    @computed get isAllowWriteEmployees() {
        return this.object.canWrite('schedule.fact.employee')
    }

    @computed
    public get formFactCostNodes() {
        try {
            const treeCopy = getSnapshot(this.dataContext.schedule)
            const model_jobs = getSnapshot(get(this.dataContext, 'reference.job'))

            const selectedTree = {}
            this.constr.selectedNodes.forEach((item) => {
                const node = treeCopy[`${ item }`]
                const job = model_jobs[node.job_id]
                node && Object.assign(selectedTree, {
                    [node.schedule_id]: {
                        schedule_id: node.schedule_id,
                        parent_id: node.parent_id,
                        name: node.name,
                        job_unit: node.job_unit,
                        job_id: job,
                        expanded: true,
                        employee: node.employee,
                        volume: 0,
                        expenditure: 0,
                        comment: ''
                    }
                })
            })
            return selectedTree
        } catch (e) {
            return null
        }
    }

    @action
    public openModalInputFactValues = () => {
        this.app.modalControls.renderModal = () =>
          <ScheduleActualValuesModal
            isAllowWriteEmployees={this.isAllowWriteEmployees}
            uploadDisabled={ this.filesUploadDisabled }
            acceptedFiles={ this.acceptedFiles }
            actualValuesMediaFiles={ this.actualValuesMediaFiles }
            addFileToFactValues={ this.addFileToFactValues }
            tree={ this.formFactCostNodes } onConfirm={ this.saveBatchFactValues }
            onClose={ this.app.modalControls.close } pending={ this.modalPending }/>
        this.app.modalControls.open({ preventEscape: true, preventClickAway: true })
    }

    @action
    public addFileToFactValues = async (fileList: Array<File>, entityId: number) => {
        if (this.actualValuesMediaFiles.get(entityId)) {
            await this.actualValuesMediaFiles.get(entityId).push(fileList, FILE_RELATION.SCHEDULE_FACT, entityId)
        } else {
            const fileListModel = new FileListModel(this.fileLoader, false)
            fileListModel.push(fileList, FILE_RELATION.SCHEDULE_FACT, entityId)
            this.actualValuesMediaFiles.set(entityId, fileListModel)
        }
    }

    @computed get acceptedFiles() {
        if (!this.currentObject) return []
        let accepted = []
        if (this.currentObject.canWrite('foreman.fact') || this.currentObject.canWrite('revisor.fact')) {
            accepted.push(...Object.values(UPLOAD_RESTRICTIONS.IMAGES_MIME_TYPES))
        }
        if (this.currentObject.canUpload('schedule.fact.file')) {
            accepted.push(...Object.values(UPLOAD_RESTRICTIONS.DOCUMENTS_MIME_TYPES), ...Object.values(UPLOAD_RESTRICTIONS.SPREAD_SHEETS_MIME_TYPES))
        }
        return accepted
    }

    @action
    public saveBatchFactValues = async (fact: Array<any>) => {
        try {
            if (this.modalPending) return
            this.modalPending = true
            const promises = []
            fact.forEach(item => {
                promises.push(this.api.scheduleFact.create({
                    schedule_id: item.schedule_id,
                    comment: item.comment,
                    fact_type: this.object.canWrite('foreman.fact') ? "foreman" : "revisor",
                    expenditure: item.expenditure,
                    employee: item.employee,
                    volume: item.volume,
                    fact_source: "web"
                }))
            })

            const result = await Promise.all(promises)

            let files = []

            result.forEach(((res) => {
                files.push(new Promise(resolve => {
                    if (get(res, 'body.schedule_fact.schedule_fact_id')) {
                        const schedule_fact_id = get(res, 'body.schedule_fact.schedule_fact_id')
                        const schedule_id = get(res, 'body.schedule_fact.schedule_id')
                        const scheduleFactFiles = this.actualValuesMediaFiles.get(schedule_id)
                        if (scheduleFactFiles) {
                            scheduleFactFiles.files.forEach(file => file.setRelation(schedule_fact_id))
                            scheduleFactFiles.uploadFiles().then(res => resolve(res)).catch(() => resolve({ status: 'error' }))
                        } else {
                            resolve(true)
                        }
                    }
                }))
            }))

            const filesResult = await Promise.all(files)
            this.actualValuesMediaFiles.clear()

            some(result, (item) => get(item, 'status') === 'error') ?
              toaster.error(loc._(t`error.fact.create`)) :
              toaster.success(loc._(t`basic.success.request.create`))
            some(filesResult, (item) => get(item, 'status') === 'error') && toaster.error(loc._(t`error.files.upload`))

        } catch (e) {
            toaster.error(loc._(t`error.fact.create`))
        } finally {
            this.constr.resetSelectedNodes()
            this.modalPending = false
            this.app.modalControls.close()
        }
    }

}

export { ScheduleActualValuesFormModel }
