import { SimpleConfirmAbortModal } from "../../../../../components/UI/Modal/SimpleConfirmAbortModal"
import { loc } from "../../../../../utils/i18n"
import { t } from "@lingui/macro"
import styles from "./styles.scss"
import { updatedDiff } from 'deep-object-diff'
import { ScheduleActualValuesForm } from "./ScheduleActualValuesForm"
import React, { Fragment, useState } from "react"
import cx from 'classnames'
import { isEmpty, pickBy } from "lodash"
import { EditableField } from "../../../../../components/EditableField"
import { FileListModel } from "../../../../../models/entities/FileListModel"

export interface IScheduleActualValuesModalProps {
    onClose: (any?: any) => any
    tree: any
    onConfirm: (any?: any) => any
    pending: boolean
    addFileToFactValues: any
    actualValuesMediaFiles: Map<number, FileListModel>
    acceptedFiles: Array<string>
    uploadDisabled?: boolean
    isAllowWriteEmployees?: boolean
}

export const ScheduleActualValuesModal: React.FC<IScheduleActualValuesModalProps> = (
  {
      onClose,
      tree,
      onConfirm,
      pending,
      addFileToFactValues,
      actualValuesMediaFiles,
      acceptedFiles,
      uploadDisabled= false,
      isAllowWriteEmployees= false
  }) => {

    const [ comment, setComment ] = useState('')
    const [ localTree, setLocalTree ] = useState(tree)
    const [ initialStateJobNodes ] = useState(pickBy(tree, (val) => !isEmpty(val.job_id)))


    const saveTree = () => {
        const modifiedTreeJobNodes = pickBy(localTree, (val) => !isEmpty(val.job_id))
        const nodesOnSave = []
        Object.entries(updatedDiff(initialStateJobNodes, modifiedTreeJobNodes)).forEach((item) => {
            if (!isEmpty(item[1]) && !Object.values(item[1]).some(item => +item === 0)) {
                nodesOnSave.push({ ...modifiedTreeJobNodes[item[0]], comment })
            }
        })
        nodesOnSave.length > 0 && onConfirm(nodesOnSave)
    }

    const onVisibilityToggle = ({ node, expanded }) => {
        node.expanded = expanded
        setLocalTree((prevTree) => {
            return {
                ...prevTree,
                [node.schedule_id]: node
            }
        })
    }

    const onSaveNode = (node) => {
        setLocalTree((prevTree) => {
            return {
                ...prevTree,
                [node.schedule_id]: node
            }
        })
    }


    const renderCols = () => (
      <Fragment>
          <div className={ cx(styles.tableCol, styles.maxWidth) }>
              { loc._(t`component.scheduleActualValuesForm.header.worksList`) }
          </div>
          <div className={ styles.tableCol }>{ loc._(t`basic.measure`) }</div>
          <div className={ styles.tableCol }>{ loc._(t`basic.value`) }</div>
          <div className={ styles.tableCol }>{ loc._(t`basic.cost`) }</div>
          { isAllowWriteEmployees && <div className={ styles.tableCol }>{ loc._(t`basic.employees`) }</div> }
          <div className={ styles.tableCol }>{ loc._(t`basic.attachments`) }</div>
      </Fragment>
    )

    return (
      <SimpleConfirmAbortModal
        confirmIsDisabled={ pending }
        abortIsDisabled={ pending }
        onAbortText={ loc._(t`basic.escape`) }
        onSuccessText={ loc._(t`basic.save`) }
        onAbort={ onClose }
        onConfirm={ saveTree }
        titleClassName={ styles.titleWrapper }
        title={
            <Fragment>
                <div className={ styles.modalTitle }>
                    <div className={ styles.left }>
                        <h4> { loc._(t`components.scheduleListBuilder.modal.createListFactValues.title`) }</h4>
                    </div>
                    <div className={ styles.right }>
                        <EditableField className={ styles.editableField } value={ '' } label={ '' }
                                       placeholder={ loc._(t`components.scheduleListBuilder.modal.createListFactValues.writeComment`) }
                                       onBlur={ (value) => setComment(value) }/>
                    </div>
                </div>
                <div className={ cx(styles.table, styles.relative) }>
                    <div className={ styles.tableHeader }>
                        { renderCols() }
                    </div>
                </div>
            </Fragment>
        }
      >
          { <ScheduleActualValuesForm
            isAllowWriteEmployees={isAllowWriteEmployees}
            disabled={ uploadDisabled }
            acceptedFiles={ acceptedFiles }
            actualValuesMediaFiles={ actualValuesMediaFiles }
            addFileToFactValues={ addFileToFactValues } onSaveNode={ onSaveNode }
            pending={ pending }
            onVisibilityToggle={ onVisibilityToggle }
            className={ styles.actualValuesForm }
            tree={ localTree }/> }
      </SimpleConfirmAbortModal>)
}
