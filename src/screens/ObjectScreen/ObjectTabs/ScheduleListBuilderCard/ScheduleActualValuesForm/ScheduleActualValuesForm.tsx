import React, { FC, Fragment, useEffect, useRef, useState } from "react"
import SortableTree, { getTreeFromFlatData, NodeRendererProps } from "react-sortable-tree"
import { get } from 'lodash'
import cx from 'classnames'
import { loc } from 'utils/i18n'
import { t, Trans } from '@lingui/macro'
import 'react-sortable-tree/style.css'
import styles from "./styles.scss"
import { useLifecycles } from "react-use"
import { ScheduleBuilder } from "models/mst"
import { observer } from "mobx-react-lite"
import { SCHEDULE_NODE_TYPE, ScheduleNode, Spinner } from "components"
import useFunctionAsState from "../../../../../utils/helpers/hooks"
import { FileListModel } from "../../../../../models/entities/FileListModel"
import { Icon } from "../../../../../components/UI/Icon"
import { DropZone } from "../../../../../components/DropZone"

type IProps = {
    tree: any,
    className: string,
    onSaveNode: (any: any) => any,
    onVisibilityToggle: (any: any) => any
    pending: boolean
    addFileToFactValues: any
    actualValuesMediaFiles: Map<number, FileListModel>
    acceptedFiles: Array<string>
    disabled: boolean
    isAllowWriteEmployees: boolean
};

const ScheduleActualValuesForm: FC<IProps> = observer((
  {
      tree,
      className,
      onSaveNode,
      onVisibilityToggle,
      pending,
      addFileToFactValues,
      actualValuesMediaFiles,
      acceptedFiles,
      disabled,
      isAllowWriteEmployees
  }): JSX.Element => {

    const getFileSelector = (entityId) => {
        return (File: Array<File>) => {
            addFileToFactValues(File, entityId)
        }
    }

    const wrapper: React.MutableRefObject<HTMLDivElement> = useRef()
    const [ containerWidth, setWidth ] = useState(0)
    const [ selectedNode, toggleSelectNode ] = useState(null)
    const [ currentSelectionFunc, toggleCurrentSelectionFunc ] = useFunctionAsState(null)

    useEffect(() => setWidth(get(wrapper, 'current.clientWidth', 0)), [ containerWidth ])

    const renderTree = () => {
        return getTreeFromFlatData<ScheduleBuilder, number, number, null>({
            flatData: [ ...Object.values(tree) ],
            getKey: (node: any) => node.schedule_id,
            getParentKey: (node: any) => node.parent_id,
            rootKey: null
        })
    }

    const windowResizeHandler = () => setWidth(get(wrapper, 'current.clientWidth', 0))
    useLifecycles(() => window.addEventListener('resize', windowResizeHandler),
      () => window.removeEventListener('resize', windowResizeHandler))

    const renderCols = () => (
      <Fragment>
          <div className={ cx(styles.tableCol, styles.maxWidth) }>
              { loc._(t`component.scheduleActualValuesForm.header.worksList`) }
          </div>
          <div className={ styles.tableCol }>{ loc._(t`basic.measure`) }</div>
          <div className={ styles.tableCol }>{ loc._(t`basic.value`) }</div>
          <div className={ styles.tableCol }>{ loc._(t`basic.cost`) }</div>
          <div className={ styles.tableCol }>{ loc._(t`basic.employees`) }</div>
          <div className={ styles.tableCol }>{ loc._(t`basic.attachments`) }</div>
      </Fragment>
    )

    const renderNodeProps = ({ node }: { node: ScheduleBuilder }) => {
        return ({
            onNodeClick: (node) => {
                toggleSelectNode(get(node, 'schedule_id') === get(selectedNode, 'schedule_id') ? null : node)
                toggleCurrentSelectionFunc(getFileSelector(get(node, 'schedule_id')))
            },
            onSave: (text, label, _) => onSaveNode({ ...node, [label]: text }),
            type: SCHEDULE_NODE_TYPE.ACTUAL_VALUES_FORM,
            isUploadAllow: acceptedFiles.length > 0,
            isAllowWriteEmployees
        })
    }

    const nodeRenderer = (props: NodeRendererProps) => <ScheduleNode { ...props }  />

    const hasFiles = actualValuesMediaFiles.get(get(selectedNode, 'schedule_id'))

    return (
      <div ref={ wrapper } className={ cx(styles.componentWrapper, className, { [styles.pending]: pending }) }>
          { tree.length === 0 ? <Trans>basic.noDataAvailable</Trans> :
            <Fragment>
                { !pending &&
                <div className={ styles.constructorContainer }>
                    { acceptedFiles.length > 0 &&
                    <div key={ get(selectedNode, 'schedule_id') }
                         className={ cx(styles.fileLoaderContainer, { [styles.isOpen]: selectedNode }) }>
                        <h5>{ get(selectedNode, 'name', '---') }</h5>
                        <DropZone disabled={ acceptedFiles.length === 0 || disabled } accept={ acceptedFiles }
                                  disabledMessage={ loc._(t`components.scheduleActualValuesForm.uploadDisabled`) }
                                  onFilesSelect={ currentSelectionFunc }/>
                        <div className={ styles.filesContainer }>
                            { hasFiles &&
                            [ ...actualValuesMediaFiles.get(get(selectedNode, 'schedule_id')).files.values() ].map(file => {
                                return <div className={ styles.file } key={ file.name }>
                                    <div>
                                        { file.name }
                                    </div>
                                    <div>
                                        <Icon name={ 'signs-delete-close-c' } onClick={ () => {
                                            actualValuesMediaFiles.get(get(selectedNode, 'schedule_id')).files.delete(file.name)
                                        } }/>
                                    </div>
                                </div>
                            })
                            }
                        </div>
                    </div>
                    }
                    <div className={ styles.treeContainer }>
                        <div style={ { width: containerWidth } } className={ styles.table }>
                            <div className={ styles.tableBody }>
                                { renderCols() }
                            </div>
                        </div>
                        <SortableTree
                          isVirtualized={ false }
                          onVisibilityToggle={ onVisibilityToggle }
                          treeData={ renderTree() }
                          generateNodeProps={ renderNodeProps }
                          getNodeKey={ ({ node }) => node.schedule_id }
                          onChange={ () => null }
                          canDrag={ false }
                          nodeContentRenderer={ nodeRenderer }
                        />
                    </div>
                </div>
                }
                {
                    pending && <Spinner/>
                }
            </Fragment>
          }
      </div>
    )
})

export {
    ScheduleActualValuesForm
}
