import { action, computed, observable, runInAction } from "mobx"
import { inject } from "react-ioc"
import { ApiService, FileLoaderService, RouterService } from "services"
import { FILE_RELATION, ROUTES } from "constants/index"
import { get } from "lodash"
import { parsePath } from "utils/helpers"
import { FileListModel } from "models/entities/FileListModel"
import { toaster } from "components/UI/Toast"
import { loc } from "../../../../utils/i18n"
import { t } from "@lingui/macro"

export class ObjectGalleryViewModel {

    @inject fileLoader: FileLoaderService
    @inject api: ApiService
    @inject router: RouterService

    @observable mediaFiles = new FileListModel(this.fileLoader)

    @observable
    public wasInitialized = false
    @observable
    public pending = false

    @observable
    selectedMedia = null

    @observable
    public currentMedia = 0
    @observable
    public lastCurrentInLightBox = -1

    @observable
    public isLightBoxOpen = false

    @action init = async () => {
        try {
            if (!this.wasInitialized) {
                this.pending = true
                const result = await this.api.files.imageList({
                    image_relation: FILE_RELATION.OBJECT,
                    image_relation_id: [ this.objectIdByRoute ],
                    extended: true,
                    sort: {
                        created: -1
                    }
                })
                if (result.status !== 'error' && get(result, 'body.image')) {
                    const images = get(result, 'body.image')
                    await this.mediaFiles.push(images, FILE_RELATION.OBJECT, this.objectIdByRoute)
                    // for (let item in images) {
                    //     this.mediaFiles.push(new FileModel(images[item]))
                    // }
                }
            }
        } catch (e) {
            console.log(e)
        } finally {
            runInAction(() => {
                this.wasInitialized = true
                this.pending = false
            })
        }
    }

    @computed
    public get filteredMediaFilesValues() {
        return [ ...this.mediaFiles.files.values() ]
          .filter(item => item.relationType === FILE_RELATION.OBJECT)
    }

    @computed
    public get objectIdByRoute(): number {
        return Number(get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId'))
    }

    @action
    public toggleLightBox = (index?: number) => {
        if (this.isLightBoxOpen) {
            this.lastCurrentInLightBox = index
        } else {
            this.currentMedia = index
        }
        this.isLightBoxOpen = !this.isLightBoxOpen
    }

    @action
    public onSelectDay(day) {
        console.log('day as callback', day)
    }

    @action
    public onSelectToUpload = async (Files: Array<File>) => {
        try {
            if (Files.length > 10) {
                toaster.info(loc._(t`components.gallery.maxFilesCountSelect`))
            } else {
                await this.mediaFiles.push(Files, FILE_RELATION.OBJECT, this.objectIdByRoute)
            }
        } catch (e) {
            toaster.error(e.message)
        }
    }
}
