import React from 'react'
import { Gallery } from "components/Gallery"
import { useInstance } from "react-ioc"
import { ObjectGalleryViewModel } from "./ObjectGalleryViewModel"
import { observer } from "mobx-react-lite"
import { useAsync } from "react-use"
import { ObjectViewModel } from "../../ObjectViewModel"

export const ObjectGallery: React.FC = observer(() => {
    const view = useInstance(ObjectGalleryViewModel)
    const viewObject = useInstance(ObjectViewModel)
    useAsync(view.init, [])
    return <Gallery onSelectToUpload={ view.onSelectToUpload }
                    mediaFiles={ view.filteredMediaFilesValues }
                    selectedMedia={ viewObject.mainImageId }
                    onSelectDay={ view.onSelectDay }
                    isLightBoxOpen={ view.isLightBoxOpen }
                    toggleLightBox={ view.toggleLightBox }
                    currentMedia={ view.currentMedia }
                    lastCurrentMedia={ view.lastCurrentInLightBox }
                    pending={ view.pending }
                    initialized={ view.wasInitialized }
                    selectAsMain={ viewObject.selectAsMain }
                    isUploadAllow={ viewObject.canWrite('image')}
    />
})
