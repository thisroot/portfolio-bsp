import React from "react"
import { observer } from "mobx-react-lite"
import styles from './styles.scss'
import { loc } from "utils/i18n"
import { t } from "@lingui/macro"
import { get } from "lodash"
import { MetaInfoEditor } from "components/MetaInfoEditor"
import { useInstance } from "react-ioc"
import { ObjectViewModel } from "../../ObjectViewModel"
import { LocalisationService } from "../../../../services/LocalisationService"

export const PassportCard: React.FC = observer(() => {
    const view = useInstance(ObjectViewModel)
    const lang = useInstance(LocalisationService)

    return <div className={ styles.passportCard }>
        <MetaInfoEditor
          metaTypes={ view.metaTypeFields }
          onChangeMetaType={ view.changeMetaType }
          editorTitle={ loc._(t`component.object.screen.objectInfo`) }
          canEdit={ view.userCanEditMeta }
          isEditMode={ view.isEditMode }
          isDirty={ get(view.metaform, 'isDirty') }
          toggleEdit={ view.toggleEdit }
          onSave={ view.saveMeta }
          resetEdit={ view.resetEdit }
          fields={ view.metaFormFields }
          toggleChangeTypeView={ view.toggleChangeMetaTypeMode }
          isChangeTypeView={ view.isChangeMetaTypeMode }
          currentType={ view.currentMetaType }
          lang={ lang.lang }
        />
    </div>
})