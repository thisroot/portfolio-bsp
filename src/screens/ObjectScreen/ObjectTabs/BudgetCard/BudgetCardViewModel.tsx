import { action, computed, observable, toJS } from "mobx"
import { loc } from "../../../../utils/i18n"
import { t } from "@lingui/macro"
import { AnalyticsService, ApiService, AppService, DataContext, RouterService } from "../../../../services"
import { get } from "lodash"
import { parsePath } from "../../../../utils"
import { ROUTES, SERVER_DATE_MASK } from "../../../../constants"
import { inject } from "react-ioc"
import React from "react"
import { AddBudgetModal } from "./AddBudgetModal"
import dayjs from "dayjs"
import utc from 'dayjs/plugin/utc'
import { Button, BUTTON_TYPE } from "../../../../components"
import { toaster } from "../../../../components/UI/Toast"

dayjs.extend(utc)

class BudgetCardViewModel {

    @inject analytics: AnalyticsService
    @inject api: ApiService
    @inject dataContext: DataContext
    @inject router: RouterService
    @inject app: AppService

    @observable
    public pending = true
    @observable
    public wasInitialized = false

    @action
    public init = async () => {
        try {
            const result = await this.api.object.budgetList({ object_id: +this.objectIdByRoute, extended: true })
            if (result.status !== 'error') {
                this.dataContext.applySnapshot(result.body)
            }

        } catch (e) {
            toaster.error(e)
            this.analytics.sendError(e)
        } finally {
            this.wasInitialized = true
            this.pending = false
        }
    }

    @computed
    public get list() {
        return {
            columns: {
                id: '',
                date: [ loc._(t`basic.date`), 0.5 ],
                expenditure: loc._(t`screens.objectTabs.budget.table.col.mastering`),
                expenditure_sd: loc._(t`screens.objectTabs.budget.table.col.masteringOnSD`),
                update: [ '', 0.06 ],
                remove: [ '', 0.06 ]
            },
            rows: this.dataContext.budget_plan_list.map(budget => {
                return {
                    id: budget.budget_plan_id,
                    ...budget,
                    date: dayjs.utc(budget.date).format(SERVER_DATE_MASK),
                    update: <Button buttonType={ BUTTON_TYPE.INFO } withoutBorders
                                    onClick={ () => this.onClickAddBudgetBtn(budget.budget_plan_id) }
                                    icon={ 'edit-pen-line' }
                    />,
                    remove: <Button buttonType={ BUTTON_TYPE.DANGER } withoutBorders onClick={ () => this.onRemove(budget.budget_plan_id) }
                                    icon={ 'account-trash-delete-bin' }/>
                }
            }),
            hidden: [ 'id' ]
        }
    }

    @computed
    public get currentObject() {
        return this.dataContext.object.get(this.objectIdByRoute)
    }

    @computed
    public get objectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId')
    }

    @computed
    public get userCanWriteBudget() {
        return this.currentObject.canWrite('suek.budget.plan')
    }

    @action
    public onClickAddBudgetBtn = async (budgetId?: number) => {
        this.app.modalControls.renderModal = () => <AddBudgetModal budgetId={ budgetId }/>
        this.app.modalControls.open({ preventEscape: true, preventClickAway: true })
    }

    @action
    public onRemove = async (bugetId: number) => {
        try {
            const result = toJS(this.dataContext.budget_plan_list.filter(item => item.budget_plan_id !== bugetId))
            this.dataContext.budget_plan_list.clear()
            this.dataContext.budget_plan_list.push(...result)
            await this.api.object.budgetDelete({ budget_plan_id: [ bugetId ] })
            this.dataContext.budget_plan.delete(`${ bugetId }`)
        } catch (e) {
            this.analytics.sendError(e)
        }
    }
}

export {
    BudgetCardViewModel
}
