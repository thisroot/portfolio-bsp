import { observer } from "mobx-react-lite"
import { loc } from "../../../../../utils/i18n"
import { t } from "@lingui/macro"
import styles from "./styles.scss"
import { DatePicker, PICKER_FORM, SimpleConfirmAbortModal, Spinner } from "../../../../../components"
import React, { useEffect } from "react"
import { provider, useInstance } from "react-ioc"
import { AddBudgetModalViewModel } from "./AddBudgetModalViewModel"
import dayjs from "dayjs"
import { SERVER_DATE_MASK } from "../../../../../constants"
import { useAsync } from "react-use"
import { Input } from 'components/UI'
import utc from 'dayjs/plugin/utc'
import { runInAction } from "mobx"

dayjs.extend(utc)

export interface IAddBudgetProps {
    budgetId?: number
}

const AddBudget: React.FC<IAddBudgetProps> = observer(({ budgetId }) => {

    const view = useInstance(AddBudgetModalViewModel)

    useAsync(async () => {
        await view.initFilterForm()
    }, [])

    useEffect(() => {
        if (budgetId) {
            view.budgetId = budgetId
            const budget = view.dataContext.budget_plan.get(`${ budgetId }`)
            runInAction(() => {
                view.filterFields.get(view.fields.date.name).value = dayjs.utc(budget.date).format(SERVER_DATE_MASK)
                view.filterFields.get(view.fields.expenditure.name).value = budget.expenditure
                view.filterFields.get(view.fields.expenditure_sd.name).value = budget.expenditure_sd
            })
        }
    }, [ budgetId ])

    const getStatus = (field) => field.error && field.error.length > 0 ? 'error' : ''

    return <SimpleConfirmAbortModal
      abortIsDisabled={ view.pendingAddBudget }
      confirmIsDisabled={ view.pendingAddBudget }
      onSuccessText={ loc._(t`basic.save`) }
      title={ loc._(t`screens.objectTabs.budget.modal.title`) }
      onConfirm={ view.onConfirm }
      onAbort={ view.onAbort }
    >
        <div className={ styles.addBudgetFormContainer }>
            {
                view.formInitialized ? <>
                    <div className={ styles.left }>
                        <div className={ styles.inputField }>
                            <h5>
                                { loc._(view.fields.expenditure.placeholder) }
                            </h5>
                            <Input
                              name={ view.fields.expenditure.name }
                              disablePlaceholderOnTop
                              { ...view.filterForm.$(view.fields.expenditure.name).bind() }
                              status={ getStatus(view.filterForm.$(view.fields.expenditure.name)) }
                              clearForm={ () => view.filterForm.$(view.fields.expenditure.name).onChange("") }
                              placeholder={ loc._(view.fields.expenditure.placeholder) }
                            />
                        </div>
                        <div className={ styles.inputField }>
                            <h5>
                                { loc._(view.fields.expenditure_sd.placeholder) }
                            </h5>
                            <Input
                              name={ view.fields.expenditure_sd.name }
                              disablePlaceholderOnTop
                              { ...view.filterForm.$(view.fields.expenditure_sd.name).bind() }
                              status={ getStatus(view.filterForm.$(view.fields.expenditure_sd.name)) }
                              clearForm={ () => view.filterForm.$(view.fields.expenditure_sd.name).onChange("") }
                              placeholder={ loc._(view.fields.expenditure_sd.placeholder) }
                            />
                        </div>
                    </div>
                    <div className={ styles.right }>
                        <DatePicker
                          disabled={ !!budgetId }
                          form={ PICKER_FORM.inline }
                          value={ view.filterFields.get(view.fields.date.name).value }
                          onDayChange={ (e) => {
                              view.filterFields.get(view.fields.date.name).value = dayjs.utc(e).format(SERVER_DATE_MASK)
                          } }
                          disabledDays={ view.filledDays }
                        />
                    </div>
                </> : <Spinner/>
            }

        </div>
    </SimpleConfirmAbortModal>
})

const AddBudgetModal = provider()(AddBudget)
AddBudgetModal.register(AddBudgetModalViewModel)

export {
    AddBudgetModal
}


