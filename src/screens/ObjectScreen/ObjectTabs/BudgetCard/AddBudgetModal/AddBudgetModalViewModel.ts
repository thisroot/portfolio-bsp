import { action, computed, observable } from "mobx"
import { BasicFilterViewModel } from "../../../../../components/Filters/BasicFilter"
import { FIELDS } from "../../../../../constants/forms"
import dayjs from "dayjs"
import { ROUTES } from "../../../../../constants"
import { inject } from "react-ioc"
import { AnalyticsService, ApiService, AppService, DataContext, RouterService } from "../../../../../services"
import { get } from "lodash"
import { parsePath, required } from "../../../../../utils"
import { toaster } from "../../../../../components/UI/Toast"

class AddBudgetModalViewModel extends BasicFilterViewModel {

    @inject app: AppService
    @inject api: ApiService
    @inject router: RouterService
    @inject dataContext: DataContext
    @inject analytics: AnalyticsService

    @observable public pendingAddBudget = false

    @observable budgetId = null

    @computed
    public get fields() {
        return {
            date: {
                ...FIELDS.date,
                validators: [ required ]
            },
            expenditure: {
                ...FIELDS.expenditure,
            },
            expenditure_sd: {
                ...FIELDS.expenditure_sd,
            }
        }
    }

    @computed
    public get filledDays() {
        return this.dataContext.budget_plan_list.map(b => dayjs(b.date).toDate())
    }

    @action onConfirm = async (e) => {
        this.pendingAddBudget = true
        await this.submitForm(e)
        try {
            if (this.filterForm.isValid) {
                const result = this.budgetId ?
                  await this.api.object.budgetUpdate({ ...this.formValues, budget_plan_id: this.budgetId }) :
                  await this.api.object.budgetCreate({ ...this.formValues, object_id: +this.objectIdByRoute })
                if (result.status !== 'error') {
                    this.dataContext.budget_plan.put(result.body.budget_plan)
                    !this.budgetId && this.dataContext.budget_plan_list.push(result.body.budget_plan.budget_plan_id)
                }
                await this.resetForm(e)
                this.app.modalControls.close()
                toaster.success()
            } else {
                if (!this.filterFields.get(this.fields.date.name).value) {
                    toaster.error('date_not_present')
                }
            }
        } catch (e) {
            toaster.error()
            this.analytics.sendError(e)
        }
        this.pendingAddBudget = false
    }

    @computed
    public get objectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId')
    }

    @action onAbort = async (e) => {
        this.pendingAddBudget = true
        await this.resetForm(e)
        this.app.modalControls.close()
        this.pendingAddBudget = false
    }
}

export { AddBudgetModalViewModel }
