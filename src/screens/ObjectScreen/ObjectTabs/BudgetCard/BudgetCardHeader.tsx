import { observer } from "mobx-react-lite"
import styles from './styles.scss'
import React from "react"
import { useInstance } from "react-ioc"
import { BudgetCardViewModel } from "./BudgetCardViewModel"
import { Button } from "../../../../components"
import { loc } from "../../../../utils/i18n"
import { t } from "@lingui/macro"

export const BudgetCardHeader: React.FC = observer(() => {
    const view = useInstance(BudgetCardViewModel)
    return (
      <div className={ styles.budgetHeader }>
          <div className={ styles.left }>
              {
                  view.userCanWriteBudget && <Button
                    onClick={ () => view.onClickAddBudgetBtn() }
                    outline
                    withoutBorders
                    icon={ 'signs-plus-add-b' }
                    text={ loc._(t`screens.objectTabs.budget.addBudget`) }
                  />
              }
          </div>
      </div>
    )
})
