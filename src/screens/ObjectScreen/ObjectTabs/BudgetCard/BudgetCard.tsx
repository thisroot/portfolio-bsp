import { observer } from "mobx-react-lite"
import styles from './styles.scss'
import React from "react"
import { Table } from "../../../../components/Table"
import { useInstance } from "react-ioc"
import { BudgetCardViewModel } from "./BudgetCardViewModel"
import { useAsync } from "react-use"
import { Spinner } from "../../../../components"

export const BudgetCard: React.FC = observer(() => {
    const view = useInstance(BudgetCardViewModel)
    useAsync(async () => {
        await view.init()
        // await view.onClickAddBudgetBtn()
    }, [])
    return (
      <div className={ styles.budgetContainer }>
          { view.wasInitialized && <Table
            rows={ view.list.rows }
            columns={ view.list.columns }
            markFirstColumn={ false }
            hidden={ view.list.hidden }
          /> }
          { !view.wasInitialized && <Spinner/> }
      </div>
    )
})
