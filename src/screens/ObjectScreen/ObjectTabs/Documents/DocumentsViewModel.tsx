import React from 'react'
import { inject } from "react-ioc"
import { ApiService, AppService, AuthService, DataContext, DictionaryService, RouterService } from "services"
import { action, computed, observable } from "mobx"
import { loc } from "utils/i18n"
import { t } from "@lingui/macro"
import { toaster } from "components/UI/Toast"
import { get } from 'lodash'
import dayjs from "dayjs"
import { parsePath } from "../../../../utils/helpers"
import { ROUTES } from "../../../../constants"
import { Button, BUTTON_TYPE } from "../../../../components/UI/Button"
import { getSnapshot } from "mobx-state-tree"
import FileSaver from 'file-saver'
import styles from './styles.scss'

class DocumentsViewModel {
    @inject app: AppService
    @inject auth: AuthService
    @inject dataContext: DataContext
    @inject api: ApiService
    @inject router: RouterService
    @inject dictionary: DictionaryService

    @observable
    public pending = true

    @action
    public initialize = () => {
        this.pending = false
    }

    @computed
    public get currentObject() {
        return this.dataContext.object.get(this.objectIdByRoute)
    }

    @computed
    public get objectIdByRoute(): string {
        return get(parsePath(this.router.location.pathname, ROUTES.OBJECT_PAGE.ROUTE), 'params.objectId')
    }

    @computed
    public get userCanRemoveFile() {
        return this.currentObject ? this.currentObject.canDelete('schedule.fact.file') : false
    }

    @action
    public init = async () => {
        try {
            this.pending = true
            const result = await this.dataContext.cacheFirst(this.api.scheduleFact.list, {
                filter: {
                    object_id: +this.objectIdByRoute
                },
                extended: true
            }, { noCaching: true })

            if (result.status !== 'error' && result.status !== 'cached') {

                this.dataContext.schedule_fact.clear()
                this.dataContext.applySnapshot(result.body)
                if(result.body.file_list && result.body.file_list.length > 0) {
                    result.body.file_list.forEach(file => {
                        const fact = this.dataContext.schedule_fact.get(this.dataContext.file.get(file).file_relation_id)
                        fact.files.push(file)
                    })
                }
            }
        } catch (e) {
            console.log(e)
            toaster.error()
        } finally {
            this.initialize()
        }
    }


    @action deleteFile = async (file) => {
        if (this.userCanRemoveFile) {
            await this.api.files.deleteFile([ file.file_id ])
            const schedule = this.dataContext.schedule_fact.get(file.file_relation_id)
            if (schedule.files.length > 0) {
                const scheduleFiles = getSnapshot(schedule.files)
                schedule.files.clear()
                schedule.files.push(...scheduleFiles.filter(item => item !== file.file_id))
                this.dataContext.file.delete(`${ file.fileId }`)
            }
        }
    }

    @computed
    public get list() {
        try {
            return {
                columns: {
                    id: [ loc._(t`basic.id`), 0.1 ],
                    date: [ loc._(t`basic.date`), 0.1 ],
                    time: [ loc._(t`basic.time`), 0.1 ],
                    link: [ loc._(t`basic.fileName`), 0.3 ],
                    schedule_name: [ loc._(t`component.SchedulePlanFact.tableHeader.constructive`), 0.3 ],
                    spec: [ '', 0.1 ]
                },
                rows: [ ...this.dataContext.schedule_fact.values() ].map((item) => {
                    if (item.files.length > 0) {
                        return item.files.map(file => {
                            return (
                              {
                                  id: file.file_id,
                                  date: dayjs(file.created).format('DD.MM.YYYY'),
                                  time: dayjs(file.created).format('hh:mm'),
                                  link: <div className={ styles.link } onClick={ () => {
                                      FileSaver.saveAs(file.url, `${ file.name }`)
                                  } }>{ file.name }</div>,
                                  schedule_name: get(item, 'schedule_id.name'),
                                  spec: this.userCanRemoveFile && <Button onClick={ () => this.deleteFile(file) }
                                                                          icon={ 'account-trash-delete-bin' }
                                                                          buttonType={ BUTTON_TYPE.DANGER }/>
                              }
                            )
                        })
                    } else {
                        return null
                    }
                }).flat().filter(item => item)
            }
        } catch (e) {
            console.log(e)
            return { columns: [], rows: [] }
        }
    }

}


export {
    DocumentsViewModel
}
