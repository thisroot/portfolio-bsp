import { observer } from "mobx-react-lite"
import React from "react"
import {  useInstance } from "react-ioc"
import { DocumentsViewModel } from "./DocumentsViewModel"
import styles from "./styles.scss"

import { Table } from "components/Table"
import { useEffectOnce } from "react-use"
import { Preloader } from "../../../../components/UI/Preloader"

export const Documents: React.FC = observer(() => {
    const docView = useInstance(DocumentsViewModel)
    useEffectOnce(() => {
        docView.init()
    })
    return <div className={ styles.listContainer }>
        { docView.pending ? <Preloader /> : <Table rows={ docView.list.rows } columns={ docView.list.columns }/> }
          </div>
})
