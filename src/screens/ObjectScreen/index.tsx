import React from 'react'
import Loadable, { LoadableComponent } from '@loadable/component'
import { Preloader } from "components/UI/Preloader"

const AsyncObjectScreen: LoadableComponent<any> = Loadable(() => import(/* client-side */'./ObjectScreen'), {
  fallback: <Preloader /> } )

export { AsyncObjectScreen }