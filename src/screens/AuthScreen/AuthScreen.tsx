import React from "react"
import { observer } from "mobx-react"
import { provider } from "react-ioc"
import { AUTH_SCREENS, AuthViewModel } from "screens/AuthScreen/AuthViewModel"
import { RouterService } from 'services/RouterService'
import { inject } from "react-ioc"
import cx from 'classnames'
import styles from 'screens/AuthScreen/styles.scss'
import { SignInLayout } from "./Layouts/SignInLayout"
import { SignUpLayout } from "./Layouts/SignUpLayout"
import { Slider } from 'components/Slider'
import { CSSTransition } from 'react-transition-group'
import { Icon } from 'components/UI/Icon'
import { loc } from 'utils/i18n'
import { t } from '@lingui/macro'
import csp from 'static/images/logo-hor.svg'
import sakhalin from 'static/images/logo-hor-sakhalin.svg'
import rtlabs from 'static/images/rtlabs_mono_h.svg'
import slide1 from 'static/images/auth-slider/slide1.svg'
import slide2 from 'static/images/auth-slider/slide2.svg'
import bg from 'static/images/auth-slider/bg.svg'
import bg2 from 'static/images/auth-slider/bg2.svg'

const logo = process.env.APP_API_URL.match('rtlabs') ? rtlabs : process.env.APP_API_URL.match('sakhalin') ? sakhalin : csp

@observer
class Auth extends React.Component {

    @inject view: AuthViewModel
    @inject router: RouterService

    renderLayout() {
        switch (this.view.screen) {
            case AUTH_SCREENS.RESTORE:
                return <div id="designerContent">RESTORE</div>
            case AUTH_SCREENS.SIGN_UP:
                return <CSSTransition in={ true } unmountOnExit timeout={ 300 }><SignUpLayout/></CSSTransition>
            case AUTH_SCREENS.SIGN_IN:
            default:
                return <CSSTransition in={ true } unmountOnExit timeout={ 300 }><SignInLayout/></CSSTransition>
        }
    }

    render() {
        return <div className={ styles.authContainer }>
            <aside className={ styles.sidebar }>
                <div className={ styles.top }>
                    <img
                      src={ logo }
                      alt={ loc._(t`project.name`) }
                      className={ styles.logo }
                    />
                </div>
                <div className={ styles.middle }>
                    {
                        this.renderLayout()
                    }
                </div>
                <div className={ styles.bottom }>
                    <span className={ styles.policy }>
                        { loc._(t`auth.privacyPolicy`) }
                    </span>
                    <Icon name={ "arrows-arrow-r" } className={ styles.status }/>
                </div>
            </aside>
            <section className={ cx(styles.page, styles.authPage) }>
                <Slider
                  delay={ 10000 }
                  itemList={ [
                      {
                          src: slide1,
                          label: 'Цифровое строительство',
                          text: 'Прозрачный процесс для нестроителей: регуляторов, кредиторов, дольщиков ',
                          backgroundColor: 'linear-gradient(180deg, #FF8622 0%, #FFA255 100%)',
                          especialBg: bg,
                      }, {
                          src: slide2,
                          label: 'Аналитический модуль',
                          text: 'Cопоставляет плановые и фактические показатели проекта и представляет данные в виде понятных отчетов ',
                          backgroundColor: 'linear-gradient(180deg, #0680F9 0%, rgba(255, 255, 255, 0) 100%), #5BAAFB',
                          especialBg: bg2,
                      },
                  ] }
                />
            </section>
        </div>
    }
}

const AuthScreen = provider()(Auth)
AuthScreen.register(AuthViewModel)

export { AuthScreen }
