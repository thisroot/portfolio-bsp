import { inject } from "react-ioc"
import { AuthService, LocalisationService, RouterService } from "services"
import { action, observable } from "mobx"
import vjf from 'mobx-react-form/lib/validators/VJF'
import MobxReactForm from 'mobx-react-form'
import { FIELDS } from "constants/forms"
import React, { Fragment } from "react"
import { Input } from 'components/UI/Input'
import styles from './styles.scss'
import { t } from '@lingui/macro'

const plugins = {
    vjf: vjf()
}

const { email, password, name, lastname, organization_id, phone } = FIELDS

const signInFields = { email, password }
const signUpFields = { email, password, name, lastname, organization_id, phone }

enum AUTH_SCREENS {
    SIGN_IN,
    SIGN_UP,
    RESTORE
}


class AuthViewModel {

    @inject
    public authService: AuthService
    @inject
    public routerService: RouterService
    @inject
    public loc: LocalisationService
    @observable
    public screen = AUTH_SCREENS.SIGN_IN
    @observable
    public errorMessage = ''

    @action
    public sendForm = async (form: any) => {
        try {
            const result = form.size === 2 ?
              await this.authService.login(form.values()) :
              await this.authService.register(form.values())

            if (!result) {
                this.errorMessage = form.size === 2 ?
                  this.loc.i18n._(t`error.incorrect_email_or_password`) :
                  this.loc.i18n._(t`error.user_create_error`)
                form.clear()
            }
        } catch (e) {
            console.log(e)
        }
    }

    @action
    public onErrorForm = (form: any) => {
        console.log('error input form value', form.errors())
    }

    @action
    public goTo = (screen: AUTH_SCREENS) => {
        this.screen = screen
    }

    public renderFormField = (form: any, name: string): JSX.Element => {
        const formProps = form.$(name).bind()
        formProps.placeholder = this.loc.i18n._(formProps.placeholder);
        formProps.label = this.loc.i18n._(formProps.label);
        formProps.isHidden = name === 'password';
        formProps.status = form.$(name).errors()? 'error': ' ';
        formProps.className = styles.input;
        formProps.caption = form.$(name).errors();
        
        return (
          <Fragment>
                <Input { ...formProps } />
          </Fragment>
        )
    }

    @observable
    public signInForm = new MobxReactForm({ fields: signInFields }, {
        plugins,
        hooks: {
            onSuccess: this.sendForm,
            onError: this.onErrorForm
        }
    })
    @observable
    public signUpForm = new MobxReactForm({ fields: signUpFields }, {
        plugins,
        hooks: {
            onSuccess: this.sendForm,
            onError: this.onErrorForm
        }
    })
}

export {
    AuthViewModel,
    AUTH_SCREENS
}
