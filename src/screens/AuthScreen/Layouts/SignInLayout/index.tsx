import React, { Fragment } from "react"
import styles from 'screens/AuthScreen/Layouts/SignInLayout/styles.scss'
import { AuthViewModel } from "screens/AuthScreen/AuthViewModel"
import { observer } from "mobx-react-lite"
import { PrimaryButton } from "components/UI/Button"
import { useInstance } from "react-ioc"
import { Trans } from '@lingui/macro'

const SignInLayout = observer(() => {
    const props = useInstance(AuthViewModel)
    return <Fragment>
        <h1 className={styles.signIn}><Trans>basic.signIn</Trans></h1>
        <form className={ styles.signInForm }>
            { props.renderFormField(props.signInForm, 'email') }
            { props.renderFormField(props.signInForm, 'password') }
            <div className={ styles.submitBlock }>
                <div className={ styles.forgotPass }>
                    <Trans>basic.forgotPass</Trans>
                </div>
                <div className={ styles.submit}>
                    <PrimaryButton 
                        type={ "submit" } 
                        onClick={ props.signInForm.onSubmit }
                        text={ <Trans>basic.enterSystem</Trans> }
                    />
                </div>
            </div>
        </form>
    </Fragment>
})

export {
    SignInLayout
}
