import React  from "react"
import styles from "screens/AuthScreen/Layouts/SignUpLayout/styles.scss"
import { AuthViewModel, AUTH_SCREENS } from "screens/AuthScreen/AuthViewModel"
import { observer } from "mobx-react-lite"
import { PrimaryButton } from "components/UI"
import { useInstance } from "react-ioc"
import { Trans } from '@lingui/macro'

const SignUpLayout = observer(() => {
    const props = useInstance(AuthViewModel)
    return <div className={ styles.wrapper }>
        <div className={ styles.signUpContainer }>
            <PrimaryButton text={ <Trans>basic.login</Trans> } onClick={ () => props.goTo(AUTH_SCREENS.SIGN_IN) }/>
        </div>
        <h1><Trans>component.signUp.greetings</Trans></h1>
        <form className={ styles.signUpForm }>
            {/* <div className={ styles.two_fields }>
                { props.renderFormField(props.signUpForm, 'name', false) }
                { props.renderFormField(props.signUpForm, 'lastname', false) }
            </div>
            <div className={ styles.fieldError }>
                {
                    [ props.signUpForm.$('name').errors(), props.signUpForm.$('lastname').errors() ].join(' ')
                }
            </div>
            <div className={ styles.two_fields }>
                { props.renderFormField(props.signUpForm, 'phone', false) }
                { props.renderFormField(props.signUpForm, 'organization_id', false) }
            </div>
            <div className={ styles.fieldError }>
                {
                    [ props.signUpForm.$('phone').errors(), props.signUpForm.$('organization_id').errors() ].join(' ')
                }
            </div>
            { props.renderFormField(props.signUpForm, 'email', false) }
            { props.renderFormField(props.signUpForm, 'password', true, false) }
            <div className={ styles.fieldError }>
                {
                    props.errorMessage || props.signUpForm.$('password').errors()
                }
            </div>
            <div className={ styles.submitBlock }>
                <DefaultButton type={ "submit" } onClick={ props.signUpForm.onSubmit } width={ 200 }
                               text={ <Trans>basic.enterSystem</Trans> } icon="arrows-forward"/>
            </div> */ }
        </form>
    </div>
})

export {
    SignUpLayout
}
