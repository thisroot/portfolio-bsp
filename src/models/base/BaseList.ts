import { action, observable, computed, toJS } from 'mobx'

export class BaseList {
    @observable
    public pending = false

    @observable
    public next = null

    @observable
    public list = new Map()

    @observable
    public isInitialized = false

    @computed
    public get wasInitialized() {
        return this.isInitialized
    }

    @computed
    public get showDisclaimer() {
        return !this.pending && !this.get().length && this.isInitialized
    }

    protected loadFunc = null
    protected args = []
    protected getGenericInstance = null
    public parent = null

    constructor(loadFunc, getGenericInstance, parent, { args = [], next = {} } = {}) {
        this.loadFunc = loadFunc
        this.next = next
        this.args = args
        this.getGenericInstance = getGenericInstance
        this.parent = parent
    }

    @action
    public load = async (params = this.next, force = false) => {
        try {
            // if (this.pending) throw new Error(ERRORS.PENDING)
            this.pending = true

            /* resets list before a request to prevent old conent flickering */
            if (force) {
                this.clear()
            }

            const queryParams = params
            /**
             * toJS uses to properly handle arrays in query params,
             * for example { state: ['rejected', 'banned'] }
             */
            const response = await this.loadFunc(...this.args, toJS(queryParams))
            if (response) {
                this.push(response.result)
                this.next = response.next
            }
            return response
        } catch (error) {
            throw error
        } finally {
            this.pending = false
            if (!this.isInitialized) this.isInitialized = true
        }
    }

    public get() {
        return Array.from(this.list.values())
    }

    public getSorted(compare) {
        return this.get().sort(compare);
    }

    public getIndexByItemId(itemId: string): number {
        return Array.from(this.list.values()).map((item) => item._id).indexOf(itemId)
    }

    public getListItemById(itemId: string) {
        return this.list.get(itemId)
    }

    public deleteListItemById(itemId: string) {
        return this.list.delete(itemId)
    }

    public hasListItemWithId(itemId: string) {
        return this.list.has(itemId)
    }

    @action
    public push(items) {
        items.forEach((item) => {
            if (this.parent) item.__parent = this.parent
            if (!this.list.has(item._id)) {
                this.list.set(item._id, this.getGenericInstance ? this.getGenericInstance(item) : item)
            } else {
                this.list.get(item._id).updateEntity(item)
            }
        })
    }

    @action
    public clear() {
        this.list.clear()
        this.next = null
        if (this.isInitialized) this.isInitialized = false
    }

    @action
    public unshift(items) {
        const shiftItems = new Map()
        let itemsSkip = 0
        items.reverse().forEach((item) => {
            if (this.parent) item.__parent = this.parent
            if (!this.list.has(item._id)) {
                shiftItems.set(item._id, this.getGenericInstance ? this.getGenericInstance(item) : item)
                itemsSkip++
            } else {
                const gotItem = this.list.get(item._id)
                gotItem.updateEntity(item)
                // TODO: Надо ли удалять, если можно переписать?
                this.list.delete(item._id)
                shiftItems.set(item._id, gotItem)
            }
        })
        this.list = new Map([...shiftItems, ...this.list])
        if (this.next && this.next.skip) {
            this.next.skip += itemsSkip
        }
    }
}
