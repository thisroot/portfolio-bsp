import { FILE_RELATION } from "constants/common"

interface File {
    name: string
    mimetype: string
    url: string
}

export interface MediaPie {
    format: string
    url: string
}

export interface ImagePie extends MediaPie {
    width?: number
    height?: number
    [key: string]: any
}

export interface VideoPie extends MediaPie {
    latency?: number
}

export interface PieBox {
    [key: string]: ImagePie | VideoPie
}

export interface IRelationIdentifier {
    relationType?: FILE_RELATION,
    relationId?: number,
    parentId?: number
}

export interface Image extends File {
    image_id: number,
    image_relation?: string
    image_relation_id?: number
    created_by: number
    converted: PieBox
    parent_id?: number
    [key: string]: any
}

export interface Video extends File {
    video_id: number
    converted: PieBox
    parent_id?: number
}

export interface Document extends File {
    file_id: number
    file_relation?: string
    file_relation_id?: number
    parent_id?: number
    folder: boolean
    created: string
    created_by: number
    size: number
    ext: string
    digest: string
    uploaded: boolean
    url: string
    deleted: boolean
}
