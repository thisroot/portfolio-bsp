import { action, computed, observable, runInAction, toJS } from "mobx"
import { get } from 'lodash'
import { FILE_RELATION, UPLOAD_RESTRICTIONS } from "constants/common"
import { DocumentsOptions, Entity, FileTypes, ImagesOptions, VideoOptions } from "../../interfaces"
import { Image, PieBox, Video, IRelationIdentifier, Document } from "../File"


export const IMAGES_TYPES = UPLOAD_RESTRICTIONS.IMAGES_MIME_TYPES
export const VIDEO_TYPES = UPLOAD_RESTRICTIONS.VIDEO_MIME_TYPES
export const DOCUMENTS_TYPES = UPLOAD_RESTRICTIONS.DOCUMENTS_MIME_TYPES
export const SPREAD_SHEETS_TYPES = UPLOAD_RESTRICTIONS.SPREAD_SHEETS_MIME_TYPES

export const IMAGES_MIME_TYPES = Object.values(IMAGES_TYPES)
export const VIDEO_MIME_TYPES = Object.values(VIDEO_TYPES)
export const DOCUMENTS_MIME_TYPES = Object.values(DOCUMENTS_TYPES)
export const SPREAD_SHEETS_MIME_TYPES = Object.values(SPREAD_SHEETS_TYPES)

export const MEDIA_FILES_MIME_TYPES = [ ...IMAGES_MIME_TYPES, ...VIDEO_MIME_TYPES ]


export const IMAGES_PICKER_OPTIONS: ImagesOptions = {
    mimeTypes: MEDIA_FILES_MIME_TYPES,
    minWidth: UPLOAD_RESTRICTIONS.IMAGES_MIN_WIDTH,
    minHeight: UPLOAD_RESTRICTIONS.IMAGES_MIN_HEIGHT,
    maxFileSize: UPLOAD_RESTRICTIONS.IMAGES_MAX_FILE_SIZE,
    limit: UPLOAD_RESTRICTIONS.IMAGES_LIMIT_ARTICLE
}

export const VIDEO_PICKER_OPTIONS: VideoOptions = {
    mimeTypes: MEDIA_FILES_MIME_TYPES,
    maxFileSize: UPLOAD_RESTRICTIONS.VIDEOS_MAX_FILE_SIZE,
    maxDuration: UPLOAD_RESTRICTIONS.VIDEOS_MAX_DURATION,
    limit: UPLOAD_RESTRICTIONS.VIDEO_LIMIT_ARTICLE
}

export const DOCUMENTS_PICKER_OPTIONS: DocumentsOptions = {
    mimeTypes: [ ...DOCUMENTS_MIME_TYPES, ...SPREAD_SHEETS_MIME_TYPES ],
    maxFileSize: UPLOAD_RESTRICTIONS.DOCUMENTS_MAX_FILE_SIZE
}

const relationType = Symbol('relationType')
const relationId = Symbol('relationId')
const parentId = Symbol('parentId')

export class FileModel {

    native?: File
    @observable file: Image | Video | Document

    [relationId]: number
    [parentId]: number
    [relationType]: string

    @observable fromSnapshot: boolean = false

    @observable
    public status: { uploading: number, converting: number } = {
        uploading: 0,
        converting: 0
    }

    @observable public uploadError: boolean = false
    @observable public wasInitialised: boolean = false
    @observable public typeGroup: string
    @observable public animated: boolean = null
    @observable public localUrl: string

    constructor(nativeFile?: Entity | Image | Video | Document, relationIdentifier?: IRelationIdentifier) {
        switch (true) {
            case 'image_id' in nativeFile:
                this.file = nativeFile as Image
                this.typeGroup = FileTypes.IMAGE
                this.initFromSnapshot()
                break
            case 'video_id' in nativeFile:
                this.file = nativeFile as Video
                this.typeGroup = FileTypes.VIDEO
                this.initFromSnapshot()
                break
            case 'file_id' in nativeFile:
                this.file = nativeFile as Document
                this.typeGroup = FileTypes.FILE
                this.initFromSnapshot()
                break
            default:
                this.native = nativeFile as Entity
                this[relationType] = relationIdentifier.relationType
                this[relationId] = relationIdentifier.relationId || null
                this[parentId] = relationIdentifier.parentId || null
        }
    }

    @action
    public initFromSnapshot = () => {
        this.status = { uploading: 100, converting: 100 }
        this.fromSnapshot = true
    }

    @computed
    public get id(): number {
        return get(this.file, `${ this.typeGroup }_id`)
    }

    @computed
    public get relationType(): FILE_RELATION {
        return this[relationType] || get(this.file, `${ this.typeGroup }_relation`)
    }

    @computed get created(): string {
        return get(this.file, 'created')
    }

    @computed get createdBy(): number {
        return get(this.file, 'created_by')
    }

    @computed get deleted(): boolean {
        return get(this.file, 'deleted')
    }

    @computed get folder(): boolean {
        return get(this.file, 'folder', false)
    }

    @computed get parentId(): number {
        return this[parentId] || get(this.file, 'parent_id')
    }

    @computed get ext(): number {
        return get(this.file, 'ext')
    }

    @computed
    public get relationId(): number {
        return this[relationId] || get(this.file, `${ this.typeGroup }_relation_id`)
    }

    @action setRelation = (relation_id: number = this[relationId], relation: string = this[relationType], parent_id: number = this[parentId]) => {
        this[relationId] = relation_id
        this[relationType] = relation
        this[parentId] = parent_id
    }

    @computed
    public get pending(): boolean {
        return Object.values(this.status).some((item) => ((item !== 0) && (item !== 100)))
    }

    @computed get getSnapshot() {
        return toJS(this.file)
    }

    @computed
    public get isError(): boolean {
        return this.uploadError // or other registered errors
    }

    @computed
    public get isUploaded(): boolean {
        return this.status.uploading === 100
    }

    @computed
    public get isConverted(): boolean {
        return this.status.converting === 100
    }

    @computed
    public get isProcessing(): boolean {
        return this.isUploaded && this.isConverted
    }

    @computed
    public get isProcessed(): boolean {
        return this.status.uploading === 100 && this.status.converting === 100
    }

    @computed
    public get isProgressVisible(): boolean {
        return this.status.uploading > 0 && this.status.converting < 100
    }

    @computed
    public get size(): number {
        return this.fromSnapshot ? get(this.file, 'size') : get(this.native, 'size')
    }

    @computed
    public get name(): string {
        return this.fromSnapshot ? this.file.name : get(this.native, 'name')
    }

    @computed
    public get dataUrl(): string {
        return get(this, 'localUrl')
    }

    @computed get location(): PieBox {
        return this.typeGroup === FileTypes.FILE ? get(this.file, 'url') : this.fromSnapshot ?
          get(this.file, 'converted') :
          { big: { url: this.dataUrl, format: this.typeGroup }, small: { url: this.dataUrl, format: this.typeGroup } }
    }

    @computed get preview(): string {
        return get(this, 'location.small.url')
    }

    @computed get mediumView(): string {
        return get(this, 'location.medium.url')
    }

    @computed get fullView(): string {
        return get(this, 'location.big.url')
    }

    @action
    public onProgress = () => {
        return {
            upload: (event) => {
                runInAction(() => {
                    this.status.uploading = event.total === 0 ? 100 : Math.round(event.loaded / event.total * 100)
                })
            },
            convert: (event) => {
                runInAction(() => {
                    this.status.converting = get(event, 'target.readyState', false) ? 100 : 50
                })
            }
        }
    }
    @action
    public onRemove = () => {
        return false
    }

}
