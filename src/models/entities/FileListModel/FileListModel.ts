import { action, computed, observable } from "mobx"
import { FileModel } from "../FileModel"
import { Image, Video, Document } from "../File"
import { reduce, get } from "lodash"
import { FILE_RELATION } from "constants/index"
import { FileLoaderService } from "services/FileLoaderService"
import { t } from '@lingui/macro'
import { loc } from "utils/i18n"
import { FileTypes } from "../../interfaces"
import { toaster } from "../../../components/UI/Toast"

export class FileListModel {
    public fl: FileLoaderService
    public saveOnPush
    public needConvert

    constructor(loader: FileLoaderService, saveOnPush = true, needConvert = true) {
        this.fl = loader
        this.saveOnPush = saveOnPush
        this.needConvert = needConvert
    }

    @observable
    public files: Map<string | number, FileModel> = new Map<string | number, FileModel>()

    @observable
    public fileList: Array<string>

    @computed get size() {
        return this.files.size
    }

    @computed get pending() {
        return [ ...this.files.values() ].some((item) => item.pending)
    }

    @computed
    get isProcessed() {
        return [ ...this.files.values() ].every((item) => item.isProcessed)
    }

    @computed
    get filesSnapshot(): Array<Image | Video> {
        return reduce([ ...this.files.values() ], (acc, v: FileModel) => {
            v.file && acc.push(v.file)
            return acc
        }, [])
    }

    @action removeFile = (id: string | number) => {
        this.files.delete(id)
    }

    @action
    public uploadFiles = async () => {
        try {
            const promises = []
            /* upload files */
            this.files.forEach((file, key) => {
                if (!file.isUploaded) {
                    promises.push(new Promise((resolve) => {
                          this.fl.uploadFile(file, this.needConvert).then((uploadedFile) => {
                              let id
                              switch (file.typeGroup) {
                                  case FileTypes.IMAGE:
                                      id = 'image_id'
                                      break
                                  default:
                                      id = 'file_id'
                                      break
                              }
                              if (uploadedFile !== null) {
                                  this.files.delete(key)
                                  this.files.set(uploadedFile[id], new FileModel(uploadedFile, {
                                      relationType: file.relationType,
                                      relationId: file.relationId,
                                      parentId: file.parentId
                                  }))
                              }
                              resolve(null)
                          }).catch(() => {
                              resolve({ error: key })
                          })
                      }
                    ))
                }
            })
            const result = await Promise.all(promises)
            let isError = false

            result.forEach((item) => {
                if (get(item, 'error')) {
                    isError = true
                }
            })

            if (isError) {
                throw new Error(loc._(t`components.imagesListModal.errors.default`))
            }
        } catch (e) {
            throw e
        }
    }

    @action
    public push = async (fileList: Array<File> | { [key: number]: Image } | { [key: number]: Video } | { [key: number]: Document }, relationType: FILE_RELATION = FILE_RELATION.PROJECT, relationId?: number, parentId?: number) => {
        try {
            let attachments: { [key: number]: File | Image | Video | Document } = {}
            let isNative: boolean = true

            if (!get(fileList, 'length', false)) {
                attachments = fileList
                isNative = false
            } else {
                for (let index in fileList) {
                    Object.assign(attachments, { [new Date().getDate() + index]: fileList[index] })
                }
            }

            /* Append new attachments to the File List Register */
            for (const file in attachments) {
                const checkedFile = isNative ? await this.fl.initFile(attachments[file] as File, {
                    relationType,
                    relationId,
                    parentId
                }) : new FileModel(attachments[file])
                if(checkedFile.typeGroup) {
                    const checkedFileId = get(checkedFile, `file.${ checkedFile.typeGroup }_id`) ||
                      get(checkedFile, 'name')
                    this.files.set(checkedFileId, checkedFile)
                } else {
                    toaster.error('incorrect_file_extension')
                }
            }
            if (this.saveOnPush) {
                await this.uploadFiles()
            }
        } catch (e) {
            throw new Error(e)
        }
    }
}
