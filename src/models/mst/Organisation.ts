import { types as t, Instance } from "mobx-state-tree";
import { User } from "models/mst/User"

export const Organisation = t.model("Organisation", {
    organisation_id: t.identifierNumber,
    name_en: t.maybeNull(t.string),
    name_ru: t.maybeNull(t.string),
    description: t.maybeNull(t.string),
    address: t.maybeNull(t.string),
    image_main: t.maybeNull(t.string),
    users: t.maybe(t.map(t.reference(t.late((): Instance<typeof User> => User)))),
    deleted:  t.maybeNull(t.boolean),
})

export type Organisation = Instance<typeof Organisation>;
