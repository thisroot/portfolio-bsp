import { types as t, Instance } from "mobx-state-tree"
import { Organisation } from "models/mst/Organisation"

export const User = t.model("User", {
    user_id: t.identifierNumber,
    email: t.maybeNull(t.string),
    firstname_en: t.maybeNull(t.string),
    firstname_ru: t.maybeNull(t.string),
    lastname_en: t.maybeNull(t.string),
    lastname_ru: t.maybeNull(t.string),
    birthdate: t.maybeNull(t.string),
    patronymic_ru: t.maybeNull(t.string),
    patronymic_en: t.maybeNull(t.string),
    organisation: t.maybe(t.reference(t.late((): Instance<typeof Organisation> => Organisation))),
    phone: t.maybeNull(t.string),
    deleted:  t.maybeNull(t.boolean),
}).views(_ => ({
    // get fullName() {
    //     return ` ${ self.firstname_ru || '' } ${ self.lastname_ru || '' }`
    // }
}))

export type User = Instance<typeof User>;
