import { types as t } from "mobx-state-tree"
import { RouterModel } from "mst-react-router"
import { User } from "models/mst/User"
import { Organisation } from "models/mst/Organisation"
import { Project } from "models/mst/Project"
import { ObjectModel } from "models/mst/ObjectModel"
import { TableState } from "models/mst/TableState"
import { SchedulePlanFact } from "models/mst/Schedule"
import { ScheduleBuilder } from "models/mst/Schedule"
import { Reference } from "models/mst/Reference"
import { ScheduleVersion } from "models/mst/Schedule"
import { ScheduleFact } from "models/mst/ScheduleFact"
import { Image } from "models/mst/Image"

import { FAVORITES_TYPES } from 'constants/common'
import { File } from "./File"
import { BudgetPlan } from "./BudgetPlan"
import { AnalyticsTemplate } from "./AnalyticsTemplate"

//TODO: не менять формат полей, завязано на данные из бека никаких CamelCase!!!
const Store = t.model({
    router: RouterModel,
    project: t.map(Project),
    project_list: t.array(t.reference(t.late(() => Project))),
    object: t.map(ObjectModel),
    object_list: t.array(t.reference(t.late(() => ObjectModel))),
    schedule: t.map(ScheduleBuilder),
    schedule_list: t.array(t.reference(t.late(() => ScheduleBuilder))),
    user: t.map(User),
    organisation: t.map(Organisation),
    table_state: t.map(TableState),
    schedule_version: t.map(ScheduleVersion),
    schedule_plan_fact: t.map(SchedulePlanFact),
    reference: t.maybeNull(Reference),
    schedule_fact: t.map(ScheduleFact),
    image: t.map(Image),
    file: t.map(File),
    budget_plan: t.map(BudgetPlan),
    budget_plan_list: t.array(t.reference(t.late(() => BudgetPlan))),
    analytics_template: t.map(AnalyticsTemplate),
    analytics_template_list: t.array(t.reference(t.late(() => AnalyticsTemplate)))
}).views(({ project, object }) => ({
    get favoriteProjects() {
        return [ ...project.values() ]
          .filter(el => el.favorite && project.get(el.project_id).canRead())
          .map(el => ({ ...el, favoriteType: FAVORITES_TYPES.PROJECT }))
    },
    get favoriteObjects() {
        return [ ...object.values() ]
          .filter(el => el.favorite && object.get(el.object_id).canRead())
          .map(el => ({ ...el, favoriteType: FAVORITES_TYPES.OBJECT }))
    },
    get allFavorites() {
        const favoriteProjects = [ ...project.values() ]
          .filter(el => el.favorite && project.get(el.project_id).canRead())
          .map(el => ({ ...el, favoriteType: FAVORITES_TYPES.PROJECT }))
        const favoriteObjects = [ ...object.values() ]
          .filter(el => el.favorite && object.get(el.object_id).canRead())
          .map(el => ({ ...el, favoriteType: FAVORITES_TYPES.OBJECT }))
        return favoriteProjects.concat(favoriteObjects)
    },
}))

export { Store }
