import { types as t, Instance, getRoot } from "mobx-state-tree"
import { User } from "./User"

export const File = t.model("File", {
    file_id: t.identifierNumber,
    file_relation: t.maybeNull(t.string),
    file_relation_id: t.maybeNull(t.number),
    parent_id: t.maybeNull(t.reference(t.late(() => File))),
    folder: t.maybeNull(t.boolean),
    created: t.string,
    created_by: t.reference(t.late(() => User), {
        get(identifier, parent) {
            return getRoot<any>(parent).user.get(identifier) || identifier
        },
        set(value) {
            return value.user_id
        }
    }),
    mimetype: t.maybeNull(t.string),
    name: t.string,
    size: t.maybeNull(t.number),
    ext: t.maybeNull(t.string),
    digest: t.maybeNull(t.string),
    url: t.maybeNull(t.string),
    children: t.maybeNull(t.optional(t.array(t.safeReference(t.late(() => File))), [])),
    deleted: t.maybeNull(t.optional(t.boolean, false)),
})

export type File = Instance<typeof File>
