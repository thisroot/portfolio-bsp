import { types as t, Instance } from "mobx-state-tree"
import { User } from "models/mst/User"
import { GeoPoint } from 'models/entities'

export const Image = t.model("Image", {
    image_id: t.identifierNumber,
    image_relation: t.string,
    image_relation_id: t.number,
    created: t.string,
    created_by: t.safeReference(t.late(() => User)),
    mimetype: t.string,
    name: t.string,
    size: t.number,
    ext: t.string,
    digest: t.string,
    converted: t.frozen({}),
    main: t.boolean,
    geo_coord: t.maybeNull(t.frozen<GeoPoint>()),
    deleted:  t.maybeNull(t.boolean),
})

export type Image = Instance<typeof Image>