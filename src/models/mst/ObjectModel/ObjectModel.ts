import { types as t, Instance } from "mobx-state-tree"
import { Project } from '../Project/Project'
import { ScheduleBuilder, SchedulePlanFact, ScheduleVersion } from "../Schedule"
import { get } from 'lodash'
import { ObjectType } from "../Reference/ObjectType"
import { Image, GeoPoint } from "../../entities"
import { Region } from "../Reference/Region"
import { StatusDataLayers } from "../StatusDataLayers"

export interface ObjectSettings {
    guntt_status_gray: boolean
}

export const ObjectModel = t.model("ObjectModel", {
    object_id: t.identifierNumber,
    project_id: t.reference(t.late(() => Project)),
    region_id: t.maybeNull(t.reference(t.late(() => Region))),
    favorite: t.boolean,
    name_en: t.maybeNull(t.string),
    name_ru: t.maybeNull(t.string),
    description_en: t.maybeNull(t.string),
    description_ru: t.maybeNull(t.string),
    address: t.maybeNull(t.string),
    meta: t.frozen({}),
    permission: t.optional(t.array(t.string), [ 'object.read' ]),
    main_image: t.frozen<Image>(),
    object_type_id: t.maybeNull(t.reference(ObjectType)),
    schedule_plan_fact: t.safeReference(t.late(() => SchedulePlanFact)),
    schedule_builder: t.array(t.safeReference(ScheduleBuilder)),
    schedule_version: t.array(t.safeReference(ScheduleVersion)),
    passport_data_en: t.maybeNull(t.frozen({})),
    passport_data_ru: t.maybeNull(t.frozen({})),
    geo_coord: t.frozen<GeoPoint>(),
    broadcast_url: t.maybeNull(t.string),
    date_start: t.maybeNull(t.string),
    date_finish: t.maybeNull(t.string),
    weighting_factor: t.maybeNull(t.number),
    calculation_method: t.enumeration("calculation_method", [ "weighting_factor", "arithmetic_mean" ]),
    status_data: t.maybeNull(StatusDataLayers),
    deleted:  t.maybeNull(t.boolean),
    settings: t.maybeNull(t.frozen<ObjectSettings>()),
}).views(self => ({
    get currentScheduleBuilderVersion() {
        const current = get(self.schedule_builder, '[0].schedule_version_id.schedule_version_id')
        // return self.schedule_builder.length > 0 ? get(self.schedule_builder, '[0].schedule_version_id.schedule_version_id') : null
        return current ? current : self.schedule_version.length > 0 ? get(self.schedule_version.filter(item => item.current)[0], 'schedule_version_id', null) : null
    },
    get currentScheduleReportVersion() {
        return get(self.schedule_plan_fact, 'schedule_version_id.schedule_version_id')
    },
})).actions(self => ({
      canWrite(alias?: string) {
          return self.permission.includes(alias ? `object.${ alias }.write` : 'object.write')
      },
      canRead(alias?: string) {
          return self.permission.includes(alias ? `object.${ alias }.read` : 'object.read')
      },
      canUpload(alias?: string) {
          return self.permission.includes(alias ? `object.${ alias }.upload` : 'object.schedule.upload')
      },
      canDelete(alias?: string) {
          return self.permission.includes(alias ? `object.${ alias }.delete` : 'object.schedule.fact.delete')
      },
      can(alias: string) {
          return self.permission.includes(alias)
      }
  }
))

export type ObjectModel = Instance<typeof ObjectModel>

