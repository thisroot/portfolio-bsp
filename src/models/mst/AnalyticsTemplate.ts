import { types as t, Instance } from "mobx-state-tree"
import { ANALYTICS_TEMPLATE_TYPE } from "../../constants"

export enum FILTER_FIELD_COMPONENT_TYPE {
    DROPDOWN = 'dropdown',
    DATE_PICKER = 'date_picker'
}

export interface FilterFields {
    [key: string]: {
        component_type: FILTER_FIELD_COMPONENT_TYPE
        name: string
        placeholder: string
        label: string
        value: any
        default: any
        extra?: Array<any> | {
            url: string,
            path: string,
            method: string,
            key: string,
            value: string
        }
    }
}

export interface AnalyticsSettings {
    method: string,
    template_contains_data: boolean,
    url: string
    filter: FilterFields
}

export const AnalyticsTemplate = t.model("AnalyticsTemplate", {
    analytics_template_id: t.number,
    comment: t.string,
    alias: t.identifier,
    name: t.string,
    permission: t.string,
    template: t.frozen({}),
    template_type: t.enumeration([
        ANALYTICS_TEMPLATE_TYPE.REPORT,
        ANALYTICS_TEMPLATE_TYPE.DASHBOARD
    ]),
    sort_order: t.number,
    settings: t.frozen<AnalyticsSettings>()
})

export type AnalyticsTemplate = Instance<typeof AnalyticsTemplate>
