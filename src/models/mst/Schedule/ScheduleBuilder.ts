import { getRoot, Instance, types as t } from "mobx-state-tree"
import { Element, Job } from "../Reference"
import { makeHash } from "utils/helpers"
import { ScheduleVersion } from "models/mst/Schedule/ScheduleVersion"
import { User } from "../User"

export const ScheduleBuilder = t.model("ScheduleBuilder", {
    // API fields
    schedule_id: t.identifierNumber,
    schedule_version_id: t.maybeNull(t.reference(t.late(() => ScheduleVersion), {
        get(identifier, parent) {
            return getRoot<any>(parent).schedule_version.get(identifier) || identifier
        },
        set(value) {
            return value.schedule_version_id
        }
    })),
    parent_id: t.maybeNull(t.reference(t.late(() => ScheduleBuilder), {
        get(identifier, parent) {
            return getRoot<any>(parent).schedule.get(identifier) || identifier
        },
        set(value) {
            return value.schedule_id
        }
    })),
    object_id: t.maybeNull(t.number),
    name: t.maybeNull(t.string),
    job_id: t.maybeNull(t.reference(Job)),
    element_id: t.maybeNull(t.reference(Element)),
    job_unit: t.maybeNull(t.string),
    date_start: t.maybeNull(t.string),
    date_finish: t.maybeNull(t.string),
    volume_actual: t.maybeNull(t.number),
    volume_planned: t.maybeNull(t.number),
    expenditure_actual: t.maybeNull(t.number),
    expenditure_planned: t.maybeNull(t.number),
    meta: t.frozen({}),
    sort_order: t.number,
    parent_path: t.string,
    weighting_factor: t.maybeNull(t.number),
    created_by: t.maybeNull(t.reference(t.late(() => User), {
        get(identifier, parent) {
            return getRoot<any>(parent).user.get(identifier) || identifier
        },
        set(value) {
            return value.user_id
        }
    })),
    // service fields
    // TODO: rename service fields start with '_'
    children: t.optional(t.array(t.safeReference(t.late((): Instance<typeof ScheduleBuilder> => ScheduleBuilder))), []),
    expanded: t.optional(t.boolean, false),
    isVirtual: t.optional(t.boolean, false),
    pending: t.optional(t.boolean, false),
    invalidFields: t.map(t.string),
    hash: t.optional(t.string, makeHash(4)),
})

export type ScheduleBuilder = Instance<typeof ScheduleBuilder>;
