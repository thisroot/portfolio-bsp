import { types as t, Instance } from "mobx-state-tree"
import { ObjectModel } from "../ObjectModel/ObjectModel"
import { ScheduleVersion } from "./ScheduleVersion"

export const SchedulePlanFact = t.model("SchedulePlanFact", {
    // API fields
    schedule_id: t.identifierNumber,
    schedule_version_id: t.safeReference(t.late(() => ScheduleVersion)),
    object_id: t.reference(t.late((): Instance<typeof ObjectModel> => ObjectModel)),
    parent_id: t.maybeNull(t.reference(t.late((): Instance<typeof SchedulePlanFact> => SchedulePlanFact))),
    parent_path: t.string,
    name: t.string,
    job: t.maybeNull(t.string),
    job_id: t.maybeNull(t.number),
    job_unit: t.maybeNull(t.string),
    date_start: t.maybeNull(t.string),
    date_finish: t.maybeNull(t.string),
    volume_percent: t.maybeNull(t.number),
    volume_planned: t.maybeNull(t.number),
    volume_actual: t.maybeNull(t.number),
    volume_fact: t.maybeNull(t.number),
    expenditure_planned: t.maybeNull(t.number),
    expenditure_actual: t.maybeNull(t.number),
    expenditure_fact: t.maybeNull(t.number),
    expenditure_percent: t.maybeNull(t.number),
    deviation_percent: t.maybeNull(t.number),
    deviation_day: t.maybeNull(t.number),
    status: t.maybeNull(t.number),
    sort_order: t.maybeNull(t.number),
    // service fields
    // TODO: rename service fields start with '_'
    children: t.optional(t.maybe(t.array(t.reference(t.late((): Instance<typeof SchedulePlanFact> => SchedulePlanFact)))), []),
    expand: t.optional(t.boolean, false),
    deleted:  t.maybeNull(t.boolean),
})

export type SchedulePlanFact = Instance<typeof SchedulePlanFact>