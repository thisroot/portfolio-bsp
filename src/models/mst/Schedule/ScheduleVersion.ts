import { types as t, Instance, getParent, IMSTMap, destroy } from "mobx-state-tree"
import { ObjectModel } from "../ObjectModel/ObjectModel"

export const ScheduleVersion = t.model("ScheduleVersion", {
    schedule_version_id: t.identifierNumber,
    object_id: t.reference(t.late((): Instance<typeof ObjectModel> => ObjectModel)),
    name: t.string,
    meta: t.maybeNull(t.frozen({})),
    current: t.boolean,
    deleted:  t.maybeNull(t.boolean),
}).views(self => ({
      get id() {
          return self.schedule_version_id
      }
  }
)).actions(self => ({
      setName(name: string) {
          self.name = name
      },
      remove() {
          const node = getParent(self, 1) as IMSTMap<any>
          const item = node.get(`${ self.id }`)
          destroy(item)
      },
      setCurrent() {
          const node = getParent(self, 1) as IMSTMap<any>
          node.forEach(item => item.current = false)
          self.current = true
      },
  }
))

export type ScheduleVersion = Instance<typeof ScheduleVersion>