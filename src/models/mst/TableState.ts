import { types as t, Instance } from "mobx-state-tree"
import { get } from 'lodash'

export interface ICacheParams {
    timeInvalidate?: number
    noCaching?: boolean
}

export const TableState = t.model('table_state', {
    request_id: t.identifier,
    timeInvalidate: t.optional(t.number, 0.8 * 60 * 1000), // time invalidation 0.8min
    lastModified: t.optional(t.maybeNull(t.number), () => new Date().getTime()),
    cached: t.optional(t.boolean, false),
    mutex: t.optional(t.boolean, false),
    deleted:  t.maybeNull(t.boolean),
})
  .actions((self) => ({

      isNeedCaching: (): boolean => {
          self.cached = new Date().getTime() - self.timeInvalidate > self.lastModified
          return !self.cached
      },
      isMutex: (): boolean => {
          return self.mutex
      },
      updateRequestCache(cacheParams?: ICacheParams) {
          self.cached = true
          self.mutex = false
          self.timeInvalidate = get(cacheParams, 'noCaching') ? 0 : get(cacheParams, 'timeInvalidate')
          self.lastModified = new Date().getTime()
      }
  }))

export type TableState = Instance<typeof TableState>;
