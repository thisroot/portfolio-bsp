export type CallbackFunction = () => void;
export type CallbackFunctionVariadic = (...args: any[]) => void;
export type CallbackFunctionVariadicAnyReturn = (...args: any[]) => any;

export type ID = string
export type Base64 = string
export type DateStamp = number

export interface Entity extends File {}

export interface ImagesOptions {
    mimeTypes: Array<String>
    minWidth: number
    minHeight: number
    maxFileSize: number
    limit: number
}

export interface VideoOptions {
    mimeTypes: Array<String>
    maxFileSize: number
    maxDuration: number
    limit: number
}

export interface DocumentsOptions {
    mimeTypes: Array<string>
    maxFileSize: number
}

export interface FileParams {
    file: File,
    uri: string,
    animated?: boolean,
    type: FileTypes
}

export enum FileTypes {
    IMAGE = 'image',
    VIDEO = 'video',
    FILE = 'file'
}

export interface ImagesOptions {
    mimeTypes: Array<String>
    minWidth: number
    minHeight: number
    maxFileSize: number
    limit: number
}

export interface VideoOptions {
    mimeTypes: Array<String>
    maxFileSize: number
    maxDuration: number
    limit: number
}

export interface ImageParams {
    file: File,
    uri: string,
    animated: boolean
}

export interface VideoParams {
    file: File,
    uri: string
}

export interface FileParams {
    file: File,
    uri: string,
    animated?: boolean,
    type: FileTypes
}