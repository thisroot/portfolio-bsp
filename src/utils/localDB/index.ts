/*
  wrapper for safe using localDB methods
  for example fixed safari 10 in private mode
*/
class LocalDB {
    public setItem = (key, value) => {
        try {
            localStorage.setItem(key, value)
            return true
        } catch (error) {
            return false
        }
    }

    public getItem = (key) => {
        try {
            return localStorage.getItem(key)
        } catch (error) {
            return null
        }
    }
    public removeItem = (key) => {
        try {
            localStorage.removeItem(key)
            return true
        } catch (error) {
            return false
        }
    }
    public clear = () => {
        try {
            localStorage.clear()
            return true
        } catch (error) {
            return false
        }
    }
}

const localDB = new LocalDB()
export { localDB, LocalDB }
