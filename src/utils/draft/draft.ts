import { action } from "mobx"
import { applySnapshot, getSnapshot } from "mobx-state-tree"
// import { cloneDeep } from 'lodash'
//import { deepEquals } from "./deepEquals"

export class Draft<T extends object> {
    /**
     * Draft data object
     */
    readonly data: T

    @action
    public commit(): void {
        applySnapshot(this.originalData, getSnapshot(this.data))
    }

    // @action reset(): void {
    //     applySnapshot(this.data, this.originalSnapshot)
    // }
    //
    // @computed get isDirty(): boolean {
    //     return !deepEquals(getSnapshot(this.data), this.originalSnapshot)
    // }

    // @computed get isValid(): boolean {
    //     return this.validate(this.data)
    // }
    //
    // @computed get saveDisabled(): boolean {
    //     return !this.isValid || !this.isDirty
    // }
    // @computed get resetDisabled(): boolean {
    //     return !this.isDirty
    // }

    /**
     * Original data object.
     */
    readonly originalData: T
    // public validate: (data: T) => boolean

    // @computed
    // private get originalSnapshot() {
    //     return getSnapshot(this.originalData)
    // }

    constructor(original: T) {
        this.originalData = original
        // this.data = clone(this.originalSnapshot) as T
    }
    // constructor(original: T, validate?: (data: T) => boolean) {
    //     this.originalData = original
    //     this.validate = validate ? validate : () => true
    //     this.data = cloneDeep(this.originalSnapshot) as T
    // }
}

export function draft<T extends object>(original: T): Draft<T> {
    return new Draft(original)
}

// export function draft<T extends object>(original: T, validate?: (data: T) => boolean): Draft<T> {
//     return new Draft(original, validate)
// }