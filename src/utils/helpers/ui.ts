import { objectName } from "utils/helpers/object"
import { get } from "lodash"

export function isClickOnButton(e) {
    const path = e.path || (e.composedPath && e.composedPath())
    return objectName(e) === 'MouseEvent' && path.slice(0, 5).join().includes('HTMLButtonElement')
}

export function isClickOnModal(e) {
    const path = e.path || (e.composedPath && e.composedPath())
    return objectName(e) === 'MouseEvent' && path.slice().map(i => i.className).join().includes('modal')
}

export function placeCaretAtEnd(el: HTMLElement) {
    el.focus()
    if (typeof window.getSelection != "undefined"
      && typeof document.createRange != "undefined") {
        const range = document.createRange()
        range.selectNodeContents(el)
        range.collapse(false)
        const sel = window.getSelection()
        sel.removeAllRanges()
        sel.addRange(range)
    } else if (get(document.body, 'createTextRange')) {
        const textRange = get(document.body, 'createTextRange')()
        textRange.moveToElementText(el)
        textRange.collapse(false)
        textRange.select()
    }
}

export function hexToRgb(hex) {
    const m = hex.match(/^#?([\da-f]{2})([\da-f]{2})([\da-f]{2})$/i)
    return {
        r: parseInt(m[1], 16),
        g: parseInt(m[2], 16),
        b: parseInt(m[3], 16)
    }
}

export function lightenDarkenColor(color, percent) {
    let R = parseInt(color.substring(1, 3), 16)
    let G = parseInt(color.substring(3, 5), 16)
    let B = parseInt(color.substring(5, 7), 16)
    if (R === 0) R = 32
    if (G === 0) G = 32
    if (B === 0) B = 32

    R = parseInt(String(R * (100 + percent) / 100))
    G = parseInt(String(G * (100 + percent) / 100))
    B = parseInt(String(B * (100 + percent) / 100))

    R = (R < 255) ? R : 255
    G = (G < 255) ? G : 255
    B = (B < 255) ? B : 255

    const RR = ((R.toString(16).length === 1) ? "0" + R.toString(16) : R.toString(16))
    const GG = ((G.toString(16).length === 1) ? "0" + G.toString(16) : G.toString(16))
    const BB = ((B.toString(16).length === 1) ? "0" + B.toString(16) : B.toString(16))

    return "#" + RR + GG + BB

}

export function isThemeExist() {
    return document.documentElement.hasAttribute('theme')
}

export function isDArkMode() {
    return window.matchMedia('(prefers-color-scheme: dark)').matches
}

export function changeTheme() {
    if (isThemeExist()) {
        document.documentElement.removeAttribute('theme')
        return false
    } else {
        document.documentElement.setAttribute('theme', 'dark')
        return true
    }
}

export function buildFileSelector(e, onFilesSelect: (file: Array<File>) => void, accept?: Array<string>) {
    const fileSelector = document.createElement('input')
    fileSelector.setAttribute('type', 'file')
    fileSelector.setAttribute('multiple', 'multiple')
    fileSelector.setAttribute('accept', accept.join(',') || 'image/* , video/*')

    fileSelector.onchange = (e) => {
        const files = new Array(...get(e.target, 'files'))
        onFilesSelect(files)
    }

    e.preventDefault()
    fileSelector.click()
}

