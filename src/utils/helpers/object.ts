import { clone, difference, get, intersection, isArray, isEmpty, isNull, isObject, mapValues } from 'lodash'
import { toJS } from "mobx"


function parseProxy(proxy: any) {
    try {
        return JSON.parse(JSON.stringify(proxy))
    } catch (e) {
        return null
    }
}

function flatTree(tree, nameNodeId, childrenName = 'children') {
    const arr = []
    tree && tree.forEach(node => {
        const item = clone(node)
        arr.push(item)
        if (node[childrenName]) {
            item[childrenName] = Object.values(mapValues(node[childrenName], item => item[nameNodeId]))
            const res = flatTree(node[childrenName], nameNodeId, childrenName)
            arr.push(...res)
        }
    })
    return arr
}

function flatNestedFields(tree: object, childrenName = 'children') {
    const arr = []
    const entries = Object.entries(tree)
    if (entries && entries.length > 0) {
        entries.forEach(node => {
            const item = clone(node[1])
            if (node[1][childrenName]) {
                item[childrenName] = Object.values(mapValues(node[1][childrenName], item => item))
                const res = flatNestedFields(node[1][childrenName], childrenName)
                arr.push(...res)
            } else {
                arr.push({ fieldKey: node[0], fieldValue: node[1].value, fieldLabel: node[1].label })
            }
        })
    }
    return arr
}

function findInObject(object, key) {
    let value
    Object.keys(object).some(function (k) {
        if (k === key) {
            value = object[k]
            return true
        }
        if (object[k] && typeof object[k] === 'object') {
            value = findInObject(object[k], key)
            return value !== undefined
        }
        return false
    })
    return value
}

function flatNestedFormFields(form: any, getOnlyValues = true) {
    const obj = {}
    const entries = Object.entries(form)
    if (entries && entries.length > 0) {
        entries.forEach(entrie => {
            //TODO; опасный метод. закостылена проверка на то что поле не может быть инициализировано числом
            if (get(entrie[1], 'fields') && !get(entrie[1], 'fields[0]')) {
                const res = flatNestedFormFields(get(entrie[1], 'fields'))
                Object.assign(obj, res)
            } else {
                const val = clone(get(toJS(entrie[1]), 'value'))
                if (!isEmpty(`${ val }`)) {
                    const parsedVal = isArray(val) || isNull(val) ? val : +val
                    Object.assign(obj, { [entrie[0]]: getOnlyValues ? parsedVal : entrie[1] })
                }
            }
        })
    }
    return obj
}

function objectName(object) {
    return get(object, 'constructor.name')
}

function getAllChildren(node: any, key?: string) {
    let children = []
    if (get(node, 'children')) {
        for (let item of node.children) {
            key ? children.push(item[key]) : children.push(item)
            if (item.children && item.children.length > 0) {
                children.push(...getAllChildren(item))
            }
        }
    }
    return children
}

function getAllParents(node: any) {
    let parents = []
    if (get(node, 'parent_id')) {
        parents.push(node.parent_id)
        parents.push(...getAllParents(node.parent_id))
    }
    return parents
}

const getAllParentsPath = (item) => {
    const arr = []
    if (item && item.parent_file_id) {
        arr.push(...getAllParentsPath(item.parent_file_id))
    }
    item && arr.push(toJS(item))
    return arr
}

function getUnselectParents(node: any, selectedNodes: Array<number>, identityKey: string) {
    let unselected = []
    const parentChildren = get(node, 'parent_id.children', []).map(item => item[identityKey])
    if (parentChildren.length > 0) {
        if (intersection(selectedNodes, parentChildren).length === 0) {
            unselected.push(node.parent_id[identityKey])
            selectedNodes = selectedNodes.filter(item => item !== node.parent_id[identityKey])
        }
        unselected.push(...getUnselectParents(node.parent_id, selectedNodes, identityKey))
    }
    return unselected
}

function toggleExpandRecursive(node, key = 'expanded', nodeName = 'parent_id', expand = true) {
    let count = 1
    node[key] = expand
    if (node[nodeName]) {
        if (isArray(node[nodeName])) {
            node[nodeName].forEach(item => {
                count = count + toggleExpandRecursive(item, key, nodeName, expand)
            })
        } else {
            count = count + toggleExpandRecursive(node[nodeName], key, nodeName, expand)
        }
    }
    return count
}

// function filterNestedNodes(scheduleBuilder: any, selectedNodes: number[], key = 'schedule_id'): number[] {
//     let filteredNodes = []
//     scheduleBuilder.forEach(node => {
//         if (selectedNodes.includes(node[key])) {
//             filteredNodes.push(node[key])
//         } else if (node.children && node.children.length > 0) {
//             filteredNodes = filteredNodes.concat(filterNestedNodes(node.children, selectedNodes))
//         }
//     })
//     return filteredNodes
// }

function filterNestedNodes(scheduleBuilder: any, selectedNodes: number[], key = 'schedule_id'): number[] {
    let allNodes = []
    let allSelected = []
    let filteredNodes = new Set<number>()
    let markOnRemove = new Set<number>()
    scheduleBuilder.forEach(node => {
        if (selectedNodes.includes(node[key])) {
            allNodes.push(node)
            allSelected.push(node[key])
        }
    })
    allNodes.forEach(node => {
        if (node.children && node.children.length > 0) {
            const allChilds = getAllChildren(node, key)
            const nodes = [ ...allSelected, ...filterNestedNodes(node.children, selectedNodes) ]
            filteredNodes = new Set<number>(difference(nodes, allChilds))
            markOnRemove = new Set<number>([ ...markOnRemove, ...allChilds ])
        } else {
            filteredNodes.add(node[key])
        }
    })
    return difference([ ...filteredNodes ], [ ...markOnRemove ])
}

function parseFilterFromTree(filter, type, isCheckable = (e: string) => !!e) {
    return filter.map((entity_type) => {
        const type_key = entity_type[`${ type }_type_id`]
        const children = entity_type.fields && entity_type.fields.length > 0 ? entity_type.fields
          .filter(val => {
              return val.type !== 'target_ref' && val.values
          })
          .map((label) => {
              const children = label.values && label.values.length > 0 ? label.values
                .map((value) => {
                    return {
                        key: `${ type_key }_${ label.key }_${ value }`,
                        title: value,
                        checkable: isCheckable(`${ type_key }_${ label.key }_${ value }`)
                    }
                }) : null
              let title = label.label
              return {
                  key: `${ type_key }_${ label.key }`,
                  title,
                  children,
                  checkable: isCheckable(`${ type_key }_${ label.key }`)
              }
          }) : null
        return {
            key: type_key,
            title: entity_type.name,
            children,
            checkable: isCheckable(type_key)
        }
    }).filter(eType => {
        return eType.key && eType.children && eType.children.length > 0
    })
}

function parseFilterAttributes(filter, entity_name) {
    const newFilter = []
    filter.forEach(item => {
        const pieces = `${ item }`.split('_')
        const el = {
            [`${ entity_name }_type_id`]: +pieces[0],
            filter: {}
        }
        if (pieces[1] && !el.filter[pieces[1]]) {
            el.filter[pieces[1]] = null
        }
        if (pieces[2]) {
            newFilter.push(el)
            el.filter[pieces[1]] = pieces[2]
        }
    })
    return newFilter
    // return Object.values(
    //   groupBy(
    //     newFilter, `${ entity_name }_type_id`))
    //   .map(item => merge(item[0], ...item))
}

function updateFormValues(values, form) {
    Object.entries(values).forEach(field => {
        if (isObject(field[1]) && !isArray(field[1])) {
            form.update({ [field[0]]: field[1] })
        } else {
            form.$(field[0]).set(field[1])
        }
    })
}

export {
    parseProxy,
    flatTree,
    flatNestedFields,
    objectName,
    getAllChildren,
    getAllParents,
    getAllParentsPath,
    getUnselectParents,
    flatNestedFormFields,
    toggleExpandRecursive,
    findInObject,
    filterNestedNodes,
    parseFilterFromTree,
    parseFilterAttributes,
    updateFormValues,
}
