import {
    BYTE_ORDER_MARK_CHAR_CODES,
    KEY_CODES,
    MARKDOWNS,
    SI_POWER,
    SI_POWER_FULL,
    SI_PREFIX,
    SI_PREFIX_FULL,
    SI_UNIT,
    SI_UNIT_FULL
} from "../../constants"
import { loc } from "../i18n"
import { memoiser } from "./common"
import { t } from "@lingui/macro"
import { uniq } from 'lodash'

export const snakeToPascalCase = (str: string) => str.split('-')
  .map((i) => {
      const [ f, ...other ] = i
      return f.toUpperCase() + other.join('')
  }).join('')

/* limits the number of characters in a string */
/**
 * this method check string on extra spaces and enters
 * remove them and trim text
 * @param {string} text
 * @return {string}
 */
export function formatTextData(text: string): string {
    const textWithoutBOM = removeBOMCharsFromText(text)
    return textWithoutBOM
      .trim()
      .replace(/[^\S\r\n]{2,}/g, ' ')
      .replace(/[\n]{3,}/g, '\n\n') // as quill have one \n by default, we should keep one more for a new line
}

/* limits the number of characters in a string */
export function limitText(text: string, limit: number): string {
    if (!limit || text.length <= limit) return text

    let short = text.substr(0, limit)

    /* if it's in the middle of the world cut the whole word */
    if (/^\S/.test(text.substr(limit))) {
        short = short.replace(/\s+\S*$/, '')
    }

    return short + '...'
}

export function removeHTML(str: string) {
    return str.replace(/<[^>]*>?/gm, '')
}

export function removeBOMCharsFromText(text) {
    if (!text || typeof text !== 'string') return text
    return text
      .trim()
      .replace(/[^\S\r\n]{2,}/g, (match) => {
          if (
            match.charCodeAt(0) === BYTE_ORDER_MARK_CHAR_CODES.ZERO_WIDTH_NO_BREAK_SPASE &&
            match.charCodeAt(1) !== KEY_CODES.SPACE
          ) {
              return ''
          } else {
              return match
          }
      })
}

export function encodeHTMLEntities(str: string) {
    let buf = []
    for (let i = str.length - 1; i >= 0; i--) {
        buf.unshift([ '&#', str.charCodeAt(i), ';' ].join(''))
    }
    return buf.join('')
}

export function decodeHTMLEntities(str: string) {
    return str.replace(/&#(\d+);/g, function (_, dec) {
        return String.fromCharCode(dec)
    })
}

export function getCurrencySymbolFromCurrencyAbbreviation(str: string) {
    switch (str.toLowerCase()) {
        case 'rub':
            return '₽'
        case 'usd':
            return '$'
        default:
            return ''
    }
}

export const parseMeasureUnit = memoiser(function (unit: string, isShort = true) {
    if (!unit) return ''

    const measureUnits = Object.entries(isShort ? SI_UNIT : SI_UNIT_FULL)
    const measurePrefix = Object.entries(isShort ? SI_PREFIX : SI_PREFIX_FULL)
    const items = unit.split('_')
    let parsed = ''
    let swapped = ''

    for (let i = 0; i < items.length; i++) {
        if (items[i] === 'per') {
            parsed += isShort ? '/' : ` ${ loc._(t`basic.per`) } `
            continue
        }

        if (SI_POWER.hasOwnProperty(items[i])) {
            swapped = isShort ? loc._(SI_POWER[items[i]]) : loc._(SI_POWER_FULL[items[i]])
            continue
        }
        for (let z = 0; z < measureUnits.length; z++) {
            if (items[i].match(measureUnits[z][0])) {
                if (items[i].length !== measureUnits[z][0].length) {
                    for (let x = 0; x < measurePrefix.length; x++) {
                        if (items[i].match(measurePrefix[x][0])) {
                            parsed += loc._(measurePrefix[x][1])
                            break
                        }
                    }
                }
                parsed += loc._(measureUnits[z][1]) + (isShort ? '' : ' ') + swapped
                swapped = ''
                break
            }
        }
    }
    return parsed
})

export function getMaxDefaultNameIndex<T>(names: Array<T | string>, defaultName: string, keyName?: string) {
    const re = new RegExp(`${ defaultName }\\((\\d)\\)|${ defaultName }$`, 'g')
    const result = names.reduce((acc, folder) => {
        const res = Array.from(keyName ? folder[keyName].matchAll(re) : (folder as string).matchAll(re))
        if (res.length > 0) {
            acc.push(res[0][1] === undefined ? -1 : +res[0][1])
        }
        return acc
    }, []).sort((a, b) => a - b)

    if (result[result.length - 1] === undefined) return defaultName
    if (result[result.length - 1] === -1) return `${ defaultName }(0)`

    let suggest = 0
    for (const item of uniq(result)) {
        if (item < 0) {
            continue
        }
        if (item === suggest) {
            ++suggest
        } else {
            break
        }
    }
    return `${ defaultName }(${ suggest })`
}

export function isMarkdown(text: string) {
    return MARKDOWNS.includes(text)
}
