import { REGEXP } from 'constants/common'

import { t } from '@lingui/macro'
import { loc } from 'utils/i18n'

export interface iField {
    label: string,
    value: any,
    isValid?: boolean
}

export interface IFormField {
    field: iField
}

export interface validationResult {
    [0]: boolean,
    [1]: string
}

/**
 // Функции универсальны, принимают на вход значение и название поля!!!
 **/

export function onlyNumbers({ field }: IFormField): validationResult {
    return [ /^[0-9]+(\.[0-9]+)?$/.test(field.value), loc._(t`form.validator.onlyNumbers`), { label: loc._(field.label) } ]
}

export function onlyPositiveNumbers({ field }: IFormField): validationResult {
    return [ /^[0-9]+(\.[0-9]+)?$/.test(field.value) && +field.value > 0, loc._(t`form.validator.onlyNumbers`), { label: loc._(field.label) } ]
}

export function betweenZeroAndOne({ field }: IFormField): validationResult {
    return [ /^(([0]+(\.[0-9]+)?)|1)$/.test(field.value) && +field.value > 0, loc._(t`form.validator.onlyNumbers`), { label: loc._(field.label) } ]
}

export function required({ field }: IFormField): validationResult {
    return [ (!!field.value || +field.value === 0) && (field.value !== ""), loc._(t`form.validator.isNotEmpty`, { label: loc._(field.label) }) ]
}

export function shouldBeEqualTo(target) {
    return shouldBe(target, '=')
}

export function shouldBeLessTo(target) {
    return shouldBe(target, '<')
}

export function shouldBeMoreTo(target) {
    return shouldBe(target, '>')
}

export function shouldBe(target, condition?: string) {
    return ({ field, form }): validationResult => {
        let fieldsAreEquals = true
        let message = ""
        const first = loc._(field.label)
        const second = loc._(form.$(target).label)
        if (!form.$(target).value || !field.value) {
            return [ true, message ]
        }
        switch (condition) {
            case '<':
                message = loc._(t`form.validator.mustLess`, { first, second })
                fieldsAreEquals = (+form.$(target).value >= +field.value)
                break
            case '>':
                message = loc._(t`form.validator.mustMore`, { first, second })
                fieldsAreEquals = (+form.$(target).value <= +field.value)
                break
            case '=':
            default:
                message = loc._(t`form.validator.mustEqual`, { first, second })
                fieldsAreEquals = (form.$(target).value === field.value)
        }
        return [ fieldsAreEquals, message ]
    }
}

export function isEmail({ field }: IFormField): validationResult {
    const isValid = field.value.match(REGEXP.email)
    const label = loc._(field.label)
    return [ isValid, loc._(t`form.validator.mustEmail`, { label }) ]
}

export function isPhoneNumber({ field }: IFormField): validationResult {
    const isValid = field.value.match(/^(\+7|8)\d{3}\d{3}\d{4}$/)
    const label = loc._(field.label)
    return [ isValid, loc._(t`form.validator.mustPhone`, { label }) ]
}

export function isPassword({ field }: IFormField): validationResult {
    const isValid = field.value && field.value.length >= 5 && field.value.length <= 32
    const label = loc._(field.label)
    return [ isValid, loc._(t`form.validator.mustPassword`, { label }) ]
}

export function isName({ field }: IFormField): validationResult {
    const isValid = field.value && field.value.length > 5 && field.value.length < 25
    const label = loc._(field.label)
    return [ isValid, loc._(t`form.validator.mustName`, { label }) ]
}

export function isOrganisationId({ field }: IFormField): validationResult {
    const isValid = field.value && Number.isInteger(field.value) && field.value <= 11
    const label = loc._(field.label)
    return [ isValid, loc._(t`form.validator.organisatonId`, { label }) ]
}

export function isNotEmpty({ field }: IFormField): validationResult {
    const isValid = field.value && field.value.length
    const label = loc._(field.label)
    return [ isValid, loc._(t`form.validator.isNotEmpty`, { label }) ]
}
