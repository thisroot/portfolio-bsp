import { REGEXP } from "constants/common"
import { intersection, isEmpty, isObject, isString, merge, values } from 'lodash'
import { ROUTES, STATIC_ROUTES } from "constants/routes"
import { matchPath } from "react-router"
import { createPath, LocationDescriptorObject } from "history"

/**
 * is current location matches routePattern with subRoutes
 *
 * for example a routePattern is '/compaigns'
 * currentLoaction '/campaigns' => true
 * currentLoaction '/campaigns/' => true
 * currentLoaction '/campaigns/campaignId' => true
 * currentLoaction '/campaignsUser' => false
 */
function testNestedRoutes(routePattern: string, currentLocation: string): boolean {
    /* remove trailing slash */
    const location = currentLocation.replace(/\/$/, '')
    return new RegExp(`^${ routePattern }(?=$|/)`, 'i').test(location)
}


function isValidUrl(url: string): boolean {
    return !isEmpty(url.match(REGEXP.urlHostname))
}

function isUrlNativeForUnauthorizedUsers(url: string): boolean {
    // статичвеские роуты пропускаем для всех пользователей
    // ROUTE_PREF, - open main page
    // ROUTE_PREF + '/',
    const withoutAuthRoutes = [
        ...values(STATIC_ROUTES).map(item => item.ROUTE)
    ]

    return withoutAuthRoutes.includes(url)
}

function isUrlValidForUnauthorizedUsers(url: string): boolean {

    const isNative = isUrlNativeForUnauthorizedUsers(url)

    if (isNative) {
        return true
    }

    const isTrailingSlash = url[url.length - 1] === '/'

    const cleanUrl = isTrailingSlash ? url.substr(0, url.length - 1) : url

    // register routes disalow unauthorized access
    const withUnAuthRoutes = [
        // ROUTES.PLAYGROUND.ROUTE
    ]

    if (withUnAuthRoutes.includes(cleanUrl)) {
        return true
    }

    return (cleanUrl.substr(1).includes('/'))
}


function isOuterURL(url: string): boolean {
    let currentHost = typeof window !== "undefined" ? window.location.hostname : process.env.APP_PUBLIC_URL
    if (isValidUrl(url)) {
        return !url.includes(currentHost)
    } else {
        return false
    }
}

function clearUrl(str: string) {
    return str.toString().toLowerCase()
      .replace(/\/\/+/g, '/') // Replace multiple / with single /
      .replace(/^\/+/, '') // Trim / from start of text
      .replace(/\/+$/, '') // Trim / from end of text
      .replace(/:\/+/g, '://') // add double slash into ptotocol
      .replace('/undefined/', '/')
}

function isAllowRoute(path: string, userPermissions: Set<string>) {
    if (!path) return false

    if (isUrlNativeForUnauthorizedUsers(path)) {
        return true
    }

    let match
    for (let route of Object.values(ROUTES)) {
        if (matchPath(path.split('?')[0], { path: route.ROUTE, exact: true, strict: false })) {
            if (!route.PERM) return true
            match = route.PERM
            break
        }
    }

    return intersection([ ...userPermissions ], match).length > 0
}


function isMatchFromRoutes(path: string, routes: Array<string>) {
    return routes.some(r => matchPath(path.split('?')[0], { path: r, exact: true, strict: false }))
}

function formatPath(path: string | LocationDescriptorObject, currentLocation?: LocationDescriptorObject) {
    if (isString(path)) {
        return path
    }
    if (isObject(path)) {
        return createPath(merge({}, currentLocation || {}, path))
    }
    return null
}

function isExternal(url) {
    const match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/)
    if (typeof match[1] === "string" && match[1].length > 0 && match[1].toLowerCase() !== window.location.protocol) return true
    return typeof match[2] === "string" && match[2].length > 0 && match[2].replace(new RegExp(":(" + {
        "http:": 80,
        "https:": 443
    }[window.location.protocol] + ")?$"), "") !== window.location.host
}

export {
    testNestedRoutes,
    isValidUrl,
    isOuterURL,
    clearUrl,
    isUrlValidForUnauthorizedUsers,
    isUrlNativeForUnauthorizedUsers,
    isAllowRoute,
    formatPath,
    isExternal,
    isMatchFromRoutes
}
