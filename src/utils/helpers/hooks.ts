import { useCallback, useState } from "react"

export default function useFunctionAsState(fn) {

    const [val, setVal] = useState(() => fn);

    const setFunc = useCallback((fn) => {
        setVal(() => fn);
    }, [])

    return [val, setFunc];
}