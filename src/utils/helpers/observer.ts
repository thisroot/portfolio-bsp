enum ActionTypes {
    fire = 'fire',
    unsubscribe = 'unsubscribe'
}

enum ObservableTypes {
    logout = 'logout'
}

class Observer {
    private subscribers = {}

    public subscribe = (type: ObservableTypes, fn: Function, context) => {
        fn = typeof fn === 'function' ? fn : context[fn]
        if (typeof this.subscribers[type] === 'undefined') {
            this.subscribers[type] = []
        }
        this.subscribers[type].push({ fn, context: context || this })
    }

    public unsubscribe = (type: ObservableTypes, fn: Function, context) => {
        this.visitSubscribers(ActionTypes.unsubscribe, type, fn, context || this)
    }

    public fire = (type: ObservableTypes, publication) => {
        this.visitSubscribers(ActionTypes.fire, type, publication, null)
    }

    private visitSubscribers = (action: ActionTypes, type: ObservableTypes, arg, context) => {
        const subscribers = this.subscribers[type]
        const max = subscribers ? subscribers.length : 0
        for (let i = 0; i < max; i += 1) {
            switch (action) {
                case ActionTypes.fire:
                    subscribers[i].fn.call(subscribers[i].context, arg)
                    break
                case ActionTypes.unsubscribe:
                default:
                    if (subscribers[i].fn === arg &&
                      subscribers[i].context === context) {
                        subscribers[i] = null
                    }
                    break
            }
        }
        this.subscribers[type] = subscribers ? subscribers.filter((item) => item !== null) : []
    }
}

const observer = new Observer()

export { observer, ObservableTypes }
