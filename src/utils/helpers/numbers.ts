import { loc } from 'utils/i18n'
import { t } from '@lingui/macro'
import { unescape } from "lodash"
import { memoiser } from "./common"
import currency from 'currency.js'


function formatCurrency(cur, loc: string = '') {
    switch (loc) {
        case 'eu':
            return currency(cur, { symbol: "€" }).format()
        case 'us':
            return currency(cur).format()
        case 'ru':
            return currency(cur, { symbol: "₽", separator: " ", decimal: " ." }).format()
        default:
            return cur % 1 !== 0 ? currency(cur, {
                symbol: "",
                separator: " ",
                decimal: " ."
            }).format() : currency(cur, { symbol: "", separator: " ", decimal: " ." }).format().split(' .')[0]
    }
}

class NumberParser {
    private _group: RegExp
    private _decimal: RegExp
    private _numeral: RegExp
    private _index: (d) => number

    constructor(locale) {
        const format = new Intl.NumberFormat(locale)
        const parts = format.formatToParts(12345.6)
        const numerals = Array.from({ length: 10 }).map((_, i) => format.format(i))
        const index = new Map(numerals.map((d, i) => [ d, i ]))
        this._group = new RegExp(`[${parts.find(d => d.type === "group").value}]`, "g")
        this._decimal = new RegExp(`[${parts.find(d => d.type === "decimal").value}]`)
        this._numeral = new RegExp(`[${numerals.join("")}]`, "g")
        this._index = d => index.get(d)
    }

    parse(string) {
        const parsedString = string.trim()
          .replace(this._group, "")
          .replace(this._decimal, ".")
          .replace(this._numeral, this._index)
        return isNaN(+parsedString) ? string : parsedString
    }
}

/* Abbreviates a number, e.g. 1253 => 1.3k */
function abbreviateNumber(val: number | string, decimalPlaces: number = 1, isDownward: boolean = false, startBorder: number = 3, saveZero = false): string {
    const value = +val
    if (isNaN(value)) return `${ val }`
    if (!value && !saveZero) return ''

    const countMethod = Math[isDownward ? 'floor' : 'round']

    let number = Math.max(0, value)

    /* This function ignores negative numbers and strings */
    if (!number) return unescape(value + '')

    const postfixes = [ '', loc._(t`x3`), loc._(t`x6`), loc._(t`x9`), loc._(t`x12`), loc._(t`x15`), loc._(t`x18`) ]

    // Border abbreviate by decimal range
    if (Math.pow(10, startBorder + 1) - 1 > number) return `${ number }`


    /*
    * 999 => 0
    * 1000 => 1
    * 1000000 => 2
    * etc
    */
    const tier = Math.floor(Math.log10(value) / 3)
    if (tier) {
        const scaled = number / (Math.pow(10, (tier * 3)))
        const ROUND_DEGREE = 1000
        number = parseFloat((countMethod(scaled * ROUND_DEGREE) / ROUND_DEGREE).toFixed(decimalPlaces))
    }

    return `${ number } ${ postfixes[tier] }`.trim()
}

function romanizeNumber(num) {
    if (isNaN(num))
        return NaN
    var digits = String(+num).split(""),
      key = [ "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
          "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
          "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" ],
      roman = "",
      i = 3
    while (i--)
        roman = (key[+digits.pop() + (i * 10)] || "") + roman
    return Array(+digits.join("") + 1).join("M") + roman
}

const computeStages = memoiser(function (value: number) {
    let stage = 0
    switch (true) {
        case value === 0:
            break
        case value > 0 && value < 30:
            stage = 1
            break
        case value > 30 && value < 50:
            stage = 2
            break
        case value >= 50 && value < 100:
        default:
            stage = 4
            break
    }
    return stage
})


export {
    abbreviateNumber,
    romanizeNumber,
    computeStages,
    formatCurrency,
    NumberParser
}
