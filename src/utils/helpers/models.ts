import { getSnapshot } from "mobx-state-tree"
import { get, keyBy, set, isEmpty, isNil } from "lodash"
import { findInObject, flatNestedFields } from './'
import { toaster } from "../../components/UI/Toast"

function getUserFullName(user, local = 'ru') {
    return `
        ${ get(user, `lastname_${ local }`) ? user[`lastname_${ local }`] : '' } 
        ${ get(user, `firstname_${ local }`) ? user[`firstname_${ local }`] : '' }
        ${ get(user, `patronymic_${ local }`) ? user[`patronymic_${ local }`] : '' }
    `
}

function getAdminFullName(user) {
    return `
        ${ isNil(get(user, 'lastname', '')) ? '' : get(user, 'lastname', '') }
        ${ isNil(get(user, 'firstname', '')) ? '' : get(user, 'firstname', '') }
        ${ isNil(get(user, 'patronymic', '')) ? '' : get(user, 'patronymic', '') }
    `
}

function getUserOrAdminFullName(user, local = 'ru') {
    return getUserFullName(user, local).trim() || getAdminFullName(user).trim()
}

function initMetaform(entity, label, templatePath = 'project', lang = 'ru') {
    const metaTemplate = get(entity, templatePath + `_type_id.passport_template_${ lang }`)
    const templateName = get(entity, templatePath + '_type_id.alias')
    const typeFieldsArr = get(metaTemplate, 'fields')
    if (!typeFieldsArr) return null

    const typeFields = keyBy(typeFieldsArr, 'fieldKey')

    const formFields = {}
    const groups = get(metaTemplate, 'groups')
    const groupedKeys = []
    if (!groups) return null
    for (const group in groups) {
        if (groups.hasOwnProperty(group)) {
            const groupFields = get(groups[group], 'groupFields')
            if (!groupFields) continue
            const newGroup = {
                name: get(groups[group], 'groupKey'),
                label: get(groups[group], 'groupLabel'),
                fields: {}
            }
            Object.assign(formFields, { [`${ get(groups[group], 'groupKey') }`]: newGroup })
            for (const field in groupFields) {
                if (groupFields.hasOwnProperty(field)) {
                    const key = get(typeFields[groupFields[field]], 'fieldKey')
                    groupedKeys.push(key)
                    const type = typeFields[groupFields[field]].fieldType
                    const value = type === 'target_ref' ?
                      get(entity, get(typeFields[groupFields[field]], 'fieldLabel[1]')) :
                      get(entity, `passport_data_${ lang }[${ templateName }][${ key }]`)
                    const label = type === 'target_ref' ?
                      get(typeFields[groupFields[field]], 'fieldLabel[0]') :
                      get(typeFields[groupFields[field]], 'fieldLabel')

                    Object.assign(newGroup.fields, {
                        [`${ key }`]: {
                            name: key,
                            value,
                            type: type === 'target_ref' ? `${ type };${ get(typeFields[groupFields[field]], 'fieldLabel[1]') }` : type,
                            label,
                        }
                    })
                }
            }
        }
    }

    const ungroupedGroup = {
        name: 'ungrouped',
        label: label,
        fields: {}
    }

    Object.entries(typeFields).forEach(entry => {
        const key = entry[1]['fieldKey']
        const type = entry[1]['fieldType']
        const value = type === 'target_ref' ? get(entity, get(entry[1], 'fieldLabel[1]')) : get(entity, `passport_data_${ lang }[${ templateName }][${ key }]`)
        const label = type === 'target_ref' ? get(typeFields[key], 'fieldLabel[0]') : get(typeFields[key], 'fieldLabel')
        if (!groupedKeys.includes(key)) {
            Object.assign(ungroupedGroup.fields, {
                [`${ key }`]: {
                    name: key,
                    value,
                    type: type === 'target_ref' ? `${ type };${ get(entry[1], 'fieldLabel[1]') }` : type,
                    label,
                }
            })
        }
    })

    if (Object.keys(get(ungroupedGroup, 'fields')).length > 0) {
        Object.assign(formFields, { [ungroupedGroup.name]: ungroupedGroup })
    }

    return formFields
}

export enum ENTITY_FIELDS {
    MAIN_WIDGET = 'mainWidget',
    LIST_WIDGET = 'listWidget',
    GUNT_WIDGET = 'guntWidget',
    ALL = 'all'
}

export interface IField {
  fieldKey: string,
  fieldLabel: string,
  fieldType: string,
  fieldValue?: any
}

export interface IWidgetProps {
  geoInfo?: string
  slotType?: string
  slots: Array<IField>
}

function getMetaInfoFields(entity, templatePath = 'project', lang = 'ru', widgetName: ENTITY_FIELDS = ENTITY_FIELDS.MAIN_WIDGET): IWidgetProps {
    let slots = []
    const metaTemplate = get(entity, templatePath + `_type_id.passport_template_${ lang }`)
    const templateName = get(entity, templatePath + '_type_id.alias')

    if (!isEmpty(metaTemplate)) {
        const typeFields = keyBy(metaTemplate.fields, 'fieldKey')
        const widget = metaTemplate.widgets.find(item => item['widgetKey'] === widgetName)
        slots = get(widget, 'widgetFields', Object.keys(typeFields)).map(item => {
            return ({
                ...typeFields[item],
                fieldLabel: typeFields[item].fieldType === 'target_ref' ? typeFields[item].fieldLabel[0] : typeFields[item].fieldLabel,
                fieldValue: typeFields[item].fieldType === 'target_ref' ? get(entity, typeFields[item].fieldLabel[1]) : get(entity, `passport_data_${ lang }[${ templateName }][${ item }]`)
            })
        })
    }

    return {
        slots,
        slotType: get(entity, templatePath + `_type_id.name_${ lang }`)
    }
}

async function onSaveMeta(form, entity, toggleEdit, onModelUpdate, metaform, templatePath, lang = 'ru') {
    const fields = keyBy(flatNestedFields(form.get(), 'fields'), 'fieldKey')
    const templateName = get(entity, templatePath + '_type_id.alias')
    const templateValues = {}
    let objectFields = {}

    Object.values(fields).forEach(value => {
        const type = findInObject(form.types(), value.fieldKey) || ''
        if (type.match('target_ref')) {
            set(objectFields, `${ type.split(';')[1] }`, +value.fieldValue)
        } else {
            Object.assign(templateValues, { [value.fieldKey]: value.fieldValue })
        }
    })

    Object.entries(objectFields).forEach(entry => {
        entity[entry[0]] = entry[1]
    })

    toggleEdit()
    try {
        entity[`passport_data_${ lang }`] = { [templateName]: templateValues }
        const result = await onModelUpdate({ ...getSnapshot(entity) })
        result.status === 'error' ? metaform.reset() : toaster.success()
    } catch (e) {
        toaster.error()
    }
}

export {
    initMetaform,
    getUserFullName,
    getMetaInfoFields,
    onSaveMeta,
    getUserOrAdminFullName,
}


