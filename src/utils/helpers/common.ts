import { matchPath } from "react-router"
import { appConfig, Route, ROUTES } from "../../constants"
import { isNumber } from "lodash"


let isLocalhost: boolean
if (typeof window !== "undefined") {
    isLocalhost = Boolean(
      window.location.hostname === 'localhost' ||
      // [::1] is the IPv6 localhost address.
      window.location.hostname === '[::1]' ||
      // 127.0.0.1/8 is considered localhost for IPv4.
      window.location.hostname.match(
        /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
      )
    )
} else {
    isLocalhost = true
}

const isDevelopment = appConfig.ENV === 'development'
const isProduction = appConfig.ENV === 'production'
const isLogRocketEnable = appConfig.API.search(/(api.csp24.ru|csp24api.sakhalin.gov.ru|api.dev.csp24.ladcloud.ru)/) !== -1

function makeHash(blocks: number = 1): string {
    let hash = ''
    while (blocks) {
        hash += (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
        --blocks
    }
    return hash
}

/**
 * extend string with params
 * usage:  formatString(string, 'param1', 'param2')
 * @return {String} formatted string | default string | logs a warning message
 */
function formatString(text: string, ...args: any[]): string {
    if (!args.length) return ''// console.warn('No arguments has been specified');
    return text.replace(/{(\d+)}/g, (_match, number) => (
      (typeof args[number] !== 'undefined' && args[number] !== null) ? args[number] : ''
    ))
}

function formatLink(title: string, path: string, key: string = makeHash(4)) {
    return `<Link key="${ key }" to="${ path }">${ title }</Link>`
}

/* token parser */
function parseJwt(token) {
    const base64Url = token.split('.')[1]
    const base64 = base64Url.replace('-', '+').replace('_', '/')
    return JSON.parse(window.atob(base64))
}

// function parsePathname(path: string) {
//     return path.split('/').filter(i => i !== "")
// }

function parsePath(path, route) {
    return matchPath(path, { path: route, exact: true, strict: true })
}

function getRouteByPath(path) {
    return Object.values(ROUTES).find((item: Route) => parsePath(path, item.ROUTE))
}

function getBreadCrumbs(path) {
    const paths = []
    let newPath = ''
    path.split('/').forEach(item => {
        if (item) {
            newPath += `/${ item }`
            paths.push({ route: getRouteByPath(newPath), path: newPath } || { route: { NAME: item }, path: newPath })
        }
    })
    return paths
}

function getRandomArbitrary(min, max) {
    return (Math.random() * (max - min) + min).toFixed(0)
}

function clearTextFromNbsp(value) {
    return !value || isNumber(value) ? value : value.replace(new RegExp('&nbsp;', 'g'), ' ')
}

function memoiser(fun) {
    let cache = {}
    return function (...args) {
        const hash = args && args.join('-')
        if (cache[hash]) {
            return cache[hash]
        } else {
            let result = fun(...args)
            cache[hash] = result
            return result
        }
    }
}

export {
    makeHash,
    formatString,
    formatLink,
    parseJwt,
    isLocalhost,
    isDevelopment,
    isProduction,
    isLogRocketEnable,
    // parsePathname,
    parsePath,
    getRandomArbitrary,
    getRouteByPath,
    getBreadCrumbs,
    clearTextFromNbsp,
    memoiser
}


