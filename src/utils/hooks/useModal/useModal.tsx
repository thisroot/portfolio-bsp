import disableScroll from 'disable-scroll'
import React, { FC, useCallback, useState } from 'react'
import { observer } from "mobx-react-lite"
import { useKey } from 'react-use'
import { createPortal } from 'react-dom'
import styles from './styles.scss'

interface IUseModalOptions {
    preventScroll?: boolean;
}

interface IOpenModalOptions {
    preventEscape?: boolean;
    preventClickAway?: boolean;
    additionalCloseCallback?: () => void | null;
}

interface IModalProps {
    children: any;
    isOpen: boolean;
    close: any;
    elementId: string;
    isPreventEscape: boolean;
    isPreventClickAway: boolean;
}

const Modal: FC<IModalProps> = observer(({
                                             children,
                                             isOpen = false,
                                             close,
                                             elementId = 'root',
                                             isPreventEscape = false,
                                             isPreventClickAway = false
                                         }) => {
    if (isOpen === false) {
        return null
    }
    useKey('Escape', () => {
        if (!isPreventEscape) {
            close()
        }
    })

    return createPortal(
      <div className={ styles.wrapperStyle }>
          <div className={ styles.maskStyle } onClick={ isPreventClickAway ? null : close }/>
          <div className={ styles.containerStyle }>{ children }</div>
      </div>,
      document.getElementById(elementId)
    )
})

export const useModal = (elementId = 'root', options: IUseModalOptions = {}) => {
    const { preventScroll = false } = options
    const [ isOpen, setOpen ] = useState(false)
    const [ isPreventEscape, setPreventEscape ] = useState(false)
    const [ isPreventClickAway, setPreventClickAway ] = useState(false)
    const [ additionalCloseCallbackHandle, setAdditionalCloseCallbackHandle ] = useState(null)

    const open = useCallback((options: IOpenModalOptions = {}) => {
        const { preventEscape = false, preventClickAway = false, additionalCloseCallback = null } = options
        setPreventEscape(preventEscape)
        setPreventClickAway(preventClickAway)
        setAdditionalCloseCallbackHandle(() => additionalCloseCallback)
        setOpen(true)
        if (preventScroll) {
            disableScroll.on()
        }
    }, [ setOpen, preventScroll, isPreventEscape, isPreventClickAway, isOpen ])

    const close = useCallback(() => {
        additionalCloseCallbackHandle && additionalCloseCallbackHandle()
        setOpen(false)
        if (preventScroll) {
            disableScroll.off()
        }
    }, [ setOpen, preventScroll, isOpen ])

    const ModalWrapper = useCallback(({ children }) => {
        return (
          <Modal
            isOpen={ isOpen }
            close={ close }
            elementId={ elementId }
            isPreventEscape={ isPreventEscape }
            isPreventClickAway={ isPreventClickAway }
          >
              { children }
          </Modal>
        )
    }, [ isOpen, close, elementId, isPreventEscape, isPreventClickAway ])

    return [ ModalWrapper as any, open, close ]
}
