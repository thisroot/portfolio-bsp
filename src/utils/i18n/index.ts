import { setupI18n } from "@lingui/core";

const loc = setupI18n()

export {
    loc
}