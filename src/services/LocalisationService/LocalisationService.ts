import { action, observable, runInAction } from "mobx"
import { get } from 'lodash'
import { loc } from 'utils/i18n'
import { Catalog } from "@lingui/core"
import { inject } from "react-ioc"
import { AnalyticsService } from "../AnalyticsService"

export class LocalisationService {

    @inject analytics: AnalyticsService

    @observable
    public pending = true
    @observable
    public i18n = loc
    @observable
    public wasInitialization: boolean
    @observable
    public lang: string = 'ru'

    @action
    public init = async (lang: string = 'ru') => {
        try {
            if (!this.wasInitialization) {
                this.load(lang)
                // for detect reactive language changes
                // isDevelopment && setTimeout(() => {
                //     this.load('en')
                // }, 3000)
            }
        } catch (error) {
            this.analytics.sendError({ error })
        }
    }

    @action
    public load = async (lang: string) => {
        this.pending = true
        if (!get(this.i18n._catalogs, lang)) {
            let catalog: Catalog = null
            try {
                catalog = await import(
                    /* webpackMode: "lazy", webpackChunkName: "i18n-[index]" */
                    `@lingui/loader!locale/${ lang }/messages.po`)
            } catch {
                catalog = await import(
                    /* webpackMode: "lazy", webpackChunkName: "i18n-[index]" */
                    `locale/${lang}/messages.js`)
            }
            await this.i18n.load(lang, catalog)
        }
        await this.i18n.activate(lang)
        runInAction(() => {
            this.lang = this.i18n._locale
            this.wasInitialization = true
            this.pending = false
        })
    }
}
