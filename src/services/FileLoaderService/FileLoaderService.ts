import { action, runInAction } from "mobx"
import {
    Base64,
    DocumentsOptions,
    FileParams,
    FileTypes,
    ID,
    ImageParams,
    ImagesOptions,
    VideoOptions,
    VideoParams
} from "../../models/interfaces"
import { loc } from "../../utils/i18n"
import { t } from "@lingui/macro"
import { FILE_RELATION, UPLOAD_RESTRICTIONS } from "../../constants"
import { ApiService, onProgressEvent } from "../ApiService"
import { inject } from "react-ioc"
import {
    DOCUMENTS_MIME_TYPES, DOCUMENTS_PICKER_OPTIONS,
    FileModel,
    IMAGES_MIME_TYPES,
    IMAGES_PICKER_OPTIONS, SPREAD_SHEETS_MIME_TYPES,
    VIDEO_MIME_TYPES,
    VIDEO_PICKER_OPTIONS
} from "../../models/entities/FileModel"
import { IRelationIdentifier } from "../../models/entities/File"
import { get, isArray } from 'lodash'
import { AnalyticsService } from "../AnalyticsService"

class FileLoaderService {

    @inject api: ApiService
    @inject analytics: AnalyticsService

    @action
    public initFile = async (item: File, relationIdentifier?: IRelationIdentifier): Promise<FileModel> => {
        const file = new FileModel(item, relationIdentifier)
        try {
            file.status.converting = 0
            file.status.uploading = 0
            if (!file.wasInitialised) {
                let data: FileParams
                switch (true) {
                    case IMAGES_MIME_TYPES.includes(get(file, 'native.type')):
                        data = { type: FileTypes.IMAGE, ...await this.loadImage(file.native, IMAGES_PICKER_OPTIONS) }
                        break
                    case VIDEO_MIME_TYPES.includes(get(file, 'native.type')):
                        data = { type: FileTypes.VIDEO, ...await this.loadVideo(file.native, VIDEO_PICKER_OPTIONS) }
                        break
                    case DOCUMENTS_MIME_TYPES.includes(get(file, 'native.type')):
                    case SPREAD_SHEETS_MIME_TYPES.includes(get(file, 'native.type')):
                        data = { type: FileTypes.FILE, ...await this.loadFile(file.native, DOCUMENTS_PICKER_OPTIONS) }
                }
                if (data) {
                    runInAction(() => {
                        file.typeGroup = data.type
                        file.animated = data.animated
                        file.localUrl = data.uri
                        file.wasInitialised = true
                    })
                }
            }
            return file
        } catch (error) {
            this.analytics.sendError({ error })
            throw error
        }
    }

    @action
    public uploadFile = async (file: FileModel, needConvert: boolean = true): Promise<any> => {
        try {
            if (file.typeGroup === FileTypes.IMAGE) {
                const image = await this.uploadImage(file.native, file.relationType, file.relationId, file.onProgress(), needConvert)
                return get(image, 'image_id') ? image : get(image, 'image')
            } else if (file.typeGroup === FileTypes.FILE) {
                const fl = await this.uploadDocument(file.native, file.relationType, file.relationId, file.parentId , file.onProgress())
                return get(fl, 'file')
            }
            return file
        } catch (error) {
            runInAction(() => {
                this.analytics.sendError({ error })
                file.uploadError = true
                return file
            })
        }
    }


    /* read and validate image */
    @action
    public loadImage = (file: File, options: ImagesOptions): Promise<ImageParams> => {
        return new Promise((resolve, reject) => {

            /* MIME TYPES & FILE SIZE */
            if (!options.mimeTypes.includes(file.type)) {
                return reject(new Error(loc._(t`components.imagesListModal.errors.mimeType.text`)))
            }
            if (file.size > options.maxFileSize) {
                const error = loc._(t`components.imagesListModal.errors.fileSize.max`, `${ options.maxFileSize / 1024 / 1024 }mb`)
                return reject(new Error(error))
            }

            const fileReader = new FileReader()
            const imageEl: any = new Image()

            fileReader.onloadend = () => {
                const image = fileReader.result

                imageEl.addEventListener('load', () => {
                    const dimensions = {
                        width: imageEl.width,
                        height: imageEl.height
                    }

                    /* DIMENSIONS */
                    if (dimensions.width < options.minWidth || dimensions.height < options.minHeight) {
                        const error =
                          loc._(t`components.imagesListModal.errors.size.text`, `${ options.minWidth } x ${ options.minHeight }`)
                        return reject(new Error(error))
                    }

                    const result: any = {
                        file,
                        uri: image,
                        animated: file.type === UPLOAD_RESTRICTIONS.IMAGES_MIME_TYPES.gif
                    }

                    return resolve(result)
                })

                imageEl.onerror = () => {
                    /* FILE ERRORS */
                    return reject(new Error(loc._(t`components.imagesListModal.errors.default`)))
                }
                imageEl.src = image
            }

            fileReader.onerror = () => {
                /* FILE ERRORS */
                return reject(new Error(loc._(t`components.imagesListModal.errors.default`)))
            }
            fileReader.readAsDataURL(file)
        })
    }

    @action
    public loadFile = (file: File, options: DocumentsOptions): Promise<FileParams> => {
        return new Promise((resolve, reject) => {
            /* MIME TYPES & FILE SIZE */
            if (!options.mimeTypes.includes(file.type)) {
                return reject(new Error(loc._(t`components.imagesListModal.errors.mimeType.text`)))
            }
            if (file.size > options.maxFileSize) {
                const error = loc._(t`components.imagesListModal.errors.fileSize.max`, `${ options.maxFileSize / 1024 / 1024 }mb`)
                return reject(new Error(error))
            }
            const fileUri = URL.createObjectURL(file)

            resolve({
                file,
                uri: fileUri,
                type: FileTypes.FILE
            })
        })
    }

    /* read and validate video */
    @action
    public loadVideo = (file: File, options: VideoOptions): Promise<VideoParams> => {
        return new Promise((resolve, reject) => {
            /* MIME TYPES */
            if (!options.mimeTypes.includes(file.type)) {
                return reject(new Error(loc._(t`error.default`)))
            }

            if (file.size > options.maxFileSize) {
                const error = loc._(t`components.imagesListModal.errors.fileSize.max`, `${ options.maxFileSize / 1024 / 1024 }mb`)
                return reject(new Error(error))
            }

            const video = document.createElement('video')
            const videoUri = URL.createObjectURL(file)
            video.preload = 'metadata'
            video.ondurationchange = () => {

                if (Math.floor(video.duration) > options.maxDuration) {
                    const error = loc._(t`components.imagesListModal.errors.videoMaxDuration`, `${ options.maxDuration / 60 }`)
                    return reject(new Error(error))
                }

                return resolve({
                    file,
                    uri: videoUri
                })
            }

            video.onerror = () => {
                return reject(new Error(loc._(t`components.imagesListModal.errors.default`)))
            }
            video.src = videoUri
        })
    }

    /**
     * Получает список файлов изображений {File}
     * Загружает их на сервер
     * Производит конвертацию под соответствующий тип объека
     * И возвращает массив ID подготовленных картинок
     *
     * @param {Array<any>} rawImages
     * @param type
     * @param relation_ids
     * @returns {Promise<Array<ID>>}
     */
    @action
    public uploadImages = async (rawImages: Array<File | Base64>, type: FILE_RELATION, relation_ids: Array<number> | number): Promise<Array<ID>> => {
        const resImages = []
        for (const item in rawImages) {
            if (typeof item === 'string') {
                resImages.push(rawImages[item])
            } else {
                const uploadResponse = await this.api.files.uploadImage(rawImages[item] as File, type, isArray(relation_ids) ? relation_ids[item] : relation_ids)
                //const convertResponse = await rest.image.convert(type, uploadResponse.result[0]._id)
                //resImages.push(convertResponse.result._id)
                resImages.push(uploadResponse.body)
            }
        }
        return resImages
    }
    /**
     * Получает файл {File}
     * Загружает их на сервер
     * Производит конвертацию под соответствующий тип объека
     * И возвращает отконвертированную картинку
     *
     * @param {FileModel.ts} image
     * @param type
     * @param relation_id
     * @param needConvert
     * @param onProgress
     * @returns {Promise<Avatar>}
     */
    @action uploadImage = async (
      image: File,
      type: FILE_RELATION,
      relation_id: number,
      onProgress?: {
          upload?: (event: onProgressEvent) => any,
          convert?: (event: onProgressEvent) => any
      },
      needConvert: boolean = true,): Promise<any> => {
        try {
            const uploadResponse = await this.api.files.uploadImage(image, type, relation_id, onProgress && onProgress.upload)
            if (uploadResponse.status === 'error') {
                throw new Error(uploadResponse.message)
            }
            if (needConvert) {
                const convertResponse = await this.api.files.convertImage(get(uploadResponse, 'body.image.image_id') as number, onProgress && onProgress.convert)
                if (convertResponse.status === 'error') {
                    throw new Error(convertResponse.message)
                }
                return get(convertResponse, 'body', null)
            } else {
                return get(uploadResponse, 'body', null)
            }
        } catch (e) {
            this.analytics.sendError({ error: e })
            throw e
        }
    }

    @action uploadDocument = async (
      doc: File, type: FILE_RELATION, relation_id: number, parent_id: number,
      onProgress?: {
          upload?: (event: onProgressEvent) => any,
          convert?: (event: onProgressEvent) => any
      }): Promise<any> => {
        try {
            const uploadResponse = await this.api.files.uploadFile(doc, type, relation_id, parent_id, onProgress && onProgress.upload)
            if (uploadResponse.status === 'error') {
                throw new Error(uploadResponse.message)
            }
            return get(uploadResponse, 'body') as number
        } catch (e) {
            this.analytics.sendError({ error: e })
            throw e
        }
    }

}

export { FileLoaderService }
