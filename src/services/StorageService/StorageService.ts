import { inject } from "react-ioc"
import { getSnapshot, applySnapshot } from "mobx-state-tree";
import { DataContext } from "services/DataContext";
import { action, observable } from "mobx"
import { isNull } from "lodash"
import { isIsoDateString } from 'utils/helpers'
import { LocalDB } from "utils/localDB"

export class StorageService {
  @observable pending: boolean = true
  @observable wasInitialized: boolean = false
  // @observable db: LocalForage = null
  @observable localDB: LocalDB = null
  @inject
  private dataContext: DataContext

  @action
  public init = async () => {
    if(!this.wasInitialized) {
      this.pending = true
      try {
        this.localDB = new LocalDB()
        const snapshot = null // JSON.parse(await this.db.getItem("data"))
        if (isNull(snapshot)) {
          await this.reset()
        } else {
          applySnapshot(this.dataContext, { ...snapshot, router: this.dataContext.router });
          JSON.stringify(this.dataContext);
        }
      } catch (e) {
        await this.reset();
      }
      this.wasInitialized = true
    }
  }

  public dispose = async () => {
    await this.localDB.setItem("data", JSON.stringify(getSnapshot(this.dataContext)));
  }

  public reset = async () => {
    // const posts = this.parse(JSON.stringify(postsJson));
    // const { entities } = normalize(posts, [PostSchema]);
    //TODO: отрефакторить сохранение данных роутера при сбросе моделей
    // applySnapshot(this.dataContext, { ...entities, router: this.dataContext.router } );
  }

  public parse = (text: string) => {
    return JSON.parse(text, (key, val) => {
      if (key === "id") return String(val);
      if (isIsoDateString(val)) return Number(new Date(val));
      return val;
    });
  }
}
