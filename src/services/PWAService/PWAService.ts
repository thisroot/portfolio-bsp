import { SingletonClass } from 'models'
import { get } from 'lodash'
import { PWAMessages, SERVICE_WORKER_STATE } from 'constants/common'
import { action, computed, observable } from "mobx"
import { register } from 'register-service-worker'
import { inject } from "react-ioc"
import { StorageService } from "../StorageService"
import { isDevelopment } from "../../utils/helpers"

// import { isDevelopment } from "utils/helpers"

interface PWAConfig {
    onSuccess?: (worker: ServiceWorker, event: Event) => void,
    onUpdate?: (worker: ServiceWorker, event: Event) => void
}

interface PWAMessage {
    type: PWAMessages,
    message?: string | Array<string>
}

class PWAService extends SingletonClass {


    @inject
    public storageService: StorageService


    @computed
    public get isAPPInstalled() {
        return this.storageService.localDB.getItem('appinstalled')
    }

    @observable
    public workerState: string = SERVICE_WORKER_STATE.DISABLED

    @observable
    public updating = false

    @observable
    public deferredPrompt = null

    private worker: ServiceWorker | null
    private navigator: Navigator | null

    @action
    public init = () => {

        if (!isDevelopment) {
            this.register()
            this.beforeInstall()
        }
    }

    constructor() {
        super()
        this.worker = null
        this.navigator = null
    }

    @action
    public messageUpdateCache = () => {
        this.sendMessage({
            type: PWAMessages.SET_ADDITIONAL_API_CACHED_RESOURCES,
            message: JSON.stringify({
                host: process.env.BASEURL,
                resources: [
                    process.env.NAVIGATION_PATH,
                    process.env.MAP_MD_FILES
                ]
            })
        })
    }

    @action
    public sendMessage = (message: PWAMessage): boolean => {
        if (get(this.navigator, 'WorkerService.controller') && get(this, 'worker.state') === SERVICE_WORKER_STATE.ACTIVATED) {
            console.log('message', message.type)
            return get(this.navigator, 'WorkerService.controller').postMessage(message) && true
        } else {
            return false
        }
    }

    @action beforeInstall = () => {
        window.addEventListener('beforeinstallprompt', (e) => {
            // Prevent Chrome 67 and earlier from automatically showing the prompt
            e.preventDefault()
            this.storageService.localDB.removeItem('appinstalled')
            // Stash the event so it can be triggered later.
            this.deferredPrompt = e
        })
    }

    @action installApp = () => {
        this.deferredPrompt.prompt()
        this.deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
                this.storageService.localDB.setItem('appinstalled', true)
                window.location.reload()
            } else {
                console.log('User dismissed the A2HS prompt')
            }
            this.deferredPrompt = null
        })
    }

    @action
    public register = (config?: PWAConfig): boolean => {

        register('/worker.js', {
            ready: (registration) => {
                console.log('Service worker is active.')
                this.worker = registration.active
                this.workerState = this.worker.state
                // по сути штука не нужная в рамках сервисов, так как они и так связаны друг с другом и могут отслеживать изменение состояний
                this.worker.onstatechange = this.onWorkerStateChange(config)
            },
            registered: () => {
                console.log('Service worker has been registered.')
            },
            cached: () => {
                console.log('Content has been cached for offline use.')
            },
            updatefound: (registration) => {
                console.log('New content is downloading.')
                this.updating = true
                const installingWorker = registration.installing
                if (installingWorker == null) {
                    return
                }
                this.worker = installingWorker
                this.workerState = this.worker.state
                this.worker.onstatechange = this.onWorkerStateChange()
            },
            updated: () => {
                console.log('New content is available; please refresh.')
                //TODO; протестировать, можно ли не перезагружать приложение
                // window.location.reload()
                console.log('refresh')
                this.sendMessage({ type: PWAMessages.SKIP_WAITING })
                this.updating = false
            },
            offline: () => {
                console.log('No internet connection found. App is running in offline mode.')
            },
            error: (error) => {
                console.error('Error during service worker registration:', error)
            }
        })
        return true
    }
    @action
    public unregister = () => {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.ready.then(registration => {
                delete this.navigator
                delete this.worker
                registration.unregister().then(() => {
                    // observer.fire(ObservableTypes.workerChangeState, SERVICE_WORKER_STATE.DISABLED)
                    // console.event('PWA', SERVICE_WORKER_STATE.DISABLED)
                })
            })
        }
    }

    @action
    private onWorkerStateChange = (config?: PWAConfig) => {
        return (event: Event): void => {
            // observer.fire(ObservableTypes.workerChangeState, installingWorker.state)
            switch (this.worker.state) {
                case 'installed':
                    if (navigator.serviceWorker.controller) {
                        // At this point, the updated precached content has been fetched,
                        // but the previous service worker will still serve the older
                        // content until all client tabs are closed.
                        // Execute callback
                        if (config && config.onUpdate) {
                            config.onUpdate(this.worker, event)
                        }
                    }
                    break
                case 'activated':
                    if (config && config.onSuccess) {
                        config.onSuccess(this.worker, event)
                    }
                    break
                default:
                    break
            }
        }
    }
}

export {
    PWAMessages,
    PWAService
}
