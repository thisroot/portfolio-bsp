import { inject } from "react-ioc"
import { AuthService } from 'services/AuthService'
import { RouterService } from "services/RouterService"
import { action, computed, observable, observe, runInAction } from "mobx"
import {
    changeTheme,
    isDevelopment,
    isThemeExist,
    isUrlNativeForUnauthorizedUsers,
    isUrlValidForUnauthorizedUsers,
    parseProxy
} from "utils/helpers"
import { APP_STATE, DEFAULT_LANG } from 'constants/common'
import React from "react"
import { get, throttle } from 'lodash'
import detectBrowserLanguage from 'detect-browser-language'
import { StorageService } from "services/StorageService"
import { PWAService } from "services/PWAService"
import { AnalyticsService } from "services/AnalyticsService"
import { LocalisationService } from "services/LocalisationService"
import { toaster } from "../../components/UI/Toast"
import { t } from "@lingui/macro"
import { SettingsService } from "../SettingsService"

export enum ScrollDirection {
    UP = 'up',
    DOWN = 'down'
}

const THRESHOLD = 0

export class AppService {

    @inject
    private authService: AuthService
    @inject
    private routerService: RouterService
    @inject
    private storageService: StorageService
    @inject
    private pwaService: PWAService
    @inject
    private analyticsService: AnalyticsService
    @inject
    private localisationService: LocalisationService
    @inject
    private settings: SettingsService
    @observable
    public appScrollEl: HTMLDivElement
    @observable
    public scrollOnTop: number = 0
    @observable
    public contentContainerWidth: number
    @observable
    public wasInitialized: boolean = false
    @observable
    public isCloseLeftMenu: boolean = false
    @observable
    public isCloseRightMenu: boolean = true
    @observable
    public triggerForActionHideHeader: number | null = null
    @observable
    public isDarkTheme = isThemeExist()

    @action
    public changeTheme = () => {
        this.isDarkTheme = changeTheme()
        this.storageService.localDB.setItem('is-dark-theme', this.isDarkTheme)
    }

    @observable modalControls = {
        close: null,
        open: null,
        onCloseEvent: null,
        renderModal: () => null,
    }

    @observable renderRightMenu = () => null

    @computed
    public get i18n() {
        return this.localisationService.i18n
    }

    @computed
    public get lang() {
        return this.localisationService.lang
    }

    @computed
    public get isPending() {
        return !this.localisationService.wasInitialization || this.state === APP_STATE.pending || this.authService.pending
    }

    @observable
    public state: APP_STATE = APP_STATE.pending
    @observable
    public browserLang: string = get(detectBrowserLanguage().split('-'), '[0]', DEFAULT_LANG)

    public computeAppState = (pathname: string): APP_STATE => {
        // обрабатываем роуты на наличие тех, которые доступны неавторизованным пользователям
        //check route if unauthorized, and route allow unautorized location
        if (!isUrlValidForUnauthorizedUsers(pathname)) {
            return APP_STATE.auth
        } else {
            if (!isUrlNativeForUnauthorizedUsers(pathname)) {
                return APP_STATE.auth
            } else {
                // need create quest user
                return APP_STATE.quest
            }
        }
    }

    @action
    public init = async () => {
        try {
            if (!this.wasInitialized) {
                this.state = APP_STATE.pending
                await Promise.all([
                    this.localisationService.init(this.browserLang),
                    this.pwaService.init(),
                    this.storageService.init(),
                    this.authService.init(),
                    this.analyticsService.init()
                ])

                const isDarkTheme = JSON.parse(this.storageService.localDB.getItem('is-dark-theme'))
                if (isDevelopment && (isDarkTheme)) {
                    this.changeTheme()
                }


                this.mapEnabled = JSON.parse(this.storageService.localDB.getItem('map-enabled'))

                if (this.authService.isAuthorized) {
                    await this.settings.init()
                    this.state = APP_STATE.app
                    if (this.routerService.history.location.pathname === '/') {
                        this.routerService.replace(this.settings.mainPage)
                    }
                    if (!this.authService.isAllowRoute(this.routerService.history.location.pathname)) {
                        this.routerService.replace('/nomatch')
                    }
                } else {
                    const { pathname } = this.routerService.history.location
                    this.state = this.computeAppState(pathname)
                }

                this.isCloseLeftMenu = JSON.parse(this.storageService.localDB.getItem('menu-position'))
                runInAction(() => this.wasInitialized = true)
                this.networkStatus()
                if (!window.navigator.onLine) {
                    this.renderOfflineToaster()
                }
            }
        } catch (error) {
            this.analyticsService.sendError({ error })
        }
    }

    private authOnChange = observe(this.authService, 'isAuthorized', async (change) => {
        if (change.newValue) {
            this.state = APP_STATE.pending
            await this.authService.init()
            await this.settings.init(true)
            this.state = APP_STATE.auth
        }
        if (this.wasInitialized) {
            if (parseProxy(change.newValue)) {
                if (this.routerService.pathname === '/') {
                    this.routerService.replace(this.settings.mainPage)
                }
                if (!this.authService.isAllowRoute(this.routerService.history.location.pathname)) {
                    this.routerService.replace('/nomatch')
                }
                this.state = APP_STATE.app
                !isDevelopment && this.authService.isAllow('user.chat') && window.postMessage({
                  token: get(this.authService.currentUserSession, 'token', false)
                });
            } else {
                runInAction(() => {
                    this.state = this.computeAppState(this.routerService.pathname)
                })
                window.postMessage({
                  logout: true
                });
            }
        }
    })

    //детектим изменения роутов для их обработки. еще надо  будет следить за статусом авторизации пользователя
    private routeOnChange = observe(this.routerService, 'pathname', (change) => {
        if (this.wasInitialized) {
            const newLocation: string = parseProxy(change.newValue)
            if (!this.authService.isAuthorized) {
                runInAction(() => {
                    this.state = this.computeAppState(newLocation)
                })
            }

            if (this.routerService.pathname === '/') {
                this.routerService.replace(this.settings.mainPage)
            }
            if (!this.authService.isAllowRoute(newLocation)) {
                this.routerService.replace('/nomatch')
            }
        }
    })

    @action renderOfflineToaster = () => {
        this.toast = toaster.error(this.i18n._(t`components.app.offlineMessage`), {
            containerId: 'controlled',
            autoClose: false,
            closeOnClick: false,
            closeButton: false
        })
    }

    @action networkStatus = () => {
        window.addEventListener('offline', () => {
            this.isOnline = false
            this.renderOfflineToaster()
        })
        window.addEventListener('online', () => {
            this.isOnline = true
            toaster.dismiss(this.toast)
            toaster.success(this.i18n._(t`components.app.onlineMessage`))

        })
    }

    @observable
    public toast

    @observable
    public isOnline = window.navigator.onLine

    @observable
    public mapEnabled: boolean
    @action
    public toggleMap = () => {
        this.storageService.localDB.setItem('map-enabled', JSON.stringify(!this.mapEnabled))
        this.mapEnabled = !this.mapEnabled
    }
    @observable
    public searchVisible: boolean
    @action
    public toggleSearch = () => {
        this.searchVisible = !this.searchVisible
    }
    @action
    public toggleLeftMenu = () => {
        this.storageService.localDB.setItem('menu-position', JSON.stringify(!this.isCloseLeftMenu))
        this.isCloseLeftMenu = JSON.parse(this.storageService.localDB.getItem('menu-position'))
    }
    @action
    public registerTriggerForHeader = (element: React.MutableRefObject<HTMLDivElement>) => {
        const currentElement = get(element, 'current', false)
        if (currentElement) {
            this.triggerForActionHideHeader = currentElement.offsetTop
        }
    }
    @action
    public registerScroll = (scroll: React.MutableRefObject<HTMLDivElement>) => {
        if (get(scroll, 'current')) {
            this.appScrollEl = scroll.current
            this.appScrollEl.addEventListener('scroll', throttle(() => {
                this.scrollOnTop = this.appScrollEl.scrollTop
            }, 150))
        }
    }

    @action
    public setContainerWidth = (width: number) => {
        this.contentContainerWidth = width
    }

    @action
    public scrollTo = (direction: ScrollDirection = ScrollDirection.UP) => {
        if (direction === ScrollDirection.DOWN) {
            this.appScrollEl.scrollTop = this.appScrollEl.scrollHeight
        } else {
            this.appScrollEl.scrollTop = THRESHOLD
        }
    }

    @action public registerModal = (open, close) => {
        this.modalControls = { ...this.modalControls, open, close }
    }

    @action
    public pageNotFound = () => {
        this.state = APP_STATE.noMatch
    }


    public dispose = () => {
        this.routeOnChange()
        this.authOnChange()
    }
}

export { APP_STATE }
