import { applySnapshot, getSnapshot, Instance, protect, unprotect, addMiddleware } from "mobx-state-tree"
import { Store } from "models/mst"
import { RouterModel, syncHistoryWithStore } from 'mst-react-router'
import merge from 'deepmerge'
import { toaster } from "components/UI/Toast"
import { runInAction } from "mobx"
import { isDevelopment, isLogRocketEnable } from "utils/helpers"
import { get, isArray, isEmpty, isObject, union } from "lodash"
import md5 from 'md5'
import { ICacheParams } from "../models/mst/TableState"
import { Response } from "./ApiService"
import LogRocket from "logrocket"

const createBrowserHistory = require('history').createBrowserHistory

export const routerModel = RouterModel.create()
export const history = syncHistoryWithStore(createBrowserHistory(), routerModel)


export class DataContext {
    private static models: any
    private static initialState: any

    static create() {
        DataContext.models = Store.create({ router: routerModel })
        DataContext.initialState = getSnapshot(DataContext.models)

        import('mst-middlewares').then(module => {
            protect(DataContext.models)
            if (isDevelopment) {
                module.connectReduxDevtools(require("remotedev"), DataContext.models, { logArgsNearName: false })
            } else {
                addMiddleware(DataContext.models, module.simpleActionLogger)
                module.asReduxStore(DataContext.models, LogRocket.reduxMiddleware())
            }
            unprotect(DataContext.models)
        })

        unprotect(DataContext.models)
        DataContext.models.applySnapshot = (snapshot: any, rewrite: boolean = false): boolean => {
            try {
                if (rewrite) {
                    applySnapshot(DataContext.models, { ...snapshot, router: routerModel })
                } else {
                    const oldSnap = getSnapshot(DataContext.models)
                    if (!isEmpty(snapshot) && isObject(snapshot)) {
                        applySnapshot(DataContext.models, merge(oldSnap, snapshot, {
                            arrayMerge: (a, b) => isObject(b[0]) ? b : union(a, b)
                        }))
                    }
                    return true
                }
                return false
            } catch (e) {
                console.error(e)
                isLogRocketEnable && LogRocket.error(e)
                toaster.error('Произошла ошибка при обработке полученных данных')
                return false
            }
        }

        DataContext.models.isCached = (requestName: string, reqParams: string | object = { "empty": true }): boolean => {
            try {
                return DataContext.models.table_state.get(md5(requestName + JSON.stringify(reqParams))).isNeedCaching()
            } catch (e) {
                isLogRocketEnable && LogRocket.error(e)
                return false
            }
        }

        DataContext.models.setMutex = (requestName: string, reqParams: string | object = { "empty": true }): boolean => {
            return DataContext.models.table_state.put({
                request_id: md5(requestName + JSON.stringify(reqParams)),
                cached: false,
                mutex: true
            })
        }

        DataContext.models.isBlocked = (requestName: string, reqParams: string | object = { "empty": true }) => {
            try {
                return DataContext.models.table_state.get(md5(requestName + JSON.stringify(reqParams))).isMutex()
            } catch (e) {
                isLogRocketEnable && LogRocket.error(e)
                return false
            }
        }

        DataContext.models.requestDone = (requestName: string, reqParams: string | object = { "empty": true }, cacheParams?: ICacheParams) => {
            try {
                const item = DataContext.models.table_state.get(md5(requestName + JSON.stringify(reqParams)))
                if (item) {
                    item.updateRequestCache(cacheParams)
                } else {
                    DataContext.models.table_state.put({
                        request_id: md5(requestName + JSON.stringify(reqParams)),
                        cached: true,
                        timeInvalidate: get(cacheParams, 'timeInvalidate') || get(cacheParams, 'noCaching') ? 0 : undefined
                    })
                }
            } catch (e) {
                isLogRocketEnable && LogRocket.error(e)
                console.error(e)
            }
        }

        DataContext.models.cacheFirst = async (request: (params: any) => any, requestParams: any, cacheParams?: ICacheParams) => {
            return request({ ...requestParams, isCacheable: !get(cacheParams, 'noCaching', false), cacheParams })
        }

        DataContext.models.invalidateCache = (request: any, requestParams?: any) => {
            DataContext.models.table_state.delete(md5(request + JSON.stringify(requestParams)))
        }

        DataContext.models.reset = () => {
            applySnapshot(DataContext.models, { ...DataContext.initialState, router: DataContext.models.router })
        }

        DataContext.models.mergeArray = (target: any, source: Array<any>, replace = false) => {
            const snapshot = getSnapshot(target)
            if (replace) {
                target.replace(source)
            } else if (isArray(snapshot)) {
                if (isEmpty(snapshot)) {
                    target.push(...source)
                } else {
                    target.replace(union(snapshot, source))
                }
            }
        }

        return DataContext.models
    }

    public applySnapshot(param: any, rewrite: boolean = false) {
        runInAction(() => {
            DataContext.models.applySnapshot(param, rewrite)
        })
    }

    public isCached(requestName: string, requestParams?: string | object): boolean {
        return DataContext.models.isCached(requestName, requestParams)
    }

    public isBlocked(requestName: string, requestParams?: string | object): boolean {
        return DataContext.models.isBlocked(requestName, requestParams)
    }

    public setMutex(requestName: string, requestParams?: string | object): boolean {
        return DataContext.models.setMutex(requestName, requestParams)
    }

    public requestDone(requestName: string, requestParams?: string | object, cacheParams?: ICacheParams) {
        return DataContext.models.requestDone(requestName, requestParams, cacheParams)
    }

    public cacheFirst(request: any, requestParams?: any, cacheParams?: ICacheParams) {
        return DataContext.models.cacheFirst(request, requestParams, cacheParams) as Response
    }

    public invalidateCache(request: any, requestParams?: any) {
        return DataContext.models.invalidateCache(request, requestParams)
    }

    public reset() {
        return DataContext.models.reset()
    }

    public mergeArray(target: any, source: Array<any>, replace: boolean = false) {
        return DataContext.models.mergeArray(target, source, replace)
    }
}

export interface DataContext extends Instance<typeof Store> {
}
