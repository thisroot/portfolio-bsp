import { inject } from "react-ioc"
import { ApiService } from "../ApiService"
import { DataContext } from "../DataContext"
import { action, observable, runInAction } from "mobx"
import { get } from 'lodash'
import { AnalyticsService } from "../AnalyticsService"
import { getSnapshot } from "mobx-state-tree"

class DictionaryService {
    @inject api: ApiService
    @inject dataContext: DataContext
    @inject analytics: AnalyticsService

    @observable isInitialized = false
    @observable pending = true

    @action
    public async init(force: boolean = false) {
        try {
            if (!this.isInitialized || force) {
                const dictionaries = [
                    this.api.reference.job_list,
                    this.api.reference.unit_list,
                    this.api.reference.element_list,
                    this.api.reference.region_list,
                    this.api.reference.object_type,
                    this.api.reference.project_type,
                    this.api.reference.fact_type,
                    this.api.reference.history_action,
                    this.api.reference.history_relation,
                    this.api.reference.extensions,
                ]

                const results = await Promise.all(dictionaries.map(api => this.dataContext.cacheFirst(
                  api,
                  {},
                  { timeInvalidate: 31 * 24 * 60 * 60 * 1000 })))
                const result = { reference: {} }
                results.forEach(res => {
                    if (get(res, 'body.reference')) {
                        Object.assign(result.reference, res.body.reference)
                    }
                })
                runInAction(() => {
                    this.dataContext.applySnapshot(result)
                })
            }

        } catch (error) {
            this.analytics.sendError({ error })
        } finally {
            this.pending = false
        }
    }

    @action get = (name: string): Array<string> => {
        return getSnapshot(this.dataContext.reference[name])
    }
}

export {
    DictionaryService
}
