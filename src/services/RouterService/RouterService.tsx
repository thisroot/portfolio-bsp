import { DataContext, history } from 'services/DataContext'
import { action, computed, observable, runInAction } from "mobx"
import { inject, useInstance } from "react-ioc"
import React, { Fragment } from "react"
import { observer } from 'mobx-react-lite'
import { AnalyticsService } from "services/AnalyticsService"
import cx from 'classnames'
import styles from './styles.scss'
import { formatPath, getBreadCrumbs, isExternal, parsePath } from "utils/helpers"
import { AuthService } from "../AuthService"
import { get, isNaN, isString, last } from 'lodash'
import { loc } from 'utils/i18n'
import { LocationDescriptorObject } from 'history'
import { LocalisationService } from "../LocalisationService"

export class RouterService {
    @inject private dataContext: DataContext
    @inject private analytics: AnalyticsService

    @observable
    public history = history

    @observable
    public location = this.history.location

    constructor() {
        history.listen((location) => {
            runInAction(() => {
                this.analytics.pageView(formatPath(location))
                this.location = location
            })
        })
    }

    @computed
    public get pathname() {
        return this.dataContext.router.location.pathname
    }

    @action
    public push = (path: string | LocationDescriptorObject, state?: any) => {
        this.dataContext.router.push(formatPath(path, this.location), state)
    }

    @action
    public replace = (path: string | LocationDescriptorObject, state?: any) => {
        this.dataContext.router.replace(formatPath(path, this.location), state)
    }

    @action
    public goBack = () => {
        this.dataContext.router.goBack()
    }

    @computed
    public get router() {
        return this.dataContext.router
    }

    @computed
    public get searchParams() {
        let result = {}
        for (let entry of new URLSearchParams(this.location.search).entries()) {
            const [ key, value ] = entry
            result[key] = isNaN(+value) ? value : +value
        }
        return result
    }
}

const Link = observer((props: { className?: string, to: string, children?: any, onClick?: any, wrapper?: string }): JSX.Element => {
    const router = useInstance(RouterService)
    const click = (e) => {
        e.preventDefault()
        e.stopPropagation()
        if (isExternal(props.to)) {
            window.open(props.to, '_blank')
        } else {
            router.push(props.to)
        }
        props.onClick && props.onClick()
    }
    return (<>
        { props.wrapper === 'div' ? <div className={ props.className } onClick={ click }>{ props.children }</div> :
          <a href={ props.to } className={ props.className } onClick={ click }>{ props.children }</a> }
    </>)
})

const BreadCrumbs = observer((props: { className?: string }) => {

    const router = useInstance(RouterService)
    const auth = useInstance(AuthService)
    const lang = useInstance(LocalisationService)
    const dataContext = useInstance(DataContext)

    const renderBreadcrumbs = (breadCrumbs) => {
        let renderer = []
        breadCrumbs.forEach((item, index) => {
            const hasRoute = !!get(item, 'route')
            let routeName = null
            if (!hasRoute) return
            if (hasRoute) {
                if (isString(get(item, 'route.NAME'))) {
                    routeName = loc._(get(item, 'route.NAME'))
                } else {
                    const entities = dataContext[get(item, 'route.NAME.key')]
                    if (entities.size > 0) {
                        const parsedPath = parsePath(item.path, get(item, 'route.ROUTE'))
                        const lastEntity = last(Object.values(get(parsedPath, 'params', [])))
                        const nameField = get(item, 'route.NAME.value')
                        routeName = get(entities.get(lastEntity), `${ nameField }_${ lang.lang }`, '---')
                    }
                }
            }

            const isAllowRoute = hasRoute ? auth.isAllowRoute(item.path) : false

            renderer.push(<Fragment key={ item.path }>
                <div title={ routeName } className={
                    cx(styles.breadCrumb,
                      { [styles.isCurrent]: router.pathname === item.path }) }>
                    { hasRoute && isAllowRoute ?
                      <Link className={ styles.link } to={ item.path }>{ routeName }</Link> :
                      <span>{ routeName }</span> }
                </div>
                { index < breadCrumbs.length - 1 && <div className={ styles.breadCrumb }>/</div> }
            </Fragment>)
        })
        return renderer
    }

    const breadCrumbs = getBreadCrumbs(router.pathname)
    return <div id={ 'breadcrumbs' }
                className={ cx('breadcrumbs', styles.breadCrumbs, { [props.className]: props.className }) }>
        {
            get(breadCrumbs, 'length') > 0 && renderBreadcrumbs(breadCrumbs)
        }
    </div>
})


export {
    Link,
    BreadCrumbs
}
