import { toaster } from "components/UI/Toast"
import { appConfig } from "constants/appConfig"
import { can, ROLE } from "constants/permissions"
import { get, isEmpty } from 'lodash'
import { action, computed, observable, runInAction } from "mobx"
import { User } from "models/mst"
import { inject } from "react-ioc"
import { AnalyticsService, searchAnalyticsEventAction } from "services/AnalyticsService"
import { ApiService, AuthBodyReq, AuthBodyResp, createUserReq, createUserResp, } from "services/ApiService"
import { RouterService } from "services/RouterService"
import { RequestService } from 'services/ApiService/RequestService'
import { DataContext } from "services/DataContext"
import { StorageService } from "services/StorageService"
import { isAllowRoute, ObservableTypes, observer } from 'utils'
import { GLOBAL_USER_PERMISSIONS } from "./constants"
import { LocalisationService } from "../LocalisationService"

interface userSession {
    user?: User,
    token?: string,
}

export class AuthService {
    @observable
    public wasInitialized = false
    @observable
    public pending = false

    @inject
    private storageService: StorageService

    @inject
    private dataContext: DataContext

    @inject
    public apiService: ApiService
    @inject
    public req: RequestService
    @inject
    public locService: LocalisationService

    @inject
    private analytics: AnalyticsService

    @inject
    private routerService: RouterService


    @computed
    public get currentUserSession() {
        return this.req.userSession
    }

    @computed
    public get userPermissions(): Set<string> {
        return this.req.currentPermissions
    }

    @action
    public processResponse = async (result: any) => {
        return !(!result || get(result, 'errorCode'));
    }

    @action
    public isAllow = (permission: string): boolean => {
        return this.req.isAllowRequest(permission)
    }

    @action
    public isAllowRoute = (path: string): boolean => {
        return isAllowRoute(path, this.userPermissions)
    }

    @action
    public init = async () => {
        this.pending = true
        await this.storageService.init()
        this.req.currentPermissions = new Set<string>([ can(ROLE.USER, 'auth'), ...GLOBAL_USER_PERMISSIONS ])
        try {
            observer.subscribe(ObservableTypes.logout, this.logout, this)
            const session: userSession = JSON.parse(await this.storageService.localDB.getItem(appConfig.LOCALSTORAGE_KEY_NAMES.userSession))
            if (!isEmpty(get(session, 'token', null)) && !isEmpty(get(session, 'user', null))) {
                runInAction(() => {
                    this.req.userSession = session
                    this.dataContext.user.set(session.user.user_id, session.user)
                })
                // const permission = await this.apiService.user.permission()
                // if (get(permission, 'body.permission')) {
                //     this.req.currentPermissions = new Set(permission.body.permission)
                // }
                const userInfo = await this.apiService.user.info()

                if (get(userInfo, 'body.permission.user[0].permission')) {
                    this.req.currentPermissions = new Set([
                        ...this.req.currentPermissions,
                        ...get(userInfo.body.permission, 'user[0].permission', [])
                    ])
                }

                this.analytics.identifyUser(this.req.userSession.user)
            }
        } catch (error) {
            toaster.error('Произошла ошибка авторизации')
            this.analytics.sendError({ error })
            this.storageService.localDB.removeItem(appConfig.LOCALSTORAGE_KEY_NAMES.userSession)
        } finally {
            runInAction(() => {
                this.wasInitialized = true
                this.pending = false
            })
        }
    }

    @action
    public login = async (body: AuthBodyReq): Promise<boolean> => {
        try {
            this.pending = true
            const result: AuthBodyResp = await this.apiService.jwt.auth(body)
            if (!this.processResponse(result)) {
                return false
            }
            runInAction(() => this.req.userSession = { token: result.body.token })

            await this.storageService.localDB.setItem(appConfig.LOCALSTORAGE_KEY_NAMES.userSession, JSON.stringify({ token: result.body.token }))
            const userInfo = await this.apiService.user.info()

            if (!this.processResponse(userInfo)) {
                return false
            }

            runInAction(() => {
                this.req.userSession = { user: userInfo.body.user, token: result.body.token }
                this.storageService.localDB.setItem(appConfig.LOCALSTORAGE_KEY_NAMES.userSession, JSON.stringify({
                    user: userInfo.body.user,
                    token: result.body.token
                }))
                this.dataContext.user.set(get(userInfo.body.user, 'user_id'), userInfo.body.user)
                this.req.currentPermissions = new Set([
                    can(ROLE.USER, 'auth'),
                    ...GLOBAL_USER_PERMISSIONS,
                    ...get(userInfo.body.permission, 'user[0].permission', [])
                ])
                if (this.routerService.history.location.pathname === '/' ||
                  !this.isAllowRoute(this.routerService.history.location.pathname)) {
                    //must be allow common route without special permissions
                    // this.routerService.replace(DEFAULT_ROUTE.PATH)
                }
            })
            get(this.currentUser, 'user_id') && this.analytics.sendEvent(searchAnalyticsEventAction.UserSignIn, { user: this.currentUser.user_id })
            this.analytics.identifyUser(this.req.userSession.user)
            return true
        } catch (error) {
            this.storageService.localDB.removeItem(appConfig.LOCALSTORAGE_KEY_NAMES.userSession)
            this.analytics.sendError({ error })
            return false
        } finally {
            this.pending = false
        }
    }

    @action
    public logout = async () => {
        try {
            this.pending = true
            this.analytics.sendEvent(searchAnalyticsEventAction.UserSignOut, { user: get(this.currentUser, 'user_id') })
            await this.apiService.jwt.logout()
        } finally {
            this.req.userSession = null
            this.storageService.localDB.removeItem(appConfig.LOCALSTORAGE_KEY_NAMES.userSession)
            this.dataContext.reset()
            // window.location.reload()
            this.pending = false
        }
    }

    @action
    public register = async (body: createUserReq): Promise<boolean> => {
        const result: createUserResp = await this.apiService.user.create(body)
        if (get(result, 'errorCode')) {
            this.req.userSession = null
            return false
        }
        return true
    }

    @action
    public updateUserSession = async (session: userSession) => {
        this.req.userSession = null
        return this.storageService.localDB.setItem(appConfig.LOCALSTORAGE_KEY_NAMES.userSession, JSON.stringify(session))
    }

    @computed
    public get isAuthorized() {
        //TODO: исправить механизм проверки
        return !!get(this.currentUserSession, 'token', false) && !!get(this.currentUserSession, 'user', false)
    }

    @computed
    public get currentUser(): User {
        return get(this.currentUserSession, 'user')
    }
}
