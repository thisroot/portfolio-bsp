import { action, computed, observable } from "mobx"
import { INavigationRoute } from "../../components/LeftMenu/Navigation"
import { NAVIGATION_ROUTES, ROUTES } from "../../constants"
import { inject } from "react-ioc"
import { get } from 'lodash'
import { AuthService } from "../AuthService"
import { ApiService } from "../ApiService"
import { AnalyticsService } from "../AnalyticsService"

export class SettingsService {
    @inject auth: AuthService
    @inject api: ApiService
    @inject analytics: AnalyticsService
    @observable pending = false

    @computed get allowRoutes(): Array<INavigationRoute> {
        return NAVIGATION_ROUTES.filter(route => this.auth.isAllowRoute(route.path) || route.type)
    }

    @observable userRoutes: Array<INavigationRoute>
    @observable mainPage
    @observable wasInitialized

    @action
    public init = async (force?: boolean) => {
        try {
            if (!this.wasInitialized || force) {
                console.log('init-service')


                this.pending = true
                const favorites = await this.api.user.getData({
                    path: [ `filters.interface_settings.filter` ]
                })

                if (favorites.status === 200) {
                    const fav = get(favorites, `body.data.filters.interface_settings.filter`)
                    const left_menu = get(fav, 'left_menu')
                    const main_page = get(fav, 'main_page')
                    this.setUserRoutes(left_menu)
                    this.mainPage = main_page ? get(this.userRoutes.filter(route => route.id === main_page), '[0].path') : get(this.userRoutes.filter(route => !route.children), '[0].path') || ROUTES.USER_PROFILE.PATH
                }
                this.wasInitialized = true
                this.pending = false
            }
        } catch (e) {
            this.analytics.sendError({ error: e })
        }
    }


    @action setMainPage = (mainPage: number) => {
        this.mainPage = get(this.userRoutes.filter(route => route.id === mainPage), '[0].path')
    }
    @action setUserRoutes = (left_menu: Array<number>) => {
        this.userRoutes = left_menu ? this.allowRoutes.filter(route => left_menu.includes(route.id) || route.children && route.children.some(item => left_menu.includes(item))) : this.allowRoutes
    }
}
