export interface IImageUploadReq {
    file: FormData;
}

export interface IImageUploadResp {
    image_id: number;
    image_relation: string;
    image_relation_id: number;
    mimetype: string;
    name: string;
    size: number;
    ext: string;
    digest: string;
    uploaded: boolean;
    converted: null;
}

export interface IImageConvertReq {
    image_is: number;
}

interface IConvertedImage {
    format: string;
    width: string;
    height: string;
    url: string;
}

export interface IImageConvertResp {
    image_id: number;
    image_relation: string;
    image_relation_id: number;
    mimetype: string;
    name: string;
    size: number;
    ext: string;
    digest: string;
    uploaded: boolean;
    converted: {
        big: IConvertedImage;
        small: IConvertedImage;
    }
}

export interface IImageListReq {
    image_relation_id: number;
    image_relation: string;
}

export interface IImageListResp {
    image_id: number;
    image_relation: string;
    image_relation_id: number;
    created_by: number;
    mimetype: string;
    name: string;
    size: number;
    ext: string;
    digest: string;
    uploaded: boolean;
    converted: {
        big: IConvertedImage;
        small: IConvertedImage;
    },
    main: boolean;
}
