import { Response } from './common'

export interface getObjectModelListReq {
    project_id?: string[],
    project_model_id?: string[],
    filter?: any
    sort?: any
}

interface ObjectResp {
    object_id: string,
    name: string,
    description: string,
    meta: {}
}

interface ObjectModelResp {
    project_id: string,
    project_model_id: string,
    date_start: string,
    object_id: number
    date_finish: string,
    date_interval_day: string,
    created_by: string,
    title: string,
    meta: {},
    volume_percent: string
}
export interface createObjectReq {
    project_id: number,
    name: string,
    object_type_id?: number,
    description?: string,
    passport_data_ru?: {},
}

interface ICreateObjectResp {
    object_id: number,
    project_id: number,
    name: string,
    object_type_id: number | null,
    description: string | null,
    passport_data_ru: {} | null,
}

export interface createObjectResp extends Response {
    body: ICreateObjectResp
}

export interface updateObjectReq {
    object_id: number
    name: string
    description: string
    passport_data_ru: object
}
export interface updateObjectResp extends Response {
    body: ObjectResp
}
export interface getObjectListResp extends Response {
    body: { object: Array<ObjectModelResp> }
}
