import { Method } from "axios"
import { ICacheParams } from "../../../models/mst/TableState"

export interface Response {
    status: 'ok' | 'error' | number | 'cached'
    errorCode?: number,
    message?: string,
    body?: any
    request?: { url: string, params: ReqParams }
    result?: any;
    headers?: any
}

export interface ReqParams {
    method?: Method
    /** url query params */
    params?: object
    /** request body params */
    body?: object
    headers?: any,
    compressed?: boolean
    errorMessage?: any,
    toasterDisable?: boolean
    permission?: string
    isCacheable?: boolean
    cacheParams?: ICacheParams,
    mapTableIdentities?: { [key: string]: string }
    responseType?: 'json' | 'text' | 'stream' | 'arraybuffer' | 'document' | 'blob'
    generateErrorMessage?: (error: any) => string
    cancellableFrom?: 'url' | 'body' | 'none'
}

export interface ApiResParams<T> {
    result: Array<T>
    next?: object
}

export interface onProgressEvent {
    loaded: number,
    total: number
}

export type onProgressRequest = (event: onProgressEvent) => any

export enum RequestSort {
    NO_SORT = null,
    ASC = 1,
    DESC = -1
}
