import { Response } from './common'

export interface createProjectReq {
    organization_id: number,
    user_id: number
    name: string
    description: string
    meta: object
}

interface ProjectResp {
    project_id: string,
    name: string,
    description: string,
    meta: {}
}

export interface createProjectResp extends Response {
    body: ProjectResp
}

export interface updateProjectReq {
    project_id: number
    name: string
    description: string
    meta: object
}

export interface updateProjectResp extends createProjectResp {
}

export interface deleteProjectReq {
    project_id: number[];
}

export interface deleteProjectResp extends Response {
    result: {
        project: Array<{ project_id: number; deleted: boolean }>;
    }
}

export interface getProjectsListReq {
    project_id?: string[],
    extended?: boolean,
    filter?: any,
    sort?: any
}

export interface getProjectsListResp extends Response {
    body: { project: Array<ProjectResp> }
}

export interface favoriteProjectDeleteReq {
    project_id: number[];
}
export interface favoriteProjectDeleteResp extends Response {
}

export interface favoriteProjectAddReq {
    project_id: number[];
}
export interface favoriteProjectAddResp extends Response {
}
