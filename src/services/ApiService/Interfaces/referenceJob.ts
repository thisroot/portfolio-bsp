import { Response } from './common'

interface ReferenceJob {
    key: string;
    code: string;
    name: string;
    description: string;
    job_element_group: number;
}

export interface listReferenceJob extends ReferenceJob {
    job_id: number;
}

export interface listReferenceJobReq {
    job_id: number[];
}

export interface listReferenceJobResp extends Response {
    body: { job: listReferenceJob[]; }
}

export interface createReferenceJob extends ReferenceJob {
}

export interface createReferenceJobReq extends createReferenceJob {
}

export interface createReferenceJobBody extends ReferenceJob {
    job_id: number;
}

export interface createReferenceJobResp extends Response {
    body: { job: createReferenceJobBody[]; }
}

export interface updateReferenceJob extends ReferenceJob {
    job_id: number;
}

export interface updateReferenceJobReq extends updateReferenceJob {
}

export interface updateReferenceJobResp extends Response {
    job: updateReferenceJob[];
}

export interface deleteReference {
    job_id: number;
}

export interface deleteReferenceReq extends deleteReference {
}

export interface deleteReferenceJobResp extends Response {
}
