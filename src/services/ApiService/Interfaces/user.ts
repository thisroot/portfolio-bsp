import { Response } from './common'

export interface createUserReq {
    organization_id: string,
    user_id: string
    firstname: string
    lastname: string
    email: string
    phone: string
    password: string
}

export interface getUserDataReq {
    path: Array<string>
}

export interface setUserDataReq {
    path: string,
    data: {}
}

export interface respBody extends createUserReq {
    organization_name: string
    created: string
    modified: string
}

export interface createUserResp extends Response {
    body: { [s: number]: respBody }
}

export interface updateUserReq {
    user_id: string
    firstname: string
    lastname: string
    email: string
    phone: string
}

export interface updateUserResp extends createUserResp {}
export interface sessionUserResp extends createUserResp {}


