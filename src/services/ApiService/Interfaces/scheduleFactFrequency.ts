import { Response } from './common'

export interface getScheduleFactFrequencyReq {
    object_id: number;
    date_from: string;
    date_to: string;
    fact_type: string;
    fact_source?: string
}

export interface getArrowReq {
    object_id: number
}

interface IFactCount {
    date: string;
    count: number;
    volume: number;
}

export interface ScheduleFactFrequencyResp {
    schedule_id: number;
    name: string;
    volume_planned: number;
    volume_fact: number;
    job_unit: string;
    fact_count: IFactCount[];
}

export interface getScheduleFactFrequencyResp extends Response {
    body: {
        result: { report_schedule_fact_frequency: ScheduleFactFrequencyResp[] }
    }
}
