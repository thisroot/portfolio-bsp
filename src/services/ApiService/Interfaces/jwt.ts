import { Response } from './common'

export interface AuthBodyReq {
    email: string,
    password: string
}

export interface AuthBodyResp extends Response {
        body: { token: string }
}


