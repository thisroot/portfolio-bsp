import { Response } from './common'

export interface getProjectModelListReq {
    project_id?: string[]
}

interface ProjectModelResp {
    project_model_id: string,
    parent_id?: string,
    model_element_id?: string
    key: string,
    code: string,
    title: string,
    quantity: number,
    is_object: boolean,
    meta: {},
    sort_order: number
}

export interface getProjectListResp extends Response{
    body: { project_model: Array<ProjectModelResp>}
}
