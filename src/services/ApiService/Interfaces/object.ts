import { Response } from './common'

export interface favoriteObjectDeleteReq {
    object_id: number[];
}
export interface favoriteObjectDeleteResp extends Response {
}

export interface favoriteObjectAddReq {
    object_id: number[];
}
export interface favoriteObjectAddResp extends Response {
}

export interface uploadModelReq {
    file: any;
    object_id: number;
    expenditure_multiplier: number;
}
export interface uploadModelResp extends Response {
    result?: {
        count: number;
    }
}
