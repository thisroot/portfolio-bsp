import { Response } from './common';
import { RequestSort } from './common'

interface Fact {
    schedule_id: number;
    volume: number;
    expenditure: number;
    comment: string;
}

export interface createFact extends Fact {}
export interface createFactReq extends createFact {}
export interface createFactBody extends createFact {
    model_job_id: number;
}
export interface createFactResp extends Response {
    body: any
}

export interface listFactReq {
    filter: {
        object_id: number;
        schedule_id: number;
        fact_source: string;
        fact_type: string;
        date_from: string;
        date_to: string;
    },
    extended: boolean;
}

export interface listFactResp extends Response {
    body: {
        schedule_fact_id: number;
        created: string;
        user_id: number;
        schedule_id: number;
        fact_type: string;
        fact_source: string;
        volume: number;
        expenditure: number;
        comment: string;
    }
}

interface FactCorrection {
    schedule_fact_correction_id: number;
    created: string;
    created_by: number;
    object_id: number;
    status: FactCorrectionStatus;
    date_from: string;
    date_to: string;
}

enum FactCorrectionStatus {
    approved = 'approved',
    archived = 'archived',
    draft = 'draft'
}

export interface correctionCreateReq {
    object_id: number;
    status: FactCorrectionStatus;
    date_from: string;
    date_to: string;
}

export interface correctionCreateResp extends Response {
    body: FactCorrection;
}

export interface correctionListReq {
    object_id: number;
    filter?: {
      status?: FactCorrectionStatus;
    };
    sort?: {
      fact_correction_id?: RequestSort;
      created?: RequestSort;
    };
}
export interface correctionListResp extends Response {
    body: FactCorrection[];
}

export interface correctionUpdateReq {
    schedule_fact_correction_id: number;
    status: FactCorrectionStatus;
    date_from: string;
    date_to: string;
}

export interface correctionUpdateResp extends Response {
    body: FactCorrection;
}

interface FactCorrectionData {
    schedule_fact_correction_data_id: number;
    schedule_fact_correction_id: number;
    schedule_id: number;
    name: string;
    job_unit: string;
    volume: number;
    expenditure: number;
}

export interface correctionDataGetReq {
    schedule_fact_correction_id: number;
}

export interface correctionDataGetResp extends Response {
    body: FactCorrectionData;
}

export interface correctionDataSetReq extends correctionDataGetReq {
    data: {
        schedule_id: number;
        volume: number;
        expenditure: number;
    }[];
}

export interface correctionDataSetResp extends Response {
    body: FactCorrectionData;
}

export interface correctionDataLoadReq {
    object_id: number;
    date_from: string;
    date_to: string;
}

export interface correctionDataLoadResp extends Response {
    body: {
        schedule_fact_correction_data: {
            schedule_id: number;
            name: string;
            job_unit: string;
            volume: number;
            expenditure: number;
        }
    }
}