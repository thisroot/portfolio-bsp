import { Response } from './common'

interface ReferenceJobUnit {
    [key: string]: string
}

export interface listReferenceJobUnit extends ReferenceJobUnit {
}

export interface listReferenceJobUnitReq {
    job_id: number[];
}

export interface listReferenceJobUnitResp extends Response {
    body: { job: listReferenceJobUnit[]; }
}
