import { Response } from './common'

export interface getPlanFactReportReq {
    object_id: number;
    extended: boolean;
}

export interface PlanFactReportResp {
    schedule_version_id: number;
    object_id: number;
    schedule_id: number;
    parent_id: number;
    parent_path: string;
    name: string;
    job: string;
    job_id: number;
    job_unit: string;
    date_start: string;
    date_finish: string;
    date_interval_day: number;
    volume_percent: number;
    volume_planned: number;
    volume_actual: number;
    volume_fact: number;
    expenditure_planned: number;
    expenditure_actual: number;
    expenditure_fact: number;
    expenditure_percent: number;
    deviation_day: number;
    status: number;
    sort_order: number;
    level: number;
}

export interface getPlanFactReportResp extends Response {
    body: { schedule: PlanFactReportResp[] }
}
