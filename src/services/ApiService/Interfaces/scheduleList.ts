import { Response } from './common'

interface ScheduleList {
    name: string;
    volume_actual: number;
    volume_planned: number;
    expenditure_actual: number;
    expenditure_planned: number;
    date_start: string;
    date_finish: string;
    job_id: number | null;
    job_unit: number | null;
    meta: {};
}

export interface ScheduleListResp extends ScheduleList {
    parent_id: number | null;
    schedule_id: number;
    object_id: number;
    sort_order: number;
    parent_path: string;
}

export interface ScheduleListRespMST extends ScheduleListResp {
    children: number[];
    expanded: boolean;
    isVirtual: boolean;
    invalidFields: {};
    weighting_factor?: number
    reset_weighting_factor?: boolean
    hash: string;
}

export interface getScheduleListReq {
    object_id: number;
    extended?: false;
}

export interface getScheduleVersionList {
    extended: boolean,
    schedule_version_id: [],
    object_id: []
}

export interface getScheduleListResp extends Response {
    body: { schedule: ScheduleListResp[] }
}

export interface createScheduleReq extends ScheduleList {
    parent_id: number | null;
    object_id: number;
    reset_weighting_factor: boolean
    weighting_factor?: number
}

export interface createScheduleResp extends Response {
    body: { schedule: number }
}

export interface updateScheduleReq extends ScheduleList {
    schedule_id: number;
    reset_weighting_factor?: boolean
    comment: string
}

export interface updateScheduleResp extends Response {
    body: any;
}

export interface deleteScheduleReq {
    schedule_id: number[];
}

export interface deleteScheduleResp extends Response {
    body: any;
}

export interface moveScheduleReq {
    schedule_id: number;
    new_parent_id: number;
    after_schedule_id: number;
}

export interface moveScheduleResp extends Response {}

export interface deleteScheduleVersionReq {
    schedule_version_id: number[];
}

export interface deleteScheduleVersionResp extends Response {
    result: {
        schedule_version: Array<{ schedule_version_id: number; deleted: boolean }>;
    }
}
