import { REST } from "../../ApiRoutes"
import {
    createFactReq, createFactResp, ReqParams, listFactReq, listFactResp,
    correctionListReq, correctionListResp, correctionUpdateReq, correctionUpdateResp,
    correctionCreateReq, correctionCreateResp, correctionDataGetReq, correctionDataGetResp,
    correctionDataSetReq, correctionDataSetResp, correctionDataLoadReq, correctionDataLoadResp, Response
} from 'services/ApiService/Interfaces'
import { canWrite, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'
import { CUSTOM_ERROR_MESSAGES } from 'constants/locale'
import { get } from 'lodash'
import { loc } from "utils/i18n"

export class ScheduleFact {
    @inject rest: Request

    public create = async (body: createFactReq): Promise<createFactResp> => {
        const reqOptions: ReqParams = { body, compressed: false, permission: canWrite(ROLE.FOREMAN, 'fact'), toasterDisable: true }
        return await this.rest.request(REST.SCHEDULE_FACT.CREATE, reqOptions) as createFactResp
    }
    public update = async (body: any): Promise<Response> => {
        const reqOptions: ReqParams = { body, compressed: false, permission: canWrite(ROLE.FOREMAN, 'fact'), toasterDisable: true }
        return await this.rest.request(REST.SCHEDULE_FACT.UPDATE, reqOptions) as Response
    }

    public list = async (body: listFactReq): Promise<listFactResp> => {
        const reqOptions: ReqParams = { body, compressed: true, permission: canWrite(ROLE.FOREMAN, 'fact'), toasterDisable: true }
        return await this.rest.request(REST.SCHEDULE_FACT.LIST, reqOptions) as createFactResp
    }

    public correctionList = async (body: correctionListReq): Promise<correctionListResp> => {
        const reqOptions: ReqParams = { body, compressed: true, toasterDisable: true }
        return await this.rest.request(REST.SCHEDULE_FACT.CORRECTION.LIST, reqOptions) as correctionListResp
    }

    public correctionCreate = async (body: correctionCreateReq): Promise<correctionCreateResp> => {
        const reqOptions: ReqParams = { body, compressed: false, toasterDisable: true }
        return await this.rest.request(REST.SCHEDULE_FACT.CORRECTION.CREATE, reqOptions) as correctionCreateResp
    }
    
    public correctionUpdate = async (body: correctionUpdateReq): Promise<correctionUpdateResp> => {
        const reqOptions: ReqParams = {
            body,
            compressed: false,
            toasterDisable: false,
            generateErrorMessage: (error) => {
                const allMessage = get(error, 'response.data.error.message', '')
                const messageArray = allMessage.split(`${CUSTOM_ERROR_MESSAGES.revisorCorrectionUpdateError.identifier}${CUSTOM_ERROR_MESSAGES.revisorCorrectionUpdateError.divider}`)
                return `${loc._(CUSTOM_ERROR_MESSAGES.revisorCorrectionUpdateError.message)} ${Array.isArray(messageArray) ? messageArray.pop() : ''}`
            }
        }
        return await this.rest.request(REST.SCHEDULE_FACT.CORRECTION.UPDATE, reqOptions) as correctionUpdateResp
    }

    public correctionDataSet = async (body: correctionDataSetReq): Promise<correctionDataSetResp> => {
        const reqOptions: ReqParams = { body, compressed: true, toasterDisable: true }
        return await this.rest.request(REST.SCHEDULE_FACT.CORRECTION.DATA_SET, reqOptions) as correctionDataSetResp
    }

    public correctionDataGet = async (body: correctionDataGetReq): Promise<correctionDataGetResp> => {
        const reqOptions: ReqParams = { body, compressed: true, toasterDisable: true }
        return await this.rest.request(REST.SCHEDULE_FACT.CORRECTION.DATA_GET, reqOptions) as correctionDataGetResp
    }

    public correctionDataLoad = async (body: correctionDataLoadReq): Promise<correctionDataLoadResp> => {
        const reqOptions: ReqParams = {
            body,
            compressed: true,
            toasterDisable: false,
            mapTableIdentities: { schedule_fact_correction_data: 'schedule_id' },
            generateErrorMessage: (error) => {
                const allMessage = get(error, 'response.data.error.message', '')
                return allMessage === CUSTOM_ERROR_MESSAGES.revisorCorrectionDataLoadError.identifier ? loc._(CUSTOM_ERROR_MESSAGES.revisorCorrectionDataLoadError.message) : ''
            }            
        }
        return await this.rest.request(REST.SCHEDULE_FACT.CORRECTION.DATA_LOAD, reqOptions) as correctionDataLoadResp
    }
}
