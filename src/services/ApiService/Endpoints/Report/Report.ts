import { REST } from "../../ApiRoutes"
import {
    getPlanFactReportReq,
    getPlanFactReportResp,
    getScheduleFactFrequencyReq,
    getScheduleFactFrequencyResp,
    Response,
    getArrowReq
} from 'services/ApiService/Interfaces'
import { can, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class Report {
    @inject rest: Request
    public planFactReport = async (body?: getPlanFactReportReq): Promise<getPlanFactReportResp> => {
        return await this.rest.request(REST.REPORT.OBJECT_GUNTT, {
            body,
            compressed: true,
            permission: can(ROLE.USER, 'auth'),
            mapTableIdentities: { schedule_report_guntt: 'schedule_id' }
        }) as getPlanFactReportResp
    }

    public scheduleFactFrequency = async (body?: getScheduleFactFrequencyReq): Promise<getScheduleFactFrequencyResp> => {
        return await this.rest.request(REST.REPORT.SCHEDULE_FACT_FREQUENCY, {
            body,
            compressed: true,
            permission: can(ROLE.USER, 'auth'),
            mapTableIdentities: { report_schedule_fact_frequency: 'schedule_id' }
        }) as getScheduleFactFrequencyResp
    }
    public arrow = async (body: getArrowReq): Promise<Response> => {
        return await this.rest.request(REST.REPORT.ARROW, {
            body,
            compressed: false,
            toasterDisable: true
        })
    }
}
