import { REST } from "../../ApiRoutes"
import {
    createUserReq,
    createUserResp,
    getUserDataReq,
    ReqParams, setUserDataReq,
    updateUserReq,
    updateUserResp,
    Response
} from "../../Interfaces"
import { can, canWrite, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class User {
    @inject rest: Request

    public create = async (body: createUserReq): Promise<createUserResp> => {
        const reqOptions: ReqParams = { method: 'POST', body, permission: canWrite(ROLE.MANAGER, 'user') }
        return await this.rest.request(REST.USER.CREATE, reqOptions) as createUserResp
    }
    public update = async (body: updateUserReq): Promise<updateUserResp> => {
        const reqOptions: ReqParams = { method: 'POST', body, permission: canWrite(ROLE.MANAGER, 'user') }
        return await this.rest.request(REST.USER.UPDATE, reqOptions) as createUserResp
    }
    public info = async (): Promise<any> => {
        return await this.rest.request(REST.USER.INFO, {
            permission: can(ROLE.USER, 'auth'),
            compressed: false
        }) as createUserResp
    }
    public permission = async (): Promise<any> => {
        return await this.rest.request(REST.USER.PERMISSION, {
            compressed: false,
            permission: can(ROLE.USER, 'auth'), toasterDisable: true
        }) as any
    }

    public getData = async (body: getUserDataReq): Promise<Response> => {
        return await this.rest.request(REST.USER.GET_DATA, {
            compressed: false,
            permission: can(ROLE.USER, 'auth'),
            body,
            cancellableFrom: 'body'
        })
    }

    public setData = async (body: setUserDataReq): Promise<Response> => {
        return await this.rest.request(REST.USER.SET_DATA, {
            compressed: false,
            permission: can(ROLE.USER, 'auth'),
            body
        }) as any
    }
}
