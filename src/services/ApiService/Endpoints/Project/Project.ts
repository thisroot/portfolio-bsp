import { REST } from "../../ApiRoutes"
import {
    createProjectReq,
    createProjectResp,
    deleteProjectReq,
    deleteProjectResp,
    getProjectsListReq,
    getProjectsListResp,
    updateProjectReq,
    updateProjectResp,
    favoriteProjectDeleteReq,
    favoriteProjectDeleteResp,
    favoriteProjectAddReq,
    favoriteProjectAddResp,
    Response
} from "../../Interfaces"
import { canRead, canWrite, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class Project {
    @inject rest: Request
    public create = async (body: createProjectReq): Promise<createProjectResp> => {
        return await this.rest.request(REST.PROJECT.CREATE, {
            body,
            permission: canWrite(ROLE.MANAGER, 'project')
        }) as createProjectResp
    }
    public update = async (body: updateProjectReq): Promise<updateProjectResp> => {
        return await this.rest.request(REST.PROJECT.UPDATE, {
            body,
            permission: canWrite(ROLE.MANAGER, 'project')
        }) as updateProjectResp
    }
    public delete = async (body: deleteProjectReq): Promise<deleteProjectResp> => {
        return await this.rest.request(REST.PROJECT.DELETE, {
            body,
            permission: canWrite(ROLE.MANAGER, 'project')
        }) as deleteProjectResp
    }
    public list = async (body?: getProjectsListReq): Promise<getProjectsListResp> => {
        return await this.rest.request(REST.PROJECT.LIST, {
            body: { ...body, extended: true },
            permission: canRead(ROLE.USER, 'project'),
            toasterDisable: false,
            mapTableIdentities: { project_schedule: 'project_id' }
        }) as getProjectsListResp
    }
    public favoriteDelete = async (body: favoriteProjectDeleteReq): Promise<favoriteProjectDeleteResp> => {
        return await this.rest.request(REST.PROJECT.FAVORITES.DELETE, {
            body,
            permission: canWrite(ROLE.MANAGER, 'project')
        }) as favoriteProjectDeleteResp
    }
    public favoriteAdd = async (body: favoriteProjectAddReq): Promise<favoriteProjectAddResp> => {
        return await this.rest.request(REST.PROJECT.FAVORITES.ADD, {
            body,
            permission: canWrite(ROLE.MANAGER, 'project')
        }) as favoriteProjectAddResp
    }
    public getFilter = async (): Promise<Response> => this.rest.request(REST.PROJECT.FILTER, { permission: canRead(ROLE.USER, 'project'), compressed: false })
}
