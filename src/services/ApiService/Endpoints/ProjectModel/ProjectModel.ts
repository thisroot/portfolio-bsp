import { REST } from "../../ApiRoutes"
import { getProjectListResp, getProjectModelListReq } from "../../Interfaces"
import { canRead, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class ProjectModel {

    @inject rest: Request
    public list = async (body?: getProjectModelListReq): Promise<getProjectListResp> => {
        return await this.rest.request(REST.PROJECT_MODEL.LIST, {
            body: {
                ...body,
                extended: true
            },
            permission: canRead(ROLE.USER, 'project_model')
        }) as getProjectListResp
    }
}
