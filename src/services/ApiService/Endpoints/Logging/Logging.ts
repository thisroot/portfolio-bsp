import { REST } from "../../ApiRoutes"
import { Response } from 'services/ApiService/Interfaces'
import { canRealRead, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class Logging {
    @inject rest: Request

    public historyList = async (body: {}): Promise<Response> => {
        return await this.rest.request(REST.HISTORY.LIST, {
            body,
            permission: canRealRead(ROLE.USER, 'history'),
        })
    }

    public historyFilterData = async (body: {}): Promise<Response> => {
        return await this.rest.request(REST.HISTORY.FILTER_DATA, {
            body,
            permission: canRealRead(ROLE.USER, 'history'),
        })
    }
}