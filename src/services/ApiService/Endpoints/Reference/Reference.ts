import { REST } from "../../ApiRoutes"
import {
    listReferenceJobReq,
    listReferenceJobResp,
    listReferenceJobUnitReq,
    listReferenceJobUnitResp,
    ReqParams,
    Response
} from 'services/ApiService/Interfaces'
import { can, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class Reference {
    @inject rest: Request

    public job_list = async (body?: listReferenceJobReq): Promise<listReferenceJobResp> => {
        const reqOptions: ReqParams = { body, compressed: true, permission: can(ROLE.USER, 'auth') }
        return await this.rest.request(REST.REFERENCE.JOB_LIST, reqOptions) as listReferenceJobResp
    }

    public unit_list = async (body?: listReferenceJobUnitReq): Promise<listReferenceJobUnitResp> => {
        const reqOptions: ReqParams = { body, compressed: false, permission: can(ROLE.USER, 'auth') }
        return await this.rest.request(REST.REFERENCE.JOB_UNIT_LIST, reqOptions) as listReferenceJobUnitResp
    }

    public element_list = async (body?: any): Promise<Response> => {
        const reqOptions: ReqParams = { body, compressed: true, permission: can(ROLE.USER, 'auth') }
        return await this.rest.request(REST.REFERENCE.ELEMENT_LIST, reqOptions)
    }

    public region_list = async (body?: any): Promise<Response> => {
        const reqOptions: ReqParams = { body, compressed: true, permission: can(ROLE.USER, 'auth') }
        return await this.rest.request(REST.REFERENCE.REGION_LIST, reqOptions)
    }

    public fact_type = async (body?: any): Promise<Response> => {
        return await this.rest.request(REST.REFERENCE.FACT_TYPE_LIST, {
            body,
            compressed: false,
            permission: can(ROLE.USER, 'auth')
        })
    }

    public project_type = async (body?:any): Promise<Response> => {
        return await this.rest.request(REST.REFERENCE.PROJECT_TYPE_LIST, {
            body,
            permission: can(ROLE.USER, 'auth')
        })
    }

    public object_type = async (body?: any): Promise<Response> => {
        return await this.rest.request(REST.REFERENCE.OBJECT_TYPE_LIST, {
            body,
            permission: can(ROLE.USER, 'auth')
        })
    }

    public history_action = async (body?: any): Promise<Response> => {
        return await this.rest.request(REST.REFERENCE.HISTORY_ACTION_LIST, {
            body,
            compressed: false,
            permission: can(ROLE.USER, 'auth')
        })
    }

    public history_relation = async (body?: any): Promise<Response> => {
        return await this.rest.request(REST.REFERENCE.HISTORY_RELATION_LIST, {
            body,
            compressed: false,
            permission: can(ROLE.USER, 'auth')
        })
    }

    public extensions = async (body?: any): Promise<Response> => {
        return await this.rest.request(REST.REFERENCE.EXTENSIONS, {
            body,
            compressed: false,
            permission: can(ROLE.USER, 'auth')
        })
    }
}
