import { REST } from "../../ApiRoutes"
import { onProgressRequest, Response } from 'services/ApiService/Interfaces'
import { can, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'
import { FILE_RELATION } from "constants/index"

export class Files {
    @inject rest: Request

    public uploadFile = async (file: File, type: FILE_RELATION, relation_id: number, parent_id: number, onProgress?: onProgressRequest): Promise<Response> => {
        const body = new FormData()
        body.append('file', file, file.name)
        let headers = {
            'Content-Type': 'multipart/form-data',
        }
        relation_id && Object.assign(headers, { 'x-file-relation-id': relation_id })
        type && Object.assign(headers, { 'x-file-relation': type })
        parent_id && Object.assign(headers, { 'x-parent-id': parent_id })
        return await this.rest.request(REST.FILE.FILE_UPLOAD, {
            body,
            compressed: false,
            toasterDisable: true,
            cancellableFrom: 'none',
            headers
        }, onProgress)
    }

    public fileList = async (body: { file_relation_id: Array<number>, file_relation: string, sort?: { created?: number } }): Promise<Response> => {
        return await this.rest.request(REST.FILE.FILE_LIST, {
            body, mapTableIdentities: { path: 'file_id' }
        })
    }

    public userList = async (body: {
        extended?: boolean,
        filter?: {
            date_from?: number,
            date_to?: number,
            search?: string,
            project_id?: Array<number>,
            object_id?: Array<number>,
            schedule_id?: Array<number>
            extension?: string
            file_relation?: string
        },
        sort?: {
            created?: number
            name?: string
            size?: number
        }
    }) => {
        return await this.rest.request(REST.FILE.FILE_USER_LIST, { body, mapTableIdentities: { path: "file_id" } })
    }

    public folderCreate = async (body: {
        file_relation_id: number,
        file_relation: string,
        parent_id?: number,
        name: string
    }): Promise<Response> => {
        return await this.rest.request(REST.FILE.FOLDER_CREATE, {
            body,
            compressed: false,
            toasterDisable: true
        })
    }

    public folderUpdate = async (body: { file_id: number, name: string }): Promise<Response> => {
        return await this.rest.request(REST.FILE.FOLDER_UPDATE, {
            body, toasterDisable: true
        })
    }

    public folderMove = async (body: { file_id: number, parent_id: number, name: string }): Promise<Response> => {
        return await this.rest.request(REST.FILE.FOLDER_MOVE, {
            body, toasterDisable: true
        })
    }

    public fileMove = async (body: { file_id: number, parent_id: number }): Promise<Response> => {
        return await this.rest.request(REST.FILE.FILE_MOVE, {
            body
        })
    }

    public folderDelete = async (file_id: Array<number>): Promise<Response> => {
        return await this.rest.request(REST.FILE.FOLDER_DELETE, { body: { file_id } })
    }

    public deleteFile = async (file_id: Array<number>): Promise<Response> => {
        return this.rest.request(REST.FILE.FILE_DELETE, { body: { file_id } })
    }

    public uploadImage = async (image: File, type: FILE_RELATION, relation_id: number, onProgress?: onProgressRequest): Promise<Response> => {
        const body = new FormData()
        body.append('file', image, image.name)
        return await this.rest.request(REST.FILE.IMAGE_UPLOAD, {
            body,
            permission: can(ROLE.USER, 'image.upload'),
            compressed: false,
            toasterDisable: true,
            cancellableFrom: 'none',
            headers: {
                'Content-Type': 'multipart/form-data',
                'x-image-relation-id': relation_id,
                'x-image-relation': type
            }
        }, onProgress)
    }

    public convertImage = async (image_id: number, onProgress?: onProgressRequest): Promise<Response> => {
        return this.rest.request(REST.FILE.IMAGE_CONVERT, {
            body: { image_id },
            permission: can(ROLE.USER, 'image.upload'),
            toasterDisable: true,
            cancellableFrom: 'none',
            compressed: false
        }, onProgress)
    }

    public imageList = async (body: { image_relation_id: Array<number>, image_relation: string }): Promise<Response> => {
        return await this.rest.request(REST.FILE.IMAGE_LIST, {
            body,
            permission: can(ROLE.USER, 'image.upload'),
        })
    }
    public setMainImage = async (image_id: string): Promise<Response> => {
        return this.rest.request(REST.FILE.IMAGE_SET_MAIN, { body: { image_id }, cancellableFrom: 'body' })
    }
    public deleteImage = async (image_id: Array<number>): Promise<Response> => {
        return this.rest.request(REST.FILE.IMAGE_DELETE, { body: { image_id }, cancellableFrom: 'body' })
    }

    public download = async (file_id: number): Promise<Response> => {
        return this.rest.request(REST.FILE.DOWNLOAD, {
            params: { file_id },
            method: 'GET',
            responseType: "blob",
            compressed: false,
            cancellableFrom: 'none'
        })
    }
    public downloadFromUrl = async (url: string) : Promise<any> => {
        return this.rest.request(url, {
            method: 'GET',
            responseType: "blob",
        })
    }
}
