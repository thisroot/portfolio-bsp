import { REST } from "../../ApiRoutes"
import { Response } from 'services/ApiService/Interfaces'
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class Analytics {
    @inject rest: Request

    public templateList = async (body: { filter?: { template_type?: string } }): Promise<Response> => {
        return await this.rest.request(REST.ANALYTICS.TEMPLATE_LIST, {
            body,
            mapTableIdentities: { analytics_template: "alias" }
        })
    }
}
