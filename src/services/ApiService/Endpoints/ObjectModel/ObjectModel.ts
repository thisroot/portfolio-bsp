import { REST } from "../../ApiRoutes"
import {
    createObjectReq,
    createObjectResp,
    favoriteObjectAddReq,
    favoriteObjectAddResp,
    favoriteObjectDeleteReq,
    favoriteObjectDeleteResp,
    getObjectListResp,
    getObjectModelListReq,
    Response,
    updateObjectReq,
    updateObjectResp,
    uploadModelResp,
} from "../../Interfaces"
import { canRead, canWrite, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class ObjectModel {

    @inject rest: Request
    public list = async (body?: getObjectModelListReq): Promise<getObjectListResp> => {
        return await this.rest.request(REST.OBJECT.LIST, {
            body: {
                ...body,
                extended: true,
            },
            permission: canRead(ROLE.USER, 'object'),
            mapTableIdentities: { schedule_plan: 'object_id', object_schedule: 'object_id' },
            toasterDisable: true,
            cancellableFrom: 'url'
        }) as getObjectListResp
    }
    public create = async (body: createObjectReq): Promise<createObjectResp> => {
        return await this.rest.request(REST.OBJECT.CREATE, {
            body,
            compressed: false,
            permission: canWrite(ROLE.MANAGER, 'object'),
        }) as createObjectResp
    }
    public update = async (body: updateObjectReq): Promise<updateObjectResp> => {
        return await this.rest.request(REST.OBJECT.UPDATE, {
            body,
            permission: canWrite(ROLE.MANAGER, 'object'),
        }) as updateObjectResp
    }
    public favoriteDelete = async (body: favoriteObjectDeleteReq): Promise<favoriteObjectDeleteResp> => {
        return await this.rest.request(REST.OBJECT.FAVORITES.DELETE, {
            body,
            permission: canWrite(ROLE.MANAGER, 'object')
        }) as favoriteObjectDeleteResp
    }
    public favoriteAdd = async (body: favoriteObjectAddReq): Promise<favoriteObjectAddResp> => {
        return await this.rest.request(REST.OBJECT.FAVORITES.ADD, {
            body,
            permission: canWrite(ROLE.MANAGER, 'object')
        }) as favoriteObjectAddResp
    }
    public uploadModel = async (file: any, object_id: number, expenditure_multiplier: string): Promise<uploadModelResp> => {
        const body = new FormData()
        body.append('file', file)
        body.append('object_id', String(object_id))
        body.append('expenditure_multiplier', expenditure_multiplier)
        return await this.rest.request(REST.OBJECT.UPLOAD_MODEL, { body, toasterDisable: true }) as uploadModelResp
    }
    public getFilter = async (): Promise<Response> => this.rest.request(REST.OBJECT.FILTER, {
        permission: canRead(ROLE.USER, 'object'),
        compressed: false
    })

    public resetWeightingFactor = async (body: any): Promise<Response> => this.rest.request(REST.OBJECT.RESET_WEIGHTING_FACTOR, {
        body,
        compressed: false
    })

    public budgetList = async (body: any): Promise<Response> => this.rest.request(REST.OBJECT.BUDGET_LIST, { body })
    public budgetCreate = async (body: any): Promise<Response> => this.rest.request(REST.OBJECT.BUDGET_CREATE, { body })
    public budgetUpdate = async (body: any): Promise<Response> => this.rest.request(REST.OBJECT.BUDGET_UPDATE, { body })
    public budgetDelete = async (body: any): Promise<Response> => this.rest.request(REST.OBJECT.BUDGET_DELETE, { body })
    public planReport = async (body: any): Promise<Response> => this.rest.request(REST.OBJECT.PLAN_REPORT, { body })
    public inputFactReport = async (body: any): Promise<Response> => this.rest.request(REST.OBJECT.FACT_REPORT, { body })
}
