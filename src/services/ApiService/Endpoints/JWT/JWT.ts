import { REST } from "../../ApiRoutes"
import { AuthBodyReq, AuthBodyResp, ReqParams } from "../../Interfaces"
import { can, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'
import { loc } from 'utils/i18n';
import { t } from '@lingui/macro';

export class JWT {
    @inject rest: Request
    public auth = async (body: AuthBodyReq, mockCb?: () => any): Promise<AuthBodyResp> => {
        const reqOptions: ReqParams = {
            method: 'POST',
            body,
            errorMessage: loc._(t`error.incorrect_email_or_password`),
            compressed: false,
            permission: can(ROLE.USER, 'auth')
        }
        return await this.rest.request(REST.JWT.AUTH, reqOptions, null, mockCb) as AuthBodyResp
    }
    public logout = async (): Promise<Response> => {
        return await this.rest.request(REST.JWT.LOGOUT) as Response
    }
}
