import { REST } from "../../ApiRoutes"
import {
    createScheduleReq,
    createScheduleResp,
    deleteScheduleReq,
    deleteScheduleResp,
    getScheduleListReq,
    getScheduleListResp,
    getScheduleVersionList,
    moveScheduleReq,
    moveScheduleResp,
    ReqParams,
    Response,
    updateScheduleReq,
    updateScheduleResp,
    deleteScheduleVersionReq,
    deleteScheduleVersionResp,
} from 'services/ApiService/Interfaces'
import { canRead, canWrite, ROLE } from "constants/permissions"
import { inject } from 'react-ioc'
import { RequestService as Request } from 'services/ApiService/RequestService'

export class Schedule {
    @inject rest: Request

    public getScheduleList = async (body?: getScheduleListReq): Promise<getScheduleListResp> => {
        return await this.rest.request(REST.SCHEDULE.GET_LIST, {
            body: { ...body, extended: true },
            compressed: true,
            permission: canRead(ROLE.USER, 'schedule')
        }) as getScheduleListResp
    }

    public create = async (body: createScheduleReq): Promise<createScheduleResp> => {
        const reqOptions: ReqParams = {
            body,
            compressed: false,
            permission: canWrite(ROLE.MANAGER, 'schedule'),
            toasterDisable: true
        }
        return await this.rest.request(REST.SCHEDULE.CREATE, reqOptions) as createScheduleResp
    }

    public update = async (body: updateScheduleReq): Promise<updateScheduleResp> => {
        const reqOptions: ReqParams = {
            body,
            compressed: false,
            permission: canWrite(ROLE.MANAGER, 'schedule'),
            toasterDisable: true
        }
        return await this.rest.request(REST.SCHEDULE.UPDATE, reqOptions) as updateScheduleResp
    }

    public delete = async (body: deleteScheduleReq): Promise<deleteScheduleResp> => {
        const reqOptions: ReqParams = { body, compressed: false, permission: canWrite(ROLE.MANAGER, 'schedule'), toasterDisable: true }
        return await this.rest.request(REST.SCHEDULE.DELETE, reqOptions) as deleteScheduleResp
    }

    public move = async (body: moveScheduleReq): Promise<moveScheduleResp> => {
        const reqOptions: ReqParams = { body, compressed: false, permission: canWrite(ROLE.MANAGER, 'schedule') }
        return await this.rest.request(REST.SCHEDULE.MOVE, reqOptions) as moveScheduleResp
    }

    public versionList = async (body: getScheduleVersionList): Promise<Response> => {
        return await this.rest.request(REST.SCHEDULE_VERSION.LIST, {
            body: { schedule_version_id: [], object_id: [], extended: true, ...body },
            permission: canRead(ROLE.USER, 'schedule.version'),
        })
    }

    public setCurrentVersion = async (body: any): Promise<Response> => {
        return await this.rest.request(REST.SCHEDULE_VERSION.SET_CURRENT, {
            compressed: false,
            body,
            permission: canWrite(ROLE.MANAGER, 'schedule.version')
        })
    }

    public versionCreate = async (body: any): Promise<Response> => {
        return await this.rest.request(REST.SCHEDULE_VERSION.CREATE, {
            compressed: false,
            body,
            permission: canWrite(ROLE.MANAGER, 'schedule.version')
        })
    }

    public versionUpdate = async (body: any): Promise<Response> => {
        return await this.rest.request(REST.SCHEDULE_VERSION.UPDATE, {
            compressed: false,
            body,
            permission: canWrite(ROLE.MANAGER, 'schedule.version')
        })
    }

    public versionDelete = async (body: deleteScheduleVersionReq): Promise<deleteScheduleVersionResp> => {
        return await this.rest.request(REST.SCHEDULE_VERSION.DELETE, {
            compressed: false,
            body,
            permission: canWrite(ROLE.MANAGER, 'schedule.version')
        }) as deleteScheduleVersionResp
    }
}
