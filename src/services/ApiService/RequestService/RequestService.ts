import axios, { AxiosInstance, CancelTokenSource } from "axios"
import { action, computed, observable } from "mobx"
import { User } from "models/mst"
import { RequestError } from "./RequestError"
import { clone, get, isArray, isEmpty, isObject } from 'lodash'
import { onProgressRequest, ReqParams, Response } from "services/ApiService/Interfaces"
import { isDevelopment, ObservableTypes, observer } from "utils/helpers"
import { toaster } from 'components/UI/Toast'
import { loc } from "../../../utils/i18n"
import { t } from "@lingui/macro"
import { inject } from "react-ioc"
import { DataContext } from "../../DataContext"
import del from 'del-object-path'
import { AnalyticsService } from "../../AnalyticsService"
import { FILE_RELATION } from "constants/index"
import { REST } from "../ApiRoutes"

const base_URL = process.env.APP_API_URL

const PARENT_FILED_NAME = 'parent_id'
const CHILDREN_ARRAY_FIELD_NAME = 'children'
const TABLES_LISTS = [
    'project', 'object', 'schedule', 'history',
    'file', 'schedule_fact_correction', 'schedule_fact',
    'path', 'image', 'budget_plan', 'analytics_template'
]

interface userSession {
    user?: User,
    token?: string,
}

class RequestService {
    @inject dataContext: DataContext
    @inject analytics: AnalyticsService

    @observable cancellations = new Map<string, CancelTokenSource>()

    @computed get bearer() {
        return `Bearer ${ get(this.userSession, 'token') }`
    }

    @observable
    public currentPermissions: Set<string>

    @action isAllowRequest(permission: string): boolean {
        return !permission || this.currentPermissions.has(permission)
    }

    protected instance: AxiosInstance = axios.create({
        baseURL: base_URL
    })

    @observable
    public userSession: userSession

    /**
     * @helper
     * Validate response code
     * @param response
     * @param url
     * @param params
     */
    public resolveResponse(response, url: string, params: ReqParams) {
        if (response.status >= 400) {
            const err = { code: response.status, message: response.error || response.statusText }
            this.analytics.sendError({ error: err })
            throw new RequestError(err)
        } else {
            const respBody = get(params, 'compressed', true) ? this.parseResponse(response.data, params.mapTableIdentities) : response.data
            params.isCacheable && this.dataContext.requestDone(url, params.body, params.cacheParams)
            const headers = get(response, 'headers')
            if (get(respBody, 'result')) {
                const { result, ...meta } = respBody
                return { body: result, status: response.status, request: { url, params }, meta, headers } as Response
            } else {
                return { body: respBody, status: response.status, request: { url, params }, headers } as Response
            }
        }
    }

    /**
     * API Request function
     *
     * @param url {String}
     * @param cancellableFrom
     * @param {ReqParams} params
     * @param onProgress
     * @param mockResponse
     * @returns {Promise<any>}
     */
    public async request(url, { cancellableFrom = 'url', ...params }: ReqParams = {
        method: "POST",
        compressed: true,
        toasterDisable: false,
    }, onProgress?: onProgressRequest, mockResponse?: () => any): Promise<Response> {
        if (!this.isAllowRequest(params.permission)) return { status: 'error', errorCode: 403 }

        params.isCacheable = clone(get(params, 'body.isCacheable', false))
        params.cacheParams = clone(get(params, 'body.cacheParams', false))
        del(params, 'body.isCacheable')
        del(params, 'body.cacheParams')

        if (params.isCacheable && (this.dataContext.isBlocked(url, params.body) || this.dataContext.isCached(url, params.body))) {
            return { status: "cached" }
        }

        this.dataContext.setMutex(url, params.body)

        const hash = cancellableFrom === 'url' ? url : url + JSON.stringify(params.body) + JSON.stringify(params.params)
        if (cancellableFrom !== 'none') {

            if (this.cancellations.get(hash)) {
                const source = this.cancellations.get(hash)
                this.cancellations.delete(hash)
                source.cancel()
            }
        }
        const cancelToken = axios.CancelToken.source()
        this.cancellations.set(hash, cancelToken)

        // use mock responses on demand
        if (mockResponse && isDevelopment) {
            const { default: MockAdapter }: any = await import('axios-mock-adapter')
            const mock = new MockAdapter(axios)
            const method = (params.method || 'POST') === 'GET' ? 'onGet' : 'onPost'
            mock[method](base_URL + url, method === 'onGet' ? params.params : params.body).reply(200, mockResponse())
        }

        const headers = params.headers || {}
        if (this.bearer) headers["Authorization"] = this.bearer
        const responseType = params.responseType && params.responseType || 'json'
        if (responseType === 'json' && !headers['Content-Type']) {
            headers['Content-Type'] = 'application/json'
        }

        return this.instance.request({
            url: url, //clearUrl(base_URL + url),
            method: params.method || 'POST',
            params: { ...params.params },
            data: params.body,
            responseType,
            headers: {
                ...headers
            },
            cancelToken: cancelToken.token,
            onUploadProgress: onProgress,
            onDownloadProgress: onProgress
        }).then((resp) => {
            this.cancellations.delete(hash)
            return this.resolveResponse(resp, url, params)
        })
          .catch((error) => {
              if (axios.isCancel(error)) return { status: 'ok' }
              const message = get(error, 'response.data.error.message', error.message)
              const split = message.split(' ')
              const err = Number.isInteger(+split[split.length - 1]) ? +split[split.length - 1] : 500
              if (err) {
                  if (err === 401 && !url.match(REST.JWT.LOGOUT)) {
                      observer.fire(ObservableTypes.logout, {})
                      return { status: 'ok' }
                  }
                  const generatedErrorMessage = params.generateErrorMessage ? params.generateErrorMessage(error) : ''
                  !params.toasterDisable && toaster.error(generatedErrorMessage || loc._(params.errorMessage || t`error.requestError`))
              }
              this.analytics.sendError({ code: err, message })
              return { status: 'error', errorCode: err, message }
          })
    }

    public parseResponse = (src: any, mapTableIdentities?: { [key: string]: string }) => {
        const result = get(src, 'result')
        if (isArray(result)) {
            const { map = {} } = this.parseArray(result)
            return { ...src, result: map }
        } else if (isObject(result)) {
            const parsed = {}
            for (let key in result) {
                if (result.hasOwnProperty(key) && ![ 'permission', 'main_image' ].includes(key)) {
                    if (key === 'reference') {
                        const parsedArr = this.parseResponse({ result: result[key] })
                        Object.assign(parsed, { [key]: parsedArr.result })
                    } else {
                        const identityFieldName = get(mapTableIdentities, key, `${ key }_id`)
                        const { map: parsedMap = {}, list = [] } = this.parseArray(result[key], identityFieldName)

                        //TODO: требует рефакторинга (надо объединить в одном цикле)
                        this.mapPermission(result, parsedMap, key)
                        this.mapMainImage(result, parsedMap, key)
                        this.mapImages(result, parsedMap, key)
                        this.mapFiles(result, parsedMap, key)

                        Object.assign(parsed, { [key]: parsedMap })
                        if (TABLES_LISTS.includes(key)) {
                            Object.assign(parsed, { [`${ key }_list`]: list })
                        }
                    }
                }
            }
            return { ...src, result: parsed }
        }
    }

    public mapImages = (result, parsedMap, key) => {
        if (key === FILE_RELATION.OBJECT_FACT || key === FILE_RELATION.SCHEDULE_FACT) {
            const { map: parsedArr = {} } = this.parseArray(get(result, 'image', []))
            Object.values(parsedMap).forEach((fact: any) => {
                Object.assign(fact, { images: [] })
                Object.values(parsedArr).forEach(image => {
                    if (get(image, 'image_relation') === key && get(image, 'image_relation_id') === get(fact, `${ key }_id`)) {
                        fact.images.push(get(image, 'image_id'))
                    }
                })
            })
        }
    }

    public mapFiles = (result, parsedMap, key) => {
        if (key === FILE_RELATION.OBJECT_FACT || key === FILE_RELATION.SCHEDULE_FACT) {
            const { map: parsedArr = {} } = this.parseArray(get(result, 'file', []))
            Object.values(parsedMap).forEach((fact: any) => {
                Object.assign(fact, { files: [] })
                Object.values(parsedArr).forEach(file => {
                    if (get(file, 'file_relation') === key && get(file, 'file_relation_id') === get(fact, `${ key }_id`)) {
                        fact.files.push(get(file, 'file_id'))
                    }
                })
            })
        }
    }

    public mapPermission = (result, parsedMap, key) => {
        if (!isEmpty(get(result, `permission.${ key }[1]`))) {
            get(result, `permission.${ key }[1]`).forEach(item => {
                if (get(parsedMap, item[0])) {
                    Object.assign(get(parsedMap, item[0]), { permission: item[1] })
                }
            })
        }
    }

    public mapMainImage = (result, parsedMap, key) => {
        if (!isEmpty(get(result, `main_image.${ key }[1]`))) {
            const { map: parsedArr = {} } = this.parseArray(get(result, `main_image.${ key }`), 'image_relation_id')
            Object.entries(parsedArr).forEach(item => {
                Object.assign(get(parsedMap, item[0]), { main_image: parsedArr[item[0]] })
            })
        }
    }

    /**
     * Parse compressed responses from server
     * @param src
     * @param fieldId
     */
    public parseArray = (src: any[], fieldId?: string) => {

        if (src.length === 0) return {}

        if (src.length === 2 && isArray(src[0]) && isArray(src[1])) {
            let fieldNames = src[0]
            const isTree = fieldNames.includes(PARENT_FILED_NAME)
            const idField = fieldId || fieldNames[0]


            const result = src[1].reduce((srcResult, srcValue) => {

                let item = srcValue.reduce((itemResult, itemValue, itemIndex) => {
                    itemResult[fieldNames[itemIndex]] = itemValue
                    return itemResult
                }, {})


                if (isTree && item[PARENT_FILED_NAME] !== null) {
                    if (!srcResult.t[item[PARENT_FILED_NAME]]) {
                        srcResult.t[item[PARENT_FILED_NAME]] = [ item[idField] ]
                    } else {
                        srcResult.t[item[PARENT_FILED_NAME]].push(item[idField])
                    }
                }

                Object.assign(srcResult.map, { [item[idField]]: item })

                //TODO: требует уточнения и рефакторинга, может зааффектить модель объекта, ганта
                // https://glab.lad24.ru/esp/bsp-frontend/-/commit/0be3e87262a33b62e5a202a36679795a4c5c013d
                // формирует лист нод модели объекта из всех элементов списка, а не только верхнего уровня
                // if ((item.hasOwnProperty('parent_id') && !item['parent_id']) || !item.hasOwnProperty('parent_id')) {
                srcResult.list.push(item[idField])
                // }


                return srcResult
            }, { map: {}, t: {}, list: [] })


            if (isTree) {
                for (let key in result.t) {
                    if (result.map[key]) {
                        result.map[key][CHILDREN_ARRAY_FIELD_NAME] = result.t[key]
                    }
                }
            }

            return result
        }
        return { map: src }
    }
}

export {
    RequestService
}
