import { JWT } from 'services/ApiService/Endpoints/JWT'
import { Project } from 'services/ApiService/Endpoints/Project'
import { User } from 'services/ApiService/Endpoints/User'
import { ObjectModel } from 'services/ApiService/Endpoints/ObjectModel'
import { Report } from 'services/ApiService/Endpoints/Report'
import { Schedule } from 'services/ApiService/Endpoints/Schedule'
import { Reference } from 'services/ApiService/Endpoints/Reference'
import { ScheduleFact } from 'services/ApiService/Endpoints/ScheduleFact'
import { Files } from 'services/ApiService/Endpoints/Files'
import { Logging } from 'services/ApiService/Endpoints/Logging'
import { inject } from "react-ioc"
import { Analytics } from "services/ApiService/Endpoints/Analytics"

class ApiService {
    @inject jwt: JWT
    @inject user: User
    @inject project: Project
    @inject object: ObjectModel
    @inject report: Report
    @inject schedule: Schedule
    @inject reference: Reference
    @inject scheduleFact: ScheduleFact
    @inject files: Files
    @inject logging: Logging
    @inject analytics: Analytics
}

export {
    ApiService
}
