export enum searchAnalyticsAction {
    Initialize = 'initialize',
    PageView = 'pageview',
    Event = 'event'
}

export interface ISearchAnalyticsInitializeArgument {
    GOOGLE: string,
    YANDEX: string
}

export interface ISearchAnalyticsEventArgument {
    action: string,
    label: string,
    category: string,
    value?: number,
    data?: any
}

export type ISearchAnalyticsArgument = string | ISearchAnalyticsEventArgument | ISearchAnalyticsInitializeArgument

export enum searchAnalyticsEventAction {
    // AUTHORIZATION
    UserSignUp = 'user_signup', // + social_index:number social_name:string
    UserSignIn = 'user_login', // + common login action
    UserSignOut = 'user_signout', // +
    // USER ACTIONS
    UserOpenPage = 'user_opened_page', // we have this info into user routes events
    UserOpenSelfProfile = 'user_opened_self_profile',
    UserCloseProfile = 'user_close_profile', // ++
    UserSentMessage = 'user_sent_message', // +
    UserAddedStatus = 'user_added_status', // +
    UserLikedPost = 'user_liked_post', // +  (post_id)
    // CHAT
    GroupChatCreated = 'group_chat_created', // not used (chat_id)
    // OTHER
    UserVisibleError = 'user_visible_error', // +
    ServerHttpError = 'server_http_error',
    // NOTIFICATIONS
    NotificationDismiss = 'notification_dismiss', // not used
    NotificationOpen = 'notification_open', // not used
     //FILES
    LoadFile = 'load_file',
    UploadFile = 'upload_file',
    ConvertFile = 'convert_file',

    //SERVICES
    ServiceInitialization = 'service_initialization',
    Localization = 'localization',
    Request = 'request',
}


export enum searchAnalyticsEventCategory {
    Main = 'main',
    Error= 'error'
}

export enum searchAnalyticsYandexEvent {
    Hit = 'hit',
    ReachGoal = 'reachGoal'
}
