import FireBase from 'firebase/app'
import 'firebase/analytics'
import { searchAnalyticsEventAction } from "services/AnalyticsService/constants"
import { isLogRocketEnable, isProduction } from "utils/helpers"
import { action } from "mobx"
import { appConfig } from "../../constants"
import LogRocket from "logrocket"
import { User } from "../../models"
import setupLogRocketReact from 'logrocket-react';

export class AnalyticsService {

    @action
    public init = async () => {
        if (isProduction) {
            const fb = FireBase.initializeApp({
                apiKey: appConfig.FIREBASE.API_KEY,
                // authDomain: "building-service-platform.firebaseapp.com",
                // databaseURL: "https://building-service-platform.firebaseio.com",
                projectId: appConfig.FIREBASE.PROJECT_ID,
                // storageBucket: "building-service-platform.appspot.com",
                // messagingSenderId: "348721117327",
                appId: appConfig.FIREBASE.APP_ID,
                measurementId: appConfig.FIREBASE.MEASUREMENT_ID
            })
            fb.analytics().setAnalyticsCollectionEnabled(true)
        }
        if (isLogRocketEnable) {
            LogRocket.init(appConfig.ANALYTICS.LOG_ROCKET)
            setupLogRocketReact(LogRocket);
        }
    }

    @action
    public identifyUser = (user: User) => {
        if (isProduction) {
            isLogRocketEnable && LogRocket.identify(user.email, user)
            FireBase.analytics().setUserId(user.email)
            FireBase.analytics().setUserProperties(user)
        }
    }

    @action
    public pageView = (location: string) => {
        if (isProduction) {
            isLogRocketEnable && LogRocket.info('change-location', location)
            FireBase.analytics().setCurrentScreen(location)
        }
    }

    @action
    public sendEvent = (action: searchAnalyticsEventAction, data?: any) => {
        if (isProduction) {
            isLogRocketEnable && LogRocket.info(action, data)
            FireBase.analytics().logEvent(action, data)
        }
    }
    @action
    public sendError = (data?: any) => {
        console.error(data)
        if (isProduction) {
            isLogRocketEnable && LogRocket.error(data)
            FireBase.analytics().logEvent('error', data)
        }
    }
}
