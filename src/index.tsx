import React, { lazy, Suspense } from 'react'
import ReactDOM from 'react-dom';
import { Preloader } from "components/UI/Preloader"


const LazyRouter = lazy(() =>
    import('./containers/Router')
        .then(({ RouterContainer }) => ({ default: RouterContainer })),
);

const container = document.getElementById("root")
ReactDOM.render(<Suspense fallback={ <Preloader /> } ><LazyRouter /></Suspense>, container)

// позволяет выполнить функцию жизненного цикла componentWillUnmount при закрытии приложения.
// в нашем случае она скидывает дамп редак стора в локальное хранилище
window.addEventListener("beforeunload", () => {
    ReactDOM.unmountComponentAtNode(container)
})
