import React from "react"
import { observer } from "mobx-react-lite"
import { useInstance } from "react-ioc"
import { Spinner } from 'components/UI'
import styles from './styles.scss'
import { AppService } from "services/AppService"
import { get } from 'lodash'
import cx from 'classnames'


export interface PendingPaths {
    all?: boolean,
    top?: boolean,
    middleLeft?: boolean,
    middleRight?: boolean,
    controls?: boolean,
    content?: boolean
}

export interface BaseListComponentProps {
    topHeight?: string
    topComponent?: React.ReactNode,
    middleLeftComponent?: React.ReactNode,
    middleRightComponent?: React.ReactNode,
    controlsContainer?: React.ReactNode,
    contentComponent: React.ReactNode,
    theme?: string,
    subClass?: string,
    pending?: PendingPaths
    withTopSearch?: boolean
}

const BaseListComponent: React.FC<BaseListComponentProps> = observer((props) => {

    const shadowApp = useInstance(AppService) // that not override wrapper app inject

    const renderSpinner = () => <div className={ styles.spinnerWrapper }><Spinner className={ styles.spinner }/></div>
    return (

          <div className={ cx(styles.container, { [styles[props.theme]]: props.theme }, { [styles[props.subClass]]: props.subClass }) }>
              <div className={ styles.topWrapper }>
                  <div style={ props.topHeight && { height: props.topHeight } } className={ styles.top }>
                      { get(props, 'pending.top') ? renderSpinner() : props.topComponent }
                  </div>
              </div>
              <div className={ styles.middleWrapper }>
                  <div className={ cx(styles.middle, { [styles.isFilled]: shadowApp.scrollOnTop > 0 }) }>
                      <div className={ styles.left }>
                          <div onClick={ () => shadowApp.scrollTo() } className={ styles.iconContainer }/>
                          { get(props, 'pending.middleLeft') ? renderSpinner() : props.middleLeftComponent }
                      </div>
                      <div className={ styles.right }>
                          { get(props, 'pending.middleRight') ? renderSpinner() : props.middleRightComponent }
                      </div>
                  </div>
                  <div className={ cx(styles.controls, { [styles.isFilled]: shadowApp.scrollOnTop > 0 }) }>

                  </div>
              </div>
              <div className={ styles.contentWrapper }>
                  <div className={ styles.contentContainer }>
                      { get(props, 'pending.content') ? renderSpinner() : props.contentComponent }
                  </div>
              </div>
          </div>)
})

export { BaseListComponent }
