import React from "react"
import styles from "components/BaseListComponent/SearchTop/styles.scss"

export const TopSearch = ({ containerWidth }) => <div className={ styles.topContainer }>
    <div style={{ width: containerWidth }} className={ styles.innerContainer }>
        <div className={ styles.searchContainerTop }>
        </div>
    </div>
</div>
