import React, { FC, useRef, useState } from 'react'
import { get } from 'lodash'
import { ENTITY_FIELDS, getMetaInfoFields, isMarkdown } from "utils/helpers"
import { renderMarkdown } from "utils/render"
import { AbbreviateField } from "components/Widgets/AbbreviateField"
import { Link } from 'services/RouterService'
import { Icon } from "components/UI/Icon"
import cx from 'classnames'
import ProgressBar from 'components/UI/ProgressBar'
import { FormatDateToQuarter } from "components/Widgets/FormatDateToQuarter/FormatDateToQuarter"
import styles from './styles.scss'
import { Trans } from "@lingui/macro"
import { observer } from "mobx-react-lite"
import { useClickAway } from 'react-use'
import { FAVORITES_TYPES } from 'constants/common'
import { StatusDataLayers } from "../../models/mst/StatusDataLayers"
import { loc } from "../../utils/i18n"


interface IFavoritesListProps {
    favorites: any[];
    removeFavorite: (favoriteType: string, favoriteId: number) => any;
    canWrite: (favoriteType: string, favoriteId: number) => any;
    lang: string
}

interface IOpenType {
    openType?: string;
    openId?: number;
}

export const FavoritesList: FC<IFavoritesListProps> = observer(({
                                                                    favorites,
                                                                    removeFavorite,
                                                                    canWrite,
                                                                    lang
                                                                }) => {
    const [ { openType, openId }, setOpen ] = useState<IOpenType>({})
    const wrapper: React.MutableRefObject<HTMLDivElement> = useRef()
    useClickAway(wrapper, () => {
        setOpen({})
    })

    return (
      <>
          { favorites.map((favorite) => {
              const { favoriteType } = favorite
              const favoriteId = favorite[`${ favoriteType }_id`]

              const statusData: StatusDataLayers = get(favorite, 'status_data')
              const finish = get(favorite, 'date_finish')
              const status = get(favorite, 'status')
              const favoriteImg = get(favorite, 'main_image.converted.small.url')
              const { slots } = getMetaInfoFields(favorite, favoriteType, lang, ENTITY_FIELDS.LIST_WIDGET)

              const renderField = (slot) => {
                  switch (true) {
                      case isMarkdown(get(slot, 'fieldType')):
                          return <div className={ styles.mdContainer }
                                      dangerouslySetInnerHTML={ renderMarkdown(get(slot, 'fieldValue')) }/>
                      default:
                          return <AbbreviateField type={ get(slot, 'fieldType') } swap
                                                  value={ get(slot, 'fieldValue', '') }
                                                  label={ get(slot, 'fieldLabel', '') }/>
                  }
              }

              let targetLink = '/'
              if (favoriteType === FAVORITES_TYPES.PROJECT) {
                  targetLink = `/projects/${ favoriteId }`
              } else if (favoriteType === FAVORITES_TYPES.OBJECT) {
                  targetLink = `/projects/${ get(favorite, 'project_id.project_id') }/objects/${ favoriteId }`
              }

              const kebabHandler = (e) => {
                  e.preventDefault()
                  e.stopPropagation()
                  setOpen({ openType: favoriteType, openId: favoriteId })
              }

              const removeFavoriteHandler = async (e, favoriteType: string, favoriteId: number) => {
                  e.preventDefault()
                  e.stopPropagation()
                  await removeFavorite(favoriteType, favoriteId)
                  setOpen({})
              }

              const image = 'account-home-door'
              return (
                <div
                  className={ styles.favoritesListItemContainer }
                  key={ `${ favoriteType }-${ favoriteId }` }
                >
                    <Link wrapper={'div'} to={ targetLink } className={ styles.row }>
                        <div className={ styles.main }>
                            <div className={ styles.left }>
                                <div className={ styles.image }>
                                    { favoriteImg ? <div
                                        className={ styles.nativeImg }
                                        style={ { backgroundImage: `url(${ favoriteImg })` } }/>
                                      : <Icon className={ styles.icon } name={ image }/> }
                                </div>
                            </div>
                            <div className={ styles.middle }>
                                <div className={ cx(styles.column, 'p-60') }>
                                    <div className={ styles.name }>
                                        { favorite[`name_${ lang }`] }
                                    </div>
                                    <div className={ styles.address }>
                                        { unescape(get(slots, '[0].fieldValue', '')) }
                                    </div>
                                </div>
                                <div className={ cx(styles.column, 'p-10') }>
                                    { renderField(slots[1]) }
                                </div>
                                <div className={ cx(styles.column, 'p-10') }>
                                    { renderField(slots[2]) }
                                </div>
                                <div className={ cx(styles.column, 'p-10') }>
                                    { renderField(slots[3]) }
                                </div>
                            </div>
                            <div className={ styles.right }>
                                <div className={ cx(styles.column) }>
                                    <FormatDateToQuarter className={ styles.quarter } date={ finish }
                                        status={ status } lastFactDate={ statusData.foreman.last_fact_datetime }/>
                                </div>
                                <div className={ cx(styles.column, 'p-30', styles.marginRight) }>
                                    <ProgressBar
                                      title={ loc._('basic.report_fact_type.foreman') }
                                      status={ statusData.foreman.status }
                                      value={ statusData.foreman.volume_percent }/>
                                </div>
                                <div className={ cx(styles.column, 'p-30') }>
                                    <ProgressBar
                                      title={ loc._('basic.report_fact_type.revisor') }
                                      status={ statusData.revisor.status }
                                      value={ statusData.revisor.volume_percent }/>
                                </div>

                                <div className={ cx(styles.column, 'p-30') }>
                                    { canWrite(favoriteType, favoriteId) && (
                                      <div className={ styles.kebabMenuWrapper }>
                                          <div className={ styles.kebabIcon } onClick={ kebabHandler }>
                                              <Icon name='signs-kebab'/>
                                          </div>
                                          { openType === favoriteType && openId === favoriteId && (
                                            <div
                                              ref={ wrapper }
                                              className={ styles.kebabMenu }
                                              onClick={ (e) => removeFavoriteHandler(e, favoriteType, favoriteId) }
                                            >
                                                <Icon name='account-trash-delete-bin'/>
                                                <span className={ styles.label }>
                                                            <Trans>component.favorites.remove</Trans>
                                                        </span>
                                            </div>
                                          ) }
                                      </div>) }
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
              )
          }) }
      </>
    )
})
