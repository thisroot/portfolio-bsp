import React, { FC } from 'react';
import { t } from "@lingui/macro";
import { loc } from "utils/i18n";
import favorites from '../../static/images/noFavorites.png';
import styles from './styles.scss';
import { observer } from "mobx-react-lite";

interface INoFavoritesItemsProps {
    title: string;
}

export const NoFavoritesItems: FC<INoFavoritesItemsProps> = observer(({ title }) => {
    return (
        <div className={ styles.noFavoritesItemsContainer }>
            <img src={favorites} alt={title} className={ styles.image }/>
            <div>
                <div className={ styles.title }>{`${title}`}</div>
                <div className={ styles.tizer }>{`${loc._(t`component.favorites.noItems.tizer`)}`}</div>
            </div>
        </div>
    );
});