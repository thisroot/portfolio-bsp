import React from "react"
import { get, unescape } from 'lodash'
import { Icon } from "components/UI/Icon"
import { Link } from "services/RouterService"
import { loc } from "utils/i18n"
import { t } from "@lingui/macro"
import { abbreviateNumber, isMarkdown } from "utils/helpers"
import { Domain } from 'components/UI'
import cx from 'classnames'
import styles from './styles.scss'
import colors from 'static/styles/colors.scss'
import { IWidgetProps, IField } from "utils/helpers"
import { renderMarkdown } from "../../../utils/render"

export const MainInfoTopWidget: React.FC<IWidgetProps> = ({ geoInfo, slotType, slots }) => {

    const formatMetaField = (field: IField) => {
        return <>
            { get(field, 'fieldLabel') ?
              <div className={ styles.metaFieldContainer }>
                  <div className={ styles.metaFieldName }>{ get(field, 'fieldLabel') }</div>
                  {
                      isMarkdown(get(field, 'fieldType')) && <div className={ styles.metaFieldValue }
                                                                  dangerouslySetInnerHTML={ renderMarkdown(get(field, 'fieldValue')) }/>
                  }
                  { !isMarkdown(get(field, 'fieldType')) && <div className={ styles.metaFieldValue }>
                      { get(field, 'fieldValue') ? abbreviateNumber(field.fieldValue) : '---' }
                  </div>
                  }
              </div> : null
            }</>
    }

    return <div className={ styles.widgetContainer }>
        { get(slots, '[0].fieldValue') &&
        <div className={ cx(styles.row, styles.slotTitle) }>{ unescape(slots[0].fieldValue) }</div> }
        {
            geoInfo && <div className={ cx(styles.row, styles.geoPosition) }>
                <Link to={ geoInfo }><Icon name={ 'map-pin' } text={ loc._(t`basic.showOnMap`) }/></Link>
            </div>
        }
        <div className={ cx(styles.row) }>
            <div className={ cx(styles.col) }>
                { slotType && <Domain icon={ 'account-home-door' } text={ slotType } labelColor={ colors.orange460 }/> }
            </div>
            { slots[1] && <div className={ cx(styles.col, styles.separator) }/> }
            <div className={ cx(styles.col, styles.slotTwo) }>
                { formatMetaField(slots[1]) }
            </div>
            <div className={ cx(styles.col, styles.slotThree) }>
                { formatMetaField(slots[2]) }
            </div>
            <div className={ cx(styles.col, styles.slotFour) }>
                { formatMetaField(slots[3]) }
            </div>
            <div className={ cx(styles.col, styles.slotFive) }>
                { formatMetaField(slots[4]) }
            </div>
        </div>
    </div>
}