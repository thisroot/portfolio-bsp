import React from "react"
import dayjs from "dayjs"
import { t } from "@lingui/macro"
import { romanizeNumber } from "utils/helpers"
import styles from "./styles.scss"
import { loc } from "utils/i18n"
import cx from 'classnames'
import { DateUtils } from "react-day-picker"

export interface IFormatDateToQuarterProps {
    date: string
    status: number
    format?: string
    className?: string
    classNameLabel?: string
    classNameValue?: string
    lastFactDate?: string
}

export const FormatDateToQuarter: React.FC<IFormatDateToQuarterProps> = (
  {
      date,
      status = 0,
      format = 'YYYY-MM-DD',
      className,
      classNameLabel,
      classNameValue,
      lastFactDate = ''
  }) => {

    const dateObj = dayjs(date, format)
    const isDataIncorrect = !DateUtils.isDate(dateObj.toDate())

    const year = dateObj.year()
    const quarter = ((dateObj.month() + 2) / 3).toFixed(0)

    const stagesLabels = [ t`components.stage.stop`, t`components.stage.stop`, t`components.stage.normal`, t`components.stage.normal`, t`components.stage.done` ]
    const label = stagesLabels[status || 0]

    const lastFactDateObj = dayjs(lastFactDate)
    const isLastFactDateIncorrect = !DateUtils.isDate(lastFactDateObj.toDate())

    return <div className={ cx(styles.metaFieldContainer, className) }>
        <div className={ cx(styles.metaFieldValue, classNameValue) }>
            { !isDataIncorrect ? `${ romanizeNumber(quarter) } ${ loc._(t`basic.quarter`) } ${ year }` : loc._(t`basic.noDataShort`) }
        </div>
        { status ? <div
          className={ cx(styles.metaFieldName, { [styles.danger]: status <= 1 }, classNameLabel) }>{ loc._(label) }</div> : null }
        { !isLastFactDateIncorrect
          ? <div className={ cx(styles.metaFieldName, styles.lastFactDate, classNameLabel) }>
              { `${ loc._(t`basic.fact`) }: ${ lastFactDateObj.format(loc._(t`basic.dateFormat.DD.MM.YYYY`)) }` }
          </div>
          : null
        }
    </div>
}
