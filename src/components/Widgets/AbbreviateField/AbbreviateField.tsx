import React from "react"
import styles from "./styles.scss"
import { abbreviateNumber, isValidUrl } from "utils/helpers"
import cx from 'classnames'
import Truncate from 'react-truncate'
import { Link } from "../../../services/RouterService"
import { loc } from "../../../utils/i18n"
import { t } from "@lingui/macro"

export enum FIELD_TYPE {
    TEXT = 'text',
    LINK = 'link',
}

export interface IAbbreviateFieldProps {
    value: number | string
    label: string
    className?: string
    labelClassName?: string
    valueClassName?: string
    swap?: boolean
    type?: FIELD_TYPE
}

export const AbbreviateField: React.FC<IAbbreviateFieldProps> = (
  {
      value,
      label,
      swap = false,
      className,
      labelClassName,
      valueClassName,
      type = FIELD_TYPE.TEXT

  }) => {
    const isLink = type === FIELD_TYPE.LINK
    let url
    let invalidUrl = false
    if (isLink) {
        try {
            if (isValidUrl(`${ value }`)) {
                const hasScheme = `${ value }`.match(/^(http|https):\/\//i)
                url = new URL(`${ hasScheme ? '' : "http://" }${ value }`)
            } else {
                invalidUrl = loc._(t`components.abbreviate.field.wrongUrlScheme`)
            }
        } catch {
            invalidUrl = loc._(t`components.abbreviate.field.wrongUrlScheme`)
        }
    }

    const renderLabel = <div title={ label } key={ 'abbreviate-1' }
                             className={ cx(styles.metaFieldName, labelClassName) }>{ label }</div>
    const renderName = isLink ? invalidUrl ||
      <Link to={ url.toString() } key={ 'abbreviate-2' }
            className={ cx(styles.metaFieldValue, styles.link, valueClassName) }>
          { url.host }
      </Link> :
      <Truncate title={ value } lines={ 2 } key={ 'abbreviate-2' }
                className={ cx(styles.metaFieldValue, valueClassName) }>{
          abbreviateNumber(value) || '---' }
      </Truncate>
    const content = swap ? [ renderName, renderLabel ] : [ renderLabel, renderName ]
    return <div className={ cx(styles.metaFieldContainer, className) }>
        { content }
    </div>
}
