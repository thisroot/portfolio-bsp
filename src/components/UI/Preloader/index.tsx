import React from "react"
import { Spinner } from "../Spinner"
// import colors from '../../../static/styles/colors.scss'
// size={ 50 } thickness={ 4 } color={ colors.orange }
import styles from './styles.scss'

export interface IPreloaderProps {
    text?: string
}

export const Preloader: React.FC<IPreloaderProps> = ({ text }) => {
    return (
      <div className="banter-loader">
          <Spinner/>
          { text && <span className={ styles.text }> { text }</span> }
      </div>)
}
