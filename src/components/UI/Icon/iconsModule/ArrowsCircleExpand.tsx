import * as React from 'react';
const SvgArrowsCircleExpand = React.memo((props: React.SVGProps<SVGSVGElement>) => (
  <svg viewBox="0 0 24 24" width="1em" height="1em" fill="currentColor" fontSize="1em" {...props}>
    <g
      fill="none"
      className="arrows-circle-expand_svg__nc-icon-wrapper"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M17.506 14.678v-4.243h-4.242M10.435 13.263v4.243h4.243" />
      <path d="M20.335 20.334A9 9 0 117.607 7.607a9 9 0 0112.728 12.727z" />
    </g>
  </svg>
));
export default SvgArrowsCircleExpand;
