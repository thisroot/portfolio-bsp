import * as React from 'react';
const SvgArrowsCircleCollapse = React.memo((props: React.SVGProps<SVGSVGElement>) => (
  <svg viewBox="0 0 24 24" width="1em" height="1em" fill="currentColor" fontSize="1em" {...props}>
    <g
      fill="none"
      className="arrows-circle-collapse_svg__nc-icon-wrapper"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M15.385 8.314v4.242h4.242M12.556 19.627v-4.242H8.314" />
      <path d="M20.334 20.334A9 9 0 117.607 7.607a9 9 0 0112.727 12.727z" />
    </g>
  </svg>
));
export default SvgArrowsCircleCollapse;
