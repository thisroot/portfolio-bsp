import React, { FC, useRef, useState } from 'react'
import ReactTags from 'react-tag-autocomplete'
import { Icon } from 'components/UI/Icon'
import { Checkbox } from "components/UI/Controls/Checkbox"
import cx from 'classnames'
import styles from './styles.scss'
import { observer } from "mobx-react-lite"

interface ITag {
    id: number | string;
    name: string;
}

interface ISuggestion {
    id: number | string;
    name: string;
    disabled?: boolean;
}

interface IMultiSelectAutocompleteProps {
    id?: number | string;
    tags?: ITag[];
    suggestions: ISuggestion[];
    onAddition: (ITag) => void;
    onDelete: (number) => void;
    placeholderText?: string;
    allowBackspace?: boolean;
    onInput?: (str: string) => Promise<void> | void
    className?: string
}

export const MultiSelectAutocomplete: FC<IMultiSelectAutocompleteProps> = (props) => {
    const reactTags = useRef(null)
    const [ isFilterActive, setIsFilterActive ] = useState<boolean>(false)

    const classNames = {
        root: 'multi-select-autocomplete',
        rootFocused: 'is-focused',
        selected: 'multi-select-autocomplete__selected',
        selectedTag: 'multi-select-autocomplete__selected-tag',
        selectedTagName: 'multi-select-autocomplete__selected-tag-name',
        search: 'multi-select-autocomplete__search',
        searchWrapper: 'multi-select-autocomplete__search-wrapper',
        searchInput: 'multi-select-autocomplete__search-input',
        suggestions: 'multi-select-autocomplete__suggestions',
        suggestionActive: 'is-active',
        suggestionDisabled: 'is-disabled'
    }

    const renderTag = (tagProps) => (
      <div className={ cx(tagProps.classNames.selectedTag, styles.tag) }>
          { tagProps.tag.name }
          <Icon
            className={ styles.controlBtn }
            onClick={ (e) => {
                e.stopPropagation()
                tagProps.onDelete()
            } }
            name={ 'signs-delete-close' }
          />
      </div>
    )

    const renderSuggestion = ({ item }) => {
        const isSelected = props.tags && props.tags.some(tag => tag.id === item.id)
        return (
          <div
            onMouseDown={ (e) => {
                e.preventDefault()
                e.stopPropagation()
                if (isSelected) {
                    const index = props.tags.findIndex(tag => tag.id === item.id)
                    props.onDelete(index)
                } else {
                    props.onAddition(item)
                }
            } }
            className={ styles.checkboxContainer }
          >
              <Checkbox checked={ isSelected } className={ styles.checkbox }/>
              { item.name }
          </div>
        )
    }

    return (
      <div className={ cx(styles.multiSelectContainer, props.className) }>
          <ReactTags
            { ...props }
            allowNew={ false }
            ref={ reactTags }
            classNames={ classNames }
            tagComponent={ renderTag }
            suggestionComponent={ observer(renderSuggestion) }
            autoresize={ false }
            onBlur={ () => {
                reactTags && reactTags.current.clearInput()
            } }
            onFocus={ () => setIsFilterActive(false) }
            onInput={ query => {
                setIsFilterActive(!!query)
                props.onInput && props.onInput(query)
            } }
            suggestionsFilter={ () => true }// isFilterActive ? undefined : () => true }
            minQueryLength={ isFilterActive ? 2 : 0 }
          />
      </div>
    )
}
