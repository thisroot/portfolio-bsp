import React, { useEffect, useState } from 'react'
import styles from './styles.scss'
import { Checkbox } from "../Controls/Checkbox"
import cx from 'classnames'

export interface IMultiSelectCheckboxProps {
    value?: Array<string>
    items?: Array<{ [key: string]: string }>
    onChange?: (val: any) => void
    fromLine?: number
    className?: string
}

const MultiSelectCheckbox: React.FC<IMultiSelectCheckboxProps> = (
  {
      value,
      items,
      onChange,
      fromLine,
      className
  }) => {

    const [ values, setValues ] = useState(new Map(value && items.filter(item => value.includes(item.key)).map(item => [ item.key, item ])))
    useEffect(() => {
        setValues(new Map(value && items.filter(item => value.includes(item.key)).map(item => [ item.key, item ])))
    }, [ value ])

    const style = fromLine ? { width: `${ fromLine * 10 }%` } : {}

    return <div className={ cx(styles.multiSelectCheckboxContainer, className) }>
        {
            items && items.map(item => {
                return <Checkbox onChange={ (e) => {
                    const newValues = values
                    e.target.checked ? newValues.set(item.key, item) : newValues.delete(item.key)
                    setValues(newValues)
                    onChange && onChange([ ...values.values() ].map(item => item.key))
                }
                } className={ cx(styles.checkbox, { [styles.fromRows]: !fromLine }) }
                                 style={ style }
                                 key={ item.key } label={ item.value }
                                 checked={ !!values.get(item.key) }/>
            })
        }
    </div>
}

export {
    MultiSelectCheckbox
}
