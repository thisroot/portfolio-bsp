import React, { FC, useState, useEffect } from 'react'
import { createPortal } from 'react-dom'
import { useKey } from "react-use"
import cx from 'classnames'
import styles from "./styles.scss"
import { observer } from "mobx-react-lite"
import { FileModel } from "../../../models/entities/FileModel"
import { get, isNull } from 'lodash'
import { parseMeasureUnit, getUserFullName } from "utils/helpers"
import { REPORT_FACT_TYPE } from 'constants/locale'
import { loc } from "utils/i18n"
import { t } from "@lingui/macro"
import dayjs from "dayjs"
import { Trans } from "@lingui/macro"
import { Button, BUTTON_TYPE } from "../Button"
import { AppService } from "services"
import { useInstance } from "react-ioc"
import { SimpleConfirmAbortModal } from "components/UI/Modal/SimpleConfirmAbortModal"
import { ObjectsMap } from "components/ObjectsMap"
import utc  from 'dayjs/plugin/utc'

dayjs.extend(utc)


interface ILightBoxProps {
    media: FileModel[];
    current?: number;
    loadMore?: () => boolean;
    onClose: (current: number) => void;
    photoFact?: any;
    onDelete?: (current: number, factId: number) => Promise<void>
    canDelete?: (file?: FileModel) => boolean
}

const LightBoxComponent: FC<ILightBoxProps> = observer((props) => {
    const app = useInstance(AppService)

    const { onClose, current = 0, media, photoFact = [], canDelete = () => true } = props
    const [ currentPhoto, setCurrentPhoto ] = useState<number>(current)
    const [ isPreventClose, setIsPreventClose ] = useState<boolean>(false)
    const factId = get(media[currentPhoto], 'file.image_relation_id')
    const fact = photoFact.find(el => get(el, 'schedule_fact_id') === factId)
    const [ pending, setPending ] = useState(true)
    const [ disabled, setDisabled ] = useState(true)

    useEffect(() => {
      setPending(true)
    }, [currentPhoto])

    useEffect(() => {
        setCurrentPhoto(current)
        if (!get(media[currentPhoto], 'fullView')) {
            onClose(currentPhoto)
        }
    }, [ current ])

    const onPrevHandler = (e: KeyboardEvent | React.MouseEvent): void => {
        e.preventDefault()
        const isRepeatPress = e.type === 'keydown' && (e as KeyboardEvent).repeat === true
        if (currentPhoto > 0 && !isRepeatPress) {
            setCurrentPhoto(currentPhoto => currentPhoto - 1)
        }
    }

    const onNextHandler = (e: KeyboardEvent | React.MouseEvent): void => {
        e.preventDefault()
        const isRepeatPress = e.type === 'keydown' && (e as KeyboardEvent).repeat === true
        if (currentPhoto < media.length - 1 && !isRepeatPress) {
            setCurrentPhoto(currentPhoto => currentPhoto + 1)
        }
    }

    const onCloseHandler = (e: KeyboardEvent | React.MouseEvent): void => {
        e.preventDefault()
        !isPreventClose && onClose(currentPhoto)
    }

    const onShowMapHandler = (photo): void => {
      const location = {
        lat: photo.geo_coord.lat,
        lng: photo.geo_coord.long,
      }
      setIsPreventClose(true)
      const additionalCloseCallback = () => setIsPreventClose(false)

      app.modalControls.renderModal = () =>
        <SimpleConfirmAbortModal
          title={
            <Button
              icon={ 'signs-delete-close-a' }
              className={ styles.closeBtn }
              onClick={ () => {
                additionalCloseCallback && additionalCloseCallback()
                app.modalControls.close()
              } }
              outline
            />
          }
          wrapperClassName={ styles.lightboxModalWrapper }
          titleClassName={ styles.lightboxModalTitle }
          footerClassName={ styles.lightboxModalFooter }
        >
          <div className={ styles.lightboxModalContentWrapper }>
            <ObjectsMap showEntity={ false } className={ styles.mapWrapper }
                        entities={ [location] } center={ location } zoom={ 10 }
                        isEntitiesMappingNeeded={ false } isCustomIconEnable={ false }
                        fullscreenControl={ false }
            />
          </div>
        </SimpleConfirmAbortModal>

      app.modalControls.open({ additionalCloseCallback })
    }

    useKey('Escape', onCloseHandler, undefined, [ currentPhoto, isPreventClose ])
    useKey('ArrowLeft', onPrevHandler, undefined, [ currentPhoto ])
    useKey('ArrowRight', onNextHandler, undefined, [ currentPhoto ])

    const getMeasureName = (photoFact) => {
        const jobUnit = get(photoFact, 'schedule_id.job_unit', null)
        return isNull(jobUnit)
          ? parseMeasureUnit(get(photoFact, 'schedule_id.job_id.job_unit'))
          : parseMeasureUnit(jobUnit)
    }

    const getFullUserInfo = (photoFact) => {
        const prof = loc._(REPORT_FACT_TYPE[get(photoFact, 'fact_type')]).toLowerCase()
        return `${ prof } ${ getUserFullName(get(photoFact, 'user_id')) }`
    }

    const getFormattedDate = (photoFact) => {
        return dayjs.utc(get(photoFact, 'created')).format('D MMMM YYYY H:m')
    }

    const isGeoCoordEntered = !isNull(get(media, `${currentPhoto}.file.geo_coord`, null))

    const onImageLoadHandle = () => {
      setDisabled(!isGeoCoordEntered)
      setPending(false)
    }

    return (
      <div className={ styles.lightBoxContainer }>
          { media[currentPhoto] &&
          <>
              <div className={ styles.innerContainer }>
                  <div className={ styles.imagesContainer }>
                    <img src={ media[currentPhoto].fullView } alt={ media[currentPhoto].name } onLoad={ onImageLoadHandle } />
                  </div>
                  { currentPhoto > 0
                    ? <button className={ cx(styles.navButtons, styles.prevButton) } onClick={ onPrevHandler }/>
                    : null
                  }
                  { currentPhoto < media.length - 1
                    ? <button className={ cx(styles.navButtons, styles.nextButton) } onClick={ onNextHandler }/>
                    : null
                  }
                  <div className={ styles.topToolBar }>
                      <button
                        className={ cx(styles.navButtons, styles.closeButton) }
                        onClick={ onCloseHandler }
                      />
                  </div>
              </div>
              { photoFact.length
                ? (
                  <div className={ styles.commentsContainer }>
                    <div className={ styles.widgetWrapper }>
                        <div
                          className={ styles.path }>{ `${ get(fact, 'schedule_id.schedule_id', "") }.${ get(fact, 'schedule_fact_id', "") }` }</div>
                        <div className={ styles.title }>{ get(fact, 'schedule_id.name') }</div>
                        <div
                          className={ styles.factVolume }>{ `${ get(fact, 'volume', '') } ${ getMeasureName(fact) }` }</div>
                        <div className={ styles.comment }><Trans>basic.comment</Trans>: { get(fact, 'comment', '') }
                        </div>
                        <div
                          className={ styles.user }>{ `${ getFormattedDate(fact) } | ${ getFullUserInfo(fact) }` }</div>
                    </div>
                    <div className={ styles.controls }>
                        {
                          <Button
                            onClick={ !pending && isGeoCoordEntered ? () => onShowMapHandler(media[currentPhoto].file) : null }
                            buttonType={ BUTTON_TYPE.INFO }
                            icon={ 'internet-search' }
                            className={ cx( styles.geoCoordButton, { [styles.disabledBtn]: pending || !isGeoCoordEntered } )}
                            text={ loc._(t`basic.showOnMap`) }
                            pending={ pending }
                            iconStyle={ styles.iconStyle }
                            disabled={ disabled }
                          />
                        }
                        {
                            props.onDelete && canDelete(media[currentPhoto]) &&
                            <Button onClick={ () => props.onDelete(media[currentPhoto].id, factId) }
                                    buttonType={ BUTTON_TYPE.DANGER }
                                    icon={ 'account-trash-delete-bin' }/>
                        }
                    </div>
                  </div>)
                : null
              }
          </>
          }
      </div>
    )
})

export const LightBox: FC<ILightBoxProps> = observer((props) => {
    const el = document.createElement('div')
    useEffect(() => {
        document.body.appendChild(el)
        return () => document.body.removeChild(el)
    }, [ el ])

    return createPortal(<LightBoxComponent { ...props } />, el)
})
