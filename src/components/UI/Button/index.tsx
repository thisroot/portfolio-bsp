import React, { CSSProperties } from 'react'
import { ShouldRender } from 'utils/render'
import { Icon } from 'components/UI/Icon'
import styles from './styles.scss'
import { omit, get } from 'lodash'
import { Spinner } from 'components/UI/Spinner'
import cx from 'classnames'

interface IButton {
    className?: string
    style?: CSSProperties
    addClass?: string
    width?: number
    height?: number
    text?: React.ReactNode
    children?: React.ReactNode
    type?: 'submit' | 'reset' | 'button'
    outline?: boolean
    withoutBorders?: boolean
    round?: boolean
    size?: number
    active?: boolean
    pending?: boolean
    disabled?: boolean
}

const BasicButton: React.ElementType<IButton> = (props: IButton) => {
    const style = { width: props.width, height: props.height, ...props.style }
    const className = cx(styles.basicBtn, props.className, {
        [styles.active]: props.active,
        [styles.outlineBtn]: props.outline,
        [styles.roundBtn]: props.round,
        [styles.withoutBorder] : props.withoutBorders,
        [styles[`s${ get(props, 'size', 1) }`]]: props.size
    })
    const newProps = omit(props, [ 'outline', 'round', 'withoutBorders', 'buttonType', 'iconStyle', 'active', 'pending' ])
    return (
      <button { ...newProps } type={ props.type } style={ style } className={ className }>
          { props.children || props.text }
      </button>
    )
}

export enum BUTTON_TYPE {
    BASIC = 'basic',
    SUCCESS = 'success',
    PRIMARY = 'primary',
    INFO = 'info',
    WARNING = 'warning',
    DANGER = 'danger',
    SECONDARY = 'secondary'
}

interface ITextIconButton extends IButton {
    customIcon?: React.ElementType
    icon?: string
    iconStyle?: string
    textStyle?: CSSProperties
    style?: CSSProperties
    buttonType?: BUTTON_TYPE
}

const MAP_STYLES = {
    [BUTTON_TYPE.INFO]: styles.infoBtn,
    [BUTTON_TYPE.DANGER]: styles.dangerBtn,
    [BUTTON_TYPE.PRIMARY]: styles.primaryBtn,
    [BUTTON_TYPE.SECONDARY]: styles.secondaryBtn,
    [BUTTON_TYPE.SUCCESS]: styles.successBtn,
    [BUTTON_TYPE.WARNING]: styles.warningBtn
}

const Button: React.ElementType<ITextIconButton> = (props: ITextIconButton) => {
    const className = cx(MAP_STYLES[props.buttonType], { [props.className]: props.className })
    return (props.icon || props.pending) ? <DefaultButton { ...props } className={ className }/> :
      <BasicButton { ...props } className={ className }/>
}

const DefaultButton: React.ElementType<ITextIconButton> = (props: ITextIconButton) => {
    const style = { width: props.width, height: props.height }
    const className = cx(styles.textIconBtn, { [props.className]: props.className })
    const { textStyle, ...newProps } = props;
    const isLightBackground = props.outline ||!props.buttonType || props.disabled

    return (
      <BasicButton { ...newProps }
                   className={ className }
                   style={ style }>
            <ShouldRender if={ props.pending }>
              <Spinner color={ isLightBackground ? undefined : '#FFFFFF'} />
          </ShouldRender>
          <ShouldRender if={ !props.customIcon && props.icon && !props.pending }>
              <Icon className={ props.iconStyle } name={ props.icon }/>
          </ShouldRender>
          <ShouldRender if={ props.customIcon && !props.pending }>
              { props.customIcon }
          </ShouldRender>
          { props.text && <span className={ styles.textBtn } style={ props.textStyle }>{ props.text }</span> }
      </BasicButton>
    )
}

const PrimaryButton: React.ElementType<ITextIconButton> = (props: ITextIconButton) => {
    return <Button {...props} buttonType={BUTTON_TYPE.PRIMARY}  />
}

const SuccessButton: React.ElementType<ITextIconButton> = (props: ITextIconButton) => {
    return <Button {...props} buttonType={BUTTON_TYPE.SUCCESS} />
}

const InfoButton: React.ElementType<ITextIconButton> = (props: ITextIconButton) => {
    return <Button {...props} buttonType={BUTTON_TYPE.INFO}  />
}

const WarningButton: React.ElementType<ITextIconButton> = (props: ITextIconButton) => {
    return <Button {...props} buttonType={BUTTON_TYPE.WARNING } />
}

const DangerButton: React.ElementType<ITextIconButton> = (props: ITextIconButton) => {
    return <Button {...props} buttonType={BUTTON_TYPE.DANGER}  />
}

const SecondaryButton: React.ElementType<ITextIconButton> = (props: ITextIconButton) => {
    return <Button {...props} buttonType={BUTTON_TYPE.SECONDARY}  />
}

interface IButtonsGroup {
    className?: string
    children?: React.ReactNode
}

const ButtonsGroup: React.ElementType<IButtonsGroup> = (props: IButtonsGroup) => {
    return <div className={ cx(styles.groupBtn, props.className) }>
        { props.children }
    </div>
}

export {
    BasicButton,
    PrimaryButton,
    DangerButton,
    SuccessButton,
    WarningButton,
    InfoButton,
    DefaultButton,
    SecondaryButton,
    Button,
    ButtonsGroup
}
