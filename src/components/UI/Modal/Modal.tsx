import React from "react"
import styles from './styles.scss'
import cx from 'classnames'

interface IModalProps {
    title?: any;
    titleClassName?: string;
    footerClassName?: string;
    contentClassName?: string;
    wrapperClassName?: string;
    onConfirm?: (any) => any;
    onAbort?: (any) => any;
    renderConfirm?:()=> any;
    renderAbort?:() => any;
}

export const Modal: React.FC<IModalProps> = ({
    children,
    title = '',
    renderConfirm,
    renderAbort,
    titleClassName,
    footerClassName,
    contentClassName,
    wrapperClassName
}) => (
    <div className={ cx(styles.modalContainer, wrapperClassName) }>
        <div className={ cx(styles.title, titleClassName) }>
            { title }
        </div>
        <div className={ cx(styles.contentContainer, contentClassName) }>
            { children }
        </div>
        <div className={ cx(styles.footer, footerClassName) }>
            { renderAbort() }
            { renderConfirm() }
        </div>
    </div>
);