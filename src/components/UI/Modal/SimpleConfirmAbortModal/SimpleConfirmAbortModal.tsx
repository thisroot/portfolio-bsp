import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { BasicButton, DangerButton, Modal, SuccessButton } from 'components/UI'
import { loc } from 'utils/i18n'
import { t } from '@lingui/macro'

export enum MODAL_THEME {
    OK = 'OK',
    REMOVE = 'REMOVE'
}

type IProps = {
    title?: any;
    titleClassName?: string
    footerClassName?: string
    contentClassName?: string
    wrapperClassName?: string
    onConfirm?: (e?: any) => any;
    onAbort?: (e?: any) => any;
    onAbortText?: string
    onSuccessText?: string
    confirmIsDisabled?: boolean
    abortIsDisabled?: boolean
    theme?: MODAL_THEME
};

export const SimpleConfirmAbortModal: FC<IProps> = observer((
  {
      title,
      children,
      onConfirm,
      onAbort,
      onAbortText,
      onSuccessText,
      titleClassName,
      footerClassName,
      contentClassName,
      wrapperClassName,
      confirmIsDisabled,
      abortIsDisabled,
      theme = MODAL_THEME.OK
  }) => {

    let successText = ''
    let abortText = loc._(t`basic.abort`)
    let btn
    switch (theme) {
        case MODAL_THEME.OK:
            successText = loc._(t`basic.ok`)
            btn = <SuccessButton
              text={ onSuccessText || successText }
              onClick={ onConfirm }
              disabled={ confirmIsDisabled || false }
            />
            break
        case MODAL_THEME.REMOVE:
            successText = loc._(t`basic.delete`)
            btn = <DangerButton
              text={ onSuccessText || successText }
              onClick={ onConfirm }
              disabled={ confirmIsDisabled || false }
            />
            break
    }

    return (
      <Modal
        title={ title }
        titleClassName={ titleClassName }
        footerClassName={ footerClassName }
        contentClassName={ contentClassName }
        wrapperClassName={ wrapperClassName }
        renderConfirm={ () => onConfirm && btn }
        renderAbort={ () => onAbort && (
          <BasicButton
            style={ { border: 'none' } }
            outline
            text={ onAbortText || abortText }
            onClick={ onAbort }
            disabled={ abortIsDisabled || false }
          />
        )
        }
      >
          { children }
      </Modal>
    )
})
