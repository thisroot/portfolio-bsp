import React, { useState } from 'react'
import { InputSelect } from "../UI/InputSelect"
import { observer } from "mobx-react-lite"

import styles from "./styles.scss"

interface IEditableSelectProps {
    placeholder?: string,
    items: {}[],
    onChange?: (any?: any) => void,
    isEditable?: boolean,
    text: string,
    className?: string,
    labelClassName?: string
}

export const EditableSelect: React.FC<IEditableSelectProps> =
  observer(({
                placeholder,
                items,
                onChange,
                isEditable,
                text,
                className,
                labelClassName
            }) => {

      const [ isEdit, setEdit ] = useState(false)

      const toggleEdit = (isEdit: boolean): void => {
          if (isEditable) {
              setEdit(isEdit)
          }
      }

      return (
        <div className={ className }>
            {
                isEdit ?
                  (
                    <InputSelect
                      inputClassName={ styles.inputSelect }
                      placeholder={ placeholder }
                      searchFields={ [ 'name' ] }
                      keyName={ 'id' }
                      itemName={ 'name' }
                      items={ items }
                      onChange={ (e) => {
                          onChange(e)
                          toggleEdit(false)
                      } }
                    />
                  ) : (
                    <span title={text} className={ labelClassName } onClick={ () => toggleEdit(true) }>
                        { text }
                    </span>
                  )
            }
        </div>
      )
  })