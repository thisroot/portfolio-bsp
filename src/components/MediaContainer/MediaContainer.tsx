import React, { Fragment, useEffect, useRef } from "react"
import { observer } from "mobx-react-lite"
import { FileTypes } from "../../models/interfaces"
import { ProgerssBarCircular } from "../UI/ProgerssBarCircular"
import styles from "./styles.scss"
import { Icon } from "../UI/Icon"
import { FileModel } from "../../models/entities/FileModel"
import cx from 'classnames'

export interface IMediaContainer {
    file: FileModel;
    onClick?: () => any;
    selectAsMain?: (file: FileModel) => any;
    canWrite?: boolean;
    imageConvertType?: string;
    imageClassName?: any;
    isMain?: boolean
}

export const MediaContainer: React.FC<IMediaContainer> = observer(({
                                                                       file,
                                                                       onClick,
                                                                       selectAsMain,
                                                                       canWrite = !!selectAsMain,
                                                                       imageConvertType = 'preview',
                                                                       imageClassName,
                                                                       isMain
                                                                   }) => {

    const progress = file.status.uploading * 0.75 + file.status.converting * 0.25

    const backgroundImage = file.typeGroup === FileTypes.IMAGE ?
      { backgroundImage: `url(${ file[imageConvertType] })` } : undefined

    const videoRef = useRef(null)

    const selectMain = (e) => {
        e.stopPropagation()
        selectAsMain(file)
    }

    useEffect(() => {
        if (videoRef.current) {
            videoRef.current.currentTime = 3
            videoRef.current.pause()
        }
    })

    const clickOnImage = (e) => {
        e.stopPropagation()
        onClick && onClick()
    }


    const renderPreloader = () => {
        return (
          <Fragment>
              { file.isProgressVisible && (
                <ProgerssBarCircular
                  percentage={ progress }
                  className={ cx([ styles['progress-bar'], styles['uploading-progressbar'] ]) }
                  sqSize={ 50 }
                  strokeWidth={ 5 }
                  fontSize={ '2rem' }
                  color={ '#5285c4' }
                  isPercentageVisible={ false }
                >
                    <Icon name={ 'signs-cancel' } className={ styles['progress-icon'] }/>
                </ProgerssBarCircular>)
              }
              { file.onRemove() &&
              <div className={ styles.remove } onClick={ (e) => {
                  e.stopPropagation()
                  e.preventDefault()
                  file.onRemove()
              } }>
                  <Icon name="signs-delete-close"/>
              </div>
              }

          </Fragment>
        )
    }

    return (
      <div className={ cx(styles.attachment, { [styles.error]: file.isError, [styles.isProcessed]: file.isProcessed }) }
           onClick={ clickOnImage }>
          { file.typeGroup === FileTypes.IMAGE &&
          <div className={ cx(styles.file, imageClassName) } style={ backgroundImage }/>
          }
          { file.typeGroup === FileTypes.VIDEO &&
          <video ref={ videoRef } className={ styles.file } autoPlay loop muted src={ file.preview }/>
          }
          { renderPreloader() }
          { canWrite &&
          <Icon name="signs-done-check" className={ cx(styles.selectAsMain, { [styles.selected]: isMain }) }
                onClick={ selectMain }/> }
      </div>
    )
})