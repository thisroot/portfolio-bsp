import React, { useEffect, useRef, useState } from "react"
import styles from "../GMapGL/styles.scss"
import { Map, TileLayer } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'

const DEFAULT_VIEWPORT = {
    center: [ 51.505, -0.09 ],
    zoom: 13,
}

const GMapLeaflet: React.FC<any> = () => {

    const [ viewport, setViewPort ] = useState(DEFAULT_VIEWPORT)

    const ref = useRef()

    useEffect(() => {
        if (ref.current) {
            // @ts-ignore
            ref.current.leafletElement.invalidateSize()
        }
        console.log(ref)
    }, [ ref ])


    const token = 'pk.eyJ1IjoidGhpc3Jvb3QiLCJhIjoiY2s3eGdtcGkzMDcyczNvc2ZvNjBrdmRsbyJ9._NYVlYrGBxvsfpvjkj4XRA'
    const id = 'streets-v9'

    return <div className={ styles.mapsContainer }>
        <Map  style={{ width: '100%', height: '100%' }} ref={ ref } viewport={ viewport } onViewPortChanged={ setViewPort }>
            <TileLayer
              url={ `https://api.mapbox.com/styles/v1/mapbox/${ id }/tiles/{z}/{x}/{y}?access_token=${ token }` }
            />
        </Map>
    </div>
}

export default GMapLeaflet