import React from "react"
import { EditableField } from "../EditableField"
import cx from 'classnames'
import { observer } from "mobx-react-lite"
import styles from './styles.scss'
import { InputSelect } from "../UI/InputSelect"
import { get, unescape } from 'lodash'
import { Accordion } from "components/UI/Accordion"
import { DangerButton, DefaultButton, SuccessButton } from "../UI/Button"
import { clearTextFromNbsp } from "../../utils/helpers"

export interface IMetaInfoEditorProps {
    editorTitle?: string | React.ReactNode
    canEdit: boolean
    isEditMode: boolean
    isDirty?: boolean
    toggleEdit?: () => void
    onSave?: () => void
    resetEdit?: () => void
    fields: Array<any>
    currentType?: any
    metaTypes?: Array<any>
    onChangeMetaType?: (type: { value: any, key: string }) => void,
    isChangeTypeView?: boolean
    toggleChangeTypeView?: () => void
    selectLabel?: string
    isCreateMode?: boolean
    classStyles?: {
        wrapper?: string,
        content?: string,
    },
    lang?: string
}

export const MetaInfoEditor: React.FC<IMetaInfoEditorProps> =
  observer(({
                metaTypes,
                onChangeMetaType,
                editorTitle,
                canEdit,
                isEditMode,
                isDirty,
                toggleEdit,
                onSave,
                resetEdit,
                fields,
                isChangeTypeView = false,
                toggleChangeTypeView,
                currentType,
                selectLabel,
                isCreateMode = false,
                classStyles,
                lang = 'ru'
            }) => {


      const isFieldsExist = fields && fields.length > 0

      const renderFields = (fields: Array<any>) => fields.map(item => {
          const hasChildren = get(item, 'fields.size') > 0

          const fieldProps = {
              isEditable: isEditMode,
              centered: false,
              value: item.value,
              label: item.label,
              type: item.type,
              onChange: (value, _, __) => item.value = value,
              onBlur: (val) => item.value = val,
              lines: 3,
              className: styles.editableField
          }

          return hasChildren ?
            <Accordion
              key={ item.label }
              headerContent={ item.label }
              arrow={ 'arrows-right-a' }
              isOpen
            >
                <ul key={ item.label }>
                    { renderFields([ ...item.fields.values() ]) }
                </ul>
            </Accordion> :
            <li key={ item.name }
                className={ cx(styles.contentRow, get(classStyles, 'contentRow', false)) }>
                <div title={ clearTextFromNbsp(unescape(item.label)) }
                     className={ cx(styles.contentCell, styles.contentCellName, get(classStyles, 'contentCell', false)) }>
                    { clearTextFromNbsp(unescape(item.label)) }
                </div>
                <div className={ cx(styles.contentCell, get(classStyles, 'contentCell', false)) }>
                    <EditableField
                      { ...fieldProps }
                    />
                </div>
            </li>
      })

      return <div className={ cx(styles.componentWrapper, get(classStyles, 'wrapper', false)) }>
          {
              !isCreateMode &&
              <div className={ styles.header }>
                  { editorTitle &&
                  <div className={ styles.headerLeft }>
                      <div className={ styles.headerTitle }>
                          { editorTitle }
                      </div>
                  </div>
                  }
                  <div className={ styles.headerRight }>
                      { !isEditMode && canEdit && isFieldsExist &&
                      <DefaultButton
                        disabled={ false }
                        onClick={ toggleEdit }
                        className={ styles.editMeta }
                        icon={ "edit-pen-fill" }/>
                      }
                      {
                          isEditMode && canEdit && isFieldsExist && <>
                              <SuccessButton
                                disabled={ !isDirty }
                                onClick={ onSave }
                                className={ styles.editMeta }
                                icon={ "signs-done-check-c" }
                              />
                              <DangerButton
                                disabled={ false }
                                onClick={ resetEdit }
                                className={ styles.editMeta }
                                icon={ 'signs-delete-close' }
                              />
                          </>
                      }
                      {
                          !isEditMode && canEdit && toggleChangeTypeView &&
                          <DefaultButton
                            disabled={ false }
                            onClick={ toggleChangeTypeView }
                            className={ styles.editMeta }
                            icon={ "arrows-shuffle" }/>
                      }
                  </div>
              </div>
          }
          <div className={ cx(styles.content, get(classStyles, 'content', false)) }>
              {
                  (isCreateMode || isChangeTypeView) &&
                  <div className={ cx(styles.noFieldsContainer, get(classStyles, 'noFieldsContainer', false)) }>
                      <InputSelect
                        label={ selectLabel || "Выберите тип проекта" }
                        placeholder={ get(currentType, `name_${ lang }`, 'Выберите тип') }
                        items={ metaTypes }
                        itemName={ `name_${ lang }` }
                        keyName={ 'alias' }
                        searchFields={ [ `name_${ lang }` ] }
                        onChange={ onChangeMetaType }
                      />
                  </div>
              }
              {
                  !isChangeTypeView && isFieldsExist && renderFields(fields)
              }

          </div>
      </div>
  })
