import React from 'react'
import { get } from 'lodash'


/**
 * @helper Компонент для загрузки изображений.
 *
 * Позволяет добавить любому элементу (тегу) в шаблоне
 * возможность при клике выбирать файлы с диска
 *
 * Example:
 * ```
 * import { LoadMedia } from 'components/load-media-wrapper'
 *
 * ...
 * <LoadMedia  onImageSelect>
 *   <button>выберите файл...</button>
 * </LoadMedia>
 *
 * ```
 *
 * todo
 * - сделать возможность выбирать контент
 * - сделать поддержку мульти-выбора
 */
interface LoadFileProps {
    children: any
    onFilesSelect: (file: Array<File>) => void

    [key: string]: any

    accept?: string
}

const FileSelector: React.FC<LoadFileProps> = (props) => {

    const buildFileSelector = () => {
        const fileSelector = document.createElement('input')
        fileSelector.setAttribute('type', 'file')
        fileSelector.setAttribute('multiple', 'multiple')
        fileSelector.setAttribute('accept', props.accept || 'image/* , video/*')
        return fileSelector
    }
    const fileSelector = buildFileSelector()

    fileSelector.onchange = (e) => {
        const files = new Array(...get(e.target, 'files'))
        props.onFilesSelect(files)
    }

    const fileSelect = (e) => {
        e.preventDefault()
        fileSelector.click()
    }

    return <div onClick={ fileSelect }>
        { props.children }
    </div>
}

export {
    FileSelector
}
