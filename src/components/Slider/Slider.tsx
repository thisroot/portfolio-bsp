import React, { useState, useEffect, useCallback } from 'react';
import cx from 'classnames';

import styles from './styles.scss';

interface IItemList {
  src: string,
  label: string,
  text: string,
  backgroundColor: string,
  especialBg: string,
}

interface ISlider {
  delay: number,
  itemList: IItemList[],
}

const Slider: React.FC<ISlider> = ({
  delay,
  itemList
}) => {
  const [count, setCount] = useState(0);

  let animate = setTimeout(function tick(): void {
    setCount((count + 1) % itemList.length);
    
    animate = setTimeout(tick, delay);
  }, delay);

  const clearAnimateTimer = useCallback(() => {
    clearTimeout(animate);
  }, [animate]);

  const changeSlide = (index) => {
    clearAnimateTimer();
    setCount(index);
  };

  useEffect(() => {
    return clearAnimateTimer;
  }, [clearAnimateTimer]);
  
  return (
    <div className={styles.slider}>
      <div className={styles.content}>
        {
          itemList.map((item, index) => (
            <div 
              key={index}
              className={cx(styles.slide, {[styles.active]: index === count})}
              style={{background: item.backgroundColor}}
            >
              <div  
                className={styles.especialBg}
                style={{backgroundImage: `url(${item.especialBg})`}}
              >
              </div>
              <div className={styles.imgWrapper}>
                <img 
                  src={ item.src }
                  alt={ item.label }
                />
              </div>
              <h1 className={styles.label}>
                {item.label}
              </h1>
              <span className={styles.text}>
                {item.text}
              </span>
            </div>
          ))
        }
      </div>
      <ul className={styles.dots}>
        {
          itemList.map((item, index) => (
            <li 
              key={ index }
              className={ cx(styles.dot, {[styles.active]: count === index}) }
              onClick = {() => changeSlide(index)}
              data-item={`${item}`}
            >
            </li>
          ))
        }
      </ul>
    </div>
  )
}

export {
  Slider
}