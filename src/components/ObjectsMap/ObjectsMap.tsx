import React, { CSSProperties, useState } from "react"
import { GMap } from "components/GMap"
import { Icon } from "components/UI/Icon"
import { ObjectModel } from "models/mst/ObjectModel"
import styles from './styles.scss'
import { get } from 'lodash'
import { TYPE_MAPS } from "constants/geocoding"
import Truncate from 'react-truncate'
import { useInstance } from "react-ioc"
import { AppService, Link, LocalisationService } from "../../services"
import cx from 'classnames'
import { computeStages, formatString } from "../../utils/helpers"
import mapPinSuccess from 'static/images/map-pin-success.svg'
import mapPinDanger from 'static/images/map-pin-danger.svg'
import { ROUTES } from "../../constants"
import { FormatDateToQuarter } from "../Widgets/FormatDateToQuarter/FormatDateToQuarter"
import { observer } from "mobx-react-lite"
import { Point } from "constants/geocoding"

export interface IProjectsMapProps {
    type?: TYPE_MAPS
    entities?: Array<ObjectModel>
    entityGeoKey?: string,
    mapPositionKey?: string // key for save current user map preferences
    className?: string
    style?: CSSProperties
    showEntity?: boolean
    onMarkerClick?: (entity: ObjectModel) => any
    onMapClick?: (e: any) => void
    center?: Point
    zoom?: number
    isEntitiesMappingNeeded?: boolean
    isCustomIconEnable?: boolean
    fullscreenControl?: boolean
}

export const ObjectsMap: React.FC<IProjectsMapProps> = observer(({
    onMapClick, onMarkerClick, showEntity = true, style, className, mapPositionKey = '', type = TYPE_MAPS.GOOGLE, entityGeoKey = 'geo_coord', entities,
    center, zoom, isEntitiesMappingNeeded = true, isCustomIconEnable = true, fullscreenControl = true
}) => {

    const [ isHidden, toggleHidden ] = useState(true)
    const [ currentObject, setCurrentObject ] = useState()
    const app = useInstance(AppService)
    const lang = useInstance(LocalisationService)

    const points = isEntitiesMappingNeeded ? [] : entities

    isEntitiesMappingNeeded && entities.forEach((entity) => {
        if (entity[entityGeoKey]) {
            const percent = get(entity, 'volume_percent')
            const date_finish = get(entity, 'date_finish')
            points.push({
                ...entity,
                lng: entity[entityGeoKey].long,
                lat: entity[entityGeoKey].lat,
                label: `${ percent ? Math.round(+percent) : 0 }%`,
                markerUrl: computeStages(+percent) <= 2 ? mapPinDanger : mapPinSuccess,
                percent,
                date_finish
            })
        }
    })

    const renderMap = (type) => {
        switch (type) {
            case TYPE_MAPS.GOOGLE:
                return <GMap
                  onMapClick={ onMapClick }
                  isDarkTheme={ app.isDarkTheme }
                  entities={ points }
                  onMarkerClick={ (entity) => {
                      onMarkerClick && onMarkerClick(entity)
                      if (showEntity) {
                          toggleHidden(false)
                          setCurrentObject(entity)
                      }
                  } }
                  onCloseMarkerInfo={ () => {
                      if (showEntity) {
                          toggleHidden(true)
                          setCurrentObject(null)
                      }
                  } }
                  mapPositionKey={ mapPositionKey }
                  center={ center }
                  zoom={ zoom }
                  isCustomIconEnable={ isCustomIconEnable }
                  fullscreenControl={ fullscreenControl }
                />
            case TYPE_MAPS.MAPBOX:
            case TYPE_MAPS.LEAFLET:
            default:
                return null
        }
    }


    const infoWindowRenderer = (entity, isHidden, lang = 'ru') => {
        // const slots = mainMataInfoProps(entity, 'object')
        const image = get(entity, 'main_image.converted.medium.url')

        return <>
            <div className={ cx(styles.nav, { [styles.shorten]: isHidden }) } style={ style }>
                { !isHidden &&
                <>
                    <div className={ styles.contentContainer }>
                        <FormatDateToQuarter className={ styles.quarter } date={ entity.date_finish }
                                             status={ entity.status }/>
                    </div>
                    <Link className={ styles.link }
                          to={ formatString(ROUTES.OBJECT_PAGE.PATH, entity.project_id.project_id, entity.object_id) }>
                        <h5 title={ entity[`name_${ lang }`] } className={ styles.rightMenuTitle }>
                            <Truncate
                              lines={ 3 }> { entity.project_id[`name_${ lang }`] + ' / ' + entity[`name_${ lang }`] }</Truncate>
                        </h5>
                    </Link>
                </> }
                <div className={ styles.imageWrapper }>
                    { image && <div
                      className={ styles.image }
                      style={ { backgroundImage: `url(${ image })` } }/>
                    }
                    {
                        !image && <div className={ styles.iconWrapper }><Icon name={ 'account-home-door' }/></div>
                    }
                </div>
            </div>
        </>
    }


    return <div className={ cx(styles.mapContainer, className) } style={ style }>
        { renderMap(type) }
        { showEntity && currentObject && infoWindowRenderer(currentObject, isHidden, lang.lang) }
    </div>
})
