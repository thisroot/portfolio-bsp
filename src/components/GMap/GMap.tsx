import React, { useRef, useState, useCallback } from "react"
import { GoogleMap, InfoWindow, Marker, MarkerClusterer, useLoadScript } from "@react-google-maps/api"
import { gDarkStyles, gstyles } from "./gmapStyles"
import styles from "./styles.scss"
import cx from 'classnames'
import { IGMapProps } from "./index"
import { useUnmount } from "react-use"
import { useInstance } from "react-ioc"
import { StorageService } from "../../services/StorageService"
import { get } from "lodash"
import { DEFAULT_POSITION, DEFAULT_ZOOM } from "constants/geocoding"
import { appConfig } from "../../constants"
import mapPin from 'static/images/map-pin-success.svg'
import { Preloader } from "../UI/Preloader"
import { observer } from "mobx-react-lite"

const GMap: React.FC<IGMapProps> = observer((
  {
      className,
      center,
      zoom,
      mapTypeControl = false,
      entities,
      infoWindowRenderer,
      onMarkerClick,
      onCloseMarkerInfo,
      mapPositionKey,
      onBoundsChanged,
      isDarkTheme = false,
      onMapClick,
      isCustomIconEnable = true,
      fullscreenControl = true
  }) => {

    const storageService = useInstance(StorageService)

    const map = useRef<any>()
    const [ isMarkerInfoOpen, toggleMarkerInfo ] = useState(false)
    const [ currentMarker, setCurrentMarker ] = useState()
    const [ z ] = useState(zoom || (mapPositionKey && get(JSON.parse(storageService.localDB.getItem(`map-position-${ mapPositionKey }`)), 'zoom', DEFAULT_ZOOM)) || DEFAULT_ZOOM)
    const [ c ] = useState(center || (mapPositionKey && get(JSON.parse(storageService.localDB.getItem(`map-position-${ mapPositionKey }`)), 'center', DEFAULT_POSITION)) || DEFAULT_POSITION)

    useUnmount(() => {
        if (mapPositionKey) {
            storageService.localDB.setItem(`map-position-${ mapPositionKey }`, JSON.stringify({
                zoom: map.current.state.map.zoom,
                center: { lat: map.current.state.map.center.lat(), lng: map.current.state.map.center.lng() }
            }))
        }
    })

    const onMarkerClickLocal = (marker: any) => {
        infoWindowRenderer && setCurrentMarker(marker)
        infoWindowRenderer && toggleMarkerInfo(true)
        onMarkerClick && onMarkerClick(marker)
    }

    const { isLoaded, loadError } = useLoadScript({ id: "script-loader", googleMapsApiKey: appConfig.GOOGLE_MAPS })

    const onLoad = useCallback((mapInstance) => {
        if (!Array.isArray(entities) || entities.length <= 1) return

        const maps = get(window, 'google.maps')
        const bounds = maps && new maps.LatLngBounds()

        for (const entity of entities) {
            bounds.extend(new maps.LatLng(entity.lat, entity.lng))
        }
        mapInstance.fitBounds(bounds)
    }, [ entities ])

    const renderMap = () => {
        const point = get(window, 'google.maps.Point')
        // @ts-ignore
        const labelPosition = new point(74, 13)
        return <GoogleMap
          key={ `${ isDarkTheme }` }
          onLoad={ onLoad }
          onClick={ (e) => {
              infoWindowRenderer && toggleMarkerInfo(false)
              onCloseMarkerInfo && onCloseMarkerInfo()
              onMapClick && onMapClick(e)
          } }
          ref={ map }
          mapContainerClassName={ cx(styles.mapsContainer, className) }
          id="example-map"
          zoom={ z }
          center={ c }
          options={ { styles: isDarkTheme ? gDarkStyles : gstyles, mapTypeControl, fullscreenControl } }
          onBoundsChanged={ () => {
              onBoundsChanged && onBoundsChanged(get(map, 'current.state.map.zoom'), {
                  lat: get(map, 'current.state.map.center.lat()'),
                  lng: get(map, 'current.state.map.center.lng()')
              })
          } }
        >
            <MarkerClusterer
              options={ {
                  imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
                  maxZoom: 10,
              } }
            >
                {
                    (clusterer) => entities.map((location, i) => {
                        return (
                          <div key={ i }><Marker
                            key={ `marker-${ i }` }
                            position={ location }
                            clusterer={ clusterer }
                            onClick={ () => onMarkerClickLocal(location) }
                            title={ get(location, 'name') }
                            label={ { text: get(location, 'label'), color: 'white' } }
                            icon={ isCustomIconEnable ? { url: get(location, 'markerUrl', mapPin), labelOrigin: labelPosition } : null }
                          />
                          </div>
                        )
                    })
                }
            </MarkerClusterer>
            { infoWindowRenderer && isMarkerInfoOpen && <InfoWindow position={ currentMarker } onCloseClick={ () => {
                toggleMarkerInfo(false)
                onCloseMarkerInfo && onCloseMarkerInfo()
            } }>
                { infoWindowRenderer(currentMarker) }
            </InfoWindow> }
        </GoogleMap>
    }
    if (loadError) {
        return <div>Map cannot be loaded right now, sorry.</div>
    }

    return isLoaded ? renderMap() : <Preloader/>

})


export default GMap