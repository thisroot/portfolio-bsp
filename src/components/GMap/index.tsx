import React from "react"
import Loadable, { LoadableComponent } from '@loadable/component'
import { Preloader } from "components/UI/Preloader"
import { Point } from "../../constants/geocoding"

export interface IGMapProps {
    className?: string
    center?: Point
    zoom?: number
    mapTypeControl?: boolean
    entities?: Array<any>
    infoWindowRenderer?: (entity: any ) => any
    onMarkerClick?: (entity) => void,
    onCloseMarkerInfo?: () => void,
    mapPositionKey?: string,
    onBoundsChanged?: (zoom: number, center: Point) => void,
    isDarkTheme?: boolean
    onMapClick?: (e: any) => void
    isCustomIconEnable?: boolean
    fullscreenControl?: boolean
}

const GMap: LoadableComponent<IGMapProps> = Loadable(() => import(/* client-side */'./GMap'), {
    fallback: <Preloader /> } )

export { GMap }