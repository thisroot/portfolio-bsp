import React from 'react'
import { ACTION_TYPE, Badge } from "components/UI/Badge/Badge"
import { Trans, t } from "@lingui/macro"
import { loc } from "utils/i18n"
import styles from './styles.scss'

export const GunntLegend = () => (
  <div className={ styles.legendContainer }>
    <div className={ styles.title }><Trans>basic.legend</Trans></div>
    <div className={ styles.description }><Trans>component.GunntLegend.description</Trans></div>
    <div className={ styles.line } />
    <Badge type={ ACTION_TYPE.SUCCESS } text={ loc._(t`component.GunntLegend.green.badge`) } className={ styles.badges } />
    <div className={ styles.list }>
      <ul>
        <li><Trans>component.GunntLegend.green.text.first</Trans></li>
        <li><Trans>component.GunntLegend.green.text.second</Trans></li>
        <li><Trans>component.GunntLegend.green.text.third</Trans></li>
      </ul>
    </div>
    <div className={ styles.line } />
    <Badge type={ ACTION_TYPE.WARNING } text={ loc._(t`component.GunntLegend.yellow.badge`) } className={ styles.badges } />
    <div className={ styles.text }><Trans>component.GunntLegend.yellow.text</Trans></div>
    <div className={ styles.line } />
    <Badge type={ ACTION_TYPE.DANGER } text={ loc._(t`component.GunntLegend.red.badge`) } className={ styles.badges } />
      <ul>
        <li><Trans>component.GunntLegend.red.text.first</Trans></li>
        <li><Trans>component.GunntLegend.red.text.second</Trans></li>
      </ul>
    <div className={ styles.line } />
    <Badge type={ ACTION_TYPE.DEFAULT } text={ loc._(t`component.GunntLegend.gray.badge`) } className={ styles.badges } />
    <div className={ styles.text }><Trans>component.GunntLegend.gray.text</Trans></div>
  </div>
)