import { BasicFilterViewModel } from "./BasicFilterViewModel"
import { action, observable, toJS } from "mobx"
import { IFilter, ISort } from "../Interfaces"

export class BasicFilterWithSortViewModel extends BasicFilterViewModel implements IFilter, ISort {

    @action
    public resetForm = async (e) => {
        this.selectedSort = null
        this.filterForm.onReset(e)
    }

    public sortLabels = []
    @observable selectedSort = null

    @action
    public selectSort = async (item) => {
        if (!this.selectedSort || (this.selectedSort && this.selectedSort.key !== item.key)) {
            this.selectedSort = item
        } else if (this.selectedSort.value === 1) {
            this.selectedSort = { ...toJS(item), value: -1 }
        } else {
            this.selectedSort = null
        }
    }
}
