import { IFilter } from "../Interfaces"
import { action, computed, observable } from "mobx"
import vjf from "mobx-react-form/lib/validators/VJF"
import MobxReactForm from 'mobx-react-form'
import { get, isArray, isEmpty, isNumber, isPlainObject, isString } from "lodash"

const plugins = {
    vjf: vjf()
}

export class BasicFilterViewModel implements IFilter {
    @observable
    public isPopoverOpen = false
    @observable
    public filterForm: any
    @observable
    public formInitialized = false
    @observable
    public pending = false

    @action
    public togglePopover = async () => {
        this.isPopoverOpen = !this.isPopoverOpen
        await this.initFilterForm()
    }
    @action
    public resetForm = async (e) => {
        this.filterForm.onReset(e)
    }
    @action
    public submitForm = async (e) => {
        this.filterForm.onSubmit(e)
    }

    @computed
    public get fields() {
        return {}
    }

    @computed get formValues() {
        const filters = {}
        if (this.filterForm) {
            Object.keys(this.filterForm.get()).forEach(item => {
                return filters[item] = !isEmpty(`${ get(this.filterForm.get()[item], 'value') }`) ?
                  isNaN(+get(this.filterForm.get()[item], 'value')) ?
                    get(this.filterForm.get()[item], 'value') :
                    +get(this.filterForm.get()[item], 'value') : null
            })
        }
        return filters
    }

    @computed get countFilledFields() {
        return this.filterForm ? Object.values(get(this.filterForm, 'validatedValues', {})).filter(val => (!isPlainObject(val) && (((isArray(val) || isString(val)) && val.length > 0) || isNumber(val)))).length : 0
    }

    @computed
    public get filterFields() {
        return this.filterForm ? this.filterForm.fields : null
    }

    @action
    public initFilterForm = async (force?: boolean) => {
        this.pending = true
        if (!this.formInitialized || force) {
            this.filterForm = !isEmpty(this.fields) ? new MobxReactForm({ fields: this.fields }, { plugins }) : null
        }

        this.formInitialized = true
        this.pending = false
    }
    @action
    public addSelected = (item, field) => {
        const selected = this.filterForm.get()[field].value
        this.filterFields.get(this.fields[field].name).value = [ ...selected, item ]
    }

    @action
    public delSelected = (index, field) => {
        const selected = this.filterForm.get()[field].value
        selected.splice(index, 1)
        this.filterFields.get(this.fields[field].name).value = isEmpty(selected) ? "" : selected
    }
    @action
    public resetSelected = (field) => {
        this.filterFields.get(this.fields[field].name).value = null
    }
}
