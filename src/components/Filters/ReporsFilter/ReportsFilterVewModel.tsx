import { action, computed } from 'mobx'
import { BasicFilterViewModel } from '../BasicFilter'
import styles from "./styles.scss"
import React from "react"
import { FILTER_FIELD_COMPONENT_TYPE, FilterFields } from "../../../models/mst/AnalyticsTemplate"
import { get, isObject } from "lodash"
import { DatePicker, Dropdown, PICKER_FORM } from "../../UI"
import dayjs from "dayjs"
import { SERVER_DATE_MASK } from "../../../constants"
import { inject } from "react-ioc"
import { RequestService } from "../../../services"


export function parseFieldFillCommand(command?: string) {
    switch (true) {
        case command === "" || command === undefined || null:
            return ""
        case !!command.match('DATE'):
            return dayjs().format(SERVER_DATE_MASK)
        default:
            return ""
    }
}


export interface FilterInitializedFields {
    [key: string]: {
        component_type: FILTER_FIELD_COMPONENT_TYPE
        name: string
        placeholder: string
        label: string
        value: any
        default: any
        extra?: Array<any> | {
            url: string,
            path: string,
            method: string,
            key: string,
            value: string
        }
        items?: Array<any>

    }
}

class ReportsFilterVewModel extends BasicFilterViewModel {

    public customFields: FilterInitializedFields = {}
    @inject rest: RequestService

    @action setFields = async (fields: FilterFields = {}) => {
        this.pending = true
        this.customFields = {}
        const entries = Object.entries(fields)
        for (const f of entries) {
            if (isObject(f[1])) {
                if (f[1].extra) {
                    if (get(f[1].extra, 'url')) {
                        const result = await this.rest.request(get(f[1].extra, 'url'))
                        if (result.status !== 'error') {
                            const items = get(result.body, get(f[1].extra, 'path')) && Object.values(get(result.body, get(f[1].extra, 'path'))) || []
                            Object.assign(this.customFields, {
                                [f[0]]: {
                                    ...f[1],
                                    items,
                                    default: parseFieldFillCommand(f[1].default),
                                    value: items[0][get(f[1].extra, 'key')]
                                }
                            })
                        }
                    }
                } else {
                    Object.assign(this.customFields, {
                        [f[0]]: {
                            ...f[1],
                            default: parseFieldFillCommand(f[1].default),
                            value: parseFieldFillCommand(f[1].value)
                        }
                    })
                }
            }
        }
        this.pending = false
    }

    @computed
    public get fields() {
        return this.customFields
    }

    @computed
    public get renderFields() {
        return Object.values(this.fields).map((f, i) => {
            switch (f.component_type) {
                case FILTER_FIELD_COMPONENT_TYPE.DATE_PICKER:
                    return <div key={ i } className={ styles.col }>
                        <div className={ styles.row }>
                            <span className={ styles.title }>{ this.fields[f.name].label }</span></div>
                        <div className={ styles.row }>
                            <DatePicker
                              form={ PICKER_FORM.inline }
                              addedClasses={ styles.dayPicker }
                              value={ this.filterFields.get(this.fields[f.name].name).value }
                              onDayChange={ e => {
                                  this.filterFields.get(this.fields[f.name].name).value = dayjs(e).format(
                                    SERVER_DATE_MASK,
                                  )
                              } }
                            />
                        </div>
                    </div>
                case FILTER_FIELD_COMPONENT_TYPE.DROPDOWN:
                    return <div key={ i } className={ styles.col }>
                        <div className={ styles.row }>
                            <span className={ styles.title }>{ this.fields[f.name].label }</span>
                        </div>
                        <div className={ styles.row }>
                            <Dropdown
                              items={ this.fields[f.name].items }
                              searchFields={ [ get(this.fields[f.name].extra, 'value') ] }
                              keyName={ get(this.fields[f.name].extra, 'key') }
                              itemName={ get(this.fields[f.name].extra, 'value') }
                              initialItem={ this.filterFields.get(this.fields[f.name].name).value }
                              onChange={ (e) => this.filterFields.get(this.fields[f.name].name).value = get(e, get(this.fields[f.name].extra, 'key'), null) }
                            /></div>
                    </div>
            }
        })
    }
}

export { ReportsFilterVewModel }
