import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { useInstance } from 'react-ioc'
import { Button, PrimaryButton, Spinner } from 'components/UI'
// import { REPORT_FACT_TYPE } from 'constants/locale';
import { t } from '@lingui/macro'
import { loc } from 'utils/i18n'
import styles from './styles.scss'
import { ReportsFilterVewModel } from "./ReportsFilterVewModel"

interface IGuntFilterProps {
    onReset: (any) => any;
    onSubmit: (any) => any;
}

const ReportsFilter: FC<IGuntFilterProps> = observer(({ onSubmit, onReset }) => {
    const view = useInstance(ReportsFilterVewModel)

    return (
      <div className={ styles.componentContainer }>
          { view.pending && <Spinner/> }
          { !view.pending && (
            <>
                <div className={ styles.filterContainer }>
                    <div className={ styles.filter }>
                        { view.renderFields }
                    </div>
                </div>
                <div className={ styles.footer }>
                    <Button
                      outline
                      withoutBorders
                      onClick={ onReset }
                      className={ styles.resetBtn }
                      text={ loc._(t`basic.reset`) }
                    />
                    <PrimaryButton onClick={ onSubmit } text={ `${ loc._(t`basic.applyFilter`) }` }/>
                </div>
            </>
          ) }
      </div>
    )
})

export { ReportsFilter }
