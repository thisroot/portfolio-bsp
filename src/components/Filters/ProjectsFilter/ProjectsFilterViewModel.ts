import { inject } from "react-ioc"
import { action, computed, observable, toJS } from "mobx"

import { ApiService, DataContext, DictionaryService, LocalisationService } from "../../../services"
import MobxReactForm from 'mobx-react-form'
import vjf from "mobx-react-form/lib/validators/VJF"
import { FIELDS } from "../../../constants/forms"
import { flatNestedFormFields, parseFilterAttributes, parseFilterFromTree, updateFormValues } from "../../../utils"
import { get } from "lodash"
import { loc } from "../../../utils/i18n"
import { t } from "@lingui/macro"
import { BasicFilterWithSortViewModel } from "../BasicFilter"
import { ISavedFilter } from "../Interfaces"

const plugins = {
    vjf: vjf()
}

class ProjectsFilterViewModel extends BasicFilterWithSortViewModel implements ISavedFilter {

    @inject dictionaryService: DictionaryService
    @inject dataContext: DataContext
    @inject lang: LocalisationService
    @inject api: ApiService

    @observable
    public filterName: string


    @action
    public registerFilter = (filterName: string) => {
        this.filterName = filterName
    }


    @action
    public resetForm = async (e) => {
        this.selectedSort = null
        this.filterForm.onReset(e)
        this.filterNeedSave && await this.saveFilter({ filter: null, sort: null })
    }

    @action
    public submitForm = async (e) => {
        this.filterForm.onSubmit(e)
        this.filterNeedSave && await this.saveFilter()
    }

    @computed get isFilterDefault() {
        return get(this.filterForm, 'isDefault', true)
    }


    @computed get regions() {
        return [ ...this.dataContext.reference.region.values() ].map(reg => ({ value: reg.name, key: reg.region_id }))
    }

    @computed get fields() {
        return {
            region_id: {
                ...FIELDS.region_id,
                extra: this.regions
            },
            status: FIELDS.status,
            deviation_day: FIELDS.deviation_day,
            volume_percent: FIELDS.volume_percent,
            [`passport_${ this.lang.lang }`]: {
                ...FIELDS.passport_fields,
                name: `passport_${ this.lang.lang }`
            },
        }
    }

    public formTree
    public sortLabels = [
        {
            key: null,
            title: loc._(t`components.filter.sortByName`),
            children: [ { key: null, title: loc._(t`basic.sort.AZ`), value: 1 } ]
        }
    ]
    @action
    public initFilterForm = async () => {
        if (!this.filterName) return
        if (!this.formInitialized) {
            await this.dictionaryService.init()

            const filterFlagRes = await this.api.user.getData({
                path: [
                    `filters.${ this.filterName }`
                ]
            })

            if (filterFlagRes.status === 200) {
                this.filterNeedSave = get(filterFlagRes, `body.data.filters[${ this.filterName }]`, false)
            }

            const result = await this.api.project.getFilter()

            if (result.status === 200) {
                const filterTypes = result.body[`passport_${ this.lang.lang }`]
                this.formTree = parseFilterFromTree(filterTypes, 'project')
                this.sortLabels.push(...this.formTree.map((item) => {
                    return {
                        ...item,
                        children: item.children.map(ch => {
                            const { key, title } = ch
                            return { key, title, value: 1 }
                        })
                    }
                }))
            }

            this.filterForm = new MobxReactForm({ fields: this.fields }, {
                plugins
            })
            if (this.filterNeedSave) {
                this.filterNeedSave.filter && updateFormValues(this.filterNeedSave.filter, this.filterForm)
                if (this.filterNeedSave.sort) {
                    this.selectedSort = this.filterNeedSave.sort
                }
            }
            this.formInitialized = true
            this.pending = false
        }
    }

    @action selectSort = async (item) => {
        if (!this.selectedSort || (this.selectedSort && this.selectedSort.key !== item.key)) {
            this.selectedSort = item
        } else if (this.selectedSort.value === 1) {
            this.selectedSort = { ...toJS(item), value: -1 }
        } else {
            this.selectedSort = null
        }
        await this.saveFilter()
    }


    @computed get formValues() {
        if (!this.filterForm) return {}
        const flatFields = flatNestedFormFields(this.filterForm.get())
        if (flatFields[`passport_${ this.lang.lang }`]) {
            flatFields[`passport_${ this.lang.lang }`] = parseFilterAttributes(
              flatFields[`passport_${ this.lang.lang }`], 'project')
        }
        return flatFields
    }

    @observable
    public filterNeedSave

    @action
    public toggleSaveFilter = async (e) => {
        this.filterNeedSave = e.target.checked
        if (e.target.checked) {
            await this.saveFilter()
        } else {
            await this.saveFilter(null)
        }
    }

    @action
    public saveFilter = async (form = {
        filter: this.filterForm.values(), sort: this.selectedSort
    }) => {
        await this.api.user.setData({
            path: 'filters',
            data: {
                [this.filterName]: form
            }
        })
    }
}

export {
    ProjectsFilterViewModel
}
