import React from 'react'
import styles from './styles.scss'
import { observer } from "mobx-react-lite"
import { useInstance } from 'react-ioc'
import { ProjectsFilterViewModel } from "./ProjectsFilterViewModel"
import { Spinner } from "../../UI/Spinner"
import { loc } from "../../../utils/i18n"
import { Button, BUTTON_TYPE, PrimaryButton } from "../../UI/Button"
import { t } from "@lingui/macro"
import { Checkbox, Input, Range } from 'components/UI'
import Tree from 'rc-tree'
import 'components/rs-tree/index.css'
import cx from 'classnames'
import { Icon } from "../../UI/Icon"
import { get } from 'lodash'
import { LocalisationService } from "../../../services/LocalisationService"

export enum STATUS {
    RED = 1,
    YELLOW = 2,
    GREEN = 3,
    GRAY = 4
}

interface IProjectsFilterProps {
    onReset: (any) => any
    onSubmit: (any) => any
}

const ProjectsFilter: React.FC<IProjectsFilterProps> = observer((props) => {

    const formView = useInstance(ProjectsFilterViewModel)
    const lang = useInstance(LocalisationService)

    const setStatus = (status: STATUS) => {
        formView.filterFields
          .get(formView.fields.status.name).value = isStatus(status) ? null : status
    }
    const isStatus = (status: STATUS) => formView.filterFields
      .get(formView.fields.status.name).value === status

    const getStatus = (field) => field.hasError && !field.isDefault ? 'error' : ''

    let devDaysFromField, devDaysToField, rangePercent, bindedRange, projectTypesField

    if (!formView.pending) {
        // regionField = formView.filterFields.get(formView.fields.region_id.name)
        devDaysFromField = formView.filterForm.$(`${ formView.fields.deviation_day.name }.${ formView.fields.deviation_day.fields[0].name }`)
        devDaysToField = formView.filterForm.$(`${ formView.fields.deviation_day.name }.${ formView.fields.deviation_day.fields[1].name }`)
        rangePercent = formView.filterForm.$(`${ formView.fields.volume_percent.name }`)
        projectTypesField = formView.filterFields.get(formView.fields[`passport_${ lang.lang }`].name)
        devDaysFromField.validate()
        devDaysToField.validate()
        rangePercent.validate()
        bindedRange = rangePercent.bind()
    }
    return <div className={ styles.componentContainer }>
        {
            formView.pending && <Spinner/>
        }
        { !formView.pending && <>
            <div className={ styles.filterContainer }>
                <div className={ styles.filter }>
                    {/*<h5>{ loc._(formView.filterFields.get(formView.fields.region_id.name).label) }</h5>*/ }

                    {/*<Dropdown items={ regionField.extra }*/ }
                    {/*          searchFields={ [ 'value' ] }*/ }
                    {/*          keyName={ 'key' }*/ }
                    {/*          itemName='value'*/ }
                    {/*          initialItem={ regionField.value }*/ }
                    {/*          onChange={ (e) => regionField.value = get(e, 'key', null)*/ }
                    {/*          }/>*/ }
                    {/*<div className={ styles.separator }/>*/ }
                    <h5>{ loc._(formView.filterFields.get(formView.fields.status.name).label) }</h5>
                    <div className={ styles.statusContainer }>
                        <Button buttonType={ isStatus(STATUS.RED) && BUTTON_TYPE.DANGER }
                                outline={ !isStatus(STATUS.RED) }
                                onClick={ () => setStatus(STATUS.RED) }
                                className={ styles.statusBtn }
                                text={ <><span
                                  className={ cx(styles.circle, { [styles.danger]: !isStatus(STATUS.RED) }) }/>
                                    <span> { loc._(t`basic.colors.red`) }</span></> }/>
                        <Button buttonType={ isStatus(STATUS.YELLOW) && BUTTON_TYPE.WARNING }
                                outline={ !isStatus(STATUS.YELLOW) }
                                onClick={ () => setStatus(STATUS.YELLOW) }
                                className={ styles.statusBtn }
                                text={ <><span
                                  className={ cx(styles.circle, { [styles.yellow]: !isStatus(STATUS.YELLOW) }) }/>
                                    <span> { loc._(t`basic.colors.yellow`) }</span></> }/>
                        <Button buttonType={ isStatus(STATUS.GREEN) && BUTTON_TYPE.SUCCESS }
                                outline={ !isStatus(STATUS.GREEN) }
                                onClick={ () => setStatus(STATUS.GREEN) }
                                className={ styles.statusBtn }
                                text={ <><span
                                  className={ cx(styles.circle, { [styles.green]: !isStatus(STATUS.GREEN) }) }/>
                                    <span> { loc._(t`basic.colors.green`) }</span></> }/>
                        <Button outline={ !isStatus(STATUS.GRAY) }
                                onClick={ () => setStatus(STATUS.GRAY) }
                                className={ styles.statusBtn }
                                text={ <><span
                                  className={ cx(styles.circle, { [styles.gray]: !isStatus(STATUS.GRAY) }) }/>
                                    <span> { loc._(t`basic.colors.gray`) }</span></> }/>
                    </div>
                    <div className={ styles.separator }/>
                    <h5>{ loc._(formView.filterFields.get(formView.fields.deviation_day.name).label) }</h5>
                    <div className={ styles.deviationDaysContainer }>
                        <div className={ styles.col }>
                            {
                                loc._(t`basic.from`)
                            }
                        </div>
                        <div className={ styles.col }>
                            <Input name={ formView.fields.deviation_day.fields[0].name } disablePlaceholderOnTop
                                   { ...devDaysFromField.bind() }
                                   status={ getStatus(devDaysFromField) }
                                   clearForm={ () => devDaysFromField.onChange("") }
                                   placeholder={ '0' }
                            />
                        </div>
                        <div className={ styles.col }>
                            {
                                loc._(t`basic.to`)
                            }
                        </div>
                        <div className={ styles.col }>
                            <Input name={ formView.fields.deviation_day.fields[1].name } disablePlaceholderOnTop
                                   { ...devDaysToField.bind() }
                                   status={ getStatus(devDaysToField) }
                                   clearForm={ () => devDaysToField.onChange("") }
                                   placeholder={ '365' }
                            />
                        </div>
                    </div>
                    <div className={ styles.separator }/>
                    <div className={ styles.labelContainer }>
                        <h5>{ loc._(formView.filterFields.get(formView.fields.volume_percent.name).label) }</h5>
                        <div className={ styles.rangeValues }>
                            { `${ loc._(t`basic.selected`) } ${ loc._(t`basic.from`).toLowerCase() } ${ Object.values(bindedRange.value)[0] || 0 }% ${ loc._(t`basic.to`).toLowerCase() } ${ Object.values(bindedRange.value)[1] || 100 }%` }
                        </div>
                    </div>
                    <div className={ styles.rangeContainer }>
                        <Range { ...bindedRange }
                               onChange={ (value) => rangePercent.set(value) }
                               measureUnit={ '%' }/>
                    </div>
                    <div className={ styles.separator }/>
                    <h5>{ loc._(formView.filterFields.get(formView.fields[`passport_${ lang.lang }`].name).label) }&nbsp;
                        { formView.formTree && formView.formTree.length === 0 && loc._(t`components.filter.empty`) }</h5>
                    <div className={ styles.projectTypeContainer }>
                        { formView.formTree && <Tree
                          selectable={ false }
                          checkable
                          className={ styles.tree }
                          checkedKeys={ projectTypesField.values() }
                          treeData={ formView.formTree }
                          onCheck={ (e) => projectTypesField.set(e) }
                          height={ 200 }
                          icon={ (e) => <Icon
                            className={ styles.treeIcon }
                            name={ get(e, 'data.children.length', 0) > 0 ?
                              'files-folder-blank' :
                              'files-doc-blank' }/>
                          }
                        />
                        }
                    </div>
                </div>
                <div className={ styles.sorter }>
                    <h5>{ loc._(t`basic.sort`) }</h5>
                    <div className={ styles.sortingContainer }>
                        {
                            formView.sortLabels && formView.sortLabels.length && formView.sortLabels.map(type => {
                                return <div key={ type.key } className={ styles.typeContainer }>
                                    <span className={ styles.sortTitle }>
                                        { type.title }
                                    </span>
                                    <div>
                                        {
                                            type.children.map(item => {
                                                const isActive = item.key === get(formView.selectedSort, 'key')
                                                const isReversed = get(formView.selectedSort, 'value') === -1
                                                return <Button
                                                  className={ styles.sortBtn }
                                                  active={ isActive }
                                                  iconStyle={ cx({
                                                      [styles.selectedIcon]: isActive,
                                                      [styles.rotateIcon]: isActive && isReversed
                                                  }) }
                                                  key={ item.key }
                                                  onClick={ () => formView.selectSort(item) }
                                                  icon={ 'arrows-duet' }
                                                  text={ item.title }
                                                  outline
                                                  withoutBorders
                                                />
                                            })
                                        }
                                    </div>
                                </div>
                            })
                        }
                    </div>
                </div>
            </div>
            <div className={ styles.footer }>
                <div className={ styles.left }>
                    <Checkbox onChange={ formView.toggleSaveFilter } checked={ formView.filterNeedSave }
                              label={ loc._(t`components.filter.saveFilterParams`) }/>
                </div>
                <Button outline withoutBorders onClick={ props.onReset } className={ styles.resetBtn }
                        text={ loc._(t`basic.reset`) }/>
                <PrimaryButton onClick={ props.onSubmit }
                               text={ `${ loc._(t`basic.applyFilter`) } (${ formView.countFilledFields })` }/>
            </div>
        </>
        }

    </div>
})

export {
    ProjectsFilter
}
