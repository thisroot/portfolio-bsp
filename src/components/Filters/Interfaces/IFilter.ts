export interface IFilter {
    isPopoverOpen: boolean
    togglePopover: () => Promise<void>
    resetForm: (e) => Promise<void>
    submitForm: (e) => Promise<void>
    fields: { [key: string]: any }
    filterForm: any
    formInitialized: boolean
    pending: boolean
    initFilterForm: () => Promise<void>
    formValues: { [key: string]: any }
    countFilledFields: number
}

export interface ISort {
    sortLabels: { [key: string]: any }
    selectSort: (item: any) => Promise<void>
    selectedSort: any
}

export interface ISavedFilter extends IFilter {
    filterNeedSave: boolean
    toggleSaveFilter: (e) => Promise<void>
    saveFilter: (form) => Promise<void>
    registerFilter: (name: string) => void
}
