import { action, computed, observable } from "mobx"
import { inject } from "react-ioc"
import { ApiService, DictionaryService } from 'services'
import MobxReactForm from 'mobx-react-form'
import { get, intersection, isEmpty } from 'lodash'
import { FIELDS } from "constants/forms"
import { PICKER_SHORT_YEAR_DATE_MASK, SERVER_DATE_MASK } from "constants/common"
import { HISTORY_ACTION, HISTORY_RELATION } from 'constants/locale'
import { getUserOrAdminFullName } from 'utils/helpers/models'
import { loc } from "utils/i18n"
import vjf from "mobx-react-form/lib/validators/VJF"
import dayjs from 'dayjs'
import { makeHash } from "utils/helpers"
import { BasicFilterViewModel } from "../BasicFilter"

const plugins = {
  vjf: vjf()
}

enum IdField {
  admin = 'admin_id',
  user = 'user_id'
}

class ObjectHistoryFilterViewModel extends BasicFilterViewModel  {

  @inject public api: ApiService
  @inject public dictionaryService: DictionaryService

  @computed
  public get fields() {
    return {
      name_ru: FIELDS.name_ru,
      user_id: FIELDS.user_id,
      action: FIELDS.action,
      date_from: FIELDS.date_from,
      date_to: FIELDS.date_to,
    }
  }

  @computed
  private get dateFields() {
    return [this.fields.date_from.name, this.fields.date_to.name]
  }

  @computed
  public get dateRange() {
    return `${dayjs(this.filterFields.get(this.fields.date_from.name).value, SERVER_DATE_MASK).format(PICKER_SHORT_YEAR_DATE_MASK)}/
    ${dayjs(this.filterFields.get(this.fields.date_to.name).value, SERVER_DATE_MASK).format(PICKER_SHORT_YEAR_DATE_MASK)}`
  }

  @computed
  public get countFilledFields() {
     const filledFields = Object.entries(get(this.filterForm, 'validatedValues', {}))
      .filter(([ , value ]) => value)
      .map(([ key ]) => key)
     return intersection(filledFields, this.dateFields).length === 2 ? filledFields.length - 1 : filledFields.length
  }

  @observable
  private nativeHistoryUser = {}

  @computed
  public get historyUserOptions() {
    return Object.values(this.nativeHistoryUser)
      .map(el => ( {
        name: getUserOrAdminFullName(el),
        id: el[IdField.user] || el[IdField.admin],
        whoAmI: el[IdField.user] ? IdField.user : IdField.admin
      } ))
  }

  @computed
  public get selectedUsers() {
    const selected = this.filterFields.get(this.fields.user_id.name).value
    return Array.isArray(selected) ? selected : []
  }

  @observable
  public historySubjectsNameOptions = []

  @computed
  public get selectedSubjectsName() {
    const selected = this.filterFields.get(this.fields.name_ru.name).value
    return Array.isArray(selected) ? selected : []
  }

  @computed
  public get selectedActions() {
    const selected = this.filterFields.get(this.fields.action.name).value
    return Array.isArray(selected) ? selected : []
  }

  @computed
  public get suggestedActions() {
    return HISTORY_ACTION.map(item => ({ id: item.key, name: loc._(item.value) }))
  }

  @action
  public initFilterForm = async () => {
    this.pending = true
    if (!this.formInitialized) {
      await this.dictionaryService.init()
      this.filterForm = new MobxReactForm({ fields: this.fields }, { plugins })

      const result = await this.api.logging.historyFilterData({
        'history_relation': [ HISTORY_RELATION.schedule.name ]
      })
      if (result.status === 200) {
        const users = get(result, 'body.user', {})
        const admins = get(result, 'body.admin', {})
        this.nativeHistoryUser = Object.assign({}, users, admins)
        this.historySubjectsNameOptions = get(result, 'body.name', []).map((el) => ({ id: makeHash(4), name: el }))
      }
    }
    this.formInitialized = true
    this.pending = false
  }

  @computed get formValues() {
    const filters = {}
    if (this.filterForm) {
      Object.keys(this.filterForm.get()).forEach(item => {
        const value = get(this.filterForm.get()[item], 'value')
        if (value) {
          switch(item) {
            case FIELDS.date_from.name:
            case FIELDS.date_to.name:
              filters[item] = value
              break
            case FIELDS.name_ru.name:
              filters[item] = value.map(el => el.name)
              break
            case FIELDS.action.name:
              filters[item] = value.map(el => el.id)
              break
            case FIELDS.user_id.name:
              const users = { [IdField.user]: [], [IdField.admin]: []}
              value.forEach(el => users[el.whoAmI].push(el.id))
              if (!isEmpty(users[IdField.user])) { filters[IdField.user] = users[IdField.user] }
              if (!isEmpty(users[IdField.admin])) { filters[IdField.admin] = users[IdField.admin] }
              break
          }
        }
      })
    }
    return filters
  }
}

export { ObjectHistoryFilterViewModel }
