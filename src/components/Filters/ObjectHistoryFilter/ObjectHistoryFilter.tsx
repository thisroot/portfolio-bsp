import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { useInstance } from 'react-ioc'
import { ObjectHistoryFilterViewModel } from 'components/Filters/ObjectHistoryFilter'
import { Spinner, DefaultButton, PrimaryButton, DatePicker } from 'components/UI'
import { MultiSelectAutocomplete } from 'components/UI/MultiSelectAutocomplete'
import { t } from "@lingui/macro"
import { loc } from "utils/i18n"
import { isEmpty, isNull } from 'lodash'
import { SERVER_DATE_MASK, PICKER_SHORT_YEAR_DATE_MASK } from 'constants/common'
import { PICKER_FORM } from 'components/UI/DatePicker'
import dayjs from 'dayjs'
import styles from "./styles.scss"

interface IObjectHistoryFilterProps {
  onReset: (any) => any
  onSubmit: (any) => any
}

const ObjectHistoryFilter: FC<IObjectHistoryFilterProps> = observer(({ onReset, onSubmit }) => {
  
  const formView = useInstance(ObjectHistoryFilterViewModel)

  if (formView.pending) {
    return <div className={ styles.filterContainer }><Spinner /></div>
  }

  return (
    <div className={ styles.filterContainer }>
      <div className={ styles.content }>
        <div className={ styles.left }>
          <div>{ loc._(t`component.Logging.subjectName`) }</div>
          <MultiSelectAutocomplete
            id={ `multi-select-${formView.fields.name_ru.name}` }
            tags={ formView.selectedSubjectsName }
            suggestions={ formView.historySubjectsNameOptions }
            onAddition={ (e) => formView.addSelected(e, formView.fields.name_ru.name) }
            onDelete={ (e) => formView.delSelected(e, formView.fields.name_ru.name) }
            placeholderText={ loc._(t`basic.search`) }
            allowBackspace={ !isEmpty(formView.selectedSubjectsName) }
          />
          <div className={ styles.title }>{ loc._(t`basic.userName`) }</div>
          <MultiSelectAutocomplete
            id={ `multi-select-${formView.fields.user_id.name}` }
            tags={ formView.selectedUsers }
            suggestions={ formView.historyUserOptions }
            onAddition={ (e) => formView.addSelected(e, formView.fields.user_id.name) }
            onDelete={ (e) => formView.delSelected(e, formView.fields.user_id.name) }
            placeholderText={ loc._(t`basic.search`) }
            allowBackspace={ !isEmpty(formView.selectedUsers) }
          />
          <div className={ styles.title }>{ loc._(t`basic.actions`) }</div>
          <MultiSelectAutocomplete
            id={ `multi-select-${formView.fields.action.name}` }
            tags={ formView.selectedActions }
            suggestions={ formView.suggestedActions }
            onAddition={ (e) => formView.addSelected(e, formView.fields.action.name) }
            onDelete={ (e) => formView.delSelected(e, formView.fields.action.name) }
            placeholderText={ loc._(t`basic.search`) }
            allowBackspace={ !isEmpty(formView.selectedActions) }
          />
        </div>
        <div>
          <DatePicker
            isRange
            format={ PICKER_SHORT_YEAR_DATE_MASK }
            form={ PICKER_FORM.inline }
            value={ formView.dateRange }
            addedClasses={ styles.dayPicker }
            onDayChange={ ({ from = null, to = null }) => {
              formView.filterFields.get(formView.fields.date_from.name).value = isNull(from) ? from : dayjs(from).format(SERVER_DATE_MASK)
              formView.filterFields.get(formView.fields.date_to.name).value = isNull(to) ? to : dayjs(to).format(SERVER_DATE_MASK)
            } }
          />
        </div>
      </div>
      <div className={ styles.footer }>
        <DefaultButton
          onClick={ onReset }
          className={ styles.resetBtn }
          text={ loc._(t`basic.reset`) }
          disabled={ formView.pending }
        />
        <PrimaryButton
          onClick={ onSubmit }
          text={ `${ loc._(t`basic.applyFilter`) }` }
          disabled={ formView.pending }
        />
      </div>
    </div>
  )
})

export { ObjectHistoryFilter }
