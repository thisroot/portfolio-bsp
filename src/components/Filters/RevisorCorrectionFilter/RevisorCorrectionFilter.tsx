import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { useInstance } from 'react-ioc'
import { RevisorCorrectionFilterViewModel } from 'components/Filters/RevisorCorrectionFilter'
import { Spinner, DefaultButton, PrimaryButton } from 'components/UI'
import { MultiSelectAutocomplete } from 'components/UI/MultiSelectAutocomplete'
import { t } from "@lingui/macro"
import { loc } from "utils/i18n"
import { isEmpty } from 'lodash'
import styles from "./styles.scss"

interface IRevisorCorrectionFilterProps {
  onReset: (any) => any
  onSubmit: (any) => any
}

const RevisorCorrectionFilter: FC<IRevisorCorrectionFilterProps> = observer(({ onReset, onSubmit }) => {
  
  const formView = useInstance(RevisorCorrectionFilterViewModel)

  if (formView.pending) {
    return <div className={ styles.filterContainer }><Spinner /></div>
  }

  return (
    <div className={ styles.filterContainer }>
      <div className={ styles.content }>
          <div>{ loc._(t`form.status.label`) }</div>
          <MultiSelectAutocomplete
            id={ `multi-select-${formView.fields.status.name}` }
            tags={ formView.selectedStatus }
            suggestions={ formView.suggestedStatus }
            onAddition={ (e) => formView.addSelected(e, formView.fields.status.name) }
            onDelete={ (e) => formView.delSelected(e, formView.fields.status.name) }
            placeholderText={ loc._(t`basic.search`) }
            allowBackspace={ !isEmpty(formView.selectedStatus) }
          />
      </div>
      <div className={ styles.footer }>
        <DefaultButton
          onClick={ onReset }
          className={ styles.resetBtn }
          text={ loc._(t`basic.reset`) }
          disabled={ formView.pending }
        />
        <PrimaryButton
          onClick={ onSubmit }
          text={ `${ loc._(t`basic.applyFilter`) }` }
          disabled={ formView.pending }
        />
      </div>
    </div>
  )
})

export { RevisorCorrectionFilter }
