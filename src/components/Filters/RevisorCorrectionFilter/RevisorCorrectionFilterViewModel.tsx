import { computed } from "mobx"
import { FIELDS } from "constants/forms"
import { FACT_CORRECTION_STATUS } from 'constants/locale'
import { loc } from "utils/i18n"
import { BasicFilterViewModel } from "../BasicFilter"


class RevisorCorrectionFilterViewModel extends BasicFilterViewModel {

    @computed
    public get fields() {
        return {
            status: FIELDS.status,
        }
    }

    @computed
    public get selectedStatus() {
        const selected = this.filterFields.get(this.fields.status.name).value
        return Array.isArray(selected) ? selected : []
    }

    @computed
    public get suggestedStatus() {
        return Object.values(FACT_CORRECTION_STATUS).map(item => ({ id: item.key, name: loc._(item.value) }))
    }
}

export { RevisorCorrectionFilterViewModel }
