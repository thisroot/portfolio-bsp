import { computed } from 'mobx'
import dayjs from 'dayjs'
import { SERVER_DATE_MASK } from 'constants/common'
import { FIELDS } from 'constants/forms'
import { BasicFilterViewModel } from '../BasicFilter'

class EveryDaySuekFilterViewModel extends BasicFilterViewModel {

    @computed
    public get fields() {
        return {
            date: {
                ...FIELDS.date,
                default: dayjs().format(SERVER_DATE_MASK),
                value: dayjs().format(SERVER_DATE_MASK),
            },
        };
    }

    @computed
    public get dateStart() {
        return new Date()
    }

}

export { EveryDaySuekFilterViewModel };
