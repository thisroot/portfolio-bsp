import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { useInstance } from 'react-ioc'
import { Button, DatePicker, PICKER_FORM, PrimaryButton, Spinner } from 'components/UI'
import { SERVER_DATE_MASK } from 'constants/common'
// import { REPORT_FACT_TYPE } from 'constants/locale';
import { t } from '@lingui/macro'
import { loc } from 'utils/i18n'
import dayjs from 'dayjs'
import styles from './styles.scss'
import { EveryDaySuekFilterViewModel } from "./EveryDaySuekFilterViewModel"

interface IGuntFilterProps {
    onReset: (any) => any;
    onSubmit: (any) => any;
}

const EveryDaySuekFilter: FC<IGuntFilterProps> = observer(({ onSubmit, onReset }) => {
    const view = useInstance(EveryDaySuekFilterViewModel)

    return (
      <div className={ styles.componentContainer }>
          { view.pending && <Spinner/> }
          { !view.pending && (
            <>
                <div className={ styles.filterContainer }>
                    <div className={ styles.filter }>
                        <span className={ styles.title }>{ loc._(t`components.filter.reportDateTo`) }</span>
                        <div className={ styles.row }>
                            <DatePicker
                              form={ PICKER_FORM.inline }
                              addedClasses={ styles.dayPicker }
                              value={ view.filterFields.get(view.fields.date.name).value }
                              onDayChange={ e => {
                                  view.filterFields.get(view.fields.date.name).value = dayjs(e).format(
                                    SERVER_DATE_MASK,
                                  )
                              } }
                            />
                        </div>
                    </div>
                </div>
                <div className={ styles.footer }>
                    <Button
                      outline
                      withoutBorders
                      onClick={ onReset }
                      className={ styles.resetBtn }
                      text={ loc._(t`basic.reset`) }
                    />
                    <PrimaryButton onClick={ onSubmit } text={ `${ loc._(t`basic.applyFilter`) }` }/>
                </div>
            </>
          ) }
      </div>
    )
})

export { EveryDaySuekFilter }
