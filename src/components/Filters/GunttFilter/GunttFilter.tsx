import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { useInstance } from 'react-ioc'
import { get } from 'lodash'
import { GunttFilterViewModel } from 'components/Filters/GunttFilter'
import { Spinner, Button, PrimaryButton, DatePicker, Dropdown, PICKER_FORM } from 'components/UI'
import { SERVER_DATE_MASK } from 'constants/common'
// import { REPORT_FACT_TYPE } from 'constants/locale';
import { t } from '@lingui/macro'
import { loc } from 'utils/i18n'
import dayjs from 'dayjs'
import styles from './styles.scss'

interface IGuntFilterProps {
    onReset: (any) => any;
    onSubmit: (any) => any;
}

const GunttInlineFilter: FC<IGuntFilterProps> = observer(({ onSubmit, onReset }) => {
    const view = useInstance(GunttFilterViewModel)
    return <div className={ styles.inlineFilterContainer }>
        { !view.formInitialized && <Spinner/> }
        { view.formInitialized && (
          <div className={ styles.filterContainer }>
              <Dropdown
                className={ styles.filterItem }
                items={ view.decimalOptions }
                keyName={ 'value' }
                itemName={ 'label' }
                initialItem={ `${ view.filterFields.get(view.fields.decimal_view.name).value }` }
                onChange={ e => view.filterFields.get(view.fields.decimal_view.name).value = get(e, 'value', null) }
              />
              <Dropdown
                className={ styles.filterItem }
                items={ view.planFactType }
                keyName={ 'value' }
                itemName={ 'label' }
                initialItem={ `${ view.filterFields.get(view.fields.fact_type.name).value }` }
                onChange={ e => view.filterFields.get(view.fields.fact_type.name).value = get(e, 'value', null) }
              />
              <Dropdown
                className={ styles.filterItem }
                items={ view.gridOptions }
                keyName={ 'value' }
                itemName={ 'label' }
                initialItem={ `${ view.filterFields.get(view.fields.grid_step.name).value }` }
                onChange={ (e) => view.setGridStep(get(e, 'value', null)) }
              />
              <DatePicker
                disabledDays={ {
                    before: new Date(view.dateStart),
                    after: new Date(),
                } }
                form={ PICKER_FORM.select }
                addedClasses={ styles.filterItem }
                value={ view.filterFields.get(view.fields.date_to.name).value }
                onDayChange={ e => {
                    view.filterFields.get(view.fields.date_to.name).value = dayjs(e).format(
                      SERVER_DATE_MASK,
                    )
                } }
              />
              <Button
                outline
                withoutBorders
                onClick={ onReset }
                className={ styles.btnItem }
                text={ loc._(t`basic.reset`) }
              />
              <PrimaryButton className={ styles.btnItem } onClick={ onSubmit }
                             text={ `${ loc._(t`basic.applyFilter`) }` }/>
          </div>)
        }
    </div>
})

const GunttFilter: FC<IGuntFilterProps> = observer(({ onSubmit, onReset }) => {
    const view = useInstance(GunttFilterViewModel)

    return (
      <div className={ styles.componentContainer }>
          { view.pending && <Spinner/> }
          { !view.pending && (
            <>
                <div className={ styles.filterContainer }>
                    <div className={ styles.filter }>
                        <div className={ styles.left }>
                            <div className={ styles.row }>
                                <Dropdown
                                  items={ view.decimalOptions }
                                  keyName={ 'value' }
                                  itemName={ 'label' }
                                  initialItem={ `${ view.filterFields.get(view.fields.decimal_view.name).value }` }
                                  onChange={ e => view.filterFields.get(view.fields.decimal_view.name).value = get(e, 'value', null) }
                                />
                            </div>
                            <div className={ styles.row }>
                                <Dropdown
                                  items={ view.planFactType }
                                  keyName={ 'value' }
                                  itemName={ 'label' }
                                  initialItem={ `${ view.filterFields.get(view.fields.fact_type.name).value }` }
                                  onChange={ e => view.filterFields.get(view.fields.fact_type.name).value = get(e, 'value', null) }
                                />
                            </div>
                            <div className={ styles.row }>
                                <Dropdown
                                  items={ view.gridOptions }
                                  keyName={ 'value' }
                                  itemName={ 'label' }
                                  initialItem={ `${ view.filterFields.get(view.fields.grid_step.name).value }` }
                                  onChange={ (e) => view.setGridStep(get(e, 'value', null)) }
                                />
                            </div>
                        </div>
                        <div className={ styles.right }>
                            <span className={ styles.title }>{ loc._(t`components.filter.reportDateTo`) }</span>
                            <div className={ styles.row }>
                                <DatePicker
                                  disabledDays={ {
                                      before: new Date(view.dateStart),
                                      after: new Date(),
                                  } }
                                  form={ PICKER_FORM.inline }
                                  addedClasses={ styles.dayPicker }
                                  value={ view.filterFields.get(view.fields.date_to.name).value }
                                  onDayChange={ e => {
                                      view.filterFields.get(view.fields.date_to.name).value = dayjs(e).format(
                                        SERVER_DATE_MASK,
                                      )
                                  } }
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className={ styles.footer }>
                    <Button
                      outline
                      withoutBorders
                      onClick={ onReset }
                      className={ styles.resetBtn }
                      text={ loc._(t`basic.reset`) }
                    />
                    <PrimaryButton onClick={ onSubmit } text={ `${ loc._(t`basic.applyFilter`) }` }/>
                </div>
            </>
          ) }
      </div>
    )
})

export { GunttFilter, GunttInlineFilter }
