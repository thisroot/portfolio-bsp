import { observable, computed, action } from 'mobx';
import { inject } from 'react-ioc';
import dayjs from 'dayjs';
import { get } from 'lodash';
import { ApiService, DataContext } from 'services';
import { getPlaceholder } from 'components/GGChart/helpers/calculateTimeGrid';
import { ObjectViewModel } from 'screens/ObjectScreen/ObjectViewModel';
import { SERVER_DATE_MASK } from 'constants/common';
import { FIELDS } from 'constants/forms';
import { loc } from 'utils/i18n';
import { t } from '@lingui/macro';
import { REPORT_FACT_TYPE } from 'constants/locale';
import { BasicFilterViewModel } from '../BasicFilter';

class GunttFilterViewModel extends BasicFilterViewModel {
  @inject public api: ApiService;
  @inject dataContext: DataContext;
  @inject object: ObjectViewModel;

  @observable decimalOptions = [
    { value: '1000', label: loc._(t`x3`) + `.\\` + loc._(t`basic.currency.short.rur`) },
    { value: '1', label: loc._(t`basic.currency.short.rur`) },
  ];

  @observable gridOptions = [
    { value: '7', label: loc._(t`component.SchedulePlanFact.intervalName.week`) },
    { value: '30', label: loc._(t`component.SchedulePlanFact.intervalName.month`) },
    { value: '90', label: loc._(t`component.SchedulePlanFact.intervalName.quarter`) },
    {
      value: '1',
      label: loc._(t`component.SchedulePlanFact.intervalName.custom`),
      isCustomRange: true,
    },
  ];

  @computed get planFactType() {
    return this.dataContext.reference.fact_type.map(item => ({
      value: item,
      label: loc._(REPORT_FACT_TYPE[item]),
    }));
  }

  @computed
  public get fields() {
    return {
      date_to: {
        ...FIELDS.date_to,
        default: dayjs().format(SERVER_DATE_MASK),
        value: dayjs().format(SERVER_DATE_MASK),
      },
      fact_type: {
        ...FIELDS.fact_type,
        default: Object.keys(REPORT_FACT_TYPE)[0],
        value: Object.keys(REPORT_FACT_TYPE)[0],
      },
      decimal_view: {
        ...FIELDS.decimal_view,
        default: '1000',
        value: '1000',
      },
      grid_step: {
        ...FIELDS.grid_step,
        default: this.gridDefaultStep,
        value: this.gridDefaultStep,
      },
      grid_state: {
        ...FIELDS.grid_state,
        default: {
          value: this.gridDefaultStep,
          label: this.gridDefaultPlaceholder,
        },
        value: {
          value: this.gridDefaultStep,
          label: this.gridDefaultPlaceholder,
        },
      },
    };
  }

  @computed
  public get dateStart() {
    return this.object.currentObject.date_start;
  }

  @computed
  public get schedule() {
    return this.dataContext.schedule_plan_fact.get(
      get(this.object.currentObject, 'schedule_plan_fact.schedule_id'),
    );
  }

  @computed get placeholderData() {
    return this.schedule && getPlaceholder(this.schedule);
  }

  @computed get gridDefaultStep() {
    return this.placeholderData && this.placeholderData.step;
  }

  @computed get gridDefaultPlaceholder() {
    return this.placeholderData && this.placeholderData.intervalName;
  }

  @action
  public setGridStep = step => {
    this.filterFields.get(this.fields.grid_step.name).value = step;
    const gridState = getPlaceholder(
      {
        date_start: this.dateStart,
        date_finish: this.filterFields.get(this.fields.date_to.name).value,
      },
      step,
    );
    const parsedGridState = { value: gridState.step, label: gridState.intervalName };
    this.filterFields.get(this.fields.grid_state.name).value = gridState.isCustomRange
      ? { ...parsedGridState, isCustomRange: true }
      : parsedGridState;
  };
}

export { GunttFilterViewModel };
