import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { useInstance } from 'react-ioc'
import { Button, DatePicker, DefaultButton, PrimaryButton, Spinner } from 'components/UI'
import { t } from "@lingui/macro"
import { loc } from "utils/i18n"
import { get, isNull } from 'lodash'
import { PICKER_SHORT_YEAR_DATE_MASK, SERVER_DATE_MASK } from 'constants/common'
import { PICKER_FORM } from 'components/UI/DatePicker'
import dayjs from 'dayjs'
import styles from "./styles.scss"
import { ProjectFilesFilterViewModel } from "./ProjectFilesFilterViewModel"
import cx from 'classnames'

interface IProjectFilesFilterProps {
    onReset: (any) => any
    onSubmit: (any) => any
}

const ProjectFilesFilter: FC<IProjectFilesFilterProps> = observer(({ onReset, onSubmit }) => {

    const formView = useInstance(ProjectFilesFilterViewModel)

    if (formView.pending) {
        return <div className={ styles.filterContainer }><Spinner/></div>
    }

    return (
      <div className={ styles.filterContainer }>
          <div className={ styles.content }>
              <div className={ styles.left }>
                  <h5>{ loc._(t`components.filter.dateRangeFilter`) }</h5>
                  <DatePicker
                    isRange
                    format={ PICKER_SHORT_YEAR_DATE_MASK }
                    form={ PICKER_FORM.inline }
                    value={ formView.dateRange }
                    addedClasses={ styles.dayPicker }
                    onDayChange={ ({ from = null, to = null }) => {
                        formView.filterFields.get(formView.fields.date_from.name).value = isNull(from) ? from : dayjs(from).format(SERVER_DATE_MASK)
                        formView.filterFields.get(formView.fields.date_to.name).value = isNull(to) ? to : dayjs(to).format(SERVER_DATE_MASK)
                    } }
                  />
              </div>
              <div className={ styles.sorter }>
                  <h5>{ loc._(t`basic.sort`) }</h5>
                  <div className={ styles.sortingContainer }>
                      {
                          formView.sortLabels && formView.sortLabels.length && formView.sortLabels.map(type => {
                              return <div key={ type.key } className={ styles.typeContainer }>
                                    <span className={ styles.sortTitle }>
                                        { type.title }
                                    </span>
                                  <div>
                                      {
                                          type.children.map(item => {
                                              const isActive = item.key === get(formView.selectedSort, 'key')
                                              const isReversed = get(formView.selectedSort, 'value') === -1
                                              return <Button
                                                className={ styles.sortBtn }
                                                active={ isActive }
                                                iconStyle={ cx({
                                                    [styles.selectedIcon]: isActive,
                                                    [styles.rotateIcon]: isActive && isReversed
                                                }) }
                                                key={ item.key }
                                                onClick={ () => formView.selectSort(item) }
                                                icon={ 'arrows-duet' }
                                                text={ item.title }
                                                outline
                                                withoutBorders
                                              />
                                          })
                                      }
                                  </div>
                              </div>
                          })
                      }
                  </div>
              </div>
          </div>
          <div className={ styles.footer }>
              <DefaultButton
                onClick={ onReset }
                className={ styles.resetBtn }
                text={ loc._(t`basic.reset`) }
                disabled={ formView.pending }
              />
              <PrimaryButton
                onClick={ onSubmit }
                text={ `${ loc._(t`basic.applyFilter`) }` }
                disabled={ formView.pending }
              />
          </div>
      </div>
    )
})

export { ProjectFilesFilter }
