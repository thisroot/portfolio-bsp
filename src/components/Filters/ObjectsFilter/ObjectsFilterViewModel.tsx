import React from "react"
import { inject } from "react-ioc"
import { action, computed, observable, toJS } from "mobx"
import { DictionaryService } from "../../../services/DictionaryService"
import { ApiService, DataContext, LocalisationService } from "../../../services"
import MobxReactForm from 'mobx-react-form'
import vjf from "mobx-react-form/lib/validators/VJF"
import { FIELDS } from "../../../constants/forms"
import {
    flatNestedFormFields,
    flatTree,
    parseFilterAttributes,
    parseFilterFromTree,
    updateFormValues
} from "../../../utils/helpers"
import { get, intersection, isArray } from "lodash"
import { loc } from "../../../utils/i18n"
import { t } from "@lingui/macro"
import styles from './styles.scss'
import { Popover } from "../../UI/Popover"
import { Button, BUTTON_TYPE, ButtonsGroup } from "../../UI/Button"
import cx from 'classnames'
import { Checkbox } from "../../UI/Controls/Checkbox"
import { BasicFilterWithSortViewModel } from "../BasicFilter"
import { ISavedFilter } from "../Interfaces"

const plugins = {
    vjf: vjf()
}

class ObjectsFilterViewModel extends BasicFilterWithSortViewModel implements ISavedFilter {

    @inject dictionaryService: DictionaryService
    @inject dataContext: DataContext
    @inject lang: LocalisationService
    @inject api: ApiService

    @observable
    public filterName: string

    @action
    public registerFilter = (filterName: string) => {
        this.filterName = filterName
    }

    @action
    public resetForm = async (e) => {
        this.selectedSort = null
        this.filterForm.onReset(e)
        this.filterNeedSave && await this.saveFilter({ filter: null, sort: null })
    }

    @action
    public submitForm = async (e) => {
        this.filterForm.onSubmit(e)
        this.filterNeedSave && await this.saveFilter()
    }

    public outerSubmit

    @action registerOuterSubmit = (sub) => {
        this.outerSubmit = sub
    }

    @computed get isFilterDefault() {
        return get(this.filterForm, 'isDefault', true)
    }

    @computed get regions() {
        return [ ...this.dataContext.reference.region.values() ].map(reg => ({ value: reg.name, key: reg.region_id }))
    }

    @computed get fields() {
        return {
            region_id: {
                ...FIELDS.region_id,
                extra: this.regions
            },
            status: FIELDS.status,
            deviation_day: FIELDS.deviation_day,
            volume_percent: FIELDS.volume_percent,
            [`passport_${ this.lang.lang }`]: {
                ...FIELDS.passport_fields,
                name: `passport_${ this.lang.lang }`
            },
        }
    }

    public formTree
    public sortLabels = [
        {
            key: null,
            title: loc._(t`components.filter.sortByName`),
            children: [ { key: null, title: loc._(t`basic.sort.AZ`), value: 1 } ]
        }
    ]

    @observable
    public favoriteFilters = []

    @action
    public initFilterForm = async () => {
        if (!this.filterName) return
        if (!this.formInitialized) {
            await this.dictionaryService.init()

            const filterFlagRes = await this.api.user.getData({
                path: [
                    `filters.${ this.filterName }`
                ]
            })
            if (filterFlagRes.status === 200) {
                this.filterNeedSave = get(filterFlagRes, `body.data.filters[${ this.filterName }]`, false)
            }

            const result = await this.api.object.getFilter()

            if (result.status === 200) {
                const filterTypes = result.body[`passport_${ this.lang.lang }`]
                this.formTree = parseFilterFromTree(filterTypes, 'object')
                this.sortLabels.push(...this.formTree.map((item) => {
                    return {
                        ...item,
                        children: item.children.map(ch => {
                            const { key, title } = ch
                            return { key, title, value: 1 }
                        })
                    }
                }))
            }

            this.filterForm = new MobxReactForm({ fields: this.fields }, {
                plugins
            })

            if (this.filterNeedSave) {
                this.filterNeedSave.filter && updateFormValues(this.filterNeedSave.filter, this.filterForm)
                if (this.filterNeedSave.sort) {
                    this.selectedSort = this.filterNeedSave.sort
                }
            }

            const favorites = await this.api.user.getData({
                path: [ `filters.interface_settings.filter.passport_${ this.lang.lang }` ]
            })

            if (favorites.status === 200) {
                const fav = get(favorites, `body.data.filters.interface_settings.filter.passport_${ this.lang.lang }`)
                if (fav) {
                    this.favoriteFilters = fav
                }
            }

            this.formInitialized = true
            this.pending = false
        }
    }

    @computed
    public get favoritesFlags() {
        const flattenTree = toJS(flatTree(this.formTree, 'key')) || []
        const filter = toJS(this.favoriteFilters) || []
        return flattenTree.filter(item => filter.includes(item.key))
    }

    @observable
    public openedFavorites = new Set()

    @action
    public toggleOpenedFavorites(key) {
        if (this.openedFavorites.has(key)) {
            this.openedFavorites.delete(key)
        } else {
            this.openedFavorites.add(key)
        }
    }

    @action
    public togglePassportValue = async (e, item) => {
        if (this.filterForm.fields.get(`passport_${ this.lang.lang }`).value.includes(item)) {
            this.filterForm.fields.get(`passport_${ this.lang.lang }`).set(this.filterForm.fields.get(`passport_${ this.lang.lang }`).value.filter(val => val !== item))
        } else {
            this.filterForm.fields.get(`passport_${ this.lang.lang }`).set([ ...this.filterForm.fields.get(`passport_${ this.lang.lang }`).value, item ])
        }
        await this.submitForm(e)
        this.outerSubmit && this.outerSubmit(e)
    }

    @computed get renderFavorites() {
        return <div className={ styles.favoritesContainer }>{
            this.favoritesFlags.map(flag => {
                const ifAnySelected = intersection(flag.children, get(this.filterForm.values(), `passport_${ this.lang.lang }`)).length > 0
                return <Popover
                  onClickOutside={ () => this.toggleOpenedFavorites(flag.key) }
                  key={ flag.key }
                  content={ <div className={ styles.favoritesFormContainer }>
                      <div className={ styles.form }>
                          {
                              flag.children.map(item => {
                                  const isChecked = get(this.filterForm.values(), `passport_${ this.lang.lang }`).includes(item)
                                  return <Checkbox
                                    key={ item }
                                    checked={ isChecked }
                                    label={ item.split('_')[2] }
                                    onChange={ (e) => this.togglePassportValue(e, item) }
                                  />
                              })
                          }
                      </div>
                  </div> }
                  isOpen={ this.openedFavorites.has(flag.key) }
                  align={ "end" } position={ 'bottom' }
                >
                    <div className={ styles.favorite } style={ { position: 'relative' } }>
                        <ButtonsGroup>
                            <Button
                              outline
                              text={ flag.title }
                              className={ cx(styles.filter, {
                                  [styles.open]: !!this.openedFavorites.has(flag.key),
                              }) }
                            />
                            <Button
                              buttonType={ ifAnySelected ? BUTTON_TYPE.PRIMARY : BUTTON_TYPE.BASIC }
                              onClick={ () => this.toggleOpenedFavorites(flag.key) }
                              className={ cx(styles.filter, {
                                  [styles.open]: !!this.openedFavorites.has(flag.key),
                                  [styles.isSelected]: ifAnySelected
                              }) }
                              icon={ 'internet-filter-sort-b' }/>
                        </ButtonsGroup>
                    </div>
                </Popover>
            })
        }</div>
    }

    @action selectSort = async (item) => {
        if (!this.selectedSort || (this.selectedSort && this.selectedSort.key !== item.key)) {
            this.selectedSort = item
        } else if (this.selectedSort.value === 1) {
            this.selectedSort = { ...toJS(item), value: -1 }
        } else {
            this.selectedSort = null
        }
        await this.saveFilter()
    }

    @computed get formValues() {
        if (!this.filterForm) return {}
        const flatFields = flatNestedFormFields(this.filterForm.get())
        if (flatFields[`passport_${ this.lang.lang }`]) {
            const res = parseFilterAttributes(
              flatFields[`passport_${ this.lang.lang }`], 'object')
            if (isArray(res) && res.length === 0) {
                delete flatFields[`passport_${ this.lang.lang }`]
            } else {
                flatFields[`passport_${ this.lang.lang }`] = res
            }
        }
        return flatFields
    }

    @observable
    public filterNeedSave

    @action
    public toggleSaveFilter = async (e) => {
        this.filterNeedSave = e.target.checked
        if (e.target.checked) {
            await this.saveFilter()
        } else {
            await this.saveFilter(null)
        }
    }

    @action
    public saveFilter = async (form = {
        filter: this.filterForm.values(), sort: this.selectedSort
    }) => {
        await this.api.user.setData({
            path: 'filters',
            data: {
                [this.filterName]: form
            }
        })
    }
}

export {
    ObjectsFilterViewModel
}
