import { observer } from "mobx-react-lite"
import { Spinner } from "../../UI/Spinner"
import React from "react"
import { useInstance } from "react-ioc"
import { FilesArchiveFilterViewModel } from "./FilesArchiveFilterViewModel"
import { loc } from "../../../utils/i18n"
import { t } from "@lingui/macro"
import { get, isEmpty } from "lodash"
import { Button, DefaultButton, PrimaryButton } from "../../UI/Button"
import cx from 'classnames'
import styles from './styles.scss'
import { Dropdown } from "../../UI/Dropdown"
import { MultiSelectAutocomplete } from "../../UI/MultiSelectAutocomplete"
import { FILE_RELATION } from "../../../constants"

interface IFilesArchiveFilterProps {
    onReset: (any) => any
    onSubmit: (any) => any
}

const FilesArchiveFilter: React.FC<IFilesArchiveFilterProps> = observer(({ onReset, onSubmit }) => {

    const formView = useInstance(FilesArchiveFilterViewModel)

    if (formView.pending) {
        return <div className={ styles.filterContainer }><Spinner/></div>
    }

    const currentRelation = formView.filterFields.get(formView.fields.file_relation.name).value

    return (
      <div className={ styles.filterContainer }>
          <div className={ styles.content }>
              <div className={ styles.left }>
                  <div className={ styles.title }>
                      <h5>Фильтрация</h5>
                  </div>
                  <div className={ styles.filters }>
                      {/*<div className={ styles.col }>*/ }
                      {/*    {*/ }
                      {/*        formView.selectedObjects.length > 0 && <>*/ }
                      {/*        <span className={ styles.sortTitle }>{ loc._(t`components.filter.schedule`) }</span>*/ }
                      {/*        <MultiSelectAutocomplete*/ }
                      {/*          className={ styles.dropdown }*/ }
                      {/*          id={ `multi-select-${ formView.fields.schedule_id.name }` }*/ }
                      {/*          tags={ formView.selectedSchedules }*/ }
                      {/*          suggestions={ formView.scheduleSuggestions }*/ }
                      {/*          onAddition={ (e) => formView.addSelected(e, formView.fields.schedule_id.name) }*/ }
                      {/*          onDelete={ (e) => formView.delSelected(e, formView.fields.schedule_id.name) }*/ }
                      {/*          placeholderText={ loc._(t`basic.search`) }*/ }
                      {/*          allowBackspace={ !isEmpty(formView.selectedSchedules) }*/ }
                      {/*          // onInput={ formView.searchInSchedules }*/ }
                      {/*        /></>*/ }
                      {/*    }*/ }
                      {/*</div>*/ }
                      <div className={ styles.col }>
                          <span className={ styles.sortTitle }>{ loc._(t`components.filter.extension`) }</span>
                          <Dropdown
                            className={ styles.dropdown }
                            items={ formView.extension }
                            searchFields={ [ 'value' ] }
                            keyName={ 'key' }
                            itemName={ 'value' }
                            initialItem={ formView.filterFields.get(formView.fields.extension.name).value }
                            onChange={ (e) => formView.filterFields.get(formView.fields.extension.name).value = get(e, 'key', null) }
                          />
                          <span className={ styles.sortTitle }>{ loc._(t`components.filter.fileRelations`) }</span>
                          <Dropdown
                            className={ styles.dropdown }
                            items={ formView.fileRelations }
                            searchFields={ [ 'value' ] }
                            keyName={ 'key' }
                            itemName={ 'value' }
                            initialItem={ formView.filterFields.get(formView.fields.file_relation.name).value }
                            onChange={ (e) => {
                                const value = get(e, 'key', null)
                                formView.filterFields.get(formView.fields.file_relation.name).value = value
                                if (!value) {
                                    formView.resetSelected(formView.fields.project_id.name)
                                    formView.resetSelected(formView.fields.object_id.name)
                                }
                            } }
                          />
                          {/*<span className={ styles.sortTitle }>{ loc._(t`components.filter.dateRangeFilter`) }</span>*/ }
                          {/*<DatePicker*/ }
                          {/*  isRange*/ }
                          {/*  format={ PICKER_SHORT_YEAR_DATE_MASK }*/ }
                          {/*  form={ PICKER_FORM.inline }*/ }
                          {/*  value={ formView.dateRange }*/ }
                          {/*  addedClasses={ styles.dayPicker }*/ }
                          {/*  onDayChange={ ({ from = null, to = null }) => {*/ }
                          {/*      formView.filterFields.get(formView.fields.date_from.name).value = isNull(from) ? from : dayjs(from).format(SERVER_DATE_MASK)*/ }
                          {/*      formView.filterFields.get(formView.fields.date_to.name).value = isNull(to) ? to : dayjs(to).format(SERVER_DATE_MASK)*/ }
                          {/*  } }*/ }
                          {/*/>*/ }
                          { currentRelation === FILE_RELATION.PROJECT && <div className={ styles.multiselectContainer }>
                              <span className={ styles.sortTitle }>{ loc._(t`components.filter.projects`) }</span>
                              <MultiSelectAutocomplete
                                className={ styles.dropdown }
                                id={ `multi-select-${ formView.fields.project_id.name }` }
                                tags={ formView.selectedProjects }
                                suggestions={ formView.projectsSuggestions }
                                onAddition={ (e) => formView.addSelected(e, formView.fields.project_id.name) }
                                onDelete={ (e) => formView.delSelected(e, formView.fields.project_id.name) }
                                placeholderText={ loc._(t`basic.search`) }
                                allowBackspace={ !isEmpty(formView.selectedProjects) }
                                onInput={ formView.searchInProjects }
                              /></div>
                          }
                          { currentRelation === FILE_RELATION.OBJECT && <div className={ styles.multiselectContainer }>
                              <span className={ styles.sortTitle }>{ loc._(t`components.filter.objects`) }</span>
                              <MultiSelectAutocomplete
                                className={ styles.dropdown }
                                id={ `multi-select-${ formView.fields.object_id.name }` }
                                tags={ formView.selectedObjects }
                                suggestions={ formView.objectSuggestions }
                                onAddition={ (e) => formView.addSelected(e, formView.fields.object_id.name) }
                                onDelete={ (e) => formView.delSelected(e, formView.fields.object_id.name) }
                                placeholderText={ loc._(t`basic.search`) }
                                allowBackspace={ !isEmpty(formView.selectedObjects) }
                                onInput={ formView.searchInObjects }
                              /></div>
                          }
                      </div>
                  </div>
              </div>
              <div className={ styles.sorter }>
                  <h5>{ loc._(t`basic.sort`) }</h5>
                  <div className={ styles.sortingContainer }>
                      {
                          formView.sortLabels && formView.sortLabels.length && formView.sortLabels.map(type => {
                              return <div key={ type.key } className={ styles.typeContainer }>
                                    <span className={ styles.sortTitle }>
                                        { type.title }
                                    </span>
                                  <div>
                                      {
                                          type.children.map(item => {
                                              const isActive = item.key === get(formView.selectedSort, 'key')
                                              const isReversed = get(formView.selectedSort, 'value') === -1
                                              return <Button
                                                className={ styles.sortBtn }
                                                active={ isActive }
                                                iconStyle={ cx({
                                                    [styles.selectedIcon]: isActive,
                                                    [styles.rotateIcon]: isActive && isReversed
                                                }) }
                                                key={ item.key }
                                                onClick={ () => formView.selectSort(item) }
                                                icon={ 'arrows-duet' }
                                                text={ item.title }
                                                outline
                                                withoutBorders
                                              />
                                          })
                                      }
                                  </div>
                              </div>
                          })
                      }
                  </div>
              </div>
          </div>
          <div className={ styles.footer }>
              <DefaultButton
                onClick={ onReset }
                className={ styles.resetBtn }
                text={ loc._(t`basic.reset`) }
                disabled={ formView.pending }
              />
              <PrimaryButton
                onClick={ onSubmit }
                text={ `${ loc._(t`basic.applyFilter`) }` }
                disabled={ formView.pending }
              />
          </div>
      </div>
    )
})

export { FilesArchiveFilter }
