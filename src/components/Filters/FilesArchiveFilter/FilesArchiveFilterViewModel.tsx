import { action, computed, observable, runInAction } from "mobx"
import { FIELDS } from "../../../constants/forms"
import { loc } from "../../../utils/i18n"
import { t } from "@lingui/macro"
import dayjs from "dayjs"
import { FILE_RELATION_OPTIONS, PICKER_SHORT_YEAR_DATE_MASK, SERVER_DATE_MASK } from "../../../constants"
import { debounce, get, intersection } from "lodash"
import { inject } from "react-ioc"
import { ApiService, DataContext, LocalisationService } from "services"
import { BasicFilterWithSortViewModel } from "../BasicFilter"


class FilesArchiveFilterViewModel extends BasicFilterWithSortViewModel {

    @inject dataContext: DataContext
    @inject api: ApiService
    @inject localisation: LocalisationService

    @computed
    public get fileRelations() {
        return FILE_RELATION_OPTIONS.map(item => ({ ...item, value: loc._(item.value) }))
    }

    @computed get extension() {
        return this.dataContext.reference.extension.map(ext => ({ key: ext, value: ext }))
    }

    @computed
    public get fields() {
        return {
            date_from: FIELDS.date_from,
            date_to: FIELDS.date_to,
            extension: FIELDS.extension,
            file_relation: FIELDS.file_relation,
            project_id: FIELDS.project_id,
            object_id: FIELDS.object_id,
            schedule_id: FIELDS.schedule_id
        }
    }

    public sortLabels = [
        {
            key: 'name',
            title: loc._(t`components.filter.sortByName`),
            children: [ { key: 'name', title: loc._(t`basic.sort.AZ`), value: 1 } ]
        },
        {
            key: 'created',
            title: loc._(t`components.filter.sortByDate`),
            children: [ { key: 'created', title: loc._(t`basic.sort.date`), value: 1 } ]
        },
        {
            key: 'size',
            title: loc._(t`components.filter.sortBySize`),
            children: [ { key: 'size', title: loc._(t`basic.sort.size`), value: 1 } ]
        }
    ]

    @computed get sortValues() {
        return get(this.selectedSort, 'key') ? { [this.selectedSort.key]: this.selectedSort.value } : null
    }


    @computed
    private get dateFields() {
        return [ this.fields.date_from.name, this.fields.date_to.name ]
    }

    @computed
    public get dateRange() {
        return `${ dayjs(this.filterFields.get(this.fields.date_from.name).value, SERVER_DATE_MASK).format(PICKER_SHORT_YEAR_DATE_MASK) }/
    ${ dayjs(this.filterFields.get(this.fields.date_to.name).value, SERVER_DATE_MASK).format(PICKER_SHORT_YEAR_DATE_MASK) }`
    }


    @computed
    public get countFilledFields() {
        const filledFields = Object.entries(get(this.filterForm, 'validatedValues', {}))
          .filter(([ , value ]) => value)
          .map(([ key ]) => key)
        return intersection(filledFields, this.dateFields).length === 2 ? filledFields.length - 1 : filledFields.length
    }

    @computed get formValues() {
        const filters = {}
        if (this.filterForm) {
            Object.keys(this.filterForm.get()).forEach(item => {
                const value = get(this.filterForm.get()[item], 'value')
                if (value) {
                    switch (item) {
                        case FIELDS.project_id.name:
                        case FIELDS.object_id.name:
                            filters[item] = value.map(el => el.id)
                            break
                        default:
                            filters[item] = value
                            break
                    }
                }
            })
        }
        return filters
    }

    @observable
    public projectsSuggestions = []

    @computed
    public get selectedProjects() {
        const selected = this.filterFields.get(this.fields.project_id.name).value
        return Array.isArray(selected) ? selected : []
    }

    @action
    public searchInProjects = debounce(async (query: string) => {
        if (query && query.length === 0 || !query) {
            this.projectsSuggestions = []
        } else {
            const projects = await this.api.project.list({ filter: { [`name_${ this.localisation.lang }`]: query } })
            if (projects.status !== 'error') {
                runInAction(() => {
                    this.projectsSuggestions = Object.values(projects.body.project).map(pr => ({
                        id: pr.project_id,
                        name: pr[`name_${ this.localisation.lang }`]
                    }))
                })
            }
        }
    }, 200)

    @observable
    public objectSuggestions = []

    @computed
    public get selectedObjects() {
        const selected = this.filterFields.get(this.fields.object_id.name).value
        return Array.isArray(selected) ? selected : []
    }

    @action
    public searchInObjects = debounce(async (query: string) => {
        if (query && query.length === 0 || !query) {
            this.projectsSuggestions = []
        } else {
            const objects = await this.api.object.list({ filter: { [`name_${ this.localisation.lang }`]: query } })
            if (objects.status !== 'error') {
                runInAction(() => {
                    this.objectSuggestions = Object.values(objects.body.object).map(ob => ({
                        id: ob.object_id,
                        name: ob[`name_${ this.localisation.lang }`]
                    }))
                })
            }
        }
    }, 200)


    @action
    public searchInSchedules = async () => {
        const objects = await this.api.schedule.getScheduleList()
        if (objects.status !== 'error') {
            runInAction(() => {
                this.scheduleSuggestions = Object.values(objects.body.schedule).map(ob => ({
                    id: ob.schedule_id,
                    name: ob.name
                }))
            })
        }
    }

    @observable
    public scheduleSuggestions = []

    @computed
    public get selectedSchedules() {
        const selected = this.filterFields.get(this.fields.schedule_id.name).value
        return Array.isArray(selected) ? selected : []
    }
}

export {
    FilesArchiveFilterViewModel
}
