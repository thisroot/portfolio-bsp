import { computed } from "mobx"
import { FIELDS } from "../../../constants/forms"
import dayjs from "dayjs"
import { PICKER_SHORT_YEAR_DATE_MASK, SERVER_DATE_MASK } from "../../../constants"
import { get, intersection } from "lodash"
import { loc } from "../../../utils/i18n"
import { t } from "@lingui/macro"
import { BasicFilterWithSortViewModel } from "../BasicFilter"

class ObjectFilesFilterViewModel extends BasicFilterWithSortViewModel {

    @computed
    public get fields() {
        return {
            date_from: FIELDS.date_from,
            date_to: FIELDS.date_to,
        }
    }

    public sortLabels = [
        {
            key: 'name',
            title: loc._(t`components.filter.sortByName`),
            children: [ { key: 'name', title: loc._(t`basic.sort.AZ`), value: 1 } ]
        },
        {
            key: 'created',
            title: loc._(t`components.filter.sortByDate`),
            children: [ { key: 'created', title: loc._(t`basic.sort.date`), value: 1 } ]
        },
        {
            key: 'size',
            title: loc._(t`components.filter.sortBySize`),
            children: [ { key: 'size', title: loc._(t`basic.sort.size`), value: 1 } ]
        }
    ]

    @computed
    private get dateFields() {
        return [ this.fields.date_from.name, this.fields.date_to.name ]
    }

    @computed
    public get dateRange() {
        return `${ dayjs(this.filterFields.get(this.fields.date_from.name).value, SERVER_DATE_MASK).format(PICKER_SHORT_YEAR_DATE_MASK) }/
    ${ dayjs(this.filterFields.get(this.fields.date_to.name).value, SERVER_DATE_MASK).format(PICKER_SHORT_YEAR_DATE_MASK) }`
    }

    @computed
    public get countFilledFields() {
        const filledFields = Object.entries(get(this.filterForm, 'validatedValues', {}))
          .filter(([ , value ]) => value)
          .map(([ key ]) => key)
        return intersection(filledFields, this.dateFields).length === 2 ? filledFields.length - 1 : filledFields.length
    }
}

export { ObjectFilesFilterViewModel }
