import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { useInstance } from 'react-ioc'
import { LoggingFilterViewModel } from 'components/Filters/LoggingFilter'
import { Dropdown, Spinner, DefaultButton, PrimaryButton } from 'components/UI'
import { t } from "@lingui/macro"
import { loc } from "utils/i18n"
import { get } from 'lodash'
import styles from "./styles.scss"

interface ILoggingFilterProps {
  onReset: (any) => any
  onSubmit: (any) => any
}

const LoggingFilter: FC<ILoggingFilterProps> = observer(({ onReset, onSubmit }) => {

  const formView = useInstance(LoggingFilterViewModel)

  if (formView.pending) {
    return <div className={ styles.filterContainer }><Spinner /></div>
  }

  return (
    <div className={ styles.filterContainer }>
      <div className={ styles.content }>
        <div>{ loc._(t`component.Logging.subjectName`) }</div>
        <Dropdown
          items={ formView.historySubjectsNameOptions }
          initialItem={ formView.filterFields.get(formView.fields.name_ru.name).value }
          onChange={ (e) => formView.filterFields.get(formView.fields.name_ru.name).value = e }
        />
        <div className={ styles.title }>{ loc._(t`basic.userName`) }</div>
        <Dropdown
          items={ formView.historyUserOptions }
          searchFields={ [ 'value' ] }
          keyName={ 'key' }
          itemName={ 'value' }
          initialItem={ formView.filterFields.get(formView.fields.user_id.name).value }
          onChange={ (e) => formView.filterFields.get(formView.fields.user_id.name).value = get(e, 'key', null) }
        />
        <div className={ styles.title }>{ loc._(t`basic.actions`) }</div>
        <Dropdown
          items={ formView.actions }
          searchFields={ [ 'value' ] }
          keyName={ 'key' }
          itemName={ 'value' }
          initialItem={ formView.filterFields.get(formView.fields.action.name).value }
          onChange={ (e) => formView.filterFields.get(formView.fields.action.name).value = get(e, 'key', null) }
        />
      </div>
      <div className={ styles.footer }>
        <DefaultButton
          onClick={ onReset }
          className={ styles.resetBtn }
          text={ loc._(t`basic.reset`) }
          disabled={ formView.pending }
        />
        <PrimaryButton
          onClick={ onSubmit }
          text={ `${ loc._(t`basic.applyFilter`) }` }
          disabled={ formView.pending }
        />
      </div>
    </div>
  )
})

export { LoggingFilter }
