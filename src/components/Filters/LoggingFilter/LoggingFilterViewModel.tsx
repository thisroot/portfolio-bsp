import { action, computed, observable } from "mobx"
import { inject } from "react-ioc"
import { ApiService, DictionaryService, LocalisationService } from 'services'
import MobxReactForm from 'mobx-react-form'
import { get } from 'lodash'
import { FIELDS } from "constants/forms"
import { HISTORY_ACTION } from 'constants/locale'
import { getUserOrAdminFullName } from 'utils/helpers/models'
import { loc } from "utils/i18n"
import vjf from "mobx-react-form/lib/validators/VJF"
import { BasicFilterViewModel } from "../BasicFilter"

const plugins = {
  vjf: vjf()
}

class LoggingFilterViewModel extends BasicFilterViewModel {

  @inject public api: ApiService
  @inject public dictionaryService: DictionaryService
  @inject public local: LocalisationService


  @computed
  public get fields() {
    return {
      name_ru: FIELDS.name_ru,
      user_id: FIELDS.user_id,
      action: FIELDS.action,
    }
  }


  @observable
  public nativeHistoryUser = {}
  @computed
  public get historyUserOptions() {
    return Object.values(this.nativeHistoryUser)
      .map(el => ( {
        value: getUserOrAdminFullName(el, this.local.lang),
        key: el['user_id'] || el['admin_id'],
        whoAmI: el['user_id'] ? 'user_id' : 'admin_id'
      } ))
  }
  @observable
  public historySubjectsNameOptions = []

  @computed
  public get actions() {
    return HISTORY_ACTION.map(item => ({ ...item, value: loc._(item.value) }));
  }

  @action
  public initFilterForm = async () => {
    this.pending = true
    if (!this.formInitialized) {
      await this.dictionaryService.init()
      this.filterForm = new MobxReactForm({ fields: this.fields }, { plugins })
    }
    if (this.isPopoverOpen) {
      const result = await this.api.logging.historyFilterData({})
      if (result.status === 200) {
        const users = get(result, 'body.user', {})
        const admins = get(result, 'body.admin', {})
        this.nativeHistoryUser = Object.assign({}, users, admins)
        this.historySubjectsNameOptions = get(result, 'body.name', [])
      }
    }
    this.formInitialized = true
    this.pending = false
  }

  @computed get formValues() {
    const filters = {}
    if (this.filterForm) {
      Object.keys(this.filterForm.get()).forEach(item => {
        const value = get(this.filterForm.get()[item], 'value')
        if (value) {
          if (item === 'user_id') {
            const whoAmI = this.historyUserOptions.find(el => el.key === value).whoAmI;
            filters[whoAmI] = [value]
          } else {
            filters[item] = [value]
          }
        }
      })
    }
    return filters
  }
}

export { LoggingFilterViewModel }
