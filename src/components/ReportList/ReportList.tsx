import React, { FC } from 'react'
import { Link } from "services/RouterService"
import { Icon } from "components/UI/Icon"
import styles from "./styles.scss"
import cx from 'classnames'
import { AnalyticsTemplate } from "../../models/mst/AnalyticsTemplate"


interface IReportListProps {
    items: {
        name: string;
        icon: string;
        path: string;
        itemBackground: string
        iconBackground: string
        iconColor: string
        template?: AnalyticsTemplate
    } [];
    itemStyle?: string
}

export const ReportList: FC<IReportListProps> = ({ items }) => (items.length &&
  (<div className={ styles.reportsListWrapper }>
      { items.map(({ name, icon, path, itemBackground, iconColor, iconBackground }, i) => (
        <Link key={ `report-${ path }-${ i }` } to={ path } className={ styles.reportLink }>
            <div className={ styles.itemContainer } style={ { backgroundColor: itemBackground } }>
                <div className={ styles.iconContainer } style={ { color: iconColor, backgroundColor: iconBackground } }>
                    <Icon name={ icon } alt={ name }/>
                </div>
                <div className={ styles.reportName }>{ name }</div>
                <div className={ styles.linkIconContainer } style={ { backgroundColor: iconBackground } }>
                    <Icon name='arrows-arrow-r' alt={ name }/>
                </div>
            </div>
        </Link>
      )) }
  </div>)
) || <div>No data</div>

export const ReportListMini: FC<IReportListProps> = ({ items, itemStyle }) => (items.length &&
  (<div className={ styles.reportsListMiniWrapper }>
      { items.map(({ name, icon, path, itemBackground, iconColor, iconBackground }, i) => (
        <Link key={ `report-${ path }-${ i }` } to={ path } className={ styles.reportLink }>
            <div className={ cx(styles.itemMiniContainer, itemStyle) } style={ { backgroundColor: itemBackground } }>
                <div className={ styles.reportMiniName }>{ name }</div>
                <div className={ styles.iconMiniContainer }
                     style={ { color: iconColor, backgroundColor: iconBackground } }>
                    <Icon name={ icon } alt={ name }/>
                </div>
            </div>
        </Link>
      )) }
  </div>)
)
