import React from 'react'
import cx from 'classnames'

import styles from './styles.scss'

interface Props {
  className?: string
}

const NoMatch = ({ className }: Props) => (
  <div className={ cx(className, styles.nomatch) }>
    <span className={ styles.text }>404 page not found</span>
  </div>
)

export { NoMatch }
