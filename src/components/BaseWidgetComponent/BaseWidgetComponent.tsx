import React, { SyntheticEvent } from "react"
import styles from './styles.scss'
import { Spinner } from "components/UI/Spinner"
import cx from 'classnames'
import { Icon } from "../UI/Icon"

export interface IBaseWidgetComponentProps {
    pending: boolean,
    renderContent: React.ReactNode,
    onClose?: () => void
}

export const BaseWidgetComponent: React.FC<IBaseWidgetComponentProps> = ({ pending, renderContent, onClose }) => {
    const close = (e: SyntheticEvent) => { e.stopPropagation(); onClose()}
    return <div className={ styles.widgetContainer }>
        { pending ? <Spinner className={ styles.spinner }/> :
          <div className={ styles.contentContainer }>
              <div className={ cx(styles.col, styles.fullWidth, 'cant-draggable') }>
                  { renderContent }
              </div>
              <div className={ cx(styles.col, styles.drag, 'draggable') }>
                  {
                      onClose && <div onClick={ close } className={ styles.close }><Icon name={ 'signs-cancel' }/></div>
                  }
              </div>
          </div> }
    </div>
}