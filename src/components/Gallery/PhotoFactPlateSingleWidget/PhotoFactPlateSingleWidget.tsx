import React, { FC } from 'react'
import { observer } from "mobx-react-lite"
import { FileModel } from "models/entities/FileModel"
import { MediaContainer } from "components/MediaContainer"
import { get, isNull } from 'lodash'
import { loc } from "utils/i18n"
import { parseMeasureUnit, getUserFullName } from "utils/helpers"
import { REPORT_FACT_TYPE } from 'constants/locale'
import dayjs from "dayjs"
import styles from './styles.scss'
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)

interface IPhotoFactWidgetProps {
    photoFact: any;
    images: FileModel[];
    onClick: () => any;
}

export const PhotoFactPlateSingleWidget: FC<IPhotoFactWidgetProps> = observer(({ images, photoFact, onClick }) => {
    const getMeasureName = (photoFact) => {
        const jobUnit = get(photoFact, 'schedule_id.job_unit', null)
        return isNull(jobUnit)
          ? parseMeasureUnit(get(photoFact, 'schedule_id.job_id.job_unit'))
          : parseMeasureUnit(jobUnit)
    }

    const getFullUserInfo = (photoFact) => {
        const prof = loc._(REPORT_FACT_TYPE[get(photoFact, 'fact_type')]).toLowerCase()
        return `${ prof } ${ getUserFullName(get(photoFact, 'user_id')) }`
    }

    return (
      <div className={ styles.widgetWrapper } onClick={ onClick }>
          <div className={ styles.image }>
              <MediaContainer
                file={ images[0] }
                imageConvertType="mediumView"
                imageClassName={ styles.mediaContainer }
                onClick={ onClick }
              />
              <div
                className={ styles.path }>{ `${ dayjs.utc(get(photoFact, 'created')).format('DD-MM-YYYY') }` }</div>
          </div>
          <div className={ styles.title }>{ get(photoFact, 'schedule_id.name') }</div>
          <div
            className={ styles.factVolume }>{ `${ get(photoFact, 'volume', '') } ${ getMeasureName(photoFact) }` }</div>
          <div className={ styles.user }>{ getFullUserInfo(photoFact) }</div>
      </div>
    )
})
