import React, { FC } from 'react'
import { observer } from "mobx-react-lite"
import { FileModel } from "models/entities/FileModel"
import styles from './styles.scss'
import { MediaContainer } from "components/MediaContainer"
import { get } from 'lodash'
import { Trans } from "@lingui/macro"
import dayjs from "dayjs"
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)

interface IPhotoFactWidgetProps {
    photoFact: any;
    images: FileModel[];
    onClick: () => any;
}

export const PhotoFactPlateGroupWidget: FC<IPhotoFactWidgetProps> = observer(({ images, photoFact, onClick }) => {

    return <div className={ styles.widgetWrapper } onClick={ onClick }>
        <div className={ styles.substrate }></div>
        <div className={ styles.image }>
            <MediaContainer
              file={ images[0] }
              imageConvertType="mediumView"
              imageClassName={ styles.mediaContainer }
              onClick={ onClick }
            />
            <div
              className={ styles.path }>{ `${ dayjs.utc(get(photoFact, 'created')).format('DD-MM-YYYY') }` }</div>
        </div>
        <div className={ styles.title }>{ get(photoFact, 'schedule_id.name') }</div>
        <div className={ styles.count }>{ images.length } <Trans>basic.photo</Trans></div>
    </div>
})
