import React, { useCallback, useEffect, useState } from 'react'
import DayPicker from 'react-day-picker'
import { Scrollbars } from 'react-custom-scrollbars'
import { LightBox } from 'components/UI/LightBox'

import styles from './styles.scss'
import { useMeasure } from "react-use"
import { NormalSecondaryButton } from "../UI/SecondaryButton"
import { loc } from "utils/i18n"
import { t } from "@lingui/macro"
import { FileSelector } from "components/FileSelector"
import { FileModel } from "models/entities/FileModel"
import { observer } from "mobx-react-lite"
import { get } from 'lodash'
import cx from 'classnames'
import { Spinner } from "../UI/Spinner"
import { MediaContainer } from "../MediaContainer"
import { DropZone } from "components/DropZone"

import { FIRST_DAY_OF_WEEK, LABELS, MONTHS, WEEKDAYS_LONG, WEEKDAYS_SHORT } from 'constants/locale'

import { PhotoFactPlateSingleWidget } from './PhotoFactPlateSingleWidget'
import { PhotoFactPlateGroupWidget } from './PhotoFactPlateGroupWidget'
import dayjs from "dayjs"
import utc from 'dayjs/plugin/utc'

dayjs.extend(utc)

export interface IMedia {
    id: string,
    url: string,
    date: Date,
    name: string
}

interface IGalleryProps {
    mediaFiles: Array<FileModel>
    selectedMedia: string | number,
    currentMedia: number
    lastCurrentMedia: number
    onSelectDay: (day: Date) => any
    onSelectToUpload?: (e: Array<File>) => any,
    isLightBoxOpen: boolean
    toggleLightBox: (index?: number) => any,
    pending: boolean,
    initialized: boolean,
    selectAsMain?: (any?: any) => any,
    isUploadAllow?: boolean;
    photoFact?: any;
    maxCountUpload?: number
    onDelete?: (id: number, factId: number) => Promise<void>
    canDelete?: (file?: FileModel) => boolean
}

export const Gallery: React.FC<IGalleryProps> = observer((props) => {
    const {
        mediaFiles,
        selectedMedia,
        onSelectDay,
        onSelectToUpload,
        isLightBoxOpen,
        currentMedia,
        toggleLightBox,
        lastCurrentMedia,
        pending,
        initialized,
        selectAsMain,
        isUploadAllow = !!props.onSelectToUpload,
        photoFact,
        onDelete,
        canDelete
    } = props


    const selectedDays: Date[] = mediaFiles.map(file => get(file, 'date')).filter(Boolean)
    let scrollbarsInstance = null

    const elementInstanceBySelectedMediaProps = React.createRef<HTMLDivElement>()

    const setBeginScrollTop = useCallback(() => {
        // const node = elementInstanceBySelectedMediaProps.current
        // const nodeOffsetTop = get(node, 'offsetTop', 0)

        // scrollbarsInstance && scrollbarsInstance.scrollTop(nodeOffsetTop)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ elementInstanceBySelectedMediaProps, scrollbarsInstance ])

    const getInstanceScrollbar = scrollbars => {
        scrollbarsInstance = scrollbars
    }

    const [ highlightedDay, setHighlightedDay ] = useState()

    const hasPhoto = (day) => {
        return mediaFiles.some(item => {
            return dayjs.utc(item.created).format('DDMMYYYY') === dayjs.utc(day).format('DDMMYYYY')
        })
    }


    const handleDayClick = (day: Date): void => {
        if (hasPhoto(day)) {
            setHighlightedDay(dayjs.utc(day).format('DDMMYYYY') === dayjs.utc(highlightedDay).format('DDMMYYYY') ? null : day)
            onSelectDay(dayjs.utc(day).format('DDMMYYYY') === dayjs.utc(highlightedDay).format('DDMMYYYY') ? null : day)
        }
    }

    useEffect(setBeginScrollTop, [ setBeginScrollTop ])

    const [ ref, { width } ] = useMeasure()
    const [ elWidth, setElWidth ] = useState(width)

    useEffect(() => {
        if (width && width > 100) {
            let x = 5
            switch (true) {
                case width >= 1150:
                    x = 7
                    break
                case width > 900 && width < 1150:
                    x = 6
                    break
                case width <= 900:
                default:
                    x = 5
                    break
            }
            setElWidth((width - x * 20) / x)
        }
    }, [ width ])

    const canUpload = initialized && !pending && mediaFiles.length === 0 && isUploadAllow

    const renderPhotoFactWidget = () => (
      photoFact.filter(fact => highlightedDay ? dayjs.utc(highlightedDay).format('DDMMYYYY') === dayjs.utc(fact.created).format('DDMMYYYY') : true).map(fact => {
          const length = get(fact, 'images.length')
          const factId = get(fact, 'schedule_fact_id')
          const currentMediaFiles = mediaFiles.filter(file => get(file, 'file.image_relation_id') === factId)
          const currentImageId = get(currentMediaFiles[0], 'file.image_id')
          const currentImageIndex = mediaFiles.findIndex(el => get(el, 'file.image_id') === currentImageId)
          return (
            currentMediaFiles.length > 0 ?
              length === 1
                ? (
                  <PhotoFactPlateSingleWidget
                    key={ `photo-fact-${ factId }` }
                    photoFact={ fact }
                    images={ currentMediaFiles }
                    onClick={ () => toggleLightBox(currentImageIndex) }
                  />
                ) : (
                  <PhotoFactPlateGroupWidget
                    key={ `photo-fact-${ factId }` }
                    photoFact={ fact }
                    images={ currentMediaFiles }
                    onClick={ () => toggleLightBox(currentImageIndex) }
                  />
                ) : null
          )
      })
    )

    return (
      <div className={ styles.gallery }>
          <div ref={ ref } className={ styles.left }>
              {
                  !initialized && pending && <Spinner className={ styles.preloader }/>
              }
              {
                  initialized && !pending && mediaFiles.length > 0 &&
                  <Scrollbars
                    ref={ getInstanceScrollbar }
                    renderTrackVertical={ props => <div { ...props } className={ styles.trackVertical }/> }
                    renderThumbVertical={ props => <div { ...props } className={ styles.thumbVertical }/> }
                    hideTracksWhenNotNeeded={ true }
                  >
                      <div className={ styles.view }>
                          { photoFact
                            ? renderPhotoFactWidget()
                            : mediaFiles.filter(fact => highlightedDay ? dayjs.utc(highlightedDay).format('DDMMYYYY') === dayjs.utc(fact.created).format('DDMMYYYY') : true).map((item, i) => (
                              <div
                                style={ { width: elWidth, height: elWidth } }
                                className={ cx(styles.item, { [styles.current]: currentMedia === i }, { [styles.lastCurrent]: lastCurrentMedia === i }) }
                                ref={ i === selectedMedia ? elementInstanceBySelectedMediaProps : null }
                                key={ i }
                              >
                                  <MediaContainer isMain={ selectedMedia === item.id } canWrite={ isUploadAllow }
                                                  selectAsMain={ selectAsMain }
                                                  onClick={ () => toggleLightBox(i) }
                                                  file={ item }/>
                              </div>
                            ))
                          }
                      </div>
                  </Scrollbars>
              }
              {
                  canUpload && <DropZone onFilesSelect={ onSelectToUpload }/>
              }
          </div>

          <div className={ styles.right }>
              <div className={ styles.calendar }>
                  <DayPicker
                    months={ MONTHS.map(d => loc._(d)) }
                    weekdaysLong={ WEEKDAYS_LONG.map(d => loc._(d)) }
                    weekdaysShort={ WEEKDAYS_SHORT.map(d => loc._(d)) }
                    labels={ LABELS }
                    firstDayOfWeek={ FIRST_DAY_OF_WEEK }
                    numberOfMonths={ 2 }
                    selectedDays={ selectedDays }
                    onDayClick={ handleDayClick }
                    modifiers={ {
                        highlighted: highlightedDay,
                        hasPhoto: hasPhoto
                    } }
                  />
              </div>
              <div className={ styles.uploadCnt }>
                  { isUploadAllow && <FileSelector onFilesSelect={ onSelectToUpload }>
                      <NormalSecondaryButton icon={ 'internet-cloud-upload' }
                                             text={ loc._(t`basic.upload.image`) }/>
                  </FileSelector> }
              </div>
          </div>

          { isLightBoxOpen &&
          <LightBox
            media={ mediaFiles }
            current={ currentMedia }
            onClose={ toggleLightBox }
            photoFact={ photoFact }
            onDelete={ onDelete }
            canDelete={ canDelete }
          />
          }
      </div>
    )
})
