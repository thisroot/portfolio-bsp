import React from "react"
import { FixedBox } from "./FixedBox"
import { Spinner } from "components/UI/Spinner"
import styles from './styles.scss'

interface IDataGridProps {
    xLabels: Array<React.ReactNode>
    yLabels: Array<React.ReactNode>
    data: Array<Array<any>>
    background: string | object
    height: number
    xLabelWidth?: number
    yLabelWidth: number
    unit: string
    yLabelTextAlign?: any
    displayYLabels: boolean
    onClick: (...any: any) => any
    cursor: string
    squares: boolean
    cellRender: (...any) => any
    cellStyle: (...any: any) => any
    title: (...any: any) => any
    pending: boolean
}


const DataGrid: React.FC<IDataGridProps> = ({
                                                xLabels,
                                                pending,
                                                yLabels,
                                                xLabelWidth,
                                                data,
                                                yLabelWidth,
                                                yLabelTextAlign,
                                                background,
                                                height,
                                                unit,
                                                displayYLabels = true,
                                                onClick = () => {
                                                },
                                                cursor = "",
                                                squares = false,
                                                cellRender,
                                                cellStyle,
                                                title = (value, unit) => (value || value === 0) && `${ value } ${ unit }`
                                            }) => {
    return (
      <div className={ styles.dataGrid }>
          { yLabels.map((y, yi) => {
            const max = Math.max(...data[yi])
            const min = Math.min(...data[yi])
            return (
            <div key={ yi } style={ { display: "flex" } }>
                <FixedBox width={ yLabelWidth }>
                    <div
                      style={ {
                          textAlign: yLabelTextAlign,
                          paddingRight: "5px",
                          width: `100%`
                      } }
                    >
                        { displayYLabels && y }
                    </div>
                </FixedBox>
                { xLabels.map((x, xi) => {
                    const value = data[yi][xi]
                    const style = Object.assign(
                      {
                          cursor: `${ cursor }`,
                          margin: "5px 5px 0 0",
                          height,
                          width: squares ? `${ height }px` : xLabelWidth,
                          flex: squares ? "none" : 1,
                          textAlign: "center",
                      },
                      cellStyle(background, value, min, max, data, xi, yi)
                    )
                    return (
                      <div
                        onClick={ onClick.bind(xi, yi) }
                        title={ title(value, unit, xi, yi) }
                        key={ `${ xi }_${ yi }` }
                        style={ style }
                      >
                          <div style={ { paddingTop: `${ height / 3.7 }px` } }>
                              { cellRender(value, x, y) }
                          </div>
                      </div>
                    )
                }) }
            </div>
          )}) }
          {pending && <div
            className={styles.dataGridSpinnerContainer}
            style={ {
              width: `calc(100% - 20px - ${yLabelWidth}px)`,
              left: `calc(${yLabelWidth}px + 20px)`
            } }
          >
              <Spinner/>
          </div>}
      </div>
    )
}

export { DataGrid }