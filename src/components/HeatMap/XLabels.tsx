import React from "react"
import { FixedBox } from "./FixedBox"

interface IXLabelsProps {
    labels: Array<React.ReactNode>
    labelsVisibility?: Array<boolean>
    width: number
    squares?: boolean
    height?: number
    yWidth?: number
    yLableTitle?: React.ReactNode
    xLabelsStyle?:string
}

const XLabels: React.FC<IXLabelsProps> = ({ labels, width, labelsVisibility = null, squares = false, height = 30, yWidth = 30, yLableTitle, xLabelsStyle }) => {
    return (
      <div style={ { display: "flex" } }>
          <FixedBox width={ yWidth }>{ yLableTitle && yLableTitle }</FixedBox>
          { labels.map((x, i) => (
            <div
              className={xLabelsStyle}
              key={ i }
              style={ {
                position: 'relative',
                  flex: squares ? "none" : 1,
                  textAlign: "center",
                  width: squares ? `${ height + 1 }px` : width,
                  visibility:
                    labelsVisibility && !labelsVisibility[i] ? "hidden" : "visible",
              } }
            >
                { x }
            </div>
          )) }
      </div>
    )
}

export { XLabels }