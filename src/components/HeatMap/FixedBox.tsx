import React from "react"

interface IFixedBoxProps {
    width: number
    children?: React.ReactNode
}



const FixedBox: React.FC<IFixedBoxProps> = ({ children, width }) => {
    return <div style={ { flex: `0 0 ${ width }px`, position: "relative" } }> { children } </div>
}

export { FixedBox }