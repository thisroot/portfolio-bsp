import React, { RefObject } from "react"
import { XLabels } from "./XLabels"
import { DataGrid } from "./DataGrid"
import styles from './styles.scss'
import { createPortal } from 'react-dom'

interface IHeatMapReportProps {
    xLabels: Array<React.ReactNode>
    yLabels: Array<React.ReactNode>
    yLabelTitle?: React.ReactNode
    data: Array<Array<any>>
    background?: string | object
    height?: number
    xLabelWidth?: number
    yLabelWidth?: number
    xLabelsStyle?: string
    xLabelsLocation?: 'top' | 'bottom'
    xLabelsVisibility?: Array<boolean>
    yLabelTextAlign?: string
    displayYLabels?: boolean
    unit?: string
    onClick?: (...any: any) => any
    squares?: boolean
    cellRender: (...any: any) => any
    cellStyle: (...any: any) => any
    title: (...any: any) => any
    pending: boolean,
    headerRef: RefObject<HTMLDivElement>
}

const HeatMap: React.FC<IHeatMapReportProps> = (
  {
      xLabels,
      yLabels,
      yLabelTitle,
      data,
      background = "#329fff",
      height = 30,
      xLabelWidth = 60,
      yLabelWidth = 40,
      xLabelsLocation = 'top',
      yLabelTextAlign = 'right',
      xLabelsVisibility = null,
      unit = "",
      displayYLabels = true,
      onClick = undefined,
      squares = false,
      cellRender = () => null,
      cellStyle = (background, value, min, max) => ({
          background,
          opacity: (value - min) / (max - min) || 0,
      }),
      title,
      pending,
      headerRef,
      xLabelsStyle
  }) => {
    let cursor = ""
    if (onClick !== undefined) {
        cursor = "pointer"
    }
    const xLabelsEle = (
      headerRef && headerRef.current && createPortal(
        <XLabels
          labels={ xLabels }
          width={ xLabelWidth }
          labelsVisibility={ xLabelsVisibility }
          height={ height }
          squares={ squares }
          yWidth={ yLabelWidth }
          yLableTitle={ yLabelTitle }
          xLabelsStyle={ xLabelsStyle }
        />,
        headerRef.current
        )
    )
    return (
      <div className={ styles.gridContainer }>
          { xLabelsLocation === "top" && xLabelsEle }
          <DataGrid
            { ...{
                xLabels,
                yLabels,
                data,
                background,
                height,
                xLabelWidth,
                yLabelWidth,
                yLabelTextAlign,
                unit,
                xLabelsLocation,
                displayYLabels,
                onClick,
                cursor,
                squares,
                cellRender,
                cellStyle,
                title,
                pending
            } }
          />
          { xLabelsLocation === "bottom" && xLabelsEle }
      </div>
    )
}

export { HeatMap }