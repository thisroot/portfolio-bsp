import React, { useCallback, useEffect, useState } from "react"
import { useInstance } from "react-ioc"
import { observer } from 'mobx-react-lite'
import cx from 'classnames'
import { AuthService } from "services/AuthService"
import { Icon } from "components/UI/Icon"
import { AppService } from "services/AppService"
//import { Search, SearchType } from "../UI/Search/Search"
import { Avatar } from "components/UI/Avatar"
import { getUserFullName, isProduction } from "utils/helpers"
import { Popover } from "components/UI/Popover"
import styles from "./styles.scss"
import { Button, DefaultButton } from "../UI/Button"
import { LocalisationService } from "../../services/LocalisationService"
import { loc } from "../../utils/i18n"
import { t } from "@lingui/macro"
import { RouterService } from "../../services/RouterService"
import { ROUTES } from "../../constants"

const Header = observer(() => {
    const auth = useInstance(AuthService)
    const app = useInstance(AppService)
    const local = useInstance(LocalisationService)
    const router = useInstance(RouterService)

    const user = getUserFullName(auth.currentUser, local.lang)

    const triggerForActionHideHeader = app.triggerForActionHideHeader
    const getIsHideHeader = useCallback(() => {
        if (triggerForActionHideHeader) return app.scrollOnTop + 76 > triggerForActionHideHeader
        return false
    }, [ app.scrollOnTop, triggerForActionHideHeader ])
    const [ isHideHeader, setIsHideHeader ] = useState(getIsHideHeader())

    useEffect(() => {
        setIsHideHeader(getIsHideHeader())
    }, [ getIsHideHeader ])

    const [ isPopoverOpen, togglePopover ] = useState(false)


    return (
      <header
        className={ cx(styles.header, { [styles.close]: app.isCloseLeftMenu, [styles.isHideHeader]: isHideHeader }) }>
          <div
            className={ styles.left }
          >
              <div className={ styles.toggleView } onClick={ app.toggleLeftMenu }>
                  <Icon name={ 'arrows-arrow-open' }/>
              </div>
              <span className={ styles.text }/>
          </div>
          <div
            className={ styles.right }
          >
              { auth.isAuthorized && (
                <div className={ styles.functionalityBox }>
                    {
                        // TODO: UNUSED_COMPONENT
                        /* <div
                          onClick={ toggleSearch }
                          className={ styles.search }
                        >
                            <Search
                              className={ styles.componentSearch }
                              items={ [] }
                              theme={ SearchType.line }
                              placeholder={ 'Search CSP' }
                              autoHide={ true }
                              isMinimized={ true }
                            />
                        </div>
                        <div className={ styles.notification }>
                            <Icon name="account-bell-notifications-normal"/>
                            {
                                <span className={ styles.signal }/>
                            }
                        </div> */ }
                    <Popover
                      onClickOutside={ () => togglePopover(false) }
                      isOpen={ isPopoverOpen }
                      position={ 'bottom' }
                      content={
                          <div className={ styles.menuContainer }>
                              <Button icon={ 'internet-settings' } className={ styles.menuBtn }
                                      onClick={ () => router.push(ROUTES.USER_PROFILE.PATH) }
                                      text={ loc._(t`routes.profile.name`) }/>
                              <DefaultButton icon={ 'account-sign-out-right' } className={ styles.menuBtn }
                                             onClick={ auth.logout }
                                             text={ loc._(t`basic.exit`) }/>
                              { !isProduction &&
                              <DefaultButton icon={ 'internet-slides-windows' } className={ styles.menuBtn }
                                             onClick={ app.changeTheme } text={ 'change theme' }/> }

                          </div>
                      }>
                        <div className={ styles.profile } onClick={ () => togglePopover(!isPopoverOpen) }>
                            <Avatar size={ 5 } url={ 'https://picsum.photos/seed/picsum/200/300' }/>
                            <span className={ styles.name }>
                          { user }
                        </span>
                        </div>
                    </Popover>
                </div>
              ) }
          </div>
      </header>)
})

export { Header }
