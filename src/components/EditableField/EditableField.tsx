import React, { useEffect, useRef, useState } from "react"
import Truncate from 'react-truncate'
import ContentEditable from "react-contenteditable"
import styles from './styles.scss'
import cx from 'classnames'
import { observer } from "mobx-react-lite"
import { clearTextFromNbsp, IFormField, NumberParser, placeCaretAtEnd, validationResult } from "../../utils/helpers"
import { isString, unescape } from 'lodash'
import DOMPurify from 'dompurify'
import { renderMarkdown } from "utils/render"

interface IEditableFieldProps {
    value?: string | number
    label: string
    lines?: number,
    type?: string
    isEditable?: boolean
    contentEditableSingleLine?: boolean
    isVisible?: boolean
    onDirty?: (any?: any) => void;
    validate?: ({ field }: IFormField) => validationResult;
    formatValue?: (value: string) => any
    formatRender?: (value: string) => any
    className?: string,
    centered?: boolean,
    placeholder?: string,
    onChange?: (value: any, label: string, isValid?: boolean) => void;
    onSave?: (value: any, label: string, isValid?: boolean) => any
    onBlur?: (value: any, label: string, isValid?: boolean) => any,
    title?: string | number
}


export const EditableField: React.FC<IEditableFieldProps> =
  observer(({
                value,
                label,
                type = 'text',
                lines = 1,
                onSave,
                isEditable: outerIsEditable = true,
                onDirty,
                validate,
                formatValue,
                formatRender,
                centered = true,
                className,
                placeholder = '---',
                onChange,
                onBlur,
                isVisible = true,
                contentEditableSingleLine = false,
                title
            }) => {


      const editableRef: React.MutableRefObject<HTMLDivElement> = useRef()
      const [ isEditable, setIsEditable ] = useState(outerIsEditable)
      const [ isEdit, setEdit ] = useState(false)
      const [ localText, setLocalText ] = useState<string>(clearTextFromNbsp(unescape(`${ value || placeholder }`)))
      const [ isValid, setValid ] = useState([ true, "" ] as validationResult)
      const parser = new NumberParser('ru')

      useEffect(() => setIsEditable(outerIsEditable), [ outerIsEditable ])
      useEffect(() => {
          if (isEdit) placeCaretAtEnd(editableRef.current)
      }, [ isEdit ])
      useEffect(() => {
          const res = clearTextFromNbsp(unescape(`${ value || '' }`))
          setLocalText(isEdit ? res : res || placeholder)
          res !== placeholder && validate && setValid(validate({ field: { value: res, label } }))
      }, [ value, label, validate, placeholder ])

      const toggle = (e?: any) => {
          e && e.stopPropagation()
          if (isEditable) {
              setEdit(!isEdit)
              if (localText === placeholder && !isEdit) {
                  setLocalText('')
              }
              if (localText === '' && isEdit) {
                  setLocalText('---')
              }
          }
      }

      const isMarkdown = [ 'link', 'md', 'html' ].includes(type)

      const formatRes = formatRender ? formatRender(localText) : localText

      return <div title={ `${ isValid[0] ? title : isValid[1] }` }
                  className={ cx(styles.fieldContainer, {
                      [styles.isNotEditable]: !isEditable,
                      [styles.invalid]: !isValid[0]
                  }, className) }>
          { !isEdit && isVisible && <div onClick={ toggle }
                                         className={ cx(styles.truncateContainer, { [styles.center]: centered }) }>
              { isMarkdown && <div dangerouslySetInnerHTML={ renderMarkdown(localText) }/> }
              { !isMarkdown &&
              <Truncate
                lines={ lines }> { formatRes === '' ? localText : formatRes }</Truncate> }
          </div> }
          {
              <ContentEditable
                innerRef={ editableRef }
                className={ cx(styles.editableContainer, {
                    [styles.singleLine]: contentEditableSingleLine,
                    [styles.noDisplay]: !isEdit,
                    [styles.center]: centered
                }) }


                html={ localText }
                onChange={ (e) => {
                    const clearVal = parser.parse(e.target.value)
                    setLocalText(clearVal)
                    const value = formatValue ? formatValue(clearVal) : clearVal
                    const isValid = validate ? validate({ field: { value, label } }) : [ true, "" ] as validationResult
                    setValid(isValid)
                    onDirty && onDirty()
                    const parsed = isString(value) ? DOMPurify.sanitize(`${ value }`.trim()) : value
                    onChange && onChange(parsed, label, isValid[0])
                    // onSave && onSave(parsed, label, isValid)
                    // placeCaretAtEnd(editableRef.current)
                } }
                onBlur={ (e) => {
                    toggle(e)
                    const clearVal = parser.parse(e.target.innerText)
                    setLocalText(e.target.innerText ? clearVal : placeholder)
                    const value = formatValue ? formatValue(clearVal) : clearVal
                    const isValid = validate ? validate({
                        field: {
                            value: clearVal,
                            label
                        }
                    }) : [ true, "" ] as validationResult
                    setValid(isValid)
                    const parsed = isString(value) ? DOMPurify.sanitize(`${ value }`.trim()) : value
                    onBlur && onBlur(parsed, label, isValid[0])
                    onSave && onSave(parsed, label, isValid[0])
                } }
              />
          }
      </div>
  })
