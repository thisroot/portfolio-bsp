import React, { useEffect, useState } from "react"
import ReactMapGL from 'react-map-gl'
import styles from './styles.scss'
import { useMeasure } from "react-use"
import { DEFAULT_POSITION, DEFAULT_ZOOM } from "../../constants/geocoding"


const GMapGL: React.FC<any> = () => {
    const token = 'pk.eyJ1IjoidGhpc3Jvb3QiLCJhIjoiY2s3eGdtcGkzMDcyczNvc2ZvNjBrdmRsbyJ9._NYVlYrGBxvsfpvjkj4XRA'

    const [ ref, { width, height } ] = useMeasure()

    const [ viewport, setViePort ] = useState({
        width,
        height,
        latitude: DEFAULT_POSITION.lat,
        longitude: DEFAULT_POSITION.lng,
        zoom: DEFAULT_ZOOM
    })

    useEffect(() => {
        setViePort(v => ({ ...v, width, height }))
    }, [ width, height ])

    return <div ref={ ref } className={ styles.mapsContainer }>
        <ReactMapGL { ...viewport }
                    mapboxApiAccessToken={ token }
                    onViewportChange={ (viewport) => setViePort({ ...viewport, width, height }) }/>
    </div>
}

export default GMapGL