import React from 'react'

import JsPdf, { jsPDFOptions } from 'jspdf'
import html2canvas from 'html2canvas'

export interface TReactToPDFProps {
    targetRef: any
    filename?: string
    x?: number
    y?: number
    options?: object
    scale?: number
    children: any
    onComplete?: () => any
}


class ReactToPdf extends React.PureComponent<TReactToPDFProps> {
    private targetRef
    private filename: string = this.props.filename || 'download.pdf'
    private x: number = this.props.x || 0
    private y: number = this.props.y || 0
    private options: object = this.props.options
    private onComplete = this.props.onComplete
    private scale = this.props.scale || 1

    constructor(props) {
        super(props)
        this.targetRef = React.createRef()
    }

    public toPdf = () => {
        const source = this.props.targetRef || this.targetRef
        const targetComponent = source.current || source

        if (!targetComponent) {
            throw new Error(
              'Target ref must be used or informed. See https://github.com/ivmarcos/react-to-pdf#usage.'
            )
        }

        html2canvas(targetComponent, {
            logging: false,
            useCORS: true,
            scale: this.scale
        }).then(canvas => {
            const imgData = canvas.toDataURL('image/png')
            const pdfOpt: jsPDFOptions = { ...this.options, format: [canvas.width + 150, canvas.height + 150], orientation: canvas.width > canvas.height ? 'l' : 'p' }
            const pdf = new JsPdf(pdfOpt)
            pdf.addImage(imgData, 'JPEG', this.x + 50, this.y + 50, canvas.width, canvas.height)
            pdf.save(this.filename)
            if (this.onComplete) this.onComplete()
        })
    }

    render() {
        const { children } = this.props
        return children({ toPdf: this.toPdf, targetRef: this.targetRef })
    }
}

export { ReactToPdf }
