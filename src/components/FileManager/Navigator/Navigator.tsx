import React from "react"
import { INavigatorProps } from "../interfaces"
import { observer } from "mobx-react-lite"
import { DropNavItem } from "../DropNavItem"
import styles from "./styles.scss"
import { Icon } from "../../UI/Icon"
import { get } from "lodash"
import cx from 'classnames'

const Navigator: React.FC<INavigatorProps> = observer(({ path, goTo, move }) => {

    const renderedPath = (path && path.length && path.length > 2) ? [ path[path.length - 2], path[path.length - 1] ] : path
    const renderer = []

    renderedPath && renderedPath.length > 0 && renderedPath.forEach((item, index) => {
        if (index == 0) {
            renderer.push(
              <DropNavItem key={ 'root' } index={ index } move={ move } entity={ null }>
                  <div onClick={ () => goTo() } className={ cx(styles.navItem, styles.root) }>
                      <Icon name={ 'account-home-small' }/>
                  </div>
              </DropNavItem>)
            path.length > 2 && renderer.push(
              <div key={ 'last' } className={ cx(styles.navItem, styles.last) }> { "..." } </div>)
        }
        renderer.push(
          <DropNavItem key={ index } index={ index } move={ move } entity={ item }>
              <div onClick={ () => index !== renderedPath.length - 1 && goTo(item) }
                   className={ cx(styles.navItem,
                     { [styles.last]: index === renderedPath.length - 1 }
                   ) }>{ get(item, 'name', "...") }
              </div>
          </DropNavItem>)
    })

    return <div className={ styles.navigatorContainer }>
        { renderer }
    </div>
})

export { Navigator }
