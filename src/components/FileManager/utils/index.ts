import { UPLOAD_RESTRICTIONS } from "../../../constants"

export function getFileTypeIcon(entity: any) {
    // 'files-folder-blank
    // 'files-folder-c'
    // 'files-doc-table'
    // 'files-doc-text'
    // 'files-doc-image-picture'
    // 'files-doc-blank

    if (entity.folder) {
        let folderImg = entity.children && entity.children.length > 0 ? 'files-folder-c' : 'files-folder-blank'

        return entity.isOver ? entity.isCurrentPlace ? folderImg : 'files-folder-add-plus' : folderImg
    } else {

        let fileImg = 'files-doc-blank'

        if (entity.children && entity.children.length > 0) {
            return entity.isOver ? 'files-folder-add-plus' : 'files-folder-c'
        }

        if (Object.keys(UPLOAD_RESTRICTIONS.SPREAD_SHEETS_MIME_TYPES).includes(entity.extension)) {
            fileImg = 'files-doc-table'
        }

        if (Object.keys(UPLOAD_RESTRICTIONS.DOCUMENTS_MIME_TYPES).includes(entity.extension)) {
            fileImg = 'files-doc-text'
        }

        if (Object.keys(UPLOAD_RESTRICTIONS.IMAGES_MIME_TYPES).includes(entity.extension)) {
            fileImg = 'files-doc-image-picture'
        }
        return entity.isOver ? entity.isCurrentPlace ? fileImg : 'signs-block-delete-stop' : fileImg
    }
}
