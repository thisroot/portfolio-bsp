import React, { useCallback, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import styles from './styles.scss'
import { DndProvider } from "react-dnd"
import { HTML5Backend } from "react-dnd-html5-backend"
import { DragItem, DropResult, IFileBrowserEntity, IFileBrowserRowProps, INavigatorProps } from "./interfaces"
import { ContextMenu } from "../UI/ContextMenu"
import { loc } from "../../utils/i18n"
import update from "immutability-helper"
import { t } from "@lingui/macro"
import { Button } from "../UI/Button"
import { useLocalStorage, useMeasure } from "react-use"
import { DropZone } from "../DropZone"
import { buildFileSelector } from "../../utils/helpers"
import { Search, SearchType } from "../UI/Search/Search"
import { FileModel } from "../../models/entities/FileModel"
import cx from 'classnames'
import { RowItem } from "./RowItem"
import { FileItem } from "./FileItem"
import { Navigator as Nav } from "./Navigator"
import { DragNDropItem } from "./DragNDropItem"
import { SpinnerOver } from "../UI/Spinner"

export interface IFileManagerProps {
    storageId: string
    selectedFile?: DragItem
    selectFile?: (item: DragItem) => void
    entities: Array<IFileBrowserEntity>
    changeOrder?: (dragIndex: number, hoverIndex: number) => Promise<void>
    move?: (item: DragItem, target: DropResult) => Promise<void>
    openFolder?: (item: IFileBrowserEntity) => Promise<void>
    path?: Array<FileModel>
    renderItem?: React.ComponentType<IFileBrowserRowProps>
    renderRow?: React.ComponentType<IFileBrowserRowProps>
    renderNavigator?: React.ComponentType<INavigatorProps>
    renderFilter?: React.ComponentType<any>
    renderLeftMenu?: React.ComponentType<any>
    initialized: boolean
    pending?: boolean
    listPending?: boolean
    userCanAddFolder?: boolean
    onSelectToUpload?: (e: Array<File>) => any,
    acceptedFiles?: Array<string>
    addFolder?: (item?: DragItem) => Promise<void>
    removeFile?: (item: DragItem) => Promise<void>
    downloadFile?: (item: DragItem) => Promise<void>
    onSearch?: (filter?: any) => any
    searchString?: string
    isFilterFill?: boolean
    goTo?: (file?: IFileBrowserEntity) => void
    updateFile?: (file: DragItem, value: string) => Promise<void>
    userCanUpdateFile?: boolean
    userCanWriteFile?: boolean
    userCanRemoveFile?: boolean
}

const FileManager: React.FC<IFileManagerProps> = observer((
  {
      entities,
      changeOrder,
      move,
      openFolder,
      renderFilter,
      initialized,
      pending,
      onSelectToUpload,
      acceptedFiles,
      addFolder,
      removeFile,
      downloadFile,
      onSearch,
      searchString,
      isFilterFill,
      renderLeftMenu,
      selectedFile = null,
      selectFile,
      storageId,
      userCanAddFolder = false,
      renderNavigator = Nav,
      path,
      goTo,
      userCanUpdateFile = false,
      updateFile,
      userCanWriteFile = false,
      renderRow = RowItem,
      renderItem = FileItem,
      userCanRemoveFile = false,
      listPending = false
  }) => {


    const canUpload = initialized && !pending && userCanWriteFile

    const [ cards, setCards ] = useState(entities)
    const [ isListMode, toggleListMode ] = useLocalStorage(storageId, true)

    const [ ref, { width } ] = useMeasure()
    const [ elWidth, setElWidth ] = useState(width)

    useEffect(() => {
        if (width && width > 100) {
            let x = 5
            switch (true) {
                case width >= 1150:
                    x = 7
                    break
                case width > 900 && width < 1150:
                    x = 6
                    break
                case width <= 900:
                default:
                    x = 5
                    break
            }
            setElWidth((width - x * 20) / x)
        }
    }, [ width ])

    useEffect(() => {
        setCards(entities)
    }, [ entities ])


    const onChangeOrder = useCallback(
      (dragIndex: number, hoverIndex: number) => {
          const dragCard = cards[dragIndex]

          setCards(update(cards, {
              $splice: [
                  [ dragIndex, 1 ],
                  [ hoverIndex, 0, dragCard ],
              ],
          }))
          changeOrder && changeOrder(dragIndex, hoverIndex)
      },
      [ cards ],
    )


    const onClickToUpload = useCallback((e) => buildFileSelector(e, onSelectToUpload, acceptedFiles), [ acceptedFiles ])
    const menuItems = []

    addFolder && userCanAddFolder && menuItems.push(
      {
          label: loc._(t`components.fileManager.addFolder`),
          onClick: async () => {
              await addFolder()
          },
          icon: 'files-folder-add-plus'
      })


    userCanWriteFile && menuItems.push({
          label: loc._(t`components.fileManager.addFile`),
          onClick: onClickToUpload,
          icon: 'files-doc-text-b'
      }
    )

    const Filter = renderFilter
    const LeftMenu = renderLeftMenu
    const Navigator = renderNavigator

    return <DndProvider backend={ HTML5Backend }>
        <div className={ styles.fileManagerContainer }>
            <div className={ styles.menuContainer }>
                <div className={ styles.left }>
                    { renderLeftMenu && <LeftMenu/> }
                    { renderNavigator && <Navigator path={ path } goTo={ goTo }/> }
                </div>
                <div className={ styles.right }>
                    { onSearch && <Search
                      className={ styles.search }
                      searchFields={ [] }
                      keyName={ '' }
                      items={ [] }
                      onSearch={ onSearch }
                      onReset={ () => onSearch() }
                      theme={ SearchType.line }
                      placeholder={ loc._(t`components.fileManager.search.placeholder`) }
                      autoHide={ true }
                      isMinimized={ true }
                      saveSelection
                      searchBtnStyle={ 'square' }
                    /> }
                    { renderFilter && <Filter/> }
                    <Button onClick={ () => toggleListMode(!isListMode) } outline withoutBorders
                            icon={ !isListMode ? 'grid-layout-k' : 'grid-layout-m' }/>
                </div>
            </div>
            <div id={ 'common-context' } className={ styles.contextContainer }>
                <div ref={ ref } className={ cx(styles.filesContainer, { [styles.list]: isListMode }) }>
                    { !pending && <>
                        <ContextMenu contextId={ 'common-context' } items={ menuItems }/>
                        { cards && cards.length > 0 ?
                          cards.map((item, index) => <DragNDropItem
                            RowRenderer={ renderRow }
                            isListMode={ isListMode }
                            style={ { width: isListMode ? '100%' : elWidth } }
                            move={ move }
                            key={ item.id }
                            changeOrder={ changeOrder ? onChangeOrder : null }
                            entity={ item }
                            index={ index }
                            onSelect={ selectFile }
                            onOpen={ openFolder }
                            isSelected={ selectedFile && selectedFile.id === item.id || false }
                            ItemRenderer={ renderItem }
                            onSelectToUpload={ userCanWriteFile && onClickToUpload }
                            addFolder={ userCanAddFolder && addFolder }
                            downloadItem={ downloadFile }
                            userCanUpdateFile={ userCanUpdateFile }
                            userCanWriteFile={ userCanWriteFile }
                            userCanRemoveFile={ userCanRemoveFile }
                            updateFile={ updateFile }
                            removeFile={ userCanRemoveFile && removeFile }
                          />) :
                          <div className={ styles.noItemsContainer }>
                              { canUpload && !searchString && !isFilterFill && !userCanAddFolder ?
                                <DropZone onFilesSelect={ onSelectToUpload } accept={ acceptedFiles }/> :
                                loc._(t`basic.noDataAvailable`)
                              }
                          </div>
                        }
                    </> }
                    {
                        <SpinnerOver pending={ listPending }/>
                    }
                </div>
            </div>
        </div>
    </DndProvider>
})


export {
    FileManager
}
