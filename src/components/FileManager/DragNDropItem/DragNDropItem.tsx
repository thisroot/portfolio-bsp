import React, { useRef, useState } from 'react'
import { DragPreviewImage, DragSourceMonitor, DropTargetMonitor, useDrag, useDrop, XYCoord } from 'react-dnd'
import cx from 'classnames'
import styles from './styles.scss'
import { DragItem, DropResult, IFileBrowserEntity, IFileBrowserRowProps, ItemTypes } from "../interfaces"
import { useClickAway } from 'react-use'
import { ContextMenu } from "components/UI/ContextMenu"
import { loc } from "utils/i18n"
import { t } from "@lingui/macro"

import fileImg from 'static/images/files-doc-blank.svg'
import folderImg from 'static/images/files-folder-dot.svg'
import { Document, FileModel } from "models/entities"


export interface IFileBrowserEntityProps {
    entity: IFileBrowserEntity
    index: number
    changeOrder: (dragIndex: number, hoverIndex: number) => void
    move?: (item: DragItem, target: DropResult) => void
    onOpen?: (item: DragItem) => void
    onSelect?: (item: DragItem) => void
    isSelected: boolean
    setIsOver?: (isOver: boolean) => void
    style: React.CSSProperties
    isListMode: boolean
    ItemRenderer: React.ComponentType<IFileBrowserRowProps>
    RowRenderer: React.ComponentType<IFileBrowserRowProps>
    onSelectToUpload?: (e) => any
    addFolder?: (item: DragItem) => Promise<void>
    removeFile?: (item: DragItem) => Promise<void>
    downloadItem?: (item: DragItem) => Promise<void>
    updateFile?: (file: DragItem, value: string) => Promise<void>
    userCanUpdateFile: boolean
    userCanWriteFile: boolean
    userCanRemoveFile?: boolean
}

const DragNDropItem: React.FC<IFileBrowserEntityProps> = (
  {
      entity,
      index,
      changeOrder,
      move,
      onOpen,
      onSelect,
      isSelected,
      style,
      isListMode,
      ItemRenderer,
      onSelectToUpload,
      addFolder,
      removeFile,
      downloadItem,
      updateFile,
      userCanWriteFile,
      userCanUpdateFile,
      RowRenderer,
      userCanRemoveFile = false
  }) => {

    Object.assign(entity, {
        type: ItemTypes.BOX, index
    })

    const item = entity as DragItem

    const ref = useRef<HTMLDivElement>(null)

    const [ canDropHere, setCanDropHere ] = useState(true)
    const [ isCurrentPlace, setCurrentPlace ] = useState(false)

    useClickAway(ref, () => isSelected && onSelect(null))

    const [ { isOver }, drop ] = useDrop({
        accept: ItemTypes.BOX,
        hover(item: DragItem, monitor: DropTargetMonitor) {

            if (!ref.current) {
                return
            }
            const dragIndex = item.index
            const hoverIndex = index

            setCanDropHere(!(entity.id !== item.id && !entity.folder))

            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                setCurrentPlace(true)
                return
            } else {
                setCurrentPlace(false)
            }

            // Determine rectangle on screen
            const hoverBoundingRect = ref.current && ref.current.getBoundingClientRect()

            // Get vertical middle
            const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
            const hoverMiddleX = (hoverBoundingRect.right - hoverBoundingRect.left) / 2


            // Determine mouse position
            const clientOffset = monitor.getClientOffset()

            // Get pixels to the top
            const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top
            const hoverClientX = (clientOffset as XYCoord).x - hoverBoundingRect.left

            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%

            if (
              (dragIndex < hoverIndex && ((hoverClientY > hoverMiddleY * 1.4) || (hoverClientX > hoverMiddleX * 1.4))) ||
              (dragIndex > hoverIndex && ((hoverClientY < hoverMiddleY * 0.6) || (hoverClientX < hoverMiddleX * 0.6)))
            ) {
                if (changeOrder) {
                    changeOrder(dragIndex, hoverIndex)
                    item.index = hoverIndex
                }
            }
        },
        drop: () => item,
        collect: (monitor: DropTargetMonitor) => ({
            // canDrop: monitor.canDrop(),
            // didDrop: monitor.didDrop(),
            isOver: monitor.isOver(),
            // target: monitor.getItem()
        }),
    })
    const [ { isDragging, canDrop }, drag, preview ] = useDrag({
        item,
        end(item: DragItem | undefined, monitor: DragSourceMonitor) {
            const dropResult = monitor.getDropResult()
            if (dropResult) {
                const file = dropResult.file ? new FileModel(dropResult.file as Document) : null
                if (item) {
                    const isDropAllowed = !file || (file.folder && item.id !== file.id)
                    if (isDropAllowed) {
                        move && move(item, file as DropResult)
                    }
                }
            }
        },
        collect: (monitor: DragSourceMonitor) => ({
            isDragging: monitor.isDragging(),
            isOver: monitor.canDrag(),
            canDrop: monitor.didDrop(),
        }),
    })
    drag(drop(ref))

    let menuItems = []
    if (!item.folder) {
        menuItems.push({
            label: loc._(t`components.fileManager.downloadFile`),
            onClick: () => downloadItem && downloadItem(item),
            icon: 'files-doc-download'
        })
        onSelectToUpload && menuItems.push({
            label: loc._(t`components.fileManager.addFile`),
            onClick: (e) => onSelectToUpload(e),
            icon: 'files-note-clip-attachment-b'
        })
    } else {
        menuItems.push({
            label: loc._(t`components.fileManager.openFolder`),
            onClick: async () => onOpen && onOpen(item),
            icon: 'files-folder-open'
        },)
        addFolder && menuItems.push({
            label: loc._(t`components.fileManager.addFolder`),
            onClick: () => addFolder(item),
            icon: 'files-folder-add-plus'
        })
    }

    removeFile && menuItems.push({
        label: loc._(t`components.fileManager.removeItem`),
        onClick: () => removeFile(item),
        icon: "account-trash-delete-bin"
    })

    const onClick = () => {
        isSelected ? !item.folder ? downloadItem(item) : onOpen(item) : onSelect(item)
    }

    return (
      <>
          <DragPreviewImage connect={ preview } src={ item.folder ? folderImg : fileImg }/>
          <div id={ `file-item-${ item.id }` } ref={ ref } className={ cx(styles.browserItem, {
              [styles.file]: !item.folder,
              [styles.dragging]: isDragging,
              [styles.canDrop]: canDrop,
              [styles.selected]: isSelected,
              [styles.cantDrop]: isOver && !canDropHere,
              [styles.canDrop]: isOver && canDropHere,
              [styles.current]: isOver && isCurrentPlace,
              [styles.listItem]: isListMode
          }) }
               style={ style }
          >
              <ContextMenu contextId={ `file-item-${ item.id }` } items={ menuItems }/>
              <div className={ styles.itemBody }>
                  <ItemRenderer
                    rowRenderer={ RowRenderer }
                    removeFile={ removeFile }
                    updateFile={ updateFile }
                    userCanWriteFile={ userCanWriteFile }
                    userCanUpdateFile={ userCanUpdateFile }
                    isDragging={ isDragging }
                    canDrop={ canDrop }
                    isSelected={ isSelected }
                    file={ item }
                    canDropHere={ canDropHere }
                    isCurrentPlace={ isCurrentPlace }
                    onClick={ onClick }
                    isListMode={ isListMode }
                    userCanRemoveFile={ userCanRemoveFile }
                    isOver={ isOver }/>
              </div>
          </div>
      </>
    )
}

export {
    DragNDropItem
}
