import React from "react"
import { IFileBrowserRowProps } from "../interfaces"
import { observer } from "mobx-react-lite"
import { getFileTypeIcon } from "../utils"
import styles from "./styles.scss"
import { Icon } from "components/UI/Icon"
import { Badge } from "components/UI/Badge"
import { Spinner } from "components/UI/Spinner"
import { EditableField } from "components/EditableField"
import dayjs from "dayjs"
import filesize from "filesize.js"
import { Button, BUTTON_TYPE } from "components/UI/Button"
import cx from 'classnames'
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)

const RowItem: React.FC<IFileBrowserRowProps> = observer((
  {
      file,
      isOver,
      canDropHere,
      isCurrentPlace,
      isSelected,
      updateFile,
      removeFile,
      userCanUpdateFile,
      userCanRemoveFile = false
  }) => {

    const iconName = getFileTypeIcon({ ...file.getSnapshot, isOver, canDropHere, isCurrentPlace })

    const fileCanUpdate = file.folder && isSelected
    const onSaveFile = async (e) => await updateFile(file, e)

    return <div className={ cx(styles.listBody, { [styles.filePending]: file.isProgressVisible }) }>
        <div className={ styles.left }>
            <div className={styles.fileType }>
                <Icon className={ styles.icon } name={ iconName }/>
            </div>
            <div className={ styles.fileIndex }>
                { !file.isProgressVisible && <Badge text={ file.index.toString() }/> }
                { file.isProgressVisible && <Spinner/> }
            </div>
            <div className={ 'p-90' }>
                <EditableField
                  centered={ false }
                  value={ file.name }
                  label={ file.name }
                  onSave={ onSaveFile }
                  isEditable={ userCanUpdateFile && fileCanUpdate }
                />
            </div>
        </div>
        <div className={ styles.right }>
            <div className={ 'p-50' }>
                { dayjs.utc(file.created).format('YYYY-MM-DD HH:mm') }
            </div>
            <div className={ 'p-20' }>
                { !file.folder ? filesize(file.size) : "" }
            </div>
            <div className={ 'p-20' }>
                { file.ext }
            </div>
            <div className={ styles.fileDelete }>
                {
                    userCanRemoveFile && !file.pending &&
                    <Button className={ styles.deleteBtn } onClick={ async (e) => {
                        e.stopPropagation()
                        await removeFile(file)
                    } }
                            buttonType={ BUTTON_TYPE.DANGER }
                            icon={ 'account-trash-delete-bin' }/>
                }
            </div>
        </div>
    </div>
})

export {
    RowItem
}
