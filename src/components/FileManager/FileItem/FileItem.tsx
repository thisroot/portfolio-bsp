import React from "react"
import { IFileBrowserRowProps } from "../interfaces"
import { observer } from "mobx-react-lite"
import styles from "./styles.scss"
import { Icon } from "../../UI/Icon"
import { getFileTypeIcon } from "../utils"
import { EditableField } from "../../EditableField"

const FileItem: React.FC<IFileBrowserRowProps> = observer((props) => {

    const {
        isListMode,
        file,
        isOver,
        canDropHere,
        isCurrentPlace,
        onClick,
        isSelected,
        updateFile,
        userCanUpdateFile,
        rowRenderer
    } = props

    const fileCanUpdate = file.folder && isSelected
    const Row = rowRenderer
    const onSaveFile = async (e) => await updateFile(file, e)

    return <div className={ styles.fileBody } onClick={ onClick }>
        { !isListMode &&
        <Icon name={ getFileTypeIcon({ ...file.getSnapshot, isOver, canDropHere, isCurrentPlace }) }/> }
        { isListMode &&
        <Row { ...props }/>
        }
        {
            !isListMode &&
            <div onClick={ (e) => e.stopPropagation() } className={ styles.itemTitle } title={ file.name }>
                <EditableField
                  isEditable={ userCanUpdateFile && fileCanUpdate }
                  value={ file.name }
                  label={ file.name }
                  onSave={ onSaveFile }
                  lines={3}
                />
            </div>
        }
    </div>
})

export {
    FileItem
}
