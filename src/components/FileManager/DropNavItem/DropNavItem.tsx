import React from "react"
import { INavItem, ItemTypes } from "../interfaces"
import { observer } from "mobx-react-lite"
import { useDrop } from "react-dnd"

const DropNavItem: React.FC<INavItem> = observer((
  {
      children,
      entity,
      index
  }) => {
    const [ , drop ] = useDrop({
        accept: ItemTypes.BOX,
        drop: () => entity,
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    })
    return React.cloneElement<INavItem>(children as React.ReactElement, { ref: drop, index })
})

export { DropNavItem }
