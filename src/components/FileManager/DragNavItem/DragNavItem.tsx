import React from "react"
import { DragItem, DropResult, INavItem, ItemTypes } from "../interfaces"
import { observer } from "mobx-react-lite"
import { DragSourceMonitor, useDrag } from "react-dnd"
import { FileModel } from "../../../models/entities/FileModel"
import { Document } from "../../../models/entities/File"

const DragNavItem: React.FC<INavItem> = observer(({ move, entity, children, index }) => {

    Object.assign(entity, {
        type: ItemTypes.BOX, index
    })

    const item = entity as DragItem

    const [ { isDragging, canDrop }, drag, preview ] = useDrag({
        item,
        end(item: DragItem | undefined, monitor: DragSourceMonitor) {
            const dropResult = monitor.getDropResult()
            if (dropResult && dropResult.file) {

                const file = new FileModel(dropResult.file as Document)
                if (item && dropResult && file) {
                    const isDropAllowed = file.folder && item.id !== file.id
                    if (isDropAllowed) {
                        move && move(item, file as DropResult)
                    }
                }
            }
        },
        collect: (monitor: DragSourceMonitor) => ({
            isDragging: monitor.isDragging(),
            isOver: monitor.canDrag(),
            canDrop: monitor.didDrop(),
        }),
    })

    return React.cloneElement(children as React.ReactElement, {
        ref: drag,
        isDragging,
        canDrop,
        preview
    })
})

export { DragNavItem }
