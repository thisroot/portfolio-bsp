import { FileModel } from "models/entities/FileModel"
import React from "react"

export interface IFileBrowserEntity extends FileModel {
}

export interface IFileBrowserRowProps {
    isOver: boolean
    isDragging: boolean
    canDrop: boolean
    isSelected: boolean
    canDropHere: boolean
    isCurrentPlace: boolean
    file: DragItem
    isListMode: boolean
    onClick: () => any
    updateFile?: (file: DragItem, value: string) => Promise<void>
    userCanUpdateFile: boolean
    userCanWriteFile: boolean
    userCanRemoveFile?: boolean
    rowRenderer: React.ComponentType<IFileBrowserRowProps>
    removeFile?: (item: DragItem) => Promise<void>
}

export interface INavigatorProps {
    path: Array<IFileBrowserEntity>
    goTo?: (file?: IFileBrowserEntity) => void
    move?: (item: DragItem, target: DropResult) => Promise<void>
}

export interface INavItem  {
    entity?: IFileBrowserEntity
    index?: number
    move?: (item: DragItem, target: DropResult) => Promise<void>
}

export interface DropResult extends FileModel {
    dropEffect: string
}

export const ItemTypes = {
    BOX: 'file',
    CARD: 'folder'
}

export interface DragItem extends IFileBrowserEntity {
    index: number
    type: string
}
