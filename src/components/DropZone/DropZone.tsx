import { useDropzone } from "react-dropzone"
import React from "react"
import styles from './styles.scss'
import { Icon } from "../UI/Icon"
import { PrimaryButton } from "../UI/Button"
import { loc } from "../../utils/i18n"
import { t, Trans } from "@lingui/macro"

export interface IDropZoneProps {
    onFilesSelect: (file: Array<File>) => void
    accept?: string | string[];
    disabled?: boolean
    disabledMessage?: string
}

export const DropZone: React.FC<IDropZoneProps> = ({ onFilesSelect, accept = 'image/*', disabled, disabledMessage = loc._(t`components.dropZone.disabled`)}) => {

    const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop: onFilesSelect, accept, disabled })

    return (
      <div className={ styles.dropZoneContainer } { ...getRootProps() }>
          <input { ...getInputProps() } />
          <Icon className={ styles.uploadIcon } name={ 'internet-cloud-upload' }
                text={ !isDragActive && !disabled ? loc._(t`components.dropZone.dragHere`) : '' }/>
          { !disabled ? !isDragActive && <span className={ styles.uploadText }>
              <Trans>basic.or</Trans>
          </span> : null
          }
          <span className={ styles.uploadText }>
              { !disabled ? isDragActive ?
                <Trans>components.dragZone.dropHere</Trans> :
                <PrimaryButton outline text={ loc._(t`components.dragZone.clickToSelect`) }/> : null
              }
              {
                  disabled && disabledMessage
              }
          </span>
      </div>
    )
}
