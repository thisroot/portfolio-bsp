import React, { useState, useRef, useEffect } from 'react'
import { get, isEmpty } from 'lodash'
import { observer } from 'mobx-react-lite'
import cx from 'classnames'
import { loc } from "utils/i18n"

import { Icon } from 'components/UI/Icon'
import { Accordion } from 'components/UI/Accordion'
import { NodeType } from 'constants/routes'
import { Link } from 'services/RouterService'

import styles from './styles.scss'

interface INavigationRoute {
  id: number;
  name: string;
  path?: string;
  type?: NodeType;
  icon?: string;
  children?: any;
  isActive?: boolean;
  perm?: string;
}

interface INavigationProps {
  fullPath: string;
  routes: Array<INavigationRoute>;
  setActiveRoute: () => void;
  onSelectRoute: (path: string) => void;
  isShorten?: boolean;
}
interface IRecursiveRendering {
  (
    branches: Array<INavigationRoute>,
    onSelectRoute: (path: string) => void,
  ): any;
}
interface IOtherNode {
  (
    node: INavigationRoute,
    onSelectRoute: (path: string) => void,
  ): any;
}
const GetGroupTitle:IOtherNode = (node, onSelectRoute) => {
  return (
    <div
      key={`${node.name}`}
      className={styles.group}
    >
      <span
        className={styles.gropeTitle}
      >
        {loc._(`${node.name}`)}
      </span>
      <div className={styles.list}>
        {RecursiveRendering(node.children, onSelectRoute)}
      </div>
    </div>
  );
};
const GetAccordion:IOtherNode = (node, onSelectRoute) => {
  const isNodeWithPath = get(node, 'path', false);
  const isNodeWithIcon = get(node, 'icon', false);
  const [isOpen, setIsOpen] = useState(false);

  const headerComponent = () => (
    <div
      className={cx({[styles.titleInner]: isNodeWithPath})}
    >
      {
        isNodeWithIcon &&
        <Icon
          className={ cx(isNodeWithIcon, styles.icon) }
          style={isNodeWithIcon}
          name={isNodeWithIcon}
        />
      }
      <span>{loc._(`${node.name}`)}</span>
    </div>
  );
  return (
    <Accordion
      key={`${isNodeWithPath}${node.name}`}
      headerContent={headerComponent()}
      arrow={'arrows-right'}
      onToggle={() => setIsOpen(!isOpen)}
      className={{
        accordion: cx(styles.accordion, {[styles.isOpen]: isOpen}),
        content: styles.accordionContent,
        header: styles.accordionHeader,
        arrowWrapper: styles.accordionArrowWrapper,
      }}
    >
      <div>
        {RecursiveRendering(node.children, onSelectRoute)}
      </div>
    </Accordion>
  );
};
const GetList:IOtherNode = (node, onSelectRoute) => {
  return (
    <div
      key={`${node.name}`}
      className={styles.list}
    >
      {RecursiveRendering(node.children, onSelectRoute)}
    </div>
  );
};
const GetMenuItem:IOtherNode = (node, onSelectRoute) => {
  const nodePath =  get(node, 'path', false);
  const isActive =  get(node, 'isActive', false);
  const isNodeIcon=  get(node, 'icon', false);

  return (
    <Link
      to={node.path}
      key={`${nodePath}${node.name}`}
      className={cx(styles.menuItem, {
        [styles.clickable]: nodePath,
        [styles.active]: isActive,
      })}
      onClick={() => {
        onSelectRoute(node.path);
      }}
    >
      <div className={styles.iconWrapper}>
        {
          isNodeIcon &&
          <Icon
            className={node.icon}
            name={node.icon}
          />
        }
      </div>
      <span
        className={styles.text}
      >
        {loc._(`${node.name}`)}
      </span>
    </Link>
  );
}
const GetNode: IOtherNode = (node, onSelectRoute) => {
  switch (node.type) {
    case NodeType.groupTitle:
      return GetGroupTitle(node, onSelectRoute);
    case NodeType.collapsible:
      return GetAccordion(node, onSelectRoute);
    case NodeType.list:
      return GetList(node, onSelectRoute);
    default:
      return GetMenuItem (node, onSelectRoute);
  }
};
const RecursiveRendering: IRecursiveRendering = (branches, onSelectRoute) => {
  if (get(branches, 'length', false)) {
    return branches.map((node) => {
      if (
          isEmpty(node.children) ||
          node.type === NodeType.collapsible ||
          node.type === NodeType.groupTitle ||
          node.type === NodeType.list
        ) {
        return GetNode(node, onSelectRoute);
      }

      return RecursiveRendering(node.children, onSelectRoute);
    });
  }

  return (
    <div className={styles.gropeTitle}>
      Нет элементов для отображения
    </div>
  )
};

const Navigation: React.FC<INavigationProps> = observer((props) => {
  const {
    fullPath,
    onSelectRoute,
    // setActiveRoute,
    routes,
    isShorten,
  } = props;

  const refLeftMenu = useRef<HTMLDivElement>(null);

  const [leftMenuInstance, setLeftMenuInstance] = useState<HTMLElement | null>(null);
  useEffect(() => {
    // setActiveRoute();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fullPath]);

  useEffect(() => {
    if (refLeftMenu) setLeftMenuInstance(refLeftMenu.current);

  }, [leftMenuInstance, refLeftMenu]);

  useEffect(() => {
    if (!leftMenuInstance) return;

    const currentItemActive: HTMLElement = leftMenuInstance.querySelector(`.${styles.active}`);

    if (!currentItemActive) return;
    const itemOffsetTop: number = currentItemActive.offsetTop;
    leftMenuInstance.style.setProperty('--position-indicator-of-the-active-element', `${itemOffsetTop}px`);
  }, [leftMenuInstance, routes]);


  useEffect(() => {
    if (!leftMenuInstance) return;

    const timeAnimationFromProps = getComputedStyle(leftMenuInstance).getPropertyValue('--time-transition');
    const timeAnimation = parseFloat(timeAnimationFromProps);
    leftMenuInstance.style.setProperty('--time-transition', '0');
    let animate = setTimeout(function tick(): void {
      const currentItemActive: HTMLElement = leftMenuInstance.querySelector(`.${styles.active}`);

      if (!currentItemActive) return;
      const itemOffsetTop: number = currentItemActive.offsetTop;

      requestAnimationFrame(() => {
        leftMenuInstance.style.setProperty('--position-indicator-of-the-active-element', `${itemOffsetTop}px`);
      });

      animate = setTimeout(tick, 8);
    }, 8);

    setTimeout(() => {
      clearTimeout(animate);
      leftMenuInstance.style.setProperty('--time-transition', timeAnimationFromProps);
    }, timeAnimation);
  }, [isShorten, leftMenuInstance]);

  const content: React.FC = !isEmpty(routes) && RecursiveRendering(routes, onSelectRoute);

  return (
    <div
      className={cx(styles.leftMenu, {[styles.isCollapsed]: isShorten})}
      ref={refLeftMenu}
    >
      {content}
    </div>
  );
});

export {
  Navigation,
  INavigationRoute,
  INavigationProps
}
