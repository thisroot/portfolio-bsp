import React from 'react'
import styles from './styles.scss'
import { Navigation, INavigationProps } from './Navigation'
import { useInstance } from "react-ioc"
import { AppService } from "services/AppService"
import { RouterService } from "services/RouterService"
import { observer } from 'mobx-react-lite'
import cx from 'classnames'
import { loc } from 'utils/i18n'
import { t } from '@lingui/macro'

import csp from '../../static/images/Logo.svg'
import rtlabs from '../../static/images/rtlabs_mono_h.svg'
import sakhalin from '../../static/images/LogoSakhalin.svg'
import { InfoButton } from "../UI"
import { PWAService } from "../../services"

const logo = process.env.APP_API_URL.match('rtlabs') ? rtlabs : process.env.APP_API_URL.match('sakhalin') ? sakhalin : csp

export interface ILeftMenuProps extends INavigationProps {
}

const LeftMenu: React.FC<ILeftMenuProps> = observer((props) => {

    const router = useInstance(RouterService)
    const app = useInstance(AppService)
    const pwa = useInstance(PWAService)

    const isHidden = app.isCloseLeftMenu

    return (
      <aside className={ cx(styles.nav, { [styles.shorten]: isHidden }) }>
          <div className={ styles.top }>
              <div className={ styles.logoWrapper }>
                  <img
                    src={ logo }
                    alt={ loc._(t`project.name`) }
                    className={ styles.logo }
                    onClick={ () => router.push('/') }
                  />
              </div>

          </div>
          <div className={ styles.middle }>
              <Navigation { ...props } isShorten={ isHidden }/>
              <div className={ styles.bottom }>
                  {
                      !pwa.isAPPInstalled && !isHidden &&
                      <InfoButton className={ styles.installBtn } onClick={ () => pwa.installApp() }
                                  icon={ 'social-windows' } text={ loc._(t`components.pwa.installApp`) }/>
                  }
              </div>
          </div>
      </aside>)
})

export {
    LeftMenu
}
