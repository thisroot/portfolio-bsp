import React, { useRef, useEffect } from "react"
import { observer } from "mobx-react-lite"
import cx from 'classnames'
import { useInstance } from "react-ioc"
import { AppService } from "services/AppService"
import styles from "./styles.scss"
import { Spinner } from 'components/UI'
import { get } from "lodash"

export interface PendingStickyPies {
    all?: boolean,
    top?: boolean,
    middle?: boolean
    content?: boolean
}

export interface IStickyScreenProps {
    theme?: string
    pending?: PendingStickyPies
    contentComponent?: any
    middleComponent?: any,
    topComponent?: any
}

export const StickyScreen: React.FC<IStickyScreenProps> = observer((
  {
      theme,
      pending,
      topComponent,
      contentComponent,
      middleComponent
  }) => {

    const appService = useInstance(AppService);
    
    const renderSpinner = () => <div className={ styles.spinnerWrapper }><Spinner className={ styles.spinner }/></div>

    const ref = useRef<HTMLDivElement>();
    
    useEffect(() => {
        appService.registerTriggerForHeader(ref);
    }, [appService, pending]);

    return <div className={ cx(styles.container, { [styles[theme]]: theme }) }>
        { get(pending, 'all') ? renderSpinner() :
          <>
              { topComponent &&
              <div className={ styles.topWrapper }>
                  { get(pending, 'top') ? renderSpinner() : topComponent() }
              </div>
              }
              { middleComponent &&
                  <div ref={ ref } className={ styles.middleWrapper }>
                      { get(pending, 'middle') ? renderSpinner() : middleComponent() }
                  </div>
              }
              { contentComponent &&
              <div className={ styles.contentWrapper }>
                  { get(pending, 'content') ? renderSpinner() : contentComponent() }
              </div>
              }
          </>
        }
    </div>
})