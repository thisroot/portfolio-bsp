import React from "react"
import styles from "./styles.scss"
import { BreadCrumbs } from "../../../services/RouterService"
import { FormatDateToQuarter } from "../../Widgets/FormatDateToQuarter/FormatDateToQuarter"
import ProgressBar from "../../UI/ProgressBar"
import Truncate from 'react-truncate'
import cx from 'classnames'
import { StatusDataLayers } from "../../../models/mst/StatusDataLayers"
import { loc } from "../../../utils/i18n"

export interface IStickyScreenHeaderProps {
    name: string
    status?: number
    percent?: number
    dateFinish?: string
}

export const StickyScreenHeader: React.FC<IStickyScreenHeaderProps> = ({ name, percent, dateFinish, status }) => {
    return <div id={ 'sticky-screen-header' } className={ cx(styles.topContainer) }>
        <div className={ styles.left }>
            <BreadCrumbs/>
            <h1><Truncate title={ name } lines={ 2 }>{ name }</Truncate></h1>
        </div>
        <div className={ styles.right }>
            {
                percent && <>
                    <div className={ styles.constructionDate }>
                        <FormatDateToQuarter className={ styles.quarter } date={ dateFinish }
                                             status={ status }/>
                    </div>
                    <div data-html2canvas-ignore className={ styles.progressBar }>
                        <ProgressBar status={ status } value={ percent }/>
                    </div>
                </>
            }

        </div>
    </div>
}

export interface IStickyScreenDoubleHeaderProps {
    dateFinish?: string
    name?: string
    statusData?: StatusDataLayers
}

export const StickyScreenDoubleHeader: React.FC<IStickyScreenDoubleHeaderProps> = ({ dateFinish, name, statusData }) => {

    return <div id={ 'sticky-screen-header' } className={ cx(styles.topContainer) }>
        <div className={ styles.left }>
            <BreadCrumbs/>
            <h1><Truncate title={ name } lines={ 2 }>{ name }</Truncate></h1>
        </div>
        <div className={ styles.right }>
            {
                statusData && <>
                    <div className={ styles.constructionDate }>
                        <FormatDateToQuarter className={ styles.quarter } date={ dateFinish }
                                            status={ statusData.foreman.status }/>
                    </div>
                    <div data-html2canvas-ignore className={ styles.progressBar }>
                        <ProgressBar title={ loc._('basic.report_fact_type.foreman') }
                                    status={ statusData.foreman.status }
                                    value={ statusData.foreman.volume_percent }/>
                    </div>
                    <div data-html2canvas-ignore className={ styles.progressBar }>
                        <ProgressBar title={ loc._('basic.report_fact_type.revisor') }
                                    status={ statusData.revisor.status }
                                    value={ statusData.revisor.volume_percent }/>
                    </div>
                </>
            }
        </div>
    </div>
}
