import { CSSProperties } from "react"
import { normalizeWidth, statusValidation } from './helpers'
import { IGetStyles } from 'screens/ObjectScreen/ObjectTabs/ReportCard/Reports/Guntt/interfaces'
import { isNumber } from 'lodash'
import { loc } from 'utils/i18n'
import { t } from '@lingui/macro'
import color from 'static/styles/colors.scss'
import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)

export const getStatusColor = (status: number = 0): string => {
    switch (status) {
        case 1:
            return color.danger500
        case 2:
            return color.warning500
        case 3:
            return color.success530
        case 4:
            return color.basic200
        case 5:
            return color.warning500
        case 6:
            return color.danger500
        case 7:
            return color.success530
        case 8:
            return color.success530
        default:
            return color.basic800
    }
}
export const getStatusColorWholeLine = (status: number = 0): string => {
    switch (status) {
        case 1:
            return color.danger200
        case 2:
            return color.warning200
        case 3:
            return color.success200
        case 4:
            return color.basic200
        case 5:
            return color.warning200
        case 6:
            return color.danger200
        case 7:
            return color.success200
        case 8:
            return color.success200
        default:
            return color.basic800
    }
}


export const getStyles: IGetStyles = (object, projectDates, ruler, roundValues) => {
    const { projectStartedAt, projectDuration } = projectDates
    const {
        date_start,
        date_finish,
        expenditure_percent = '',
        volume_percent,
        deviation_day,
        status,
    } = object

    const round = (num, flag) => flag ? Math.round(+num) : num

    const durationWidth: number = (new Date(date_finish).getTime() - new Date(date_start).getTime()) * 100 / projectDuration
    const duration: CSSProperties = statusValidation(status) ? { width: `${ normalizeWidth(durationWidth) }%` } : { width: '100%' }

    const beforeStartWidth: number = (new Date(date_start).getTime() - new Date(projectStartedAt).getTime()) * 100 / projectDuration
    const beforeStart: CSSProperties = statusValidation(status) ? { width: `${ normalizeWidth(beforeStartWidth) }%` } : { width: '0' }

    const factStyle: CSSProperties = statusValidation(status) ? { width: `${ normalizeWidth(+volume_percent) }%` } : {
        width: '100%',
        color: color.basicw
    }
    const factStatusColor: CSSProperties = { backgroundColor: getStatusColor(status) }
    const factStatusColorWholeLine: CSSProperties = { backgroundColor: getStatusColorWholeLine(status) }

    const labelPercent: string = isNumber(volume_percent) ? `${ round(volume_percent.toFixed(2), roundValues.round_percent) }\u00A0%` : '0%'
    const labelDays: string = (isNumber(deviation_day) ? `${ round(deviation_day, roundValues.round_day) }\u00A0` : '0\u00A0') + loc._(t`component.GGChart.days`)
    const labelCommon: string = statusValidation(status) ? `${ labelPercent }\u00A0(${ labelDays })` : loc._(t`component.GGChart.error`)

    const labelFinance: string = isNumber(expenditure_percent) ? `${ round(expenditure_percent.toFixed(2), roundValues.round_percent) }\u00A0%` : '0%'

    const completedFinance: CSSProperties = isNumber(expenditure_percent) ? { width: `${ normalizeWidth(expenditure_percent) }%` } : { width: '0' }

    const lastRuleTimeStamp: number = ruler && ruler.length > 0 ? new Date(dayjs(ruler.slice(-1)[0], 'DD-MM-YYYY').valueOf()).getTime() : projectStartedAt
    const rulerWidth: CSSProperties = { width: `${ normalizeWidth(100 * (lastRuleTimeStamp - projectStartedAt) / projectDuration) }%` }
    const finishRule: string = ruler && ruler.length && !((projectStartedAt + projectDuration) === lastRuleTimeStamp)
      ? dayjs(projectStartedAt + projectDuration).format(loc._(t`basic.dateFormat.DD.MM.YYYY`))
      : ''

    return {
        finishRule,
        rulerWidth,
        beforeStart,
        duration,
        completed: factStyle,
        color: factStatusColor,
        colorLine: factStatusColorWholeLine,
        label: labelCommon,
        labelFinance,
        completedFinance,
    }
}
