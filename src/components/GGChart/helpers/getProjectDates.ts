import { IProjectDates } from 'screens/ObjectScreen/ObjectTabs/ReportCard/Reports/Guntt/interfaces'
import { PlanFactReportResp } from 'services/ApiService/Interfaces/schedulePlanFact';

export const getProjectDates = (report: PlanFactReportResp): IProjectDates | undefined => {
    if (!report) {
        return undefined;
    }
    const projectStartedAt = new Date(report.date_start).getTime();
    const projectFinishedAt = new Date(report.date_finish).getTime();
    const projectDuration = projectFinishedAt - projectStartedAt;

    return { projectStartedAt, projectFinishedAt, projectDuration };
}
