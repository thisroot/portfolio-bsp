import { IProjectDates } from 'screens/ObjectScreen/ObjectTabs/ReportCard/Reports/Guntt/interfaces'
import { CSSProperties } from 'react'
import { normalizeWidth } from './helpers'

export const calculateCurrentDateLine = (projectDates: IProjectDates): CSSProperties | undefined => {
    if (!projectDates) {
        return undefined;
    }
    const durationFromStartToCurrentDate = new Date().getTime() - new Date(projectDates.projectStartedAt).getTime();
    const currentDateLineWidth = (durationFromStartToCurrentDate / projectDates.projectDuration) * 100;
    return durationFromStartToCurrentDate > 0
        ? { width: `${ normalizeWidth(currentDateLineWidth) }%` }
        : { display: "none" };
}
