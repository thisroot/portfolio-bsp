import dayjs from "dayjs"
import quarterOfYear from 'dayjs/plugin/quarterOfYear';
import { loc } from 'utils/i18n'
import { t } from '@lingui/macro'
dayjs.extend(quarterOfYear);

const getPlaceholder = (elements, customStep?: number) => {
    const start = elements ? new Date(elements.date_start) : new Date()
    const finish = elements ? new Date(elements.date_finish) : new Date()
    const distance = getDistance(start, finish)
    const step = customStep || getInterval(distance)

    switch (step) {
        case 7:
            return {
                step,
                intervalName: loc._(t`component.SchedulePlanFact.intervalName.week`)
            }
        case 30:
            return {
                step,
                intervalName: loc._(t`component.SchedulePlanFact.intervalName.month`)
            }
        case 90:
            return {
                step,
                intervalName: loc._(t`component.SchedulePlanFact.intervalName.quarter`)
            }
        default:
            return {
                step,
                intervalName: step.toString(),
                isCustomRange: true
            }
    }
}

const getDistance = (beginning, ending) => {
    const ms = ending - beginning
    const days = ms / (3600 * 24 * 1000)
    return days
}
const getInterval = distance => {
    return distance > 360 ? 90 : distance > 120 ? 30 : 7
}
const getRuler = (elements, step?: any) => {
    let st = step
    const start = new Date(elements.date_start)
    const finish = new Date(elements.date_finish)
    let current = new Date(elements.date_start)
    const days = getDistance(start, finish)

    if (step === null) {
        st = getInterval(days)
    }

    const stepsCount = days / st
    const result = []

    for (let i = 0; i <= stepsCount; i++) {
        switch (st) {
            case 30:
                current = i === 0 ? current : dayjs(current).add(1, 'month').toDate();
            break;
            case 90:
                current = i === 0 ? current : dayjs(current).add(1, 'quarter').toDate();
            break;
            default:
                current = i === 0 ? current : dayjs(current).add(st, 'day').toDate();
            break;
        }
        result.push(dayjs(current).format(loc._(t`basic.dateFormat.DD.MM.YYYY`)))
    }
    return result
}

export { getPlaceholder, getDistance, getInterval, getRuler }
