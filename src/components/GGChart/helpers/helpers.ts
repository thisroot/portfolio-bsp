export const normalizeWidth = (width: number): string => {
  const normWidth: number = width > 100 ? 100 : width;
  return (Math.floor(normWidth * 100) / 100).toFixed(2);
};

export const normalizeName = (name: string): string => {
  return name.replace(/[,.!?;:()][s]*/g, "$& ");
};

export const normalizeNumber = (num: number): number => {
  return num ? Math.floor(num * 100) / 100 : 0;
};

export const statusValidation = (status: number): boolean => {
  if (status < 1 && status > 4) return false;
  return true;
}
