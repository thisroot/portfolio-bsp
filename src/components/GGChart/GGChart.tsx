import { ProjectSettings, SchedulePlanFact } from "models/mst"
import React, { CSSProperties, DetailedHTMLProps, FC, useEffect, useRef, useState } from "react"
import { Icon } from "components/UI/Icon"
import { normalizeName } from "./helpers/helpers"
import { getStyles } from "./helpers/getStyles"
import { IDataModes, IProjectDates } from 'screens/ObjectScreen/ObjectTabs/ReportCard/Reports/Guntt/interfaces'
import dayjs from "dayjs"
import { Trans } from "@lingui/macro"
import cx from 'classnames'
import { observer } from "mobx-react-lite"
import styles from "components/GGChart/styles.scss"
import { GunttViewModel } from "screens/ObjectScreen/ObjectTabs/ReportCard/Reports/Guntt/GunntViewModel"
import { useInstance } from "react-ioc"
import { get, isNil, isNumber } from 'lodash'
import { loc } from "utils/i18n"
import { MEASURE_UNITS } from "constants/locale"
import { RouterService } from "../../services/RouterService"
import { ROUTES } from "../../constants"
import { formatCurrency } from "../../utils"

export interface IGGChartProps extends DetailedHTMLProps<any, any> {
    isFinanceShown: boolean;
    projectDates: IProjectDates;
    level: number;
    construct: SchedulePlanFact;
    dataMode: IDataModes;
    ruler?: string[];
    elemFromProps?: any;
    currentDateLineStyle: CSSProperties;
    expand?: boolean,
    toggleExpand?: (node: any) => void
    nodeKey: string,
    loadHistory?: (node: any) => void
    viewMode?: number
    roundValues?: ProjectSettings
    currentLocale?: string
}

export const GGChart: FC<IGGChartProps> = observer(({
                                                        isFinanceShown,
                                                        level,
                                                        elemFromProps,
                                                        construct,
                                                        dataMode,
                                                        ruler,
                                                        projectDates,
                                                        currentDateLineStyle,
                                                        expand = false,
                                                        nodeKey,
                                                        toggleExpand,
                                                        loadHistory,
                                                        viewMode = 1000,
                                                        roundValues,
                                                        currentLocale
                                                    }) => {
    const schedule = useInstance(GunttViewModel)
    const router = useInstance(RouterService)
    const refElem = useRef()

    useEffect(() => {
        const currentElem = elemFromProps || refElem
        const height = currentElem.current.offsetHeight
        currentElem.current.style.setProperty("--height", height + "px")
    })

    const [ isListOpen, toggleList ] = useState(false)

    const {
        date_start: startedAt,
        date_finish: finishedAt,
        volume_planned,
        volume_fact,
        volume_actual,
        children,
        name,
        job_unit,
        expenditure_planned: ep,
        expenditure_fact: ef,
        expenditure_actual: ea,
    } = construct

    const expenditure_planned = ep && ep / viewMode
    const expenditure_fact = ef && ef / viewMode
    const expenditure_actual = ea && ea / viewMode

    const styleLeft = { paddingLeft: 15 * level + "px" }
    const cs = getStyles(construct, projectDates, ruler, roundValues)

    const renderHistory = (construct, isOpen) => {
        isOpen && console.log(construct)
        return null
    }


    const round = (num, flag) => flag ? Math.round(+num) : num.toFixed(viewMode === 1 ? 3 : 5)
    const roundFinancial = (num, flag) => flag ? formatCurrency(Math.round(+num)) : formatCurrency(num.toFixed(viewMode === 1 ? 2 : 2))
    const normalizeNumber = (num: number): string => num ? formatCurrency(Math.floor(num * 100) / 100) : '0';
    const roundTittle = (num) => isNil(num) ? 0 : +num.toFixed(5)
    const volume = volume_fact + (isNil(volume_actual) ? 0 : volume_actual)
    const exp = expenditure_fact + (isNil(expenditure_actual) ? 0 : expenditure_actual)

    const renderDates = () => (
      <div className={ cx(styles.element_title_beginning_ending, { [styles.finMode]: isFinanceShown }) }>
          <div>{ startedAt && dayjs(startedAt).format(schedule.dateMask) }</div>
          <div>{ finishedAt && dayjs(finishedAt).format(schedule.dateMask) }</div>
      </div>
    )

    const renderPlanFact = () => (
      <div className={ cx(styles.element_title_plan_fact, { [styles.finMode]: isFinanceShown }) }>
          <div>
              <div className={ styles.volumes }
                   title={ volume_planned }>{ volume_planned === null || !hasNotChildren ? '' : normalizeNumber(round(volume_planned, roundValues.round_value)) }</div>
              { isFinanceShown
                ? <div title={ `${ roundTittle(expenditure_planned) }` } className={ styles.isFinanceShown }>
                    { expenditure_planned === null ? '' : roundFinancial(expenditure_planned, roundValues.round_expenditure) }
                </div>
                : null }
          </div>
          <div>
              <div className={ styles.volumes }
                   title={ volume }>{
                  children.length === 0 &&
                  normalizeNumber(round(volume, roundValues.round_value))
              }
              </div>
              { isFinanceShown
                ?
                <div
                  title={ `${ roundTittle(expenditure_fact + (isNil(expenditure_actual) ? 0 : expenditure_actual)) }` }
                  className={ styles.isFinanceShown }>
                    { expenditure_fact === null ? '' :
                      roundFinancial(exp, roundValues.round_expenditure) }
                </div>
                : null }
          </div>
      </div>
    )

    const renderTitle = () => `${ normalizeName(name) }  ${ MEASURE_UNITS[job_unit] ? `, ${ loc._(MEASURE_UNITS[job_unit]) }` : '' }`

    const renderPopUpInfoBlock = () => (
      <div data-html2canvas-ignore="true" className={ cx(styles.element_popup, 'popup') }>
          <div>
              <span><Trans>basic.dateStart</Trans>: </span>
              <span>{ dayjs(startedAt).format(schedule.dateMask) }</span>
          </div>
          <div>
              <span><Trans>basic.dateEnd</Trans>: </span>
              <span>{ dayjs(finishedAt).format(schedule.dateMask) }</span>
          </div>
          { isNumber(volume_planned) && !children.length ? (
            <div>
                <span><Trans>basic.plan</Trans>: </span>
                <span>{ normalizeNumber(volume_planned) }</span>
            </div>
          ) : null }
          { isNumber(volume_fact) && !children.length ? (
            <div>
                <span><Trans>basic.fact</Trans>: </span>
                <span>{ normalizeNumber(volume_fact + (isNil(volume_actual) ? 0 : volume_actual)) }</span>
            </div>
          ) : null }
          { isNumber(expenditure_planned) && !children.length && isFinanceShown ? (
            <div>
                <span><Trans>basic.planFinance</Trans>: </span>
                <span>{ normalizeNumber(expenditure_planned) }</span>
            </div>
          ) : null }
          { isNumber(expenditure_fact) && !children.length && isFinanceShown ? (
            <div>
                <span><Trans>basic.factFinance</Trans>: </span>
                <span>{ normalizeNumber(expenditure_fact + (isNil(expenditure_actual) ? 0 : expenditure_actual)) }</span>
            </div>
          ) : null }
      </div>
    )

    const hasNotChildren = (!children || (children && children.length === 0))

    return (
      <div className={ cx(styles.graphic_gannt_table_content, { [styles.pointer]: hasNotChildren }) }
           onClick={ (e) => {
               e.stopPropagation()
               if (hasNotChildren) {
                   router.push({
                       pathname: ROUTES.LOGS.PATH,
                       search: `?object_id=${ get(construct, 'object_id.object_id') }&schedule_id=${ get(construct, 'schedule_id') }&project_id=${ get(construct, 'object_id.project_id.project_id') }`,
                       hash: '#fact_values'
                   })
               } else {
                   toggleList(!isListOpen)
               }
           } }>
          <div className={ styles.element } ref={ refElem }>
              <div className={ cx('gunnt-node', styles.element_content) }>
                  <div className={ styles.element_title }>
                      <div className={ styles.element_title_name }>
                          <div
                            onClick={ (e) => {
                                e.stopPropagation()
                                children && children.length && toggleExpand(construct)
                            } }
                            className={ styles.element_open }
                            style={ styleLeft }
                          >
                              { children && children.length
                                ? <Icon name={ expand ? "signs-minus" : "signs-plus-add" }/>
                                : null }
                          </div>
                          { renderTitle() }
                      </div>
                      { renderDates() }
                      { renderPlanFact() }
                  </div>
                  <div className={ styles.element_graph }>
                      <div className={ styles.element_ruler } style={ cs.rulerWidth }>
                          { ruler && ruler.map(item => (
                            <span className={ styles.element_step } key={ item }/>
                          ))
                          }
                      </div>
                      <div className={ styles.current_date } style={ currentDateLineStyle }/>
                      <div className={ styles.graph_lines }>
                          <div className={ styles.graph_line_physical }>
                              <div
                                className={ cx(styles.graph_line_grey, { [styles.isFinanceShown]: isFinanceShown }) }
                                style={ cs.beforeStart }
                              />
                              <div
                                className={ cx(styles.graph_line, { [styles.isFinanceShown]: isFinanceShown }) }
                                style={ { ...cs.duration, ...cs.colorLine } }
                              >
                                  <div
                                    className={ cx(styles.graph_line_fact, { [styles.isFinanceShown]: isFinanceShown }) }
                                    style={ { ...cs.completed, ...cs.color } }
                                  >
                                      { cs.label }
                                  </div>
                              </div>
                          </div>
                          { isFinanceShown
                            ? (
                              <div className={ styles.graph_line_financial }>
                                  <div className={ styles.graph_line_grey } style={ cs.beforeStart }/>
                                  <div className={ cx(styles.graph_line) }
                                       style={ { ...cs.duration, ...cs.colorLine } }>
                                      <div className={ cx(styles.graph_line_fact, styles.isFinanceShown) }
                                           style={ { ...cs.completedFinance, ...cs.color } }>
                                          { cs.labelFinance }
                                      </div>
                                  </div>
                              </div>
                            ) : null }
                      </div>
                      { cs.finishRule && (
                        <div className={ styles.finishRule }>
                          <span className={ styles.element_step } key={ cs.finishRule }>

                          </span>
                        </div>
                      ) }
                      { renderPopUpInfoBlock() }
                  </div>
              </div>
              { expand ? (
                <>
                    { children && children.length > 0
                      ? children.map(child => (
                        <GGChart
                          currentLocale={ currentLocale }
                          isFinanceShown={ isFinanceShown }
                          nodeKey={ nodeKey }
                          key={ `schedule-${ level }-${ child.schedule_id }-${ viewMode }` }
                          projectDates={ projectDates }
                          level={ level + 1 }
                          construct={ child }
                          dataMode={ dataMode }
                          elemFromProps={ level === 0 ? refElem : elemFromProps }
                          currentDateLineStyle={ currentDateLineStyle }
                          toggleExpand={ toggleExpand }
                          expand={ child.expand }
                          loadHistory={ loadHistory }
                          viewMode={ viewMode }
                          ruler={ ruler }
                          roundValues={ roundValues }
                        />
                      ))
                      : null }
                </>
              ) : null }
              {
                  (!children || (children && children.length === 0)) &&
                  <div className={ cx(styles.historyList, { [styles.opened]: isListOpen }) }>
                      {
                          renderHistory(construct, isListOpen)
                      }
                  </div>
              }
          </div>
      </div>
    )
})
