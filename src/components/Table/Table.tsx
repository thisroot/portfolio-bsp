import React from 'react'
import styles from './styles.scss'
import { isArray } from "lodash"
import { loc } from "../../utils/i18n"
import { t } from "@lingui/macro"
import { observer } from "mobx-react-lite"
import { LightBox } from 'components/UI/LightBox'
import cx from 'classnames'

export interface Columns {
    [key: string]: any
}

export interface Row {
    id?: any
    [key: string]: any
}

export interface ITableClassificationProps {
    upperHeader?: Columns // если надо отобразить объединение столбцов в группы, вес группы должен быть равен сумме весов объединяющихся колонок
    rows: Array<Row>
    columns: Columns
    hidden?: Array<string>
    isLightBoxOpened?: boolean
    toggleLightBox?: any
    media?: any
    markFirstColumn?: boolean // если true, то первая колонка выделяется стандартными стилями
    isGridNeeded?: boolean // если true, то будет высчитываться и отрисовывапться вертикальная сетка между колокнами
    rowClassName?: any; // позволяет расширять стандартные классы для строк
    colClassName?: any; // позволяет расширять стандартные классы для колонок
    onRowClick?: any;
    idField?: string; // название поля с id строки; по умолчанию = 'id'
}

const Table: React.FC<ITableClassificationProps> = observer(({
    upperHeader,
    columns= {},
    rows = [],
    hidden= [],
    isLightBoxOpened = false,
    toggleLightBox = () => {},
    media,
    markFirstColumn = true,
    isGridNeeded = false,
    rowClassName = '',
    colClassName = '',
    onRowClick = null,
    idField = 'id'
}) => {
    const colNames = Object.keys(columns)
    const defaultStyle = { flex: '1 1' }

    const renderUpperHeader = () => {
        return (
            <div className={ styles.upperHeader }>
                {
                    Object.entries(upperHeader).filter(([ , item]) => !hidden.includes(item)).map(([key, item], index) => {
                        const name = isArray(item) ? item[0] : item
                        const style = isArray(item) ? { width: `${ item[1] as unknown as number * 100 }%` } : defaultStyle
                        return (
                            <div
                                style={ style }
                                key={ `upperHeader-${key}-${name}` }
                                className={ cx(styles.col, colClassName, {
                                    [styles.first]: index === 0 && markFirstColumn,
                                    [styles.isGridNeeded]: isGridNeeded,
                                }) }>
                                    { name }
                                </div>
                        )
                    })
                }
            </div>
        )
    }

    return (
        <>
            <div className={ styles.tableClassificationContainer }>
                { rows && rows.length > 0
                    ? <>
                        { upperHeader && renderUpperHeader() }
                        <div className={ styles.header }>
                            {
                                colNames.filter(item => !hidden.includes(item)).map((item, index) => {
                                    const name = isArray(columns[item]) ? columns[item][0] : columns[item]
                                    const style = isArray(columns[item]) ? { width: `${ columns[item][1] as unknown as number * 100 }%` } : defaultStyle
                                    return (
                                        <div
                                            style={ style }
                                            key={ `${name}-${item}-${index}` }
                                            className={ cx(styles.col, colClassName, {
                                                [styles.first]: index === 0 && markFirstColumn,
                                                [styles.isGridNeeded]: isGridNeeded,
                                            })
                                            }>
                                                { name }
                                            </div>
                                    )
                                })
                            }
                        </div>
                        <div className={ styles.rows }>
                            {
                                rows.map(row => {
                                    return <div key={ row[idField] } className={ cx(styles.row, rowClassName) } onClick={ onRowClick ? (e) => onRowClick(e, row) : null }> {
                                        colNames.filter(item => !hidden.includes(item)).map((item, index) => {
                                            const name = isArray(columns[item]) ? columns[item][0] : columns[item]
                                            const style = isArray(columns[item]) ? { width: `${ columns[item][1] as unknown as number * 100 }%` } : defaultStyle
                                            const cellStyle = isArray(row[item]) ? row[item][1] : {}
                                            const cellName = isArray(row[item]) ? row[item][0] : row[item]
                                            return (
                                                <div
                                                    style={ style }
                                                    key={ `${name}-${item}-${row[idField]}` }
                                                    className={ cx( styles.col, colClassName, {
                                                        [styles.link]: item === 'photo',
                                                        [styles.first]: index === 0 && markFirstColumn,
                                                        [styles.isGridNeeded]: isGridNeeded,
                                                    } ) }
                                                >
                                                    <span style={ cellStyle }>{ cellName }</span>
                                                </div>
                                            )
                                        })
                                    }
                                    </div>
                                }) 
                            }
                        </div>
                    </>
                    : (
                        <div className={ styles.noItemsContainer }>
                            { loc._(t`basic.noDataAvailable`) }
                        </div>
                    )
                }
            </div>
            { isLightBoxOpened && <LightBox media={ media } onClose={ toggleLightBox } /> }
        </>
    )
})

export {
    Table
}