import React from "react"
import styles from './styles.scss'
import noItemsImg from 'static/images/noItems.svg'
import { t } from '@lingui/macro'
import { loc } from "../../utils/i18n"
import { observer } from 'mobx-react-lite'

interface INoResultContainerProps {
    image?: React.ReactNode
    name?: React.ReactNode
    tizer?: React.ReactNode
    isFilterDefault?: boolean
}

const NoResultContainer: React.FC<INoResultContainerProps> = observer((
  {
      image,
      name,
      tizer,
      isFilterDefault = true
  }) => {

    let i, n, s
    if (!isFilterDefault) {
        i = image || noItemsImg
        n = name || loc._(t`components.noResultContainer.name`)
        s = tizer || loc._(t`components.noResultContainer.tizer`)
    } else {
        i = image || noItemsImg
        n = name || loc._(t`components.noResultContainer.noFilled.name`)
        s = tizer || loc._(t`components.noResultContainer.noFilled.tizer`)
    }

    return <div className={ styles.noResultContainer }>
        <img alt={ n } className={ styles.image } src={ i }/>
        <h4 className={ styles.name }>{ n }</h4>
        <h5 className={ styles.tizer }>{ s }</h5>
    </div>
})

export {
    NoResultContainer
}