import { TreeItem } from "react-sortable-tree"

export interface INodeProps {
    node: TreeItem,
    canDrag: boolean,
    path: (string | number)[]
    onUploadBtnClick?: (node: any) => Promise<void>
    isUploadAllow?: boolean
}
