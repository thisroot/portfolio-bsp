import React from 'react'
import { isDescendant, NodeRendererProps } from "react-sortable-tree"
import { observer } from 'mobx-react-lite'
import cx from 'classnames'
import { ScheduleNodeBody } from "./ScheduleNodeBody"
import { Icon } from "../../UI/Icon"
import styles from './styles.scss'
import { ScheduleNodeActualValuesBody } from "./ScheduleNodeActualValuesBody"
import { get } from 'lodash'

export enum SCHEDULE_NODE_TYPE {
    SHEDULE_NODE = 'schedule_node',
    ACTUAL_VALUES_FORM = 'actual_values_form'
}

export const ScheduleNode: React.FC<NodeRendererProps> =
  observer(({
                scaffoldBlockPxWidth,
                toggleChildrenVisibility = null,
                connectDragPreview,
                connectDragSource,
                isDragging,
                canDrop = false,
                canDrag = false,
                node,
                title = null,
                subtitle = null,
                draggedNode = null,
                path,
                treeIndex,
                isSearchMatch = false,
                isSearchFocus = false,
                buttons = [],
                className = '',
                style = {},
                didDrop,
                treeId,
                isOver, // Not needed, but preserved for other renderers
                parentNode = null, // Needed for dndManager
                rowDirection = 'ltr',
                ...otherProps
            }) => {

      const type = get(otherProps, 'type')
      const onSave = get(otherProps, 'onSave')
      const onNodeClick = get(otherProps, 'onNodeClick')
      const addFileToFactValues = get(otherProps, 'addFileToFactValues')
      const isUploadAllow = get(otherProps, 'isUploadAllow')
      const onUploadBtnClick = get(otherProps, 'onUploadBtnClick')
      const isAllowWriteEmployees = get(otherProps, 'isAllowWriteEmployees')

      const rowDirectionClass = rowDirection === 'rtl' ? 'rst__rtl' : null

      const handle = canDrag ? typeof node.children === 'function' && node.expanded ? (
        <div className="rst__loadingHandle">
            <div className="rst__loadingCircle">
                { [ ...new Array(12) ].map((_, index) => (
                  <div key={ index } className={ cx('rst__loadingCirclePoint', rowDirectionClass) }/>))
                }
            </div>
        </div>
      ) : connectDragSource(<div className="rst__moveHandle"/>, { dropEffect: 'copy', }) : undefined

      const isDraggedDescendant = draggedNode && isDescendant(draggedNode, node)
      const isLandingPadActive = !didDrop && isDragging
      const buttonStyle = rowDirection === 'rtl' ? { right: -0.5 * scaffoldBlockPxWidth } : { left: -0.5 * scaffoldBlockPxWidth }
      const wrapperStyle = cx('rst__row', isLandingPadActive && 'rst__rowLandingPad', isLandingPadActive && !canDrop && 'rst__rowCancelPad',
        isSearchMatch && 'rst__rowSearchMatch', isSearchFocus && 'rst__rowSearchFocus', rowDirectionClass, className)

      const level = path.map(() => 'l').join('')

      return (
        <div id={ path.join('') } style={ { height: '100%' } }>
            { toggleChildrenVisibility && node.children &&
            (node.children.length > 0 || typeof node.children === 'function') && (
              <div>
                  <div
                    aria-label={ node.expanded ? 'Collapse' : 'Expand' }
                    className={ cx(node.expanded ? 'rst__collapseButton' : 'rst__expandButton', rowDirectionClass, level) }
                    style={ buttonStyle }
                    onClick={ () => toggleChildrenVisibility({ node, path, treeIndex })
                    }
                  ><Icon className={ cx(styles.toggleBtn, styles[level]) }
                         name={ node.expanded ? 'signs-minus' : 'signs-plus-add' }/></div>
                  { node.expanded && !isDragging && (
                    <div style={ { width: scaffoldBlockPxWidth } }
                         className={ cx('rst__lineChildren', rowDirectionClass) }/>
                  ) }
              </div>
            ) }

            <div className={ cx('rst__rowWrapper', rowDirectionClass) }>
                {/* Set the row preview to be used during drag and drop */ }
                {/* { connectDragPreview() } */ }
                <div className={ wrapperStyle } style={ { opacity: isDraggedDescendant ? 0.5 : 1, ...style, } }>
                    { handle }
                    <div
                      className={ cx('rst__rowContents', !canDrag && 'rst__rowContentsDragDisabled', rowDirectionClass) }>
                        <div className={ cx('rst__rowLabel', rowDirectionClass) }>
                            <span className={ 'rst__rowTitle' }>
                                { type === SCHEDULE_NODE_TYPE.SHEDULE_NODE &&
                                <ScheduleNodeBody onUploadBtnClick={ onUploadBtnClick } node={ node }
                                                  canDrag={ canDrag } path={ path }
                                                  isUploadAllow={ isUploadAllow }
                                /> }
                                { type === SCHEDULE_NODE_TYPE.ACTUAL_VALUES_FORM &&
                                <ScheduleNodeActualValuesBody addFileToFactValues={ addFileToFactValues }
                                                              onNodeClick={ onNodeClick } onSave={ onSave }
                                                              node={ node }
                                                              isUploadAllow={ isUploadAllow }
                                                              isAllowWriteEmployees={isAllowWriteEmployees}
                                                              canDrag={ canDrag } path={ path }/> }
                            </span>
                        </div>
                        <div className="rst__rowToolbar">
                            { buttons.map((btn, index) => (
                              <div key={ index } className="rst__toolbarButton">{ btn }</div>)) }
                        </div>
                    </div>
                </div>
            </div>
        </div>)
  })
