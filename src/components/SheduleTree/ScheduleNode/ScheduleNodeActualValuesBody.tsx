import React from "react"
import { observer } from "mobx-react-lite"
import { get, isEmpty, isNull } from "lodash"
import { onlyNumbers, parseMeasureUnit } from "utils/helpers"
import { EditableField, } from "components"
import cx from 'classnames'
import styles from "./styles.scss"
import att from './attachment.scss'
import { INodeProps } from "./interfaces"
import { Button } from "components/UI/Button"
import Truncate from 'react-truncate'

export interface IScheduleNodeActualValuesProps extends INodeProps {
    onSave?: any
    onNodeClick?: any,
    isAllowWriteEmployees: boolean
}


export const ScheduleNodeActualValuesBody: React.FC<IScheduleNodeActualValuesProps> = observer((
  {
      node,
      onSave,
      onNodeClick,
      isUploadAllow,
      isAllowWriteEmployees
  }) => {

    const hasChilds = !!get(node, 'children.length')

    const jobUnit = get(node, 'job_unit')
    const nameMeasure = get(node, 'job_id.job_unit', null)

    const jobMeasure = isNull(jobUnit)
      ? parseMeasureUnit(nameMeasure)
      : parseMeasureUnit(jobUnit)

    return <div id={ node.schedule_id }
                className={ cx(styles.nodeContainer, { [styles.notEditableNode]: isEmpty(node.job_id) }) }>
        <div className={ cx(styles.nodeCol, styles.titleCol) }>
            <Truncate lines={ 3 }> { `${ node.schedule_id } - ${ node.name }` } </Truncate>
        </div>
        <div className={ styles.nodeCol }>
            { jobMeasure }
        </div>
        <div className={ styles.nodeCol }>
            <EditableField label={ 'volume' }
                           value={ node.volume }
                           isEditable={ !isEmpty(node.job_id) }
                           onSave={ onSave }
                           validate={ onlyNumbers }
                           formatValue={ (value) => isNaN(+value) ? false : +value }
                           isVisible={ !hasChilds }
            />
        </div>
        <div className={ styles.nodeCol }>
            <EditableField label={ 'expenditure' }
                           onSave={ onSave }
                           value={ node.expenditure }
                           isEditable={ !isEmpty(node.job_id) }
                           validate={ onlyNumbers }
                           formatValue={ (value) => isNaN(+value) ? false : +value }
                           isVisible={ !hasChilds }
            />
        </div>
        { isAllowWriteEmployees &&
        <div className={ styles.nodeCol }>
            <EditableField label={ 'employee' }
                           onSave={ onSave }
                           value={ node.employee }
                           isEditable={ !isEmpty(node.job_id) }
                           validate={ onlyNumbers }
                           formatValue={ (value) => isNaN(+value) ? false : +value }
                           isVisible={ !hasChilds }
            />
        </div> }
        { isUploadAllow &&
        <div className={ styles.nodeCol }>
            {
                !hasChilds && <div className={ att.attachmentContainer }>
                    <Button onClick={ (e) => {
                        e.stopPropagation()
                        onNodeClick(!hasChilds ? node : null)
                    }
                    } outline icon={ 'internet-clip-attachment' }/>
                </div>
            }
        </div>
        }
    </div>
})
