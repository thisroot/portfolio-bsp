import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { get, isNull } from "lodash"
import dayjs from "dayjs"
import { loc } from "utils/i18n"
import { MEASURE_UNITS } from "constants/locale"
import { abbreviateNumber, betweenZeroAndOne, onlyNumbers, parseMeasureUnit } from "utils/helpers"
import { useInstance } from "react-ioc"
import { ScheduleListBuilderCardViewModel } from "screens/ObjectScreen/ObjectTabs/ScheduleListBuilderCard/ScheduleListBuilderCardViewModel"
import { t } from "@lingui/macro"
import {
    Button,
    Checkbox,
    ContextMenu,
    DangerButton,
    DatePicker,
    DefaultButton,
    EditableField,
    EditableSelect,
    InfoButton,
    InputSelect
} from "components"
import cx from 'classnames'
import styles from "./styles.scss"
import { INodeProps } from "./interfaces"
import { LocalisationService } from "../../../services/LocalisationService"
import att from "./attachment.scss"


export const ScheduleNodeBody: React.FC<INodeProps> = observer((
  {
      node,
      canDrag,
      path,
      onUploadBtnClick,
      isUploadAllow
  }) => {
    const view = useInstance(ScheduleListBuilderCardViewModel)
    const lang = useInstance(LocalisationService)
    const isSelected = view.selectedNodes.has(node.schedule_id)
    const isNeedToSave = view.editingNodesSnaps.has(node.schedule_id)
    const isNodeEditable = node.job_id
    const isNoChild = !get(view.dataContext.schedule.get(`${ view.lastNode }`), 'children.length')
    const isMultiselectMode = view.isTreePathSelectMode


    const hasChilds = !!get(node, 'children.length')

    const jobUnit = get(node, 'job_unit')
    const nameMeasure = get(node, 'job_id.job_unit', null)
    const jobId = get(node, 'job_id.job_id')

    const jobMeasure = isNull(jobUnit)
      ? parseMeasureUnit(nameMeasure)
      : parseMeasureUnit(jobUnit)

    const [ placeholderForSortOrder, setPlaceholderForSortOrder ] = useState(loc._(MEASURE_UNITS['null']))
    const [ isEditMeasure, setIsEditMeasure ] = useState(false)
    const [ isEditJob, setIsEditJob ] = useState<boolean>(false)
    const [ elementDictionary, setElementDictionary ] = useState([])

    const formatRender = (value) => abbreviateNumber(value)

    const isHidden = node.children.length > 0
    const isFieldEditable = isNodeEditable && view.userCanWrite && !isMultiselectMode && view.isActiveVersion

    const id = node.schedule_id
    const level = path.map(() => 'l').join('')

    const menuItems = []

    if (view.isActiveVersion) {
        if (!jobId) {
            menuItems.push({
                label: loc._(t`components.schedule.addWork`),
                onClick: async (e) => {
                    e.stopPropagation()
                    await view.confirmAppendNode(e, node)
                },
                icon: 'signs-plus-add-b',
            })
        }

        menuItems.push({
            label: loc._(t`basic.delete`),
            onClick: async (e) => {
                e.stopPropagation()
                await view.confirmRemoveNode(e, id)
            },
            icon: 'account-trash-delete-bin',
            id
        })
    }

    const saveChanges = (value, label, isValid) => {
        const invalidFields = get(node, 'invalidFields') as Map<string, any>
        if (invalidFields) {
            isValid ? invalidFields.delete(label) : invalidFields.set(label, `${ value }`)
        }
        const newNode = {
            ...node,
            invalidFields,
        }
        if (isValid) {
            Object.assign(newNode, { [label]: value })
        } else {
            //TODO: Крешится приложение.
            // Object.assign(newNode, { hash: makeHash(4) })
        }
        view.saveNodeChanges(newNode)
    }

    const getValue = (label: string) => node.invalidFields.get(label) || node[label]

    const formatNumber = (value) => isNaN(+value) ? value : +value
    const renderNumberWithFixed = (value) => isNaN(+value) ? value : (+value).toFixed(3)

    const onEditJobClickHandler = () => {
        view.onDirty(node)
        setElementDictionary(view.generateElementDictionary(jobId))
        setIsEditJob(true)
    }

    const isEditJobInvalid = () => isEditJob && !get(node, 'job_id')

    return <div key={ node.hash } id={ `${ node.schedule_id }` }
                className={ cx(styles.nodeContainer, { [styles.selectedNode]: isSelected }, styles[level]) }>
        { view.userCanWrite && !isMultiselectMode && view.isActiveVersion &&
        <ContextMenu contextId={ id } items={ menuItems }/> }
        {
            (view.userCanWrite || (view.userCanWriteFact && view.isTreePathSelectMode)) && view.isActiveVersion &&
            <Checkbox onChange={ () => view.onSelectNode(node, path) }
                      className={ cx(styles.nodeCol, styles.iconCol) }
                      checked={ isSelected }/>
        }
        { isNeedToSave && !isMultiselectMode && view.userCanWrite && view.isActiveVersion &&
        <div className={ cx(styles.nodeCol, styles.controls) }>
            <InfoButton
              pending={ node.pending }
              disabled={ node.invalidFields.size > 0 || isEditJobInvalid() }
              onClick={ (e) => {
                  if (!node.pending) {
                      setIsEditJob(false)
                      node.isVirtual ? view.createNodeModal(e, node) : view.updateNode(e, node)
                  }
              } }
              className={ styles.saveNode }
              icon={ "signs-done-check-c" }
            />
            <DefaultButton
              onClick={ (e) => {
                  setIsEditJob(false)
                  view.confirmNodeChangesCancellation(e, node)
              } }
              className={ styles.saveNode }
              icon={ "signs-delete-close" }
            />
        </div>
        }
        {
            !isMultiselectMode && !isNeedToSave && isSelected && isNoChild && view.lastNode && view.userCanWrite && view.selectedNodes.size === 1 &&
            <div className={ cx(styles.nodeCol, styles.controls) }>
                <DangerButton
                  disabled={ !isNoChild }
                  onClick={ view.confirmRemoveNode }
                  className={ styles.saveNode }
                  icon={ "account-trash-delete-bin" }
                />
            </div>
        }
        <div title={ node.title as string }
             className={ cx(styles.nodeCol, styles.titleCol, styles.textLeft, { [styles.canDrag]: canDrag }) }>
            <div className={ styles.titleText }>
                <div className={ styles.row }>
                    { !node.isVirtual &&
                    <div className={ cx(styles.pathId, styles[level]) }>{ path.join('.') }</div> }
                    <EditableField label={ "name" }
                                   value={ node.name ? node.name as string : '' }
                                   lines={ node.job_id ? 2 : 3 }
                                   onSave={ saveChanges }
                                   onDirty={ () => view.onDirty(node) }
                                   className={ styles.editableTitle }
                                   centered={ false }
                                   isEditable={ view.userCanWrite && !isMultiselectMode && view.isActiveVersion }
                                   contentEditableSingleLine={ false }

                    />
                </div>
                { !node.isVirtual && !isEditJob && node.job_id &&
                <div className={ styles.row }>
                    <div className={ cx(styles.workType, styles[level]) }
                         onClick={ isFieldEditable ? onEditJobClickHandler : null }>
                        <span>{ loc._(t`basic.workType`) } : </span>
                        <span
                          title={ get(node, `job_id.name_${ lang.lang }`, '---') }> { get(node, `job_id.name_${ lang.lang }`, '---') } </span>
                    </div>
                    <div className={ cx(styles.workType, styles[level]) }
                         onClick={ isFieldEditable ? onEditJobClickHandler : null }>
                        <span>{ loc._(t`basic.elementType`) } : </span>
                        <span
                          title={ get(node, `element_id.name_${ lang.lang }`, '---') }> { get(node, `element_id.name_${ lang.lang }`, '---') } </span>
                    </div>
                </div>
                }
                { (node.isVirtual || isEditJob) && view.userCanWrite && view.isActiveVersion &&
                <>
                    <div className={ styles.row }>
                        <InputSelect
                          initialItem={ get(node, 'job_id') }
                          disabled={ !view.isActiveVersion }
                          className={ styles.inputSelectWrapper }
                          inputClassName={ styles.inputSelect }
                          searchFields={ [ `name_${ lang.lang }`, 'key' ] }
                          keyName={ 'job_id' }
                          itemName={ `name_${ lang.lang }` }
                          items={ view.jobDictionary }
                          onChange={ (e) => {
                              view.onDirty(node)
                              const newNode = {
                                  ...node,
                                  job_id: +get(e, 'job_id') || null,
                                  name: get(e, `name_${ lang.lang }`),
                                  element_id: null
                              }
                              setElementDictionary(view.generateElementDictionary(+get(e, 'job_id')))
                              setPlaceholderForSortOrder(loc._(MEASURE_UNITS[get(e, 'job_unit', null)]))
                              if (view.defaultTitle === newNode.name || view.jobDictionaryTitles.some(item => item === get(e, `name_${ lang.lang }`))) {
                                  newNode.name = e[`name_${ lang.lang }`]
                              }
                              if (get(node, 'weighting_factor') === 0 &&
                                view.currentObject.calculation_method === 'weighting_factor' &&
                                view.userCanWriteWeightingFactor) {
                                  node.invalidFields.set('weighting_factor', '0')
                              }
                              view.saveNodeChanges(newNode)
                          } }/>
                        <InputSelect
                          initialItem={ get(node, 'element_id') }
                          disabled={ !get(node, 'job_id') && !view.isActiveVersion }
                          className={ styles.inputSelectWrapper }
                          inputClassName={ styles.inputSelect }
                          searchFields={ [ `name_${ lang.lang }`, 'key' ] }
                          keyName={ 'element_id' }
                          itemName={ `name_${ lang.lang }` }
                          items={ elementDictionary }
                          onChange={ (e) => {
                              view.onDirty(node)
                              const newNode = { ...node, element_id: +get(e, 'element_id') || null }
                              view.saveNodeChanges(newNode)
                          } }/>
                    </div>
                </>
                }
            </div>
        </div>
        {
            view.currentObject.calculation_method === 'weighting_factor' && view.userCanReadWeightingFactor &&
            <div  className={ cx(styles.nodeCol, styles.editableField) }>
                <EditableField label={ 'weighting_factor' }
                               title={ node.weighting_factor }
                               value={ getValue('weighting_factor') }
                               isEditable={ isFieldEditable && view.userCanWriteWeightingFactor }
                               onSave={ saveChanges }
                               validate={ betweenZeroAndOne }
                               formatValue={ formatNumber }
                               formatRender={ renderNumberWithFixed }
                               onDirty={ () => view.onDirty(node) }/>
            </div>
        }

        <div className={ styles.nodeCol }>{ get(node.job_id, 'key') || '---' }</div>
        <div className={ styles.nodeCol }>
            {
                <DatePicker
                  hidden={ isHidden }
                  disabled={ (!node.job_id && !node.isVirtual) || !view.userCanWrite || isMultiselectMode || !view.isActiveVersion }
                  value={ dayjs(node.date_start, 'YYYY-MM-DD').format('DD.MM.YYYY') }
                  format={ 'DD.MM.YYYY' }
                  addedClasses={ styles.datepicker }
                  onDayChange={ (day) => {
                      view.onDirty(node)
                      view.saveNodeChanges({ ...node, date_start: dayjs(day).format('YYYY-MM-DD') })
                  } }
                />
            }
        </div>
        <div className={ styles.nodeCol }>
            {
                <DatePicker
                  hidden={ isHidden }
                  disabledDays={ { before: new Date(node.date_start) } }
                  disabled={ (!node.job_id && !node.isVirtual) || !view.userCanWrite || isMultiselectMode || !view.isActiveVersion }
                  value={ dayjs(node.date_finish, 'YYYY-MM-DD').format('DD.MM.YYYY') }
                  addedClasses={ styles.datepicker }
                  format={ 'DD.MM.YYYY' }
                  onDayChange={ (day) => {
                      view.onDirty(node)
                      view.saveNodeChanges({ ...node, date_finish: dayjs(day).format('YYYY-MM-DD') })
                  } }
                />
            }
        </div>
        <div title={ node.job_id ? node.job_id.name : '' }
             className={ cx(styles.nodeCol,
               { [styles.selectableField]: (node.isVirtual && view.userCanWrite) || isEditMeasure })
             }
        >
            { !node.isVirtual && node.job_id &&
            <div
              className={ styles.row }
              onClick={ () => setIsEditMeasure(true) }
            >
                <EditableSelect
                  labelClassName={ styles.label }
                  placeholder={ placeholderForSortOrder }
                  items={ view.generateJobUnitListOption }
                  text={ jobMeasure }
                  isEditable={ isFieldEditable }
                  isVisible={ !hasChilds }
                  onChange={ (e) => {
                      view.onDirty(node)
                      const newNode = { ...node, job_unit: get(e, 'value', null) }
                      view.saveNodeChanges(newNode)
                      setIsEditMeasure(false)
                  } }
                />
            </div>
            }
            { node.isVirtual && view.userCanWrite &&
            <div className={ styles.row }>
                <InputSelect
                  disabled={ !isFieldEditable }
                  className={ cx(styles.inputSelect, styles.selectJob) }
                  inputClassName={ styles.selectJob }
                  placeholder={ placeholderForSortOrder }
                  searchFields={ [ 'name' ] }
                  keyName={ 'id' }
                  itemName={ 'name' }
                  items={ view.generateJobUnitListOption }
                  onChange={ (e) => {
                      view.onDirty(node)
                      const newNode = { ...node, job_unit: get(e, 'value', null) }
                      view.saveNodeChanges(newNode)
                  } }/>
            </div>
            }
        </div>
        <div  className={ cx(styles.nodeCol, styles.editableField) }>
            <EditableField label={ 'volume_planned' }
                           title={ node.volume_planned }
                           value={ getValue('volume_planned') }
                           isEditable={ isFieldEditable }
                           isVisible={ !hasChilds }
                           onSave={ saveChanges }
                           validate={ onlyNumbers }
                           formatValue={ formatNumber }
                           formatRender={ formatRender }
                           onDirty={ () => view.onDirty(node) }/>
        </div>
        <div  className={ cx(styles.nodeCol, styles.editableField) }>
            <EditableField label={ 'volume_actual' }
                           title={ node.volume_actual }
                           value={ getValue('volume_actual') }
                           isEditable={ isFieldEditable }
                           isVisible={ !hasChilds }
                           onSave={ saveChanges }
                           validate={ onlyNumbers }
                           formatValue={ formatNumber }
                           formatRender={ formatRender }
                           onDirty={ () => view.onDirty(node) }/>
        </div>
        { view.userCanReadExpenditure && <div
                                              className={ cx(styles.nodeCol, styles.editableField) }>
            <EditableField label={ 'expenditure_planned' }
                           title={ abbreviateNumber(node.expenditure_planned) }
                           value={ getValue('expenditure_planned') }
                           isEditable={ isFieldEditable }
                           isVisible={ !hasChilds }
                           onSave={ saveChanges }
                           validate={ onlyNumbers }
                           formatRender={ formatRender }
                           formatValue={ formatNumber }
                           onDirty={ () => view.onDirty(node) }/>
        </div> }
        { view.userCanReadExpenditure && <div
                                              className={ cx(styles.nodeCol, styles.editableField) }>
            <EditableField label={ 'expenditure_actual' }
                           title={ abbreviateNumber(node.expenditure_actual) }
                           value={ getValue('expenditure_actual') }
                           isEditable={ isFieldEditable }
                           isVisible={ !hasChilds }
                           onSave={ saveChanges }
                           validate={ onlyNumbers }
                           formatValue={ formatNumber }
                           formatRender={ formatRender }
                           onDirty={ () => view.onDirty(node) }/>
        </div>
        }
        { isUploadAllow &&
        <div className={ cx(styles.nodeCol) }>
            {
                !hasChilds && <div className={ att.attachmentContainer }>
                    <Button onClick={ (e) => {
                        e.stopPropagation()
                        onUploadBtnClick(!hasChilds ? node : null)
                    }
                    } outline icon={ 'internet-clip-attachment' }/>
                </div>
            }
        </div>
        }
    </div>
})
