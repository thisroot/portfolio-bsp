import React, { FC, useEffect, useRef, useState } from "react"
import SortableTree, {
    FullTree,
    NodeData, NodeRendererProps,
    OnMovePreviousAndNextLocation,
    OnDragPreviousAndNextLocation,
} from "react-sortable-tree"
import { get } from 'lodash'
import { isClickOnButton, isClickOnModal } from "utils/helpers"
import { t, Trans } from '@lingui/macro'
import 'react-sortable-tree/style.css'
import styles from "./styles.scss"
import { useLifecycles } from "react-use"
import { Project } from "models/mst"
import { useClickAway } from 'react-use'
import { observer } from "mobx-react-lite"
import { SCHEDULE_NODE_TYPE, ScheduleNode } from '../ScheduleNode/ScheduleNode'
import { DropZone } from "../../DropZone"
import { loc } from "../../../utils/i18n"
import { Icon } from "../../UI/Icon"
import cx from 'classnames'
import useFunctionAsState from "../../../utils/helpers/hooks"
import { FileListModel } from "../../../models/entities/FileListModel"

type IProps = {
    tree: any,
    onChange: () => void;
    onMoveNode: (data: NodeData & FullTree & OnMovePreviousAndNextLocation) => void;
    currentProject: Project;
    onSelectNode: (node: any, path: Array<number>, onlySelect?: boolean) => any;
    selectedNodes: Array<number>;
    onVisibilityToggle: ({ node: any, expanded: boolean }) => void;
    treeHasChanged: boolean;
    resetSelectedNodes: () => any;
    isTreePathSelectMode: boolean;
    canDrop: (data: OnDragPreviousAndNextLocation & NodeData) => boolean;
    acceptedFiles: Array<string>
    addFileToUpload: any
    disabledUpload: boolean
    scheduleFiles: Map<number, FileListModel>
    scrollOnTop: number
};


const ScheduleListBuilder: FC<IProps> =
  observer(({
                tree,
                onChange,
                onVisibilityToggle,
                onMoveNode,
                resetSelectedNodes,
                isTreePathSelectMode,
                canDrop,
                acceptedFiles,
                addFileToUpload,
                disabledUpload,
                scheduleFiles,
                scrollOnTop
            }): JSX.Element => {

      const getFileSelector = (entityId) => {
          return (File: Array<File>) => {
              addFileToUpload(File, entityId)
          }
      }

      const wrapper: React.MutableRefObject<HTMLDivElement> = useRef()
      const [ containerWidth, setWidth ] = useState(0)
      useEffect(() => setWidth(get(wrapper, 'current.clientWidth', 0)), [ containerWidth ])

      const [ selectedNode, toggleSelectNode ] = useState(null)
      const [ currentSelectionFunc, toggleCurrentSelectionFunc ] = useFunctionAsState(null)

      useClickAway(wrapper, (e) => {
          if (!isClickOnButton(e) && !isClickOnModal(e)) {
              resetSelectedNodes()
          }
      })

      const changeWidthHandler = () => {
          setWidth(get(wrapper, 'current.clientWidth', 0))
      }

      useLifecycles(() => window.addEventListener('resize', changeWidthHandler),
        () => window.removeEventListener('resize', changeWidthHandler))


      const nodeRenderer = (props: NodeRendererProps) => <ScheduleNode { ...props }  />
      const renderNodeProps = () => ({
          type: SCHEDULE_NODE_TYPE.SHEDULE_NODE,
          onUploadBtnClick: (node) => {
              toggleSelectNode(get(node, 'schedule_id') === get(selectedNode, 'schedule_id') ? null : node)
              toggleCurrentSelectionFunc(getFileSelector(get(node, 'schedule_id')))
          },
          isUploadAllow: acceptedFiles.length > 0
      })
      const nodeHeight = (index) => get(index, 'node.isVirtual') || get(index, 'node.job_id') ? 90 : 70
      const hasFiles = scheduleFiles.get(get(selectedNode, 'schedule_id'))


      let top = 350
      if(scrollOnTop < 210) {
          top = top - scrollOnTop
      }
      if(scrollOnTop > 210 && scrollOnTop < 900) {
          top = 150
      }
      else if(scrollOnTop > 900) {
          top = 70
      }

      return (
        <div ref={ wrapper } className={ styles.componentWrapper }>
            { tree.length === 0 ? <div className={ styles.noData }><Trans>basic.noDataAvailable</Trans></div> :
              <SortableTree
                isVirtualized={ false }
                rowHeight={ nodeHeight }
                treeData={ tree }
                getNodeKey={ ({ node }) => node.schedule_id }
                onChange={ onChange }
                onMoveNode={ onMoveNode }
                onVisibilityToggle={ onVisibilityToggle }
                canNodeHaveChildren={ (node) => node.job_id === null }
                canDrag={ isTreePathSelectMode }
                canDrop={ canDrop }
                nodeContentRenderer={ nodeRenderer }
                generateNodeProps={ renderNodeProps }
              />
            }
            { acceptedFiles.length > 0 &&
            <div key={ get(selectedNode, 'schedule_id') }
                 className={ cx(styles.fileLoaderContainer, { [styles.isOpen]: selectedNode }) }>
                <div style={ { top } }
                     className={ cx(styles.fixed, { [styles.isOpen]: selectedNode }) }>
                    <h5>{ get(selectedNode, 'name', '---') }</h5>
                    <DropZone disabled={ acceptedFiles.length === 0 || disabledUpload } accept={ acceptedFiles }
                              disabledMessage={ loc._(t`components.scheduleActualValuesForm.uploadDisabled`) }
                              onFilesSelect={ currentSelectionFunc }/>
                    <div className={ styles.filesContainer }>
                        { hasFiles &&
                        [ ...scheduleFiles.get(get(selectedNode, 'schedule_id')).files.values() ].map(file => {
                            return <div className={ styles.file } key={ file.name }>
                                <div>
                                    { file.name }
                                </div>
                                <div>
                                    <Icon name={ 'signs-delete-close-c' } onClick={ () => {
                                        scheduleFiles.get(get(selectedNode, 'schedule_id')).files.delete(file.name)
                                    } }/>
                                </div>
                            </div>
                        })
                        }
                    </div>
                </div>
            </div>
            }
        </div>
      )
  })

export {
    ScheduleListBuilder
}
