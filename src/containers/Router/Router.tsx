import React, { Component } from 'react'
import { Router, Route, Switch } from 'react-router'
import { history } from "services"
import { App, AppErrorBoundary } from 'containers'
import { ROUTES, STATIC_ROUTES } from 'constants/routes'
// если надо экспортировать модуль как отдельный чанк, то обязательно прописывать после пути к модулю,
// путь к его Асинхронной версии, если импорт будет из индекса модуля то он подключится к общему бандлу
import { observer } from "mobx-react"
import { PlaygroundContainer } from "containers/Playground"
import { NoMatch } from 'components/NoMatch'
import { AsyncProjectScreen } from "../../screens/ProjectScreen"
import { AsyncProjectsListScreen } from "../../screens/ProjectsListScreen"
import { AsyncObjectsListScreen } from "../../screens/ObjectsListScreen"
import { AsyncObjectScreen } from "../../screens/ObjectScreen"
import { AsyncServicesScreen } from "../../screens/ServicesScreen"
import { AsyncControlScreen } from "../../screens/ControlScreen"
import { AsyncFavoritesScreen } from "../../screens/FavoritesScreen"
import { AsyncClassificationsScreen } from "../../screens/ClassificationsScreen"
import { AsyncLogsScreen } from "../../screens/LogsScreen"
import { AsyncLoggingScreen } from "../../screens/LoggingScreen"

import { isProduction } from "../../utils/helpers"
import { AsyncDashboardScreen } from "../../screens/DashboardScreen"
import { AsyncUserProfileScreen } from "../../screens/UserProfileScreen"
import { AsyncFilesArchiveScreen } from "../../screens/FilesArchiveScreeen"
import { AsyncReportsScreen } from "../../screens/ReportsScreen"
import { AsyncReportScreen } from "../../screens/ReportsScreen/ReportScreen"

@observer
class RouterContainer extends Component {

    render() {
        return (
          <AppErrorBoundary>
              <Router history={ history }>
                  <App>
                      <Switch>
                          <Route path={ ROUTES.DASHBOARDS.ROUTE }>
                              <Switch>
                                  <Route path={ ROUTES.DASHBOARDS.ROUTE } exact component={ AsyncDashboardScreen }/>
                                  <Route path={ ROUTES.DASHBOARD.ROUTE } exact component={ AsyncDashboardScreen }/>
                              </Switch>
                          </Route>
                          <Route path={ ROUTES.PROJECTS_LIST.ROUTE } exact component={ AsyncProjectsListScreen }/>
                          <Route path={ ROUTES.OBJECTS_LIST.ROUTE } exact component={ AsyncObjectsListScreen }/>
                          <Route path={ ROUTES.PROJECT_PAGE.ROUTE } exact component={ AsyncProjectScreen }/>
                          <Route path={ ROUTES.OBJECT_PAGE.ROUTE } exact component={ AsyncObjectScreen }/>
                          <Route path={ ROUTES.SERVICES.ROUTE } exact component={ AsyncServicesScreen }/>
                          <Route path={ ROUTES.FAVORITES.ROUTE } exact component={ AsyncFavoritesScreen }/>
                          <Route path={ ROUTES.CONTROL.ROUTE } exact component={ AsyncControlScreen }/>
                          <Route path={ ROUTES.CLASSIFICATIONS.ROUTE } exact component={ AsyncClassificationsScreen }/>
                          <Route path={ ROUTES.LOGS.ROUTE } exact component={ AsyncLogsScreen }/>
                          <Route path={ ROUTES.LOGGING.ROUTE } exact component={ AsyncLoggingScreen }/>
                          <Route path={ ROUTES.USER_PROFILE.ROUTE } exact component={ AsyncUserProfileScreen }/>
                          <Route path={ ROUTES.FILES_ARCHIVE.ROUTE } exact component={ AsyncFilesArchiveScreen }/>
                          <Route path={ ROUTES.REPORTS.ROUTE }>
                              <Switch>
                                  <Route path={ ROUTES.REPORTS.ROUTE } exact component={ AsyncReportsScreen }/>
                                  <Route exact path={ ROUTES.REPORT.ROUTE } component={ AsyncReportScreen }/>
                              </Switch>
                          </Route>
                          { !isProduction &&
                          <Route exact path={ STATIC_ROUTES.PLAYGROUND.ROUTE } component={ PlaygroundContainer }/> }
                          <Route path={ '/nomatch' } component={ NoMatch }/>
                          <Route component={ NoMatch }/>
                      </Switch>
                  </App>

              </Router>
          </AppErrorBoundary>
        )
    }
}

export { RouterContainer }
