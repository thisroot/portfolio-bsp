import React, { Fragment } from 'react'
import { Catch } from 'containers/ErrorBoundaryContainer'
import styles from './styles.scss'
import { get } from 'lodash'
import FireBase from 'firebase/app'
import 'firebase/analytics'

import { DefaultButton } from "../../components/UI/Button"

const errImg = '/images/Pic_error.svg'

export interface IAppErrorBoundaryContainerProps {
    children: React.ReactNode
}

export const AppErrorBoundary = Catch(function MyErrorBoundary(props: IAppErrorBoundaryContainerProps, error?: Error) {
    //TODO: сераис локализации еще не инициализирован, используем язык браузера для вывода сообщения об ошибюке
    const userLang = get(navigator, 'language') || get(navigator, 'userLanguage', '') as string
    let title = <div>An unexpected error occurred</div>
    let message = <>if it repeat after refresh browser, please call with technical support team</>
    let btnError = 'reload'
    switch (true) {
        case userLang.includes('ru'):
            title = <div>Возникла <br/> непредвиденная ошибка</div>
            message = <>Если она повторяется после перезагрузки окна браузера,
                пожалуйста свяжитесь со службой технической поддержки</>
            btnError = 'обновить'
            break
        default:
            break
    }

    if (error) {
        FireBase.analytics().logEvent('error', { error })
        return (
          <div className={ styles.appErrorContainer }>
              <div className={ styles.left }>
                  <div>
                      <h2>{ title }</h2>
                  </div>
                  { message }
                  <div>
                      <DefaultButton onClick={ () => {
                          window.location.reload()
                      } } icon={ 'arrows-rotate' } text={ btnError }/>
                  </div>
              </div>
              <div className={ styles.right }>
                  <img alt={ "" } src={ errImg }/>
              </div>
          </div>
        )
    } else {
        return <Fragment>{ props.children }</Fragment>
    }
})
