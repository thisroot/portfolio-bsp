import React, { Fragment, useCallback, useEffect, useRef, useState } from "react"
import { useEffectOnce, useMeasure } from "react-use"
import { get } from 'lodash'
import { Helmet } from 'react-helmet'
import { provider, toFactory, useInstance } from "react-ioc"
import { toast, ToastContainer } from 'react-toastify'
import { LeftMenuScreen } from "screens/LeftMunuScreen"
import { AuthScreen } from "screens/AuthScreen"
import { Header } from 'components/Header'
import { observer } from "mobx-react-lite"
import { useModal } from 'utils/hooks'
import { Preloader } from 'components/UI'
import { YMInitializer } from "react-yandex-metrika"
import { appConfig } from "constants/appConfig"
import { isProduction } from "utils/helpers"
import cx from 'classnames'
import styles from 'containers/App/styles.scss'
import 'static/styles/global.scss'
import 'static/styles/theme.scss'

import {
    AnalyticsService,
    ApiService,
    APP_STATE,
    AppService,
    AuthService,
    DataContext,
    DictionaryService,
    FileLoaderService,
    Files,
    JWT,
    LocalisationService,
    Logging,
    ObjectModel,
    Project,
    PWAService,
    Reference,
    Report,
    RequestService,
    RouterService,
    Schedule,
    ScheduleFact,
    SettingsService,
    StorageService,
    User
} from "services"
import { I18nProvider } from "@lingui/react"
import { RightMenuScreen } from "../../screens/RightMenuScreen/RightMenuScreen"
import { loc } from "../../utils/i18n"
import { t } from "@lingui/macro"
import { Analytics } from "../../services/ApiService"

const AppContainer = observer((props) => {

    const appService = useInstance(AppService)
    const scroll = useRef<HTMLDivElement>()
    const [ Modal, open, close ] = useModal('modal')

    const authRender = () => {
        return <AuthScreen key={ `auth-${ appService.lang }` }/>
    }

    const [ ref, { width } ] = useMeasure()

    useEffect(() => appService.setContainerWidth(width), [ appService, width ])
    useEffectOnce(() => {
        const initApp = () => {
            appService.init().then(() => {
                appService.registerModal(open, close)
            })
        }
        initApp()
    })
    useEffect(() => {
        appService.registerScroll(scroll)
    }, [ scroll.current, appService ])

    const getIsHideHeader = useCallback(() => {
        if (appService.triggerForActionHideHeader) return appService.scrollOnTop + 76 > appService.triggerForActionHideHeader
        return false
    }, [ appService.scrollOnTop, appService.triggerForActionHideHeader ])

    const [ isHideHeader, setIsHideHeader ] = useState(getIsHideHeader())
    useEffect(() => {
        setIsHideHeader(getIsHideHeader())
    }, [ getIsHideHeader ])

    const appRender = (children?: JSX.Element) => {
        return (<Fragment>
              <Header/>
              <div ref={ ref } className={ styles.app } key={ `app-${ appService.lang }` } lang={ appService.lang }>
                  <main className={ styles.main }>
                      <LeftMenuScreen/>
                      <section ref={ scroll } className={ cx(styles.content, { [styles.isHideHeader]: isHideHeader }) }>
                          { children || props.children }
                      </section>
                      <RightMenuScreen/>
                  </main>
              </div>
          </Fragment>
        )
    }

    const questRender = (children?: JSX.Element) => {
        return (<div className={ styles.app } key={ `quest-${ appService.lang }` }>
            <aside style={ { display: "flex" } }>
                { children || props.children }
            </aside>
        </div>)
    }

    const renderApp = () => {
        if (appService.isPending) {
            return <Preloader/>
        }
        switch (appService.state) {
            case APP_STATE.auth:
                return authRender()
            case APP_STATE.app:
                return appRender()
            case APP_STATE.noMatch:
                return window.location.pathname = '/nomatch'
            case APP_STATE.quest:
            default:
                return questRender()
        }
    }

    return (
      <I18nProvider i18n={ appService.i18n }>
          <Helmet>
              <title>{ appService.wasInitialized && loc._(t`project.name`) }</title>
              <meta name="description"
                    content={ appService.wasInitialized ? loc._(t`project.description`) : 'Платформа строительных сервисов' }/>
          </Helmet>
          {
              isProduction &&
              <YMInitializer
                accounts={ [ appConfig.ANALYTICS.YANDEX ] }
                options={ { clickmap: true, trackLinks: true, accurateTrackBounce: true, webvisor: true } }
                version={ '2' }
              />
          }
          {
              renderApp()
          }
          <ToastContainer enableMultiContainer position={ toast.POSITION.BOTTOM_RIGHT }/>
          <ToastContainer enableMultiContainer autoClose={ false } containerId={ 'controlled' }
                          position={ toast.POSITION.BOTTOM_RIGHT }/>
          <div id={ 'modal' } className={ styles.modalContainer }>
              <Modal>{ get(appService, 'modalControls.renderModal') && appService.modalControls.renderModal() }</Modal>
          </div>
      </I18nProvider>
    )
})

const App = provider()(AppContainer)

App.register(
  // api endpoints
  RequestService, ApiService, JWT, User, Project, ObjectModel, Report, Schedule,
  Reference, ScheduleFact, Files, Logging, Analytics,
  // oter services
  DictionaryService, StorageService, RouterService, AnalyticsService,
  PWAService, AuthService, LocalisationService, AppService, FileLoaderService, SettingsService,
  // models
  // MST service
  [ DataContext, toFactory(DataContext.create) ]
)

export { App }
