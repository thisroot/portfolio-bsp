import React, { Fragment } from "react"
import { observer } from 'mobx-react-lite'
import { PrimaryButtons } from "./PrimaryButtons"
import { LoadersAndProgresses } from "./LoadersAndProgresses"
import { Checkboxes } from "./Checkboxes"
import { Accordion } from "./Accordion"
import { Toggles } from "./Toggles"
import { Radios } from "./Radios"
import { Dropdowns } from "./Dropdowns"
// import { SecondaryButtons } from "./SecondaryButtons"
import { Avatars } from "./Avatars"
// import { ToggleButtons } from "containers/Playground/ToggleButtons"
import { Searchbars } from "containers/Playground/Searchbar"
// import { ActionButtons } from "containers/Playground/ActionButtons"
// import { Selectors } from './Selectors'
import { DatePickers } from "containers/Playground/DatePickers"
// import { Lists } from "./Lists"
import { Icons } from './Icons'
import { InputFields } from './InputFields'
import { Badges } from './Badges'

import styles from './styles.scss'
import { Colors } from "./Colors"
import { Paginations } from "./Paginations"
import { MultiSelects } from "./MultiSelects"

const PlaygroundContainer: React.SFC<any> = observer(() => {
    return (
      <Fragment>
          <div className={ styles.componentsWrapper }>
              <MultiSelects/>
              <Paginations/>
              <Badges/>
              <DatePickers/>
              <PrimaryButtons/>
              <LoadersAndProgresses/>
              <Checkboxes/>
              <Toggles/>
              <Radios/>
              <Accordion/>
              <InputFields/>
              <Dropdowns/>
              <Searchbars/>
              <Icons/>
              <Colors/>
              <Avatars/>
          </div>
      </Fragment>
    )
})


export { PlaygroundContainer }
